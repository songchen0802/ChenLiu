// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <vector>
#include <mpi.h>

#include "ChenLiu.hh"

using namespace ChenLiu;

int main(int argc, char **argv) {

  MPI_Init(&argc, &argv);

  MPI_Comm comm = MPI_COMM_WORLD;
  int master_rank = 0;
  int rank;
  MPI_Comm_rank(comm, &rank);

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  std::filesystem::path mesh_file = "../data/unit_16squares.inp";

  mesh_editor.read_mesh(mesh_file, mesh_ptr, 2, 2, MPI_COMM_WORLD, true);

  Mesh::Partition partitioner = Mesh::Partition();
  std::shared_ptr mesh_ptr_no_ghost = partitioner.partition_mesh(mesh_ptr, comm,
                                      master_rank);

  std::shared_ptr mesh_ptr_d = partitioner.add_ghost_layer(mesh_ptr_no_ghost,
                               comm);

  std::vector<Element_Family> ele_family = {Element_Family::CG, Element_Family::CG};
  std::vector<int> degree = {1, 1};

  Space::FunctionSpace<double> V(mesh_ptr_d, comm, ele_family, degree);

//  if (rank != master_rank) {
//      int tdim = 2;
//    for (Mesh::EntityIterator cell_it(mesh_ptr_no_ghost, tdim); !cell_it.is_end(); ++cell_it) {
//
//        //std::cout << cell_it->local_index() << " " << cell_it->global_index() << std::endl;
//        int l_idx = cell_it->local_index();
//        int g_idx = cell_it->global_index();
//        std::vector<int> idx = mesh_ptr_no_ghost->topology().get_connectivities(tdim, 0, l_idx);
//        std::cout << l_idx << " " << g_idx << " ";
//        for (int e : idx) {
//            std::cout << mesh_ptr_no_ghost->topology().get_global_index(0,e) << " " ;
//        }
//        std::cout << std::endl;
//
//        //Coordinate coord = mesh_ptr_no_ghost->geometry().coordinate(l_idx);
//        //std::cout << l_idx << " " << g_idx << " " << coord[0] << " " << coord[1] << std::endl;
//
//    }
//
//  }


  MPI_Finalize();

  return 0;

}

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/Element.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
Element<DT>::Element() {
  // do nothing
  gdim_ = -1;
  ref_dof_coords_.clear();
  reference_cell_ = nullptr;
}

// destructor
template<class DT>
Element<DT>::~Element() {
  clear();
}

// clear
template<class DT>
void Element<DT>::clear() {
  gdim_ = -1;
  ele_deg_ = -1;

  ref_dof_coords_.clear();
  ref_dof_coords_.shrink_to_fit();

  reference_cell_->clear();
}

// element degree
template<class DT>
int Element<DT>::ele_degree() const {
  return ele_deg_;
}

// finite element type
template<class DT>
FE_Type Element<DT>::fe_type() const {
  return fe_type_;
}

//geometrical dimension
template<class DT>
int Element<DT>::gdim() const {
  return gdim_;
}

// topological dimension
template<class DT>
int Element<DT>::tdim() const {
  return reference_cell_->tdim();
}

// coordinates of dofs on reference cell
template<class DT>
std::vector<Coordinate> Element<DT>::dof_coordinates_ref() {
  assert(ref_dof_coords_.size() > 0);

  return ref_dof_coords_;
}

// coordinates of dofs on physical cell
template<class DT>
std::vector<Coordinate> Element<DT>::dof_coordinates_phy(
  const std::vector<Coordinate> cell_verts) {
  assert(ref_dof_coords_.size() > 0);

  std::vector<Coordinate> coords_phy(ref_dof_coords_.size());

  for (int i = 0; i != ref_dof_coords_.size(); ++i) {
    Coordinate coord(gdim_);

    for (int d = 0; d != gdim_; ++d)   {

      coord[d] = reference_cell_->map2phy(d, ref_dof_coords_[i], cell_verts);

    } // for d

    coords_phy[i] = coord;

  } // for i

  return coords_phy;
}


template<class DT>
DT Element<DT>::eval_ref_basis_sum(const Coordinate coord_ref) const {

  std::vector<DT> val;
  this->eval_ref_basis(coord_ref, val);

  return std::accumulate(val.begin(), val.end(), 0.0);

}

template<class DT>
DT Element<DT>::eval_ref_basis_deriv1_sum(
  const Coordinate coord_ref, int d) const {

  std::vector<DT> val;
  this->eval_ref_basis_deriv1(coord_ref, d, val);

  return std::accumulate(val.begin(), val.end(), 0.0);
}

template<class DT>
DT Element<DT>::eval_ref_basis_deriv2_sum(
  const Coordinate coord_ref, int d1, int d2) const {

  std::vector<DT> val;
  this->eval_ref_basis_deriv2(coord_ref, d1, d2, val);

  return std::accumulate(val.begin(), val.end(), 0.0);
}

// identify indices of dofs on a sub entity
template<class DT>
std::vector<int> Element<DT>::identify_dof_idx_on_sub_entity(
  std::vector<int> entity_vert_idx,
  std::vector<int> sub_entity_vert_idx,
  std::vector<std::vector<int>> entity_idx,
  std::vector<std::vector<int>> dof_idx) const {

  assert(entity_idx.size() > 0);
  assert(sub_entity_vert_idx.size() > 0);
  assert(entity_idx.size() > 0);
  assert(dof_idx.size() > 0);

  for (int i = 0; i != entity_idx.size(); ++i) {
    std::vector<int> loc_sub_entity_vert_idx;

    for (int j = 0; j != entity_idx[i].size(); ++j) {
      const int pos = entity_idx[i][j];
      loc_sub_entity_vert_idx.push_back(entity_vert_idx[pos]);
    }

    if (has_same_elements(loc_sub_entity_vert_idx, sub_entity_vert_idx)) {
      return dof_idx[i];
      break;
    }

  } // for

  //return dof_idx[pos];

  CL_Error("Wrong sub-entity!");
  std::exit(EXIT_FAILURE);

}

// template instantiation
template class Element<double>;
template class Element<float>;

}; // namespace FEM
}; // namespace ChenLiu

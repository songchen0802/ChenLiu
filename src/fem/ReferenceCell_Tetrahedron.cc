// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/ReferenceCell_Tetrahedron.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ReferenceCell_Tetrahedron<DT>::ReferenceCell_Tetrahedron() {
  // do nothing
  this->tdim_ = 3;
  this->cell_type_ = CellType::Tetrahedron;
  this->init_ref_vert_coords();
}

// destructor
template<class DT>
ReferenceCell_Tetrahedron<DT>::~ReferenceCell_Tetrahedron() {
  this->clear();
}

// map a point on reference cell to physical cell
template<class DT>
DT ReferenceCell_Tetrahedron<DT>::map2phy(const int d,
    const Coordinate coord_ref,
    std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  switch(d) {
  case 0:
  {
    const DT val = + (cell_verts[1][0] - cell_verts[0][0]) * coord_ref[0]
                   + (cell_verts[2][0] - cell_verts[0][0]) * coord_ref[1]
                   + (cell_verts[3][0] - cell_verts[0][0]) * coord_ref[2]
                   + cell_verts[0][0];

    return val;
  } // x

  case 1:
  {
    const DT val = + (cell_verts[1][1] - cell_verts[0][1]) * coord_ref[0]
                   + (cell_verts[2][1] - cell_verts[0][1]) * coord_ref[1]
                   + (cell_verts[3][1] - cell_verts[0][1]) * coord_ref[2]
                   + cell_verts[0][1];

    return val;
  } // y

  case 2:
  {
    const DT val = + (cell_verts[1][2] - cell_verts[0][2]) * coord_ref[0]
                   + (cell_verts[2][2] - cell_verts[0][2]) * coord_ref[1]
                   + (cell_verts[3][2] - cell_verts[0][2]) * coord_ref[2]
                   + cell_verts[0][2];

    return val;
  } // z

  default:
  {
    return 0.0;
  } // default

  } // switch

}

template<class DT>
DT ReferenceCell_Tetrahedron<DT>::map2phy_deriv1(const int d, const int d1,
    const Coordinate coord_ref,
    const std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 10 + (d1 + 1);

  switch(switch_val) {
  case 11:
  {
    const DT val = (cell_verts[1][0] - cell_verts[0][0]);

    return val;
  } // x_x

  case 12:
  {
    const DT val = (cell_verts[2][0] - cell_verts[0][0]);

    return val;
  } // x_y

  case 13:
  {
    const DT val = (cell_verts[3][0] - cell_verts[0][0]);

    return val;
  } // x_z

  case 21:
  {
    const DT val = (cell_verts[1][1] - cell_verts[0][1]);

    return val;
  } // y_x

  case 22:
  {
    const DT val = (cell_verts[2][1] - cell_verts[0][1]);

    return val;
  } // y_y

  case 23:
  {
    const DT val = (cell_verts[3][1] - cell_verts[0][1]);

    return val;
  } // y_z

  case 31:
  {
    const DT val = (cell_verts[1][2] - cell_verts[0][2]);

    return val;
  } // z_x

  case 32:
  {
    const DT val = (cell_verts[2][2] - cell_verts[0][2]);

    return val;
  } // z_y

  case 33:
  {
    const DT val = (cell_verts[3][2] - cell_verts[0][2]);

    return val;
  } // z_z

  default:
  {
    return 0.0;
  } // default

  } // switch

}

template<class DT>
DT ReferenceCell_Tetrahedron<DT>::map2phy_deriv2(
  const int d, const int d1, const int d2,
  const Coordinate coord_ref,
  const std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(0 <= d2 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 100 + (d1 + 1) * 10 + (d2 + 1);

  switch(switch_val) {

  default:
  {
    return 0.0;
  }// default

  } // switch

}

// map a point on physical cell to reference cell
template<class DT>
void ReferenceCell_Tetrahedron<DT>::map2ref(const Coordinate coord_phy,
    const std::vector<Coordinate> cell_verts, Coordinate& coord) const {

  // TODO: check coord_phy on physical cell
  assert(coord_phy.dim() >= this->tdim_);
  assert(coord.dim() == coord_phy.dim());
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  for (int i = 0; i != coord.dim(); ++i) {
    coord[i] = 0.0;
  }

  DT a11 = cell_verts[1][0] - cell_verts[0][0];
  DT a12 = cell_verts[2][0] - cell_verts[0][0];
  DT a13 = cell_verts[3][0] - cell_verts[0][0];

  DT a21 = cell_verts[1][1] - cell_verts[0][1];
  DT a22 = cell_verts[2][1] - cell_verts[0][1];
  DT a23 = cell_verts[3][1] - cell_verts[0][1];

  DT a31 = cell_verts[1][2] - cell_verts[0][2];
  DT a32 = cell_verts[2][2] - cell_verts[0][2];
  DT a33 = cell_verts[3][2] - cell_verts[0][2];

  DT det = + a11 * (a33 * a22 - a32 * a23)
           - a21 * (a33 * a12 - a32 * a13)
           + a31 * (a23 * a12 - a22 * a13);

  assert(det != 0.0);


  coord[0] = (1.0 / det) * (
               + (a33 * a22 - a32 * a23) * (coord_phy[0] - cell_verts[0][0])
               - (a33 * a12 - a32 * a13) * (coord_phy[1] - cell_verts[0][1])
               + (a23 * a12 - a22 * a13) * (coord_phy[2] - cell_verts[0][2])
             );

  coord[1] = (1.0 / det) * (
               - (a33 * a21 - a31 * a23) * (coord_phy[0] - cell_verts[0][0])
               + (a33 * a11 - a31 * a13) * (coord_phy[1] - cell_verts[0][1])
               - (a23 * a11 - a21 * a13) * (coord_phy[2] - cell_verts[0][2])
             );

}

// initialiaze coordinate of vertices on reference cell
template<class DT>
void ReferenceCell_Tetrahedron<DT>::init_ref_vert_coords() {
  this->ref_vert_coords_.resize(4);

  // initialize size of coord
  Coordinate coord(this->tdim_);

  // 0
  coord[0] = 0.0;
  coord[1] = 0.0;
  coord[2] = 0.0;
  this->ref_vert_coords_[0] = coord;

  // 1
  coord[0] = 1.0;
  coord[1] = 0.0;
  coord[2] = 0.0;
  this->ref_vert_coords_[1] = coord;

  // 2
  coord[0] = 0.0;
  coord[1] = 1.0;
  coord[2] = 0.0;
  this->ref_vert_coords_[2] = coord;

  // 3
  coord[0] = 0.0;
  coord[1] = 0.0;
  coord[2] = 1.0;
  this->ref_vert_coords_[3] = coord;

}

// check the coordinate is on reference cell
template<class DT>
bool ReferenceCell_Tetrahedron<DT>::is_on_reference_cell(
  const Coordinate coord_ref) const {

  assert(coord_ref.dim() >= this->tdim_);
  assert((0.0 - COMP_TOL) <= coord_ref[0] <= (1.0 + COMP_TOL));
  assert((0.0 - COMP_TOL) <= coord_ref[1] <= (1.0 + COMP_TOL));
  assert((0.0 - COMP_TOL) <= coord_ref[2] <= (1.0 + COMP_TOL));

  return ((0.0 - COMP_TOL) <= coord_ref[2]) &&
         (coord_ref[2] <= (1.0 - coord_ref[0] - coord_ref[1] + COMP_TOL));
}

// template instantiation
template class ReferenceCell_Tetrahedron<double>;
template class ReferenceCell_Tetrahedron<float>;

}; // namespace FEM
}; // namespace ChenLiu

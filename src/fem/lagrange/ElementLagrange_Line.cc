// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/lagrange/ElementLagrange_Line.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ElementLagrange_Line<DT>::ElementLagrange_Line() {
  this->reference_cell_ =
    std::unique_ptr<ReferenceCell<DT>>(new ReferenceCell_Line<DT>);

}

// destructor
template<class DT>
ElementLagrange_Line<DT>::~ElementLagrange_Line() {
  // do nothing
}

// init
template<class DT>
void ElementLagrange_Line<DT>::init(int ele_deg, int gdim) {
  assert(0 <= ele_deg);
  assert(1 <= gdim <= 3);

  this->ele_deg_ = ele_deg;

  this->gdim_ = gdim;

  // initialize coordinates of vertices on reference cell
  init_ref_dof_coords();
}

// clear
template<class DT>
void ElementLagrange_Line<DT>::clear() {
  this->clear();
}

// number of dofs on cell
template<class DT>
int ElementLagrange_Line<DT>::num_dofs_on_cell() const {
  assert(this->ref_dof_coords_.size() > 0);

  return this->ref_dof_coords_.size();
}

// evaluate reference basis function
template<class DT>
void ElementLagrange_Line<DT>::eval_ref_basis(
  const Coordinate coord_ref, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    assert(num_dofs_on_cell() == 1);

    val[0] = 1.0;
    return;
  }

  // evaluate coord_ref on basis function
  for (int i = 0; i != num_dofs_on_cell(); ++i) {
    val[i2idx(i)] = this->lp_.poly(this->ele_deg_, i, coord_ref[0]);
  }

}

// evaluate derivative reference basis function
template<class DT>
void ElementLagrange_Line<DT>::eval_ref_basis_deriv1(
  const Coordinate coord_ref, int d, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    assert(num_dofs_on_cell() == 1);

    val[0] = 0.0;
    return;
  }

  // evaluate coord_ref on first derivative on basis function on direction d
  for (int i = 0; i != num_dofs_on_cell(); ++i) {
    val[i2idx(i)] = this->lp_.poly_deriv1(this->ele_deg_, i, coord_ref[0]);
  }

}

template<class DT>
void ElementLagrange_Line<DT>::eval_ref_basis_deriv2(
  const Coordinate coord_ref, int d1, int d2, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d1 <= this->tdim());
  assert(0 <= d2 <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    assert(num_dofs_on_cell() == 1);

    val[0] = 0.0;
    return;
  }

  // evaluate coord_ref on second derivative on basis function on direction d
  for (int i = 0; i != num_dofs_on_cell(); ++i) {
    val[i2idx(i)] = this->lp_.poly_deriv2(this->ele_deg_, i, coord_ref[0]);
  }

}

// function for initialize coordinates of dofs of referece cell
template<class DT>
void ElementLagrange_Line<DT>::init_ref_dof_coords() {
  assert(this->ele_deg_ >= 0);
  assert(this->tdim() == 1);
  assert(1 <= this->gdim() <= 3);

  if (this->ele_deg_ == 0) {
    this->ref_dof_coords_.clear();

    // initialize coord
    Coordinate coord(this->gdim());

    coord[0] = 0.5;

    // put into ref_dof_coords_
    this->ref_dof_coords_.push_back(coord);

  } else {

    // Lexicographical ordering

    const DT offset = (1.0 / this->ele_deg_);

    // clear ref_dof_coords_
    const int n_dofs_cell = this->ele_deg_ + 1;
    const int n_dofs_line = this->ele_deg_ + 1;

    this->ref_dof_coords_.clear();
    this->ref_dof_coords_.resize(n_dofs_cell);

    // filling coordinates for dofs by lexicographical ordering
    for (int i = 0; i != n_dofs_line; ++i) {
      Coordinate coord(this->gdim());

      coord[0] = i * offset;

      this->ref_dof_coords_[i2idx(i)] = coord;
    }

  } // else

}

// i2idx
template<class DT>
int ElementLagrange_Line<DT>::i2idx(int i) const {
  assert(i >= 0);

  return i;
}

// provide indices of dofs (local) on sub-entity
template<class DT>
std::vector<int> ElementLagrange_Line<DT>::dof_indices_on_sub_entity(
  std::vector<int> entity_vert_idx,
  std::vector<int> sub_entity_vert_idx) const {

  assert(0 < this->ele_deg_ <= 2);

  assert(entity_vert_idx.size() == 2);
  assert(sub_entity_vert_idx.size() == 1);

  std::vector<std::vector<int>> entity_idx;
  std::vector<std::vector<int>> dof_idx;

  switch(sub_entity_vert_idx.size()) {
  case 1: // point
    entity_idx = {{0}, {1}};

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {{0}, {1}};
      break;

    case 2: // line
      dof_idx = {{0}, {2}};
      break;
    } // switch ele_deg_

    break; // case 1

  } // switch sub_entity_vert_idx.size()

  return this->identify_dof_idx_on_sub_entity(
           entity_vert_idx,
           sub_entity_vert_idx,
           entity_idx, dof_idx);

}

// template instantiation
template class ElementLagrange_Line<double>;
template class ElementLagrange_Line<float>;

}; // namespace FEM
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/lagrange/ElementLagrange.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ElementLagrange<DT>::ElementLagrange() {
  // set explicitly FE_Type
  this->fe_type_ = FE_Type::Lagrange;
}

// destructor
template<class DT>
ElementLagrange<DT>::~ElementLagrange() {
  lp_.clear();
}

// clear
template<class DT>
void ElementLagrange<DT>::clear() {
  this->clear();
  lp_.clear();
}

// template instantiation
template class ElementLagrange<double>;
template class ElementLagrange<float>;

}; // namespace FEM
}; // namespace ChenLiu

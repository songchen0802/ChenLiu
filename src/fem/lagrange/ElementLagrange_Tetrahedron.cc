// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/lagrange/ElementLagrange_Tetrahedron.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ElementLagrange_Tetrahedron<DT>::ElementLagrange_Tetrahedron() {
  // CellType
  this->reference_cell_ =
    std::unique_ptr<ReferenceCell<DT>>(new ReferenceCell_Tetrahedron<DT>());
}

// destructor
template<class DT>
ElementLagrange_Tetrahedron<DT>::~ElementLagrange_Tetrahedron() {
  // do nothing
}

// init
template<class DT>
void ElementLagrange_Tetrahedron<DT>::init(int ele_deg, int gdim) {
  assert(0 <= ele_deg);
  assert(gdim == 3);

  this->ele_deg_ = ele_deg;

  this->gdim_ = gdim;

  // initialize coordinates of vertices on reference cell
  init_ref_dof_coords();
}

// clear
template<class DT>
void ElementLagrange_Tetrahedron<DT>::clear() {
  this->clear();
}

// number of dofs on cell
template<class DT>
int ElementLagrange_Tetrahedron<DT>::num_dofs_on_cell() const {
  assert(this->ref_dof_coords_.size() > 0);

  return this->ref_dof_coords_.size();
}

// evaluate reference basis function
template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis(
  const Coordinate coord_ref, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    val[0] = 1.0;
    return;
  }

  // ele_deg_ > 0
  const DT ele_deg = static_cast<DT>(this->ele_deg_);

  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dh_1 = ele_deg * help;

  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] = this->lp_.poly(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      this->lp_.poly(this->ele_deg_ - i, this->ele_deg_ - i,
                     ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] =
    this->lp_.poly(this->ele_deg_, this->ele_deg_, coord_ref[0]);

  val[ijk2idx(0, this->ele_deg_, 0)] =
    this->lp_.poly(this->ele_deg_, this->ele_deg_, coord_ref[1]);

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] =
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j, dp_0 / (ele_deg - j)) *
      this->lp_.poly(j, j, ele_deg * coord_ref[1] / j);

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] =
    this->lp_.poly(this->ele_deg_, this->ele_deg_, coord_ref[2]);

  for (int k = 1; k != this->ele_deg_; ++k) {
    val[ijk2idx(0, 0, k)] =
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k,
                     ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, ele_deg * coord_ref[2] / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] =
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k, dp_0 / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    val[ijk2idx(0, this->ele_deg_ - k, k)] =
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k, dp_1 / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    for (int i = 1; i != this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        this->lp_.poly(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                       ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k);

    } // i

    for (int j = 1; j != this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        this->lp_.poly(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                       ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        this->lp_.poly(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                       dp_0 / (ele_deg - k - j)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k != this->ele_deg_; ++k) {
    for (int j = 1; j != this->ele_deg_ - k; ++j) {
      for (int i = 1; i != this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly(this->ele_deg_ - i - j - k, this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

// evaluate derivative reference basis function
template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv1(
  const Coordinate coord_ref, int d, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    val[0] = 0.0;
    return;
  }

  // ele_deg_ > 0
  switch (d) {

  case 0:
  {
    eval_ref_basis_deriv1_x(coord_ref, val);

    break;
  } // d = 0

  case 1:
  {
    eval_ref_basis_deriv1_y(coord_ref, val);

    break;
  } // d = 1

  case 2:
  {
    eval_ref_basis_deriv1_z(coord_ref, val);

    break;
  } // d = 2

  default:
  {
    CL_Error("d should be 0, 1 or 2!");
    exit(EXIT_FAILURE);

  } // default

  } // switch

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv2(
  const Coordinate coord_ref, int d1, int d2, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d1 <= this->tdim());
  assert(0 <= d2 <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    val[0] = 0.0;
    return;
  }

  // ele_deg_ > 0
  const int switch_val = (d1 + 1) * 10 + (d2 + 1);

  switch (switch_val) {
  case 11:
  {
    eval_ref_basis_deriv2_xx(coord_ref, val);

    break;
  } // d1 = 0, d2 = 0

  case 22:
  {
    eval_ref_basis_deriv2_yy(coord_ref, val);

    break;
  } // d1 = 1, d2 = 1

  case 33:
  {
    eval_ref_basis_deriv2_zz(coord_ref, val);

    break;
  } // d1 = 2, d2 = 2

  case 12:
  {
    eval_ref_basis_deriv2_xy(coord_ref, val);

    break;
  } // d1 = 0, d2 = 1

  case 21:
  {
    eval_ref_basis_deriv2_xy(coord_ref, val);

    break;
  } // d1 = 1, d2 = 0

  case 13:
  {
    eval_ref_basis_deriv2_xz(coord_ref, val);

    break;
  } // d1 = 0, d2 = 2

  case 31:
  {
    eval_ref_basis_deriv2_xz(coord_ref, val);

    break;
  } // d1 = 2, d2 = 0

  case 23:
  {
    eval_ref_basis_deriv2_yz(coord_ref, val);

    break;
  } // d1 = 1, d2 = 2

  case 32:
  {
    eval_ref_basis_deriv2_yz(coord_ref, val);

    break;
  } // d1 = 2, d2 = 1

  default:
  {
    CL_Error("d1 and d2 should be 0, 1 or 2!");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

// function for initialize coordinates of dofs of referece cell
template<class DT>
void ElementLagrange_Tetrahedron<DT>::init_ref_dof_coords() {
  assert(this->ele_deg_ >= 0);
  assert(this->tdim() == 3);
  assert(this->gdim() == 3);

  if (this->ele_deg_ == 0) {
    this->ref_dof_coords_.clear();

    // initialize coord
    Coordinate coord(this->gdim());

    coord[0] = 0.25;
    coord[1] = 0.25;
    coord[2] = 0.25;

    // put into ref_dof_coords_
    this->ref_dof_coords_.push_back(coord);

  } else {

    // Lexicographical ordering

    const DT offset = (1.0 / this->ele_deg_);

    // clear ref_dof_coords_
    const int n_dofs_cell = (this->ele_deg_ + 1) * (this->ele_deg_ + 2) *
                            (this->ele_deg_ + 3) / 6;
    const int n_dofs_line = this->ele_deg_ + 1;

    this->ref_dof_coords_.clear();
    this->ref_dof_coords_.resize(n_dofs_cell);

    // filling coordinates for dofs by lexicographical ordering
    for (int k = 0; k != n_dofs_line; ++k) {
      for (int j = 0; j != n_dofs_line - k; ++j) {
        for (int i = 0; i != n_dofs_line - k - j; ++i) {

          Coordinate coord(this->gdim());

          coord[0] = i * offset;
          coord[1] = j * offset;
          coord[2] = k * offset;

          this->ref_dof_coords_[ijk2idx(i, j, k)] = coord;

        } // i
      } // j
    } // k

  } // else

}

// ijk2idx
template<class DT>
int ElementLagrange_Tetrahedron<DT>::ijk2idx(int i, int j, int k) const {
  assert(i >= 0);
  assert(j >= 0);
  assert(k >= 0);

  int offset = 0;
  const int n_dofs_line = this->ele_deg_ + 1;

  for (int m = 0; m != k; ++m) {
    const int help = n_dofs_line - m;
    for (int d = 0; d != (n_dofs_line - m); ++d) {
      offset += help - d;
    }
  }

  for (int n = 0; n != j; ++n) {
    offset += n_dofs_line - n - k;
  }

  return (i + offset);
}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv1_x(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    - this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      - (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i)

      + (ele_deg / i) *
      this->lp_.poly(this->ele_deg_ - i, this->ele_deg_ - i,
                     ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] =
    this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, coord_ref[0]);

  val[ijk2idx(0, this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      - (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] =
      + (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            dp_0 / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    for (int i = 1; i != this->ele_deg_ - j; ++i) {
      val[ijk2idx(i, j, 0)] =
        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        + (ele_deg / i) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] = 0.0;

  for (int k = 1; k != this->ele_deg_; ++k) {
    val[ijk2idx(0, 0, k)] =
      - (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] =
      (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            dp_0 / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    for (int i = 1; i < this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        - (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / i) *
        this->lp_.poly(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                       ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] = 0.0;

    for (int j = 1; j != this->ele_deg_ - k; ++j) {
      val[ijk2idx(0, j, k)] =
        - (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / (ele_deg - k - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                              dp_0 / (ele_deg - k - j)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k < this->ele_deg_; ++k) {
    for (int j = 1; j < this->ele_deg_ - k; ++j) {
      for (int i = 1; i < this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          - (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + (ele_deg / i) *
          this->lp_.poly(this->ele_deg_ - i - j - k, this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv1_y(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    - this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      - (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] = 0.0;

  val[ijk2idx(0, this->ele_deg_, 0)] =
    this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, coord_ref[1]);

  for (int j  = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      - (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j)

      + (ele_deg / j) *
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] =
      + (ele_deg / j) *
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     dp_0 / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        + (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] = 0.0;

  for (int k = 1; k != this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      - (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] = 0.0;

    for (int i = 1; i != this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        - (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] =
      + (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * coord_ref[1] / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    for (int j = 1; j != this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        - (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                       ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                       dp_0 / (ele_deg - k - j)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k != this->ele_deg_; ++k) {
    for (int j = 1; j != this->ele_deg_ - k; ++j) {
      for (int i = 1; i != this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          - (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + this->lp_.poly(this->ele_deg_ - i - j - k,
                           this->ele_deg_ - i - j - k,
                           ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
          this->lp_.poly_deriv1(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv1_z(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    - this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      - (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] = 0.0;

  val[ijk2idx(0, this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      - (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] = 0.0;

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] =
    this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, coord_ref[2]);

  for (int k = 1; k != this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      - (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k)

      + (ele_deg / k) *
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k,
                     ele_deg * help / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] =
      + (ele_deg / k) *
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k, dp_0 / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k);

    for (int i = 1; i != this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        - (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                       ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] =
      + (ele_deg / k) *
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k, dp_1 / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k);

    for (int j = 1; j != this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        - (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                       ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                       dp_0 / (ele_deg - k - j)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k < this->ele_deg_; ++k) {
    for (int j = 1; j < this->ele_deg_ - k; ++j) {
      for (int i = 1; i < this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          - (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + (ele_deg / k) *
          this->lp_.poly(this->ele_deg_ - i - j - k, this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly_deriv1(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv2_xx(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {
    val[ijk2idx(i, 0, 0)] =
      + (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i)

      - (ele_deg / (ele_deg - i)) * (ele_deg / i) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i)

      - (ele_deg / (ele_deg - i)) * (ele_deg / i) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i)

      + (ele_deg / i) * (ele_deg / i) *
      this->lp_.poly(this->ele_deg_ - i, this->ele_deg_ - i,
                     ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv2(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, coord_ref[0]);

  val[ijk2idx(0, this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            dp_0 / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        + (ele_deg / i) * (ele_deg / i) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] = 0.0;

  for (int k = 1; k != this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            dp_0 / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    for (int i = 1; i != this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        + (ele_deg / (ele_deg - i - k)) * (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / i) * (ele_deg / i) *
        this->lp_.poly(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                       ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly_deriv2(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] = 0.0;

    for (int j = 1; j != this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        + (ele_deg / (ele_deg - j - k)) * (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / (ele_deg - k - j)) * (ele_deg / (ele_deg - k - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                              dp_0 / (ele_deg - k - j)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k < this->ele_deg_; ++k) {
    for (int j = 1; j < this->ele_deg_ - k; ++j) {
      for (int i = 1; i < this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          + (ele_deg / (ele_deg - i - j - k)) * (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv2(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / i) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / i) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + (ele_deg / i) * (ele_deg / i) *
          this->lp_.poly(this->ele_deg_ - i - j - k,
                         this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv2(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv2_yy(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] = 0.0;

  val[ijk2idx(0, this->ele_deg_, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, coord_ref[1]);

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j)

      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j)

      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j)

      + (ele_deg / j) * (ele_deg / j) *
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv2(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] =
      + (ele_deg / j) * (ele_deg / j) *
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j, dp_0 / (ele_deg - j)) *
      this->lp_.poly_deriv2(j, j, dp_1 / j);

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
        this->lp_.poly_deriv1(j, j, dp_1 / j)

        + (ele_deg / j) * (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv2(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] = 0.0;

  for (int k = 1; k != this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] = 0.0;

    for (int i = 1; i < this->ele_deg_ - k; ++i)
      val[ijk2idx(i, 0, k)] =
        + (ele_deg / (ele_deg - i - k)) * (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k);

    val[ijk2idx(0, this->ele_deg_ - k, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            dp_1 / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    for (int j = 1; j != this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        + (ele_deg / (ele_deg - j - k)) * (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / j) * (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                       ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly_deriv2(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / j) * (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                       dp_0 / (ele_deg - k - j)) *
        this->lp_.poly_deriv2(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k != this->ele_deg_; ++k) {
    for (int j = 1; j != this->ele_deg_ - k; ++j) {
      for (int i = 1; i != this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          + (ele_deg / (ele_deg - i - j - k)) * (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv2(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
          this->lp_.poly_deriv1(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
          this->lp_.poly_deriv1(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + (ele_deg / j) * (ele_deg / j) *
          this->lp_.poly(this->ele_deg_ - i - j - k,
                         this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly_deriv2(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv2_zz(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i < this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] = 0.0;

  val[ijk2idx(0, this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] = 0.0;

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, coord_ref[2]);

  for (int k = 1; k < this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k)

      - (ele_deg / (ele_deg - k)) * (ele_deg / k) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k)

      - (ele_deg / (ele_deg - k)) * (ele_deg / k) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k)

      + (ele_deg / k) * (ele_deg / k) *
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k,
                     ele_deg * help / (ele_deg - k)) *
      this->lp_.poly_deriv2(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] =
      + (ele_deg / k) * (ele_deg / k) *
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k,
                     dp_0 / (ele_deg - k)) *
      this->lp_.poly_deriv2(k, k, dp_2 / k);

    for (int i = 1; i != this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        + (ele_deg / (ele_deg - i - k)) * (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) * (ele_deg / k) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) * (ele_deg / k) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(k, k, dp_2 / k)

        + (ele_deg / k) * (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                       ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv2(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] =
      + (ele_deg / k) * (ele_deg / k) *
      this->lp_.poly(this->ele_deg_ - k, this->ele_deg_ - k,
                     dp_1 / (ele_deg - k)) *
      this->lp_.poly_deriv2(k, k, dp_2 / k);

    for (int j = 1; j != this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        + (ele_deg / (ele_deg - j - k)) * (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) * (ele_deg / k) *
        this->lp_.poly_deriv1(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) * (ele_deg / k) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k)

        + (ele_deg / k) * (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                       ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv2(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / k) * (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                       dp_0 / (ele_deg - k - j)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv2(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k != this->ele_deg_; ++k) {
    for (int j = 1; j != this->ele_deg_ - k; ++j) {
      for (int i = 1; i != this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          + (ele_deg / (ele_deg - i - j - k)) * (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv2(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / k) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly_deriv1(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / k) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly_deriv1(k, k, dp_2 / k)

          + (ele_deg / k) * (ele_deg / k) *
          this->lp_.poly(this->ele_deg_ - i - j - k, this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly_deriv2(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv2_xy(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      + (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i)

      - (ele_deg / (ele_deg - i)) * (ele_deg / i) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] = 0.0;

  val[ijk2idx(0, this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j)

      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            dp_0 / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        + (ele_deg / i) * (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(j, j, dp_1 / j);

    } // i

    val[ijk2idx(0, 0, this->ele_deg_)] = 0.0;

  } // j

  for (int k = 1; k != this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] = 0.0;

    for (int i = 1; i < this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        + (ele_deg / (ele_deg - i - k)) * (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] = 0.0;

    for (int j = 1; j < this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        + (ele_deg / (ele_deg - j - k)) * (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / (ele_deg - k - j)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                              dp_0 / (ele_deg - k - j)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k < this->ele_deg_; ++k) {
    for (int j = 1; j < this->ele_deg_ - k; ++j) {
      for (int i = 1; i < this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          + (ele_deg / (ele_deg - i - j - k)) * (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv2(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / j) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly_deriv1(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / i) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + (ele_deg / i) * (ele_deg / j) *
          this->lp_.poly(this->ele_deg_ - i - j - k, this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(i, i, dp_0 / i) *
          this->lp_.poly_deriv1(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv2_xz(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      + (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i)

      - (ele_deg / (ele_deg - i)) * (ele_deg / i) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] = 0.0;

  val[ijk2idx(0, this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ijk2idx(0, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] = 0.0;

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] = 0.0;

  for (int k = 1; k < this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k)

      - (ele_deg / (ele_deg - k)) * (ele_deg / k) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / k) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            dp_0 / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k);

    for (int i = 1; i < this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        + (ele_deg / (ele_deg - i - k)) * (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) * (ele_deg / k) *
        this->lp_.poly_deriv1(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / i) * (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                       ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] = 0.0;

    for (int j = 1; j < this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        + (ele_deg / (ele_deg - j - k)) * (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) * (ele_deg / k) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / (ele_deg - k - j)) * (ele_deg / k) *
        this->lp_.poly_deriv1(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                              dp_0 / (ele_deg - k - j)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k < this->ele_deg_; ++k) {
    for (int j = 1; j < this->ele_deg_ - k; ++j) {
      for (int i = 1; i < this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          + (ele_deg / (ele_deg - i - j - k)) * (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv2(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / k) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly_deriv1(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / i) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + (ele_deg / i) * (ele_deg / k) *
          this->lp_.poly(this->ele_deg_ - i - j - k, this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly_deriv1(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Tetrahedron<DT>::eval_ref_basis_deriv2_yz(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);
  const DT help = 1.0 - coord_ref[0] - coord_ref[1] - coord_ref[2];
  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];
  const DT dp_2 = ele_deg * coord_ref[2];

  val[ijk2idx(0, 0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i < this->ele_deg_; ++i) {

    val[ijk2idx(i, 0, 0)] =
      + (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ijk2idx(this->ele_deg_, 0, 0)] = 0.0;

  val[ijk2idx(0, this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {
    val[ijk2idx(0, j, 0)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j)

      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

    val[ijk2idx(this->ele_deg_ - j, j, 0)] = 0.0;

    for (int i = 1; i < this->ele_deg_ - j; ++i) {

      val[ijk2idx(i, j, 0)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(j, j, dp_1 / j);

    } // i

  } // j

  val[ijk2idx(0, 0, this->ele_deg_)] = 0.0;

  for (int k = 1; k < this->ele_deg_; ++k) {

    val[ijk2idx(0, 0, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / (ele_deg - k)) *
      this->lp_.poly_deriv2(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly(k, k, dp_2 / k)

      - (ele_deg / (ele_deg - k)) * (ele_deg / k) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            ele_deg * help / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k);

    val[ijk2idx(this->ele_deg_ - k, 0, k)] = 0.0;

    for (int i = 1; i < this->ele_deg_ - k; ++i) {

      val[ijk2idx(i, 0, k)] =
        + (ele_deg / (ele_deg - i - k)) * (ele_deg / (ele_deg - i - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - i - k)) * (ele_deg / k) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - k, this->ele_deg_ - i - k,
                              ele_deg * help / (ele_deg - i - k)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

    } // i

    val[ijk2idx(0, this->ele_deg_ - k, k)] =
      + (ele_deg / (ele_deg - k)) * (ele_deg / k) *
      this->lp_.poly_deriv1(this->ele_deg_ - k, this->ele_deg_ - k,
                            dp_1 / (ele_deg - k)) *
      this->lp_.poly_deriv1(k, k, dp_2 / k);

    for (int j = 1; j < this->ele_deg_ - k; ++j) {

      val[ijk2idx(0, j, k)] =
        + (ele_deg / (ele_deg - j - k)) * (ele_deg / (ele_deg - j - k)) *
        this->lp_.poly_deriv2(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) * (ele_deg / k) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k)

        - (ele_deg / (ele_deg - j - k)) * (ele_deg / j) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                              ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly(k, k, dp_2 / k)

        + (ele_deg / j) * (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - j - k, this->ele_deg_ - j - k,
                       ele_deg * help / (ele_deg - j - k)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

      val[ijk2idx(this->ele_deg_ - k - j, j, k)] =
        + (ele_deg / j) * (ele_deg / k) *
        this->lp_.poly(this->ele_deg_ - k - j, this->ele_deg_ - k - j,
                       dp_0 / (ele_deg - k - j)) *
        this->lp_.poly_deriv1(j, j, dp_1 / j) *
        this->lp_.poly_deriv1(k, k, dp_2 / k);

    } // j

  } // k

  for (int k = 1; k < this->ele_deg_; ++k) {
    for (int j = 1; j < this->ele_deg_ - k; ++j) {
      for (int i = 1; i < this->ele_deg_ - k - j; ++i) {

        val[ijk2idx(i, j, k)] =
          + (ele_deg / (ele_deg - i - j - k)) * (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv2(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) * (ele_deg / k) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly(j, j, dp_1 / j) *
          this->lp_.poly_deriv1(k, k, dp_2 / k)

          - (ele_deg / (ele_deg - i - j - k)) *
          this->lp_.poly_deriv1(this->ele_deg_ - i - j - k,
                                this->ele_deg_ - i - j - k,
                                ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
          this->lp_.poly_deriv1(j, j, dp_1 / j) *
          this->lp_.poly(k, k, dp_2 / k)

          + (ele_deg / k) * (ele_deg / j) *
          this->lp_.poly(this->ele_deg_ - i - j - k,
                         this->ele_deg_ - i - j - k,
                         ele_deg * help / (ele_deg - i - j - k)) *
          this->lp_.poly(i, i, dp_0 / i) *
          this->lp_.poly_deriv1(j, j, dp_1 / j) *
          this->lp_.poly_deriv1(k, k, dp_2 / k);

      } // i
    } // j
  } // k

}

// provide indices of dofs (local) on sub-entity
template<class DT>
std::vector<int> ElementLagrange_Tetrahedron<DT>::dof_indices_on_sub_entity(
  std::vector<int> entity_vert_idx,
  std::vector<int> sub_entity_vert_idx) const {

  assert(0 < this->ele_deg_ <= 2);

  assert(entity_vert_idx.size() == 4);
  assert(sub_entity_vert_idx.size() == 1 || 2 || 3);

  std::vector<std::vector<int>> entity_idx;
  std::vector<std::vector<int>> dof_idx;

  switch(sub_entity_vert_idx.size()) {
  case 1: // point
    entity_idx = {{0}, {1}, {2}, {3}};

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {{0}, {1}, {2}, {3}};
      break;

    case 2:
      dof_idx = {{0}, {2}, {5}, {9}};
      break;
    } // switch ele_deg_

    break; // case 1

  case 2: // line
    entity_idx = {{0, 1}, {1, 2}, {2, 0}, {0, 3}, {1, 3}, {2, 3}};

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {{0, 1}, {1, 2}, {2, 0}, {0, 3}, {1, 3}, {2, 3}};
      break;

    case 2:
      dof_idx = {
        {0, 1, 2}, {2, 4, 5}, {5, 3, 0},
        {0, 6, 9}, {2, 7, 9}, {5, 8, 9}
      };
      break;
    }

    break; // case 2

  case 3: // tri
    entity_idx = {{0, 1, 2}, {0, 1, 3}, {1, 2, 3}, {2, 0, 3}};

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {{0, 1, 2}, {0, 1, 3}, {1, 2, 3}, {2, 0, 3}};
      break;

    case 2:
      dof_idx = {
        {0, 1, 2, 3, 4, 5}, {0, 1, 2, 6, 7, 9},
        {2, 4, 5, 7, 8, 9}, {5, 3, 0, 8, 6, 9}
      };
      break;
    }

    break; // case 3

  } // switch sub_entity_vert_idx.size()

  return this->identify_dof_idx_on_sub_entity(
           entity_vert_idx,
           sub_entity_vert_idx,
           entity_idx, dof_idx);

}

// template instantiation
template class ElementLagrange_Tetrahedron<double>;
template class ElementLagrange_Tetrahedron<float>;

}; // namespace FEM
}; // namespace ChenLiu

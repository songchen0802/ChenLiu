// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/lagrange/ElementLagrange_Hexahedron.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ElementLagrange_Hexahedron<DT>::ElementLagrange_Hexahedron() {
  // CellType
  this->reference_cell_ =
    std::unique_ptr<ReferenceCell<DT>>(new ReferenceCell_Hexahedron<DT>());
}

// destructor
template<class DT>
ElementLagrange_Hexahedron<DT>::~ElementLagrange_Hexahedron() {
  // do nothing
}

// init
template<class DT>
void ElementLagrange_Hexahedron<DT>::init(int ele_deg, int gdim) {
  assert(0 <= ele_deg);
  assert(gdim == 3);

  this->ele_deg_ = ele_deg;

  this->gdim_ = gdim;

  // initialize coordinates of vertices on reference cell
  init_ref_dof_coords();
}

// clear
template<class DT>
void ElementLagrange_Hexahedron<DT>::clear() {
  this->clear();
}

// number of dofs on cell
template<class DT>
int ElementLagrange_Hexahedron<DT>::num_dofs_on_cell() const {
  assert(this->ref_dof_coords_.size() > 0);

  return this->ref_dof_coords_.size();
}

// evaluate reference basis function
template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis(
  const Coordinate coord_ref, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    val[0] = 1.0;
    return;
  }

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

// evaluate derivative reference basis function
template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv1(
  const Coordinate coord_ref, int d, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    val[0] = 0.0;
    return;
  }

  switch(d) {
  case 0:
  {
    eval_ref_basis_deriv1_x(coord_ref, val);

    break;
  } // d = 0

  case 1:
  {
    eval_ref_basis_deriv1_y(coord_ref, val);

    break;
  } // d = 1

  case 2:
  {
    eval_ref_basis_deriv1_z(coord_ref, val);

    break;
  } // d = 2

  default:
  {
    CL_Error("d should be 0, 1 or 2!");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv2(
  const Coordinate coord_ref, int d1, int d2, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d1 <= this->tdim());
  assert(0 <= d2 <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    val[0] = 0.0;
    return;
  }

  // switch value
  const int switch_val = (d1 + 1) * 10 + (d2 + 1);

  switch(switch_val) {
  case 11:
  {
    eval_ref_basis_deriv2_xx(coord_ref, val);

    break;
  } // d1 = 0, d2 = 0

  case 22:
  {
    eval_ref_basis_deriv2_yy(coord_ref, val);

    break;
  } // d1 = 1, d2 = 1

  case 33:
  {
    eval_ref_basis_deriv2_zz(coord_ref, val);

    break;
  } // d1 = 2, d2 = 2

  case 12:
  {
    eval_ref_basis_deriv2_xy(coord_ref, val);

    break;
  } // d1 = 0, d2 = 1

  case 21:
  {
    eval_ref_basis_deriv2_xy(coord_ref, val);

    break;
  } // d1 = 1, d2 = 0

  case 13:
  {
    eval_ref_basis_deriv2_xz(coord_ref, val);

    break;
  } // d1 = 0, d2 = 2

  case 31:
  {
    eval_ref_basis_deriv2_xz(coord_ref, val);

    break;
  } // d1 = 2, d2 = 0

  case 23:
  {
    eval_ref_basis_deriv2_yz(coord_ref, val);

    break;
  } // d1 = 1, d2 = 2

  case 32:
  {
    eval_ref_basis_deriv2_yz(coord_ref, val);

    break;
  } // d1 = 2, d2 = 1

  default:
  {
    CL_Error("d1 and d2 should be 0 or 1!");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

// function for initialize coordinates of dofs of referece cell
template<class DT>
void ElementLagrange_Hexahedron<DT>::init_ref_dof_coords() {
  assert(this->ele_deg_ >= 0);
  assert(this->tdim() == 3);
  assert(this->gdim() == 3);

  if (this->ele_deg_ == 0) {
    this->ref_dof_coords_.clear();

    // initialize coord
    Coordinate coord(this->gdim());

    coord[0] = 0.5;
    coord[1] = 0.5;
    coord[2] = 0.5;

    // put into ref_dof_coords_
    this->ref_dof_coords_.push_back(coord);

  } else {

    // Lexicographical ordering

    const DT offset = (1.0 / this->ele_deg_);

    // clear ref_dof_coords_
    const int n_dofs_cell = (this->ele_deg_ + 1) * (this->ele_deg_ + 1) *
                            (this->ele_deg_ + 1);
    const int n_dofs_line = this->ele_deg_ + 1;

    this->ref_dof_coords_.clear();
    this->ref_dof_coords_.resize(n_dofs_cell);

    // filling coordinates for dofs by lexicographical ordering
    for (int k = 0; k != this->ele_deg_ + 1; ++k) {
      for (int j = 0; j != this->ele_deg_ + 1; ++j) {
        for (int i = 0; i != this->ele_deg_ + 1; ++i) {

          Coordinate coord(this->gdim());

          coord[0] = i * offset;
          coord[1] = j * offset;
          coord[2] = k * offset;

          this->ref_dof_coords_[ijk2idx(i, j, k)] = coord;

        } // i
      } // j
    } // k

  } // else

}

// ijk2idx
template<class DT>
int ElementLagrange_Hexahedron<DT>::ijk2idx(int i, int j, int k) const {
  assert(i >= 0);
  assert(j >= 0);
  assert(k >= 0);

  const int n_dofs_line = this->ele_deg_ + 1;

  return (i + j * n_dofs_line + k * n_dofs_line * n_dofs_line);
}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv1_x(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());


  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly_deriv1(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv1_y(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly_deriv1(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv1_z(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly_deriv1(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv2_xx(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly_deriv2(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv2_yy(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly_deriv2(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv2_zz(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly_deriv2(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv2_xy(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly_deriv1(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly_deriv1(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv2_xz(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly_deriv1(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly_deriv1(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

template<class DT>
void ElementLagrange_Hexahedron<DT>::eval_ref_basis_deriv2_yz(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  for (int k = 0; k != this->ele_deg_ + 1; ++k) {
    for (int j = 0; j != this->ele_deg_ + 1; ++j) {
      for (int i = 0; i != this->ele_deg_ + 1; ++i) {

        val[ijk2idx(i, j, k)] =
          this->lp_.poly(this->ele_deg_, i, coord_ref[0]) *
          this->lp_.poly_deriv1(this->ele_deg_, j, coord_ref[1]) *
          this->lp_.poly_deriv1(this->ele_deg_, k, coord_ref[2]);

      } // i
    } // j
  } // k

}

// provide indices of dofs (local) on sub-entity
template<class DT>
std::vector<int> ElementLagrange_Hexahedron<DT>::dof_indices_on_sub_entity(
  std::vector<int> entity_vert_idx,
  std::vector<int> sub_entity_vert_idx) const {

  assert(0 < this->ele_deg_ <= 2);

  assert(entity_vert_idx.size() == 8);
  assert(sub_entity_vert_idx.size() == 1 || 2 || 4);

  std::vector<std::vector<int>> entity_idx;
  std::vector<std::vector<int>> dof_idx;

  switch(sub_entity_vert_idx.size()) {
  case 1: // point
    entity_idx = {{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}};

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {{0}, {1}, {3}, {2}, {4}, {5}, {7}, {6}};
      break;

    case 2: // line
      dof_idx = {{0}, {2}, {8}, {6}, {18}, {20}, {26}, {24}};
      break;
    } // switch ele_deg_

    break; // case 1

  case 2: // line
    entity_idx = {
      {0, 1}, {1, 2}, {2, 3}, {3, 0},
      {0, 4}, {1, 5}, {2, 6}, {3, 7},
      {4, 5}, {5, 6}, {6, 7}, {7, 4}
    };

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {
        {0, 1}, {1, 3}, {2, 3}, {2, 0},
        {0, 4}, {1, 5}, {3, 7}, {2, 6},
        {4, 5}, {5, 7}, {6, 7}, {6, 4}
      };
      break;

    case 2:
      dof_idx = {
        {0, 1, 2}, {2, 5, 8}, {8, 7, 6}, {6, 3, 0},
        {0, 9, 18}, {2, 11, 20}, {8, 17, 26}, {6, 15, 24},
        {18, 19, 20}, {20, 23, 26}, {26, 25, 24}, {24, 21, 18}
      };
      break;
    } // switch ele_deg_

    break; // case 2

  case 4: // quad
    entity_idx = {
      {0, 1, 2, 3}, {0, 1, 5, 4}, {1, 2, 6, 5},
      {3, 2, 6, 7}, {3, 0, 4, 7}, {4, 5, 6, 7}
    };

    switch(this->ele_deg_) {

    case 1:
      dof_idx = {
        {0, 1, 2, 3}, {0, 1, 5, 4}, {1, 3, 5, 7},
        {3, 2, 6, 7}, {2, 0, 4, 6}, {4, 5, 6, 7}
      };
      break;

    case 2:
      dof_idx = {
        {0, 1, 2, 3, 4, 5, 6, 7, 8}, {0, 1, 2, 9, 10, 11, 18, 19, 20},
        {2, 5, 8, 11, 14, 17, 20, 23, 26}, {6, 7, 8, 15, 16, 17, 24, 25, 26},
        {0, 3, 6, 9, 12, 15, 18, 21, 24}, {18, 19, 20, 21, 22, 23, 24, 25, 26}
      };
      break;
    }

    break; // case 4

  } // switch sub_entity_vert_idx.size()

  return this->identify_dof_idx_on_sub_entity(
           entity_vert_idx,
           sub_entity_vert_idx,
           entity_idx, dof_idx);

}

// template instantiation
template class ElementLagrange_Hexahedron<double>;
template class ElementLagrange_Hexahedron<float>;

}; // namespace FEM
}; // namespace ChenLiu

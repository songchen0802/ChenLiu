// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/lagrange/ElementLagrange_Triangle.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ElementLagrange_Triangle<DT>::ElementLagrange_Triangle() {
  // CellType
  this->reference_cell_ =
    std::unique_ptr<ReferenceCell<DT>>(new ReferenceCell_Triangle<DT>());
}

// destructor
template<class DT>
ElementLagrange_Triangle<DT>::~ElementLagrange_Triangle() {
  // do nothing
}

// init
template<class DT>
void ElementLagrange_Triangle<DT>::init(int ele_deg, int gdim) {
  assert(0 <= ele_deg);
  assert(2 <= gdim <= 3);

  this->ele_deg_ = ele_deg;

  this->gdim_ = gdim;

  // initialize coordinates of vertices on reference cell
  init_ref_dof_coords();
}

// clear
template<class DT>
void ElementLagrange_Triangle<DT>::clear() {
  this->clear();
}

// number of dofs on cell
template<class DT>
int ElementLagrange_Triangle<DT>::num_dofs_on_cell() const {
  assert(this->ref_dof_coords_.size() > 0);

  return this->ref_dof_coords_.size();
}

// evaluate reference basis function
template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis(
  const Coordinate coord_ref, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    assert(num_dofs_on_cell() == 1);

    val[0] = 1.0;
    return;
  }

  // ele_deg_ > 0
  const DT ele_deg = static_cast<DT>(this->ele_deg_);

  const DT help = 1.0 - coord_ref[0] - coord_ref[1];
  const DT dh_1 = ele_deg * help;

  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];

  // evaluate coord_ref on basis function
  val[ij2idx(0, 0)] =
    this->lp_.poly(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i != this->ele_deg_; ++i) {

    val[ij2idx(i, 0)] =
      this->lp_.poly(this->ele_deg_ - i, this->ele_deg_ - i,
                     dh_1 / (this->ele_deg_ - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ij2idx(this->ele_deg_, 0)] =
    this->lp_.poly(this->ele_deg_, this->ele_deg_, coord_ref[0]);

  for (int j = 1; j != this->ele_deg_; ++j) {

    val[ij2idx(0, j)] =
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     dh_1 / (this->ele_deg_ - j)) *
      this->lp_.poly(j, j, dp_1 / j);

    for (int i = 1; i != this->ele_deg_ - j; ++i) {

      val[ij2idx(i, j)] =
        this->lp_.poly(this->ele_deg_ - j - i, this->ele_deg_ - j - i,
                       dh_1 / (this->ele_deg_ - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

    val[ij2idx(this->ele_deg_ - j, j)] =
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     dp_0 / (this->ele_deg_ - j)) *
      this->lp_.poly(j, j, dp_1 / j);

  } // j

  val[ij2idx(0, this->ele_deg_)] =
    this->lp_.poly(this->ele_deg_, this->ele_deg_, coord_ref[1]);

}

// evaluate derivative reference basis function
template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis_deriv1(
  const Coordinate coord_ref, int d, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    assert(num_dofs_on_cell() == 1);

    val[0] = 0.0;
    return;
  }

  // evaluate derivative reference basis function
  switch(d) {
  case 0:
  {
    eval_ref_basis_deriv1_x(coord_ref, val);

    break;
  } // d = 0

  case 1:
  {
    eval_ref_basis_deriv1_y(coord_ref, val);

    break;
  } // d = 1

  default:
  {
    CL_Error("d should be 0 or 1!");
    exit(EXIT_FAILURE);

  } // default

  } // switch

}

template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis_deriv2(
  const Coordinate coord_ref, int d1, int d2, std::vector<DT>& val) const {

  assert(coord_ref.dim() >= this->tdim());
  assert(val.size() == 0 || val.size() == num_dofs_on_cell());
  assert(this->ele_deg_ >= 0);
  assert(0 <= d1 <= this->tdim());
  assert(0 <= d2 <= this->tdim());

  // resize val if necessary
  if (val.size() == 0) {
    val.resize(num_dofs_on_cell());
  }

  // treat corner case ele_deg_ = 0
  if (this->ele_deg_ == 0) {
    assert(num_dofs_on_cell() == 1);

    val[0] = 0.0;
    return;
  }

  // switch value
  const int switch_val = (d1 + 1) * 10 + (d2 + 1);

  // evaluate coord_ref on first derivative on basis function on direction d
  switch(switch_val) {

  case 11:
  {
    eval_ref_basis_deriv2_xx(coord_ref, val);

    break;
  } // d1 = 0, d2 = 0

  case 12:
  {
    eval_ref_basis_deriv2_xy(coord_ref, val);

    break;
  } // d1 = 0, d2 = 1

  case 21:
  {
    eval_ref_basis_deriv2_xy(coord_ref, val);

    break;
  } // d1 = 1, d2 =0

  case 22:
  {
    eval_ref_basis_deriv2_yy(coord_ref, val);

    break;
  } // d1 = 1, d2 = 1

  default:
  {
    CL_Error("d1 and d2 should be 0 or 1!");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

// function for initialize coordinates of dofs of referece cell
template<class DT>
void ElementLagrange_Triangle<DT>::init_ref_dof_coords() {
  assert(this->ele_deg_ >= 0);
  assert(this->tdim() == 2);

  if (this->ele_deg_ == 0) {
    this->ref_dof_coords_.clear();

    // initialize coord
    Coordinate coord(this->tdim());

    coord[0] = 1.0 / 3;
    coord[1] = 1.0 / 3;

    // put into ref_dof_coords_
    this->ref_dof_coords_.push_back(coord);

  } else {

    // Lexicographical ordering

    const DT offset = (1.0 / this->ele_deg_);

    // clear ref_dof_coords_
    const int n_dofs_cell = ((this->ele_deg_ + 1) * (this->ele_deg_ + 2)) / 2;
    const int n_dofs_line = this->ele_deg_ + 1;

    this->ref_dof_coords_.clear();
    this->ref_dof_coords_.resize(n_dofs_cell);

    // filling coordinates for dofs by lexicographical ordering
    for (int j = 0; j != n_dofs_line; ++j) {
      const DT j_offset = j * offset;
      for (int i = 0; i != n_dofs_line - j; ++i) {

        Coordinate coord(this->tdim());

        coord[0] = i * offset;
        coord[1] = j_offset;

        this->ref_dof_coords_[ij2idx(i, j)] = coord;
      }
    }

  } // else

}

// ij2idx
template<class DT>
int ElementLagrange_Triangle<DT>::ij2idx(int i, int j) const {
  assert(i >= 0);
  assert(j >= 0);

  int offset = 0;
  const int n_dofs_line = this->ele_deg_ + 1;

  for (int n = 0; n != j; ++n) {
    offset += n_dofs_line - n;
  }

  return (i + offset);
}

template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis_deriv1_x(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);

  const DT help = 1.0 - coord_ref[0] - coord_ref[1];
  const DT dh_1 = ele_deg * help;

  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];

  val[ij2idx(0, 0)] =
    - this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i < this->ele_deg_; ++i) {

    val[ij2idx(i, 0)] =
      - (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            dh_1 / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i)

      + (ele_deg / i) *
      this->lp_.poly(this->ele_deg_ - i, this->ele_deg_ - i, dh_1 / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i);

  } // i

  val[ij2idx(this->ele_deg_, 0)] =
    this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, coord_ref[0]);

  for (int j = 1; j < this->ele_deg_; ++j) {

    val[ij2idx(0, j)] =
      - (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            dh_1 / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

  } // j

  val[ij2idx(0, this->ele_deg_)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {

    for (int i = 1; i < this->ele_deg_ - j; ++i) {

      val[ij2idx(i, j)] =
        - (ele_deg / (ele_deg - j - i)) *
        this->lp_.poly_deriv1(this->ele_deg_ - j - i, this->ele_deg_ - j - i,
                              dh_1 / (ele_deg - j - i)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        + (ele_deg / i) *
        this->lp_.poly(this->ele_deg_ - j - i, this->ele_deg_ - j - i,
                       dh_1 / (ele_deg - j - i)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

    val[ij2idx(this->ele_deg_ - j, j)] =
      + (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            dp_0 / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

  } // j

}

template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis_deriv1_y(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);

  const DT help = 1.0 - coord_ref[0] - coord_ref[1];
  const DT dh_1 = ele_deg * help;

  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];

  val[ij2idx(0, 0)] =
    - this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i < this->ele_deg_; ++i) {
    val[ij2idx(i, 0)] =
      - (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);
  }

  val[ij2idx(this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {
    val[ij2idx(0, j)] =
      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j) +
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);
  }

  val[ij2idx(0, this->ele_deg_)] =
    this->lp_.poly_deriv1(this->ele_deg_, this->ele_deg_, coord_ref[1]);

  for (int j = 1; j < this->ele_deg_; ++j) {

    for (int i = 1; i < this->ele_deg_ - j; ++i) {
      val[ij2idx(i, j)] =
        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly(j, j, ele_deg * coord_ref[1] / j)

        + (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(j, j, ele_deg * coord_ref[1] / j);
    } // i

    val[ij2idx(this->ele_deg_ - j, j)] =
      + (ele_deg / j) *
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j, dp_0 / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

  } // j

}

template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis_deriv2_xx(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);

  const DT help = 1.0 - coord_ref[0] - coord_ref[1];
  const DT dh_1 = ele_deg * help;

  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];

  val[ij2idx(0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i < this->ele_deg_; ++i) {

    val[ij2idx(i, 0)] =
      + (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i)

      - (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      (ele_deg / i) * this->lp_.poly_deriv1(i, i, dp_0 / i)

      - (ele_deg / (ele_deg - i)) * (ele_deg / i) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i)

      + (ele_deg / i) * (ele_deg / i) *
      this->lp_.poly(this->ele_deg_ - i, this->ele_deg_ - i,
                     ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv2(i, i, dp_0 / i);

  } // i

  val[ij2idx(this->ele_deg_, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, coord_ref[0]);

  for (int j = 1; j < this->ele_deg_; ++j) {

    val[ij2idx(0, j)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

  } // j

  val[ij2idx(0, this->ele_deg_)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {

    for (int i = 1; i < this->ele_deg_ - j; ++i) {
      val[ij2idx(i, j)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) * this->lp_.poly(j, j, dp_1 / j) +
        - (ele_deg / (ele_deg - i - j)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        + (ele_deg / i) * (ele_deg / i) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j);

    } // i

    val[ij2idx(this->ele_deg_ - j, j)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            dp_0 / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j);

  } // j

}

template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis_deriv2_yy(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);

  const DT help = 1.0 - coord_ref[0] - coord_ref[1];
  const DT dh_1 = ele_deg * help;

  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];

  val[ij2idx(0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i < this->ele_deg_; ++i) {

    val[ij2idx(i, 0)] =
      + (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            this->ele_deg_ * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i);

  } // i

  val[ij2idx(this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {

    val[ij2idx(0, j)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j)

      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j)

      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j)

      + (ele_deg / j) * (ele_deg / j) *
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j,
                     ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv2(j, j, dp_1 / j);

  } // j

  val[ij2idx(0, this->ele_deg_)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, coord_ref[1]);

  for (int j = 1; j < this->ele_deg_; ++j) {

    for (int i = 1; i < this->ele_deg_ - j; ++i) {
      val[ij2idx(i, j)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) * this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
        this->lp_.poly_deriv1(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
        this->lp_.poly_deriv1(j, j, dp_1 / j)

        + (ele_deg / j) * (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) *
        this->lp_.poly_deriv2(j, j, dp_1 / j);

    } // i

    val[ij2idx(this->ele_deg_ - j, j)] =
      this->lp_.poly(this->ele_deg_ - j, this->ele_deg_ - j, dp_0 / (ele_deg - j)) *
      (ele_deg / j) * (ele_deg / j) * this->lp_.poly_deriv2(j, j, dp_1 / j);

  } // j

}

template<class DT>
void ElementLagrange_Triangle<DT>::eval_ref_basis_deriv2_xy(
  const Coordinate coord_ref, std::vector<DT>& val) const {
  assert(this->ele_deg_ > 0);
  assert(val.size() > 0);
  assert(val.size() == num_dofs_on_cell());

  const DT ele_deg = static_cast<DT>(this->ele_deg_);

  const DT help = 1.0 - coord_ref[0] - coord_ref[1];
  const DT dh_1 = ele_deg * help;

  const DT dp_0 = ele_deg * coord_ref[0];
  const DT dp_1 = ele_deg * coord_ref[1];

  val[ij2idx(0, 0)] =
    this->lp_.poly_deriv2(this->ele_deg_, this->ele_deg_, help);

  for (int i = 1; i < this->ele_deg_; ++i) {

    val[ij2idx(i, 0)] =
      + (ele_deg / (ele_deg - i)) * (ele_deg / (ele_deg - i)) *
      this->lp_.poly_deriv2(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly(i, i, dp_0 / i)

      - (ele_deg / (ele_deg - i)) * (ele_deg / i) *
      this->lp_.poly_deriv1(this->ele_deg_ - i, this->ele_deg_ - i,
                            ele_deg * help / (ele_deg - i)) *
      this->lp_.poly_deriv1(i, i, dp_0 / i);

  } // j

  val[ij2idx(this->ele_deg_, 0)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {

    val[ij2idx(0, j)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / (ele_deg - j)) *
      this->lp_.poly_deriv2(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly(j, j, dp_1 / j)

      - (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            ele_deg * help / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

  } // j

  val[ij2idx(0, this->ele_deg_)] = 0.0;

  for (int j = 1; j < this->ele_deg_; ++j) {

    for (int i = 1; i < this->ele_deg_ - j; ++i) {

      val[ij2idx(i, j)] =
        + (ele_deg / (ele_deg - i - j)) * (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv2(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) * this->lp_.poly(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly(i, i, dp_0 / i) * (ele_deg / j) *
        this->lp_.poly_deriv1(j, j, dp_1 / j)

        - (ele_deg / (ele_deg - i - j)) * (ele_deg / i) *
        this->lp_.poly_deriv1(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                              ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly(j, j, dp_1 / j)

        + (ele_deg / i) * (ele_deg / j) *
        this->lp_.poly(this->ele_deg_ - i - j, this->ele_deg_ - i - j,
                       ele_deg * help / (ele_deg - i - j)) *
        this->lp_.poly_deriv1(i, i, dp_0 / i) *
        this->lp_.poly_deriv1(j, j, dp_1 / j);

    } // i

    val[ij2idx(this->ele_deg_ - j, j)] =
      + (ele_deg / (ele_deg - j)) * (ele_deg / j) *
      this->lp_.poly_deriv1(this->ele_deg_ - j, this->ele_deg_ - j,
                            dp_0 / (ele_deg - j)) *
      this->lp_.poly_deriv1(j, j, dp_1 / j);

  } // j

}

// provide indices of dofs (local) on sub-entity
template<class DT>
std::vector<int> ElementLagrange_Triangle<DT>::dof_indices_on_sub_entity(
  std::vector<int> entity_vert_idx,
  std::vector<int> sub_entity_vert_idx) const {

  assert(0 < this->ele_deg_ <= 2);

  assert(entity_vert_idx.size() == 3);
  assert(sub_entity_vert_idx.size() == 1 || 2);

  std::vector<std::vector<int>> entity_idx;
  std::vector<std::vector<int>> dof_idx;

  switch(sub_entity_vert_idx.size()) {
  case 1: // point
    entity_idx = {{0}, {1}, {2}};

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {{0}, {1}, {2}};
      break;

    case 2:
      dof_idx = {{0}, {2}, {5}};
      break;

    } // switch ele_deg_

    break; // case 1

  case 2: // line
    entity_idx = {{0, 1}, {1, 2}, {2, 0}};

    switch(this->ele_deg_) {
    case 1:
      dof_idx = {{0, 1}, {1, 2}, {2, 0}};
      break;

    case 2:
      dof_idx = {{0, 1, 2}, {2, 4, 5}, {5, 3, 0}};
      break;

    } // switch ele_deg_

    break; // case 2

  } // switch sub_entity_vert_idx.size()

  return this->identify_dof_idx_on_sub_entity(
           entity_vert_idx,
           sub_entity_vert_idx,
           entity_idx, dof_idx);
}

// template instantiation
template class ElementLagrange_Triangle<double>;
template class ElementLagrange_Triangle<float>;

}; // namespace FEM
}; // namespace ChenLiu

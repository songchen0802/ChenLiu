// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/ElementFactory.hh"
#include "fem/lagrange/ElementLagrange.hh"
#include "fem/lagrange/ElementLagrange_Line.hh"
#include "fem/lagrange/ElementLagrange_Triangle.hh"
#include "fem/lagrange/ElementLagrange_Quadrilateral.hh"
#include "fem/lagrange/ElementLagrange_Tetrahedron.hh"
#include "fem/lagrange/ElementLagrange_Hexahedron.hh"

namespace ChenLiu {
namespace FEM {

// create
template<class DT>
std::shared_ptr<Element<DT>> ElementFactory<DT>::create(FE_Type fe_type,
CellType cell_type) {

  switch(fe_type) {
  case FE_Type::Lagrange:
    switch(cell_type) {
    case CellType::Line: {
      return std::shared_ptr<Element<DT>>(new ElementLagrange_Line<DT>());
    } // Line
    case CellType::Triangle: {
      return std::shared_ptr<Element<DT>>(new ElementLagrange_Triangle<DT>());
    } // Triangle
    case CellType::Quadrilateral: {
      return std::shared_ptr<Element<DT>>(new ElementLagrange_Quadrilateral<DT>());
    } // Quadrilateral
    case CellType::Tetrahedron: {
      return std::shared_ptr<Element<DT>>(new ElementLagrange_Tetrahedron<DT>());
    } // Tetrahedron
    case CellType::Hexahedron: {
      return std::shared_ptr<Element<DT>>(new ElementLagrange_Hexahedron<DT>());
    } // Hexahedron
    default: {
      CL_Error("Cell Type doesn't exist!");
      exit(EXIT_FAILURE);
    }
    } // cell_type

    break; // FE_Type::Lagrange

  default:
    CL_Error("FE Type doesn't exist!");
    exit(EXIT_FAILURE);
  }

}

// create
template<class DT>
std::shared_ptr<Element<DT>> ElementFactory<DT>::create(FE_Type fe_type,
CellType cell_type, int ele_deg, int gdim) {

  std::shared_ptr<Element<DT>> ele = create(fe_type, cell_type);
  ele->init(ele_deg, gdim);

  return ele;
}

// create
template<class DT>
std::shared_ptr<Element<DT>> ElementFactory<DT>::create(std::string fe_type,
std::string cell_type) {

  std::shared_ptr<Element<DT>> ele = create(str2fe_type(fe_type),
                                     str2cell_type(cell_type));

  return ele;
}

// create
template<class DT>
std::shared_ptr<Element<DT>> ElementFactory<DT>::create(std::string fe_type,
std::string cell_type, int ele_deg, int gdim) {

  std::shared_ptr<Element<DT>> ele = create(str2fe_type(fe_type),
                                     str2cell_type(cell_type));
  ele->init(ele_deg, gdim);

  return ele;
}

// template instantiation
template class ElementFactory<double>;
template class ElementFactory<float>;

}; // namespace FEM
}; // namespace ChenLiu

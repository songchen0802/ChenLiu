// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/ReferenceCell_Hexahedron.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ReferenceCell_Hexahedron<DT>::ReferenceCell_Hexahedron() {
  // do nothing
  this->tdim_ = 3;
  this->cell_type_ = CellType::Hexahedron;
  this->init_ref_vert_coords();
}

// destructor
template<class DT>
ReferenceCell_Hexahedron<DT>::~ReferenceCell_Hexahedron() {
  this->clear();
}

// map a point on reference cell to physical cell
template<class DT>
DT ReferenceCell_Hexahedron<DT>::map2phy(const int d,
    const Coordinate coord_ref,
    std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  switch(d) {
  case 0:
  {
    const DT val = + cell_verts[0][0] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1]) * 
                   (1.0 - coord_ref[2])
                   + cell_verts[1][0] * coord_ref[0] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[2][0] * coord_ref[0]* coord_ref[1] * (1.0 - coord_ref[2])
                   + cell_verts[3][0] * (1.0 - coord_ref[0]) * coord_ref[1] * (1.0 - coord_ref[2])
                   + cell_verts[4][0] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[5][0] * coord_ref[0] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[6][0] * coord_ref[0] * coord_ref[1] * coord_ref[2]
                   + cell_verts[7][0] * (1.0 - coord_ref[0]) * coord_ref[1] * coord_ref[2];

    return val;
  } // x

  case 1:
  {
    const DT val = + cell_verts[0][1] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1]) * 
                   (1.0 - coord_ref[2])
                   + cell_verts[1][1] * coord_ref[0] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[2][1] * coord_ref[0] * coord_ref[1] * (1.0 - coord_ref[2])
                   + cell_verts[3][1] * (1.0 - coord_ref[0]) * coord_ref[1] * (1.0 - coord_ref[2])
                   + cell_verts[4][1] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[5][1] * coord_ref[0] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[6][1] * coord_ref[0] * coord_ref[1] * coord_ref[2]
                   + cell_verts[7][1] * (1. - coord_ref[0]) * coord_ref[1] * coord_ref[2];

    return val;
  } // y

  case 2:
  {
    const DT val = + cell_verts[0][2] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1]) * 
                   (1.0 - coord_ref[2])
                   + cell_verts[1][2] * coord_ref[0] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[2][2] * coord_ref[0] * coord_ref[1] * (1.0 - coord_ref[2])
                   + cell_verts[3][2] * (1.0 - coord_ref[0]) * coord_ref[1] * (1.0 - coord_ref[2])
                   + cell_verts[4][2] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[5][2] * coord_ref[0] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[6][2] * coord_ref[0] * coord_ref[1] * coord_ref[2]
                   + cell_verts[7][2] * (1.0 - coord_ref[0]) * coord_ref[1] * coord_ref[2];

    return val;
  } // z

  default:
  {
    return 0.0;
  } // default

  } // switch

}

template<class DT>
DT ReferenceCell_Hexahedron<DT>::map2phy_deriv1(const int d, const int d1,
    const Coordinate coord_ref,
    const std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 10 + (d1 + 1);

  switch(switch_val) {
  case 11:
  {
    const DT val = - cell_verts[0][0] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[1][0] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[2][0] * coord_ref[1] * (1.0 - coord_ref[2])
                   - cell_verts[3][0] * coord_ref[1] * (1.0 - coord_ref[2])
                   - cell_verts[4][0] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[5][0] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[6][0] * coord_ref[1] * coord_ref[2]
                   - cell_verts[7][0] * coord_ref[1] * coord_ref[2];

    return val;
  } // x_x

  case 12:
  {
    const DT val = - cell_verts[0][0] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[2])
                   - cell_verts[1][0] * coord_ref[0] * (1.0 - coord_ref[2])
                   + cell_verts[2][0] * coord_ref[0] * (1.0 - coord_ref[2])
                   + cell_verts[3][0] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[2])
                   - cell_verts[4][0] * (1.0 - coord_ref[0]) * coord_ref[2]
                   - cell_verts[5][0] * coord_ref[0] * coord_ref[2]
                   + cell_verts[6][0] * coord_ref[0] * coord_ref[2]
                   + cell_verts[7][0] * (1.0 - coord_ref[0]) * coord_ref[2];

    return val;
  } // x_y

  case 13:
  {
    const DT val = - cell_verts[0][0] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1])
                   - cell_verts[1][0] * coord_ref[0] * (1.0 - coord_ref[1])
                   - cell_verts[2][0] * coord_ref[0] * coord_ref[1]
                   - cell_verts[3][0] * (1.0 - coord_ref[0]) * coord_ref[1]
                   + cell_verts[4][0] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1])
                   + cell_verts[5][0] * coord_ref[0] * (1.0 - coord_ref[1])
                   + cell_verts[6][0] * coord_ref[0] * coord_ref[1]
                   + cell_verts[7][0] * (1.0 - coord_ref[0]) * coord_ref[1];

    return val;
  } // x_z

  case 21:
  {
    const DT val = - cell_verts[0][1] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[1][1] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[2][1] * coord_ref[1] * (1.0 - coord_ref[2])
                   - cell_verts[3][1] * coord_ref[1] * (1.0 - coord_ref[2])
                   - cell_verts[4][1] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[5][1] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[6][1] * coord_ref[1] * coord_ref[2]
                   - cell_verts[7][1] * coord_ref[1] * coord_ref[2];

    return val;
  } // y_x

  case 22:
  {
    const DT val = - cell_verts[0][1] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[2])
                   - cell_verts[1][1] * coord_ref[0] * (1.0 - coord_ref[2])
                   + cell_verts[2][1] * coord_ref[0] * (1.0 - coord_ref[2])
                   + cell_verts[3][1] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[2])
                   - cell_verts[4][1] * (1.0 - coord_ref[0]) * coord_ref[2]
                   - cell_verts[5][1] * coord_ref[0] * coord_ref[2]
                   + cell_verts[6][1] * coord_ref[0] * coord_ref[2]
                   + cell_verts[7][1] * (1.0 - coord_ref[0]) * coord_ref[2];

    return val;
  } // y_y

  case 23:
  {
    const DT val = - cell_verts[0][1] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1])
                   - cell_verts[1][1] * coord_ref[0] * (1.0 - coord_ref[1])
                   - cell_verts[2][1] * coord_ref[0] * coord_ref[1]
                   - cell_verts[3][1] * (1.0 - coord_ref[0]) * coord_ref[1]
                   + cell_verts[4][1] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1])
                   + cell_verts[5][1] * coord_ref[0] * (1.0 - coord_ref[1])
                   + cell_verts[6][1] * coord_ref[0] * coord_ref[1]
                   + cell_verts[7][1] * (1.0 - coord_ref[0]) * coord_ref[1];

    return val;
  } // y_z

  case 31:
  {
    const DT val = - cell_verts[0][2] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[1][2] * (1.0 - coord_ref[1]) * (1.0 - coord_ref[2])
                   + cell_verts[2][2] * coord_ref[1] * (1.0 - coord_ref[2])
                   - cell_verts[3][2] * coord_ref[1] * (1.0 - coord_ref[2])
                   - cell_verts[4][2] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[5][2] * (1.0 - coord_ref[1]) * coord_ref[2]
                   + cell_verts[6][2] * coord_ref[1] * coord_ref[2]
                   - cell_verts[7][2] * coord_ref[1] * coord_ref[2];

    return val;
  } // z_x

  case 32:
  {
    const DT val = - cell_verts[0][2] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[2])
                   - cell_verts[1][2] * coord_ref[0] * (1.0 - coord_ref[2])
                   + cell_verts[2][2] * coord_ref[0] * (1.0 - coord_ref[2])
                   + cell_verts[3][2] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[2])
                   - cell_verts[4][2] * (1.0 - coord_ref[0]) * coord_ref[2]
                   - cell_verts[5][2] * coord_ref[0] * coord_ref[2]
                   + cell_verts[6][2] * coord_ref[0] * coord_ref[2]
                   + cell_verts[7][2] * (1.0 - coord_ref[0]) * coord_ref[2];

    return val;
  } // z_y

  case 33:
  {
    const DT val = - cell_verts[0][2] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1])
                   - cell_verts[1][2] * coord_ref[0] * (1.0 - coord_ref[1])
                   - cell_verts[2][2] * coord_ref[0] * coord_ref[1]
                   - cell_verts[3][2] * (1.0 - coord_ref[0]) * coord_ref[1]
                   + cell_verts[4][2] * (1.0 - coord_ref[0]) * (1.0 - coord_ref[1])
                   + cell_verts[5][2] * coord_ref[0] * (1.0 - coord_ref[1])
                   + cell_verts[6][2] * coord_ref[0] * coord_ref[1]
                   + cell_verts[7][2] * (1.0 - coord_ref[0]) * coord_ref[1];

    return val;
  } // z_z

  default:
  {
    return 0.0;
  } // default

  } // switch

}

template<class DT>
DT ReferenceCell_Hexahedron<DT>::map2phy_deriv2(
  const int d, const int d1, const int d2,
  const Coordinate coord_ref,
  const std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(0 <= d2 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 100 + (d1 + 1) * 10 + (d2 + 1);

  switch(switch_val) {

  case 112:
  {
    const DT val = + cell_verts[0][0] * (1.0 - coord_ref[2])
                   - cell_verts[1][0] * (1.0 - coord_ref[2])
                   + cell_verts[2][0] * (1.0 - coord_ref[2])
                   - cell_verts[3][0] * (1.0 - coord_ref[2])
                   + cell_verts[4][0] * coord_ref[2]
                   - cell_verts[5][0] * coord_ref[2]
                   + cell_verts[6][0] * coord_ref[2]
                   - cell_verts[7][0] * coord_ref[2];

    return val;
  } // x_xy

  case 113:
  {
    const DT val = + cell_verts[0][0] * (1.0 - coord_ref[1])
                   - cell_verts[1][0] * (1.0 - coord_ref[1])
                   - cell_verts[2][0] * coord_ref[1]
                   + cell_verts[3][0] * coord_ref[1]
                   - cell_verts[4][0] * (1.0 - coord_ref[1])
                   + cell_verts[5][0] * (1.0 - coord_ref[1])
                   + cell_verts[6][0] * coord_ref[1]
                   - cell_verts[7][0] * coord_ref[1];

    return val;
  } // x_xz

  case 123:
  {
    const DT val = + cell_verts[0][0] * (1.0 - coord_ref[0])
                   + cell_verts[1][0] * coord_ref[0]
                   - cell_verts[2][0] * coord_ref[0]
                   - cell_verts[3][0] * (1.0 - coord_ref[0])
                   - cell_verts[4][0] * (1.0 - coord_ref[0])
                   - cell_verts[5][0] * coord_ref[0]
                   + cell_verts[6][0] * coord_ref[0]
                   + cell_verts[7][0] * (1.0 - coord_ref[0]);

    return val;
  } // x_yz

  case 212:
  {
    const DT val = + cell_verts[0][1] * (1.0 - coord_ref[2])
                   - cell_verts[1][1] * (1.0 - coord_ref[2])
                   + cell_verts[2][1] * (1.0 - coord_ref[2])
                   - cell_verts[3][1] * (1.0 - coord_ref[2])
                   + cell_verts[4][1] * coord_ref[2]
                   - cell_verts[5][1] * coord_ref[2]
                   + cell_verts[6][1] * coord_ref[2]
                   - cell_verts[7][1] * coord_ref[2];

    return val;
  } // y_xy

  case 213:
  {
    const DT val = + cell_verts[0][1] * (1.0 - coord_ref[1])
                   - cell_verts[1][1] * (1.0 - coord_ref[1])
                   - cell_verts[2][1] * coord_ref[1]
                   + cell_verts[3][1] * coord_ref[1]
                   - cell_verts[4][1] * (1.0 - coord_ref[1])
                   + cell_verts[5][1] * (1.0 - coord_ref[1])
                   + cell_verts[6][1] * coord_ref[1]
                   - cell_verts[7][1] * coord_ref[1];

    return val;
  } // y_xz

  case 223:
  {
    const DT val = + cell_verts[0][1] * (1.0 - coord_ref[0])
                   + cell_verts[1][1] * coord_ref[0]
                   - cell_verts[2][1] * coord_ref[0]
                   - cell_verts[3][1] * (1.0 - coord_ref[0])
                   - cell_verts[4][1] * (1.0 - coord_ref[0])
                   - cell_verts[5][1] * coord_ref[0]
                   + cell_verts[6][1] * coord_ref[0]
                   + cell_verts[7][1] * (1.0 - coord_ref[0]);

    return val;
  } // y_yz

  case 312:
  {
    const DT val = + cell_verts[0][2] * (1.0 - coord_ref[2])
                   - cell_verts[1][2] * (1.0 - coord_ref[2])
                   + cell_verts[2][2] * (1.0 - coord_ref[2])
                   - cell_verts[3][2] * (1.0 - coord_ref[2])
                   + cell_verts[4][2] * coord_ref[2]
                   - cell_verts[5][2] * coord_ref[2]
                   + cell_verts[6][2] * coord_ref[2]
                   - cell_verts[7][2] * coord_ref[2];

    return val;
  } // z_xy

  case 313:
  {
    const DT val = + cell_verts[0][2] * (1.0 - coord_ref[1])
                   - cell_verts[1][2] * (1.0 - coord_ref[1])
                   - cell_verts[2][2] * coord_ref[1]
                   + cell_verts[3][2] * coord_ref[1]
                   - cell_verts[4][2] * (1.0 - coord_ref[1])
                   + cell_verts[5][2] * (1.0 - coord_ref[1])
                   + cell_verts[6][2] * coord_ref[1]
                   - cell_verts[7][2] * coord_ref[1];

    return val;
  } // z_xz

  case 323:
  {
    const DT val = + cell_verts[0][2] * (1.0 - coord_ref[0])
                   + cell_verts[1][2] * coord_ref[0]
                   - cell_verts[2][2] * coord_ref[0]
                   - cell_verts[3][2] * (1.0 - coord_ref[0])
                   - cell_verts[4][2] * (1.0 - coord_ref[0])
                   - cell_verts[5][2] * coord_ref[0]
                   + cell_verts[6][2] * coord_ref[0]
                   + cell_verts[7][2] * (1.0 - coord_ref[0]);

    return val;
  } // z_yz

  default:
  {
    return 0.0;
  } // default

  } // switch

}

// map a point on physical cell to reference cell
template<class DT>
void ReferenceCell_Hexahedron<DT>::map2ref(const Coordinate coord_phy,
    const std::vector<Coordinate> cell_verts,
    Coordinate& coord) const {

  // TODO: check coord_phy on physical cell
  assert(coord_phy.dim() >= this->tdim_);
  assert(coord.dim() == coord_phy.dim());
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const bool check = map2ref_newton(coord_phy, cell_verts, coord);

  if (check == false) {
    CL_Error("Newton method not converged!");
    exit(EXIT_FAILURE);
  }

}

//// map a point on physical cell to reference cell by using analytical definition
//template<class DT>
//bool ReferenceCell_Hexahedron<DT>::map_to_ref_analytic(
//  const Coordinate coord_phy,
//  const std::vector<Coordinate> cell_verts, Coordinate& coord) const {
//
//  assert(coord_phy.dim() >= this->tdim_);
//  assert(coord.dim() == coord_phy.dim());
//  assert(cell_verts.size() == this->ref_vert_coords_.size());
//
//  //const DT A =
//
//}

// mapping by Newton method
template<class DT>
bool ReferenceCell_Hexahedron<DT>::map2ref_newton(
  const Coordinate coord_phy,
  const std::vector<Coordinate> cell_verts,
  Coordinate& coord) const {

  assert(coord_phy.dim() >= this->tdim_);
  assert(coord.dim() == coord_phy.dim());
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  // maximum iteration
  const int iter_max = 1000;

  // initial guess on (0.5, 0.5)
  for (int i = 0; i != coord.dim(); ++i) {
    coord[i] = 0.5;
  }

  const int size = 3;

  // tolerance
  const DT tol_eps = std::numeric_limits<DT>::epsilon() * 1.0e3;

  // Jacobian matrix
  ChenLiu::Mat<DT> J(size, size);

  // inverse of Jacobian matrix
  ChenLiu::Mat<DT> inv_J(size, size);

  // residual vector
  ChenLiu::Vec<DT> r(size);

  // delta x
  ChenLiu::Vec<DT> delta_x(size);

  // r_backtracking
  ChenLiu::Vec<DT> r_b(size);

  int iter = 0;
  while (iter < iter_max) {
    // residual
    r(0) = map2phy(0, coord, cell_verts) - coord_phy[0];
    r(1) = map2phy(1, coord, cell_verts) - coord_phy[1];
    r(2) = map2phy(2, coord, cell_verts) - coord_phy[2];

    if (ChenLiu::Norm(r) < tol_eps) {
      return true;
    }

    // Jacobian
    J(0,0) = map2phy_deriv1(0, 0, coord, cell_verts);
    J(0,1) = map2phy_deriv1(0, 1, coord, cell_verts);
    J(0,2) = map2phy_deriv1(0, 2, coord, cell_verts);
    J(1,0) = map2phy_deriv1(1, 0, coord, cell_verts);
    J(1,1) = map2phy_deriv1(1, 1, coord, cell_verts);
    J(1,2) = map2phy_deriv1(1, 2, coord, cell_verts);
    J(2,0) = map2phy_deriv1(2, 0, coord, cell_verts);
    J(2,1) = map2phy_deriv1(2, 1, coord, cell_verts);
    J(2,2) = map2phy_deriv1(2, 2, coord, cell_verts);

    // inverse of Det Jacobian
    inv_J = Inverse(J);

    // compute delta_x
    delta_x = MatVecMult(inv_J, r);

    // backtracking
    DT alpha = 1.0;
    const DT beta = 0.5;
    const int b_iter = 10;

    r_b = r;
    while(ChenLiu::Norm(r_b) >= ChenLiu::Norm(r) && alpha > pow(beta, b_iter)) {

      Coordinate coord_tmp(size);
      coord_tmp[0] = coord[0] - alpha * delta_x(0);
      coord_tmp[1] = coord[1] - alpha * delta_x(1);
      coord_tmp[2] = coord[2] - alpha * delta_x(2);

      r_b(0) = map2phy(0, coord_tmp, cell_verts) - coord_phy[0];
      r_b(0) = map2phy(1, coord_tmp, cell_verts) - coord_phy[1];
      r_b(1) = map2phy(2, coord_tmp, cell_verts) - coord_phy[2];

      alpha *= beta;
    }

    // update coord
    coord[0] -= alpha * delta_x(0);
    coord[1] -= alpha * delta_x(1);
    coord[2] -= alpha * delta_x(2);

    // increase iter
    ++ iter;
  }

  return false;

}

// initialiaze coordinate of vertices on reference cell
template<class DT>
void ReferenceCell_Hexahedron<DT>::init_ref_vert_coords() {
  this->ref_vert_coords_.resize(8);

  // initialize size of coord
  Coordinate coord(this->tdim_);

  // 0
  coord[0] = 0.0;
  coord[1] = 0.0;
  coord[2] = 0.0;
  this->ref_vert_coords_[0] = coord;

  // 1
  coord[0] = 1.0;
  coord[1] = 0.0;
  coord[2] = 0.0;
  this->ref_vert_coords_[1] = coord;

  // 2
  coord[0] = 1.0;
  coord[1] = 1.0;
  coord[2] = 0.0;
  this->ref_vert_coords_[2] = coord;

  // 3
  coord[0] = 0.0;
  coord[1] = 1.0;
  coord[2] = 0.0;
  this->ref_vert_coords_[3] = coord;

  // 4
  coord[0] = 0.0;
  coord[1] = 0.0;
  coord[2] = 1.0;
  this->ref_vert_coords_[4] = coord;

  // 5
  coord[0] = 1.0;
  coord[1] = 0.0;
  coord[2] = 1.0;
  this->ref_vert_coords_[5] = coord;

  // 6
  coord[0] = 1.0;
  coord[1] = 1.0;
  coord[2] = 1.0;
  this->ref_vert_coords_[6] = coord;

  // 7
  coord[0] = 0.0;
  coord[1] = 1.0;
  coord[2] = 1.0;
  this->ref_vert_coords_[7] = coord;
}

// check the coordinate is on reference cell
template<class DT>
bool ReferenceCell_Hexahedron<DT>::is_on_reference_cell(
  const Coordinate coord_ref) const {

  assert(coord_ref.dim() >= this->tdim_);
  assert((0.0 - COMP_TOL) <= coord_ref[0] <= (1.0 + COMP_TOL));
  assert((0.0 - COMP_TOL) <= coord_ref[1] <= (1.0 + COMP_TOL));
  assert((0.0 - COMP_TOL) <= coord_ref[2] <= (1.0 + COMP_TOL));

  return ((0.0 - COMP_TOL) <= coord_ref[0] <= (1.0 + COMP_TOL)) &&
         ((0.0 - COMP_TOL) <= coord_ref[1] <= (1.0 + COMP_TOL)) &&
         ((0.0 - COMP_TOL) <= coord_ref[2] <= (1.0 + COMP_TOL));
}

// template instantiation
template class ReferenceCell_Hexahedron<double>;
template class ReferenceCell_Hexahedron<float>;

}; // namespace FEM
}; // namespace ChenLiu

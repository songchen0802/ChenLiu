// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/ReferenceCell_Triangle.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ReferenceCell_Triangle<DT>::ReferenceCell_Triangle() {
  // do nothing
  this->tdim_ = 2;
  this->cell_type_ = CellType::Triangle;
  this->init_ref_vert_coords();
}

// destructor
template<class DT>
ReferenceCell_Triangle<DT>::~ReferenceCell_Triangle() {
  this->clear();
}

// map a point on reference cell to physical cell
template<class DT>
DT ReferenceCell_Triangle<DT>::map2phy(const int d, const Coordinate coord_ref,
                                       std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  switch(d) {
  case 0:
  {
    const DT val = + (cell_verts[1][0] - cell_verts[0][0]) * coord_ref[0]
                   + (cell_verts[2][0] - cell_verts[0][0]) * coord_ref[1]
                   + cell_verts[0][0];

    return val;
  } // x

  case 1:
  {
    const DT val = + (cell_verts[1][1] - cell_verts[0][1]) * coord_ref[0]
                   + (cell_verts[2][1] - cell_verts[0][1]) * coord_ref[1]
                   + cell_verts[0][1];

    return val;
  } // y

  default:
  {
    return 0.0;
  } // default

  } // switch

}

template<class DT>
DT ReferenceCell_Triangle<DT>::map2phy_deriv1(const int d, const int d1,
    const Coordinate coord_ref,
    const std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 10 + (d1 + 1);

  switch(switch_val) {
  case 11:
  {
    const DT val = (cell_verts[1][0] - cell_verts[0][0]);

    return val;
  } // x_x

  case 12:
  {
    const DT val = (cell_verts[2][0] - cell_verts[0][0]);

    return val;
  } // x_y

  case 22:
  {
    const DT val = (cell_verts[2][1] - cell_verts[0][1]);

    return val;
  } // y_y

  case 21:
  {
    const DT val = (cell_verts[1][1] - cell_verts[0][1]);

    return val;
  } // y_x

  default:
  {
    return 0.0;
  }
  }

}

template<class DT>
DT ReferenceCell_Triangle<DT>::map2phy_deriv2(
  const int d, const int d1, const int d2,
  const Coordinate coord_ref,
  const std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(0 <= d2 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 100 + (d1 + 1) * 10 + (d2 + 1);

  switch(switch_val) {

  default:
  {
    return 0.0;
  } // default

  } // switch

}

// map a point on physical cell to reference cell
template<class DT>
void ReferenceCell_Triangle<DT>::map2ref(const Coordinate coord_phy,
    const std::vector<Coordinate> cell_verts, Coordinate& coord) const {

  // TODO: check coord_phy on physical cell
  assert(coord_phy.dim() >= this->tdim_);
  assert(coord.dim() == coord_phy.dim());
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  for (int i = 0; i != coord.dim(); ++i) {
    coord[i] = 0.0;
  }

  DT a11 = cell_verts[1][0] - cell_verts[0][0];
  DT a12 = cell_verts[2][0] - cell_verts[0][0];
  DT a21 = cell_verts[1][1] - cell_verts[0][1];
  DT a22 = cell_verts[2][1] - cell_verts[0][1];

  DT det = a11 * a22 - a21 * a12;

  assert(det != 0.0);

  coord[0] = (1.0 / det) * (a22 * (coord_phy[0] - cell_verts[0][0]) -
                            a12 * (coord_phy[1] - cell_verts[0][1]));
  coord[1] = (1.0 / det) * (-a21 * (coord_phy[0] - cell_verts[0][0]) +
                            a11 * (coord_phy[1] - cell_verts[0][1]));

}

// initialiaze coordinate of vertices on reference cell
template<class DT>
void ReferenceCell_Triangle<DT>::init_ref_vert_coords() {
  this->ref_vert_coords_.resize(3);

  // initialize size of coord
  Coordinate coord(this->tdim_);

  // 0
  coord[0] = 0.0;
  coord[1] = 0.0;
  this->ref_vert_coords_[0] = coord;

  // 1
  coord[0] = 1.0;
  coord[1] = 0.0;
  this->ref_vert_coords_[1] = coord;

  // 2
  coord[0] = 0.0;
  coord[1] = 1.0;
  this->ref_vert_coords_[2] = coord;
}

// check the coordinate is on reference cell
template<class DT>
bool ReferenceCell_Triangle<DT>::is_on_reference_cell(
  const Coordinate coord_ref) const {

  assert(coord_ref.dim() >= this->tdim_);
  assert((0.0 - COMP_TOL) <= coord_ref[0] <= (1.0 + COMP_TOL));
  assert((0.0 - COMP_TOL) <= coord_ref[1] <= (1.0 + COMP_TOL));

  return ((0.0 - COMP_TOL) <= coord_ref[1])
         && (coord_ref[1] <= (1.0 - coord_ref[0] + COMP_TOL));
}

// template instantiation
template class ReferenceCell_Triangle<double>;
template class ReferenceCell_Triangle<float>;

}; // namespace FEM
}; // namespace ChenLiu

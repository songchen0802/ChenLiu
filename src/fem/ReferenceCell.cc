// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/ReferenceCell.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ReferenceCell<DT>::ReferenceCell() {
  // do nothing
  tdim_ = -1;
  ref_vert_coords_.clear();
}

// destructor
template<class DT>
ReferenceCell<DT>::~ReferenceCell() {
  clear();
}

// clear
template<class DT>
void ReferenceCell<DT>::clear() {
  tdim_ = -1;
  ref_vert_coords_.clear();
  ref_vert_coords_.shrink_to_fit();
}

// cell type
template<class DT>
CellType ReferenceCell<DT>::cell_type() const {
  return cell_type_;
}

//topological dimension
template<class DT>
int ReferenceCell<DT>::tdim() const {
  return tdim_;
}

// template instantiation
template class ReferenceCell<double>;
template class ReferenceCell<float>;

}; // namespace FEM
}; // namespace ChenLiu

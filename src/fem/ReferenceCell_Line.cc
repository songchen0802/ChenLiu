// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "fem/ReferenceCell_Line.hh"

namespace ChenLiu {
namespace FEM {

// constructor
template<class DT>
ReferenceCell_Line<DT>::ReferenceCell_Line() {
  // do nothing
  this->tdim_ = 1;
  this->cell_type_ = CellType::Line;
  this->init_ref_vert_coords();
}

// destructor
template<class DT>
ReferenceCell_Line<DT>::~ReferenceCell_Line() {
  this->clear();
}

// map a point on reference cell to physical cell
template<class DT>
DT ReferenceCell_Line<DT>::map2phy(const int d, const Coordinate coord_ref,
                                   const std::vector<Coordinate> cell_verts) const {
  assert(0 <= d <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  switch(d) {

  case 0:
  {
    DT val = coord_ref[0] * (cell_verts[1][0] - cell_verts[0][0])
             + cell_verts[0][0];

    return val;
  } // x

  case 1:
  {
    return 0.0;
  } // y

  case 2:
  {
    return 0.0;
  } // z

  default:
  {
    CL_Error("Newton method not converged!");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

template<class DT>
DT ReferenceCell_Line<DT>::map2phy_deriv1(const int d, const int d1,
    const Coordinate coord_ref,
    const std::vector<Coordinate> cell_verts) const {

  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 10 + (d1 + 1);

  switch(switch_val) {
  case 11: {

    DT val = cell_verts[1][0] - cell_verts[0][0];

    return val;
  } // x_x

  default:
  {
    return 0.0;
  } // default

  } // switch

}

template<class DT>
DT ReferenceCell_Line<DT>::map2phy_deriv2(const int d, const int d1,
    const int d2,
    const Coordinate coord_ref,
    const std::vector<Coordinate> cell_verts) const {

  assert(0 <= d <= 2);
  assert(0 <= d1 <= 2);
  assert(0 <= d2 <= 2);
  assert(is_on_reference_cell(coord_ref));
  assert(coord_ref.dim() >= this->tdim_);
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  const int switch_val = (d + 1) * 100 + (d1 + 1) * 10 + (d2 + 1);

  switch(switch_val) {

  default:
  {
    return 0.0;
  } // default

  } // switch

}

// map a point on physical cell to reference cell
template<class DT>
void ReferenceCell_Line<DT>::map2ref(const Coordinate coord_phy,
                                     const std::vector<Coordinate> cell_verts,
                                     Coordinate& coord) const {

  // TODO: check coord_phy on physical cell
  assert(coord_phy.dim() >= this->tdim_);
  assert(coord.dim() == coord_phy.dim());
  assert(cell_verts.size() == this->ref_vert_coords_.size());

  for (int i = 0; i != coord.dim(); ++i) {
    coord[i] = 0.0;
  }

  coord[0] = (coord_phy[0] - cell_verts[0][0]) /
             (cell_verts[1][0] - cell_verts[0][0]);
}

// initialiaze coordinate of vertices on reference cell
template<class DT>
void ReferenceCell_Line<DT>::init_ref_vert_coords() {
  this->ref_vert_coords_.resize(2);

  // initialize size of coord
  Coordinate coord(this->tdim_);

  // 0
  coord[0] = 0.0;
  this->ref_vert_coords_[0] = coord;

  // 1
  coord[0] = 1.0;
  this->ref_vert_coords_[1] = coord;
}

// check the coordinate is on reference cell
template<class DT>
bool ReferenceCell_Line<DT>::is_on_reference_cell(const Coordinate coord_ref)
const {
  assert(coord_ref.dim() >= this->tdim_);
  assert((0.0 - COMP_TOL) <= coord_ref[0] <= (1.0 + COMP_TOL));

  return (0.0 <= coord_ref[0]) && (coord_ref[0] <= 1.0);
}

// template instantiation
template class ReferenceCell_Line<double>;
template class ReferenceCell_Line<float>;

}; // namespace FEM
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "quadrature/QuadratureGauss_Tetrahedron.hh"

namespace ChenLiu {
namespace Quadrature {

// constructor
template<class DT>
QuadratureGauss_Tetrahedron<DT>::QuadratureGauss_Tetrahedron() {
}

// destructor
template<class DT>
QuadratureGauss_Tetrahedron<DT>::~QuadratureGauss_Tetrahedron() {
}

// initialize
template<class DT>
void QuadratureGauss_Tetrahedron<DT>::init(int order) {
  assert(0 < order <= 4);

  // assign cell type
  this->cell_type_ = CellType::Tetrahedron;

  // dimension
  this->dim_ = 3;

  // assign order of quadrature points
  this->order_ = order;

  // initialize member variables
  this->coords_.clear();
  this->weights_.clear();
  this->size_ = -1;

  switch (this->order_) {
  case 1:
  {
    set_points_weights_order1();
    break;
  } // order = 1

  case 2:
  {
    set_points_weights_order2();
    break;
  } // order = 2

  case 3:
  {
    set_points_weights_order3();
    break;
  } // order = 3

  case 4:
  {
    set_points_weights_order4();
    break;
  } // order = 4

  default:
  {
    CL_Error("order should be less than 5");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

// order 1
template<class DT>
void QuadratureGauss_Tetrahedron<DT>::set_points_weights_order1() {
  assert(this->order_ == 1);

  this->size_ = 1;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.25};
  std::vector<DT> y = {0.25};
  std::vector<DT> z = {0.25};

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
    this->coords_[i][2] = z[i];
  }

  // weights
  std::vector<DT> w = {0.166666666666667};

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 2
template<class DT>
void QuadratureGauss_Tetrahedron<DT>::set_points_weights_order2() {
  assert(this->order_ == 2);

  this->size_ = 4;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.13819660112501, 0.585410196624968,
                       0.13819660112501, 0.138196601125010
                      };

  std::vector<DT> y = {0.138196601125010, 0.138196601125010,
                       0.585410196624968, 0.138196601125010
                      };

  std::vector<DT> z = {0.13819660112501, 0.138196601125010,
                       0.13819660112501, 0.585410196624968
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
    this->coords_[i][2] = z[i];
  }

  // weights
  std::vector<DT> w = {0.0416666666666666, 0.0416666666666666,
                       0.0416666666666666, 0.0416666666666666
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 3
template<class DT>
void QuadratureGauss_Tetrahedron<DT>::set_points_weights_order3() {
  assert(this->order_ == 3);

  this->size_ = 5;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.25, 0.166666666666666, 0.166666666666666,
                       0.166666666666666, 0.5
                      };

  std::vector<DT> y = {0.25, 0.166666666666666, 0.166666666666666,
                       0.5, 0.166666666666666
                      };

  std::vector<DT> z = {0.25, 0.166666666666666, 0.5,
                       0.166666666666666, 0.166666666666666
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
    this->coords_[i][2] = z[i];
  }

  // weights
  std::vector<DT> w = {-0.133333333333333, 0.075, 0.075, 0.075, 0.075};

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 4
template<class DT>
void QuadratureGauss_Tetrahedron<DT>::set_points_weights_order4() {
  assert(this->order_ == 4);

  this->size_ = 11;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.25,
                       0.0714285714285715,
                       0.0714285714285715,
                       0.0714285714285715,
                       0.785714285714286,
                       0.399403576166799,
                       0.399403576166799,
                       0.100596423833201,
                       0.399403576166799,
                       0.100596423833201,
                       0.100596423833201
                      };

  std::vector<DT> y = {0.25,
                       0.0714285714285715,
                       0.0714285714285715,
                       0.785714285714286,
                       0.0714285714285715,
                       0.399403576166799,
                       0.100596423833201,
                       0.399403576166799,
                       0.100596423833201,
                       0.399403576166799,
                       0.100596423833201
                      };

  std::vector<DT> z = {0.25,
                       0.0714285714285715,
                       0.785714285714286,
                       0.0714285714285715,
                       0.0714285714285715,
                       0.100596423833201,
                       0.399403576166799,
                       0.399403576166799,
                       0.100596423833201,
                       0.100596423833201,
                       0.399403576166799
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
    this->coords_[i][2] = z[i];
  }

  // weights
  std::vector<DT> w = {-0.0131555555555555, 0.00762222222222225,
                       0.00762222222222225, 0.00762222222222225,
                       0.00762222222222225, 0.0248888888888889,
                       0.0248888888888889,  0.0248888888888889,
                       0.0248888888888889,  0.0248888888888889,
                       0.0248888888888889
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// template instantiation
template class QuadratureGauss_Tetrahedron<double>;
template class QuadratureGauss_Tetrahedron<float>;

}; // namespace FEM
}; // namespace ChenLiu

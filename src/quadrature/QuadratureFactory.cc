// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "quadrature/QuadratureFactory.hh"

namespace ChenLiu {
namespace Quadrature {

// constructor
template<class DT>
std::unique_ptr<Quadrature<DT>> QuadratureFactory<DT>::create(
QP_Type qp_type, CellType cell_type, int order) {

  // set member variables
  qp_type_ = qp_type;
  cell_type_ = cell_type;
  order_ = order;

  switch (qp_type) {

  case (QP_Type::Gauss):
  {
    return Gauss_quadrature(cell_type_, order);
    break;
  } // Gauss quadrature point

  default:
  {
    CL_Error("Quadrature point type does not exist!");
    exit(EXIT_FAILURE);
  } // default

  } // quadrature point type

}

// quadratrue point type
template<class DT>
QP_Type QuadratureFactory<DT>::qp_type() const {
  return qp_type_;
}

// cell type
template<class DT>
CellType QuadratureFactory<DT>::cell_type() const {
  return cell_type_;
}

// order
template<class DT>
int QuadratureFactory<DT>::order() const {
  return order_;
}

// Lagrange element case
template<class DT>
std::unique_ptr<Quadrature<DT>> QuadratureFactory<DT>::Gauss_quadrature(
CellType cell_type, int order) const {

  std::unique_ptr<Quadrature<DT>> q_ptr = nullptr;

  switch (cell_type) {
  case CellType::Line:
  {
    q_ptr = std::unique_ptr<Quadrature<DT>>(new QuadratureGauss_Line<DT>());
    break;
  } // Line

  case CellType::Triangle:
  {
    q_ptr = std::unique_ptr<Quadrature<DT>>(new QuadratureGauss_Triangle<DT>());
    break;
  } // Triangle

  case CellType::Quadrilateral:
  {
    q_ptr = std::unique_ptr<Quadrature<DT>>(new
                                            QuadratureGauss_Quadrilateral<DT>());
    break;
  } // Quadrilateral

  case CellType::Tetrahedron:
  {
    q_ptr = std::unique_ptr<Quadrature<DT>>(new QuadratureGauss_Tetrahedron<DT>());
    break;
  } // Tetrahedron

  case CellType::Hexahedron:
  {
    q_ptr = std::unique_ptr<Quadrature<DT>>(new QuadratureGauss_Hexahedron<DT>());
    break;
  } // Hexahedron

  default:
  {
    CL_Error("Wrong cell type!");
    exit(EXIT_FAILURE);
  } // default

  } // switch

  // initialization
  q_ptr->init(order);

  // return
  return q_ptr;
}

// template instantiation
template class QuadratureFactory<double>;
template class QuadratureFactory<float>;

}; // namespace FEM
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "quadrature/QuadratureGauss_Line.hh"

namespace ChenLiu {
namespace Quadrature {

// constructor
template<class DT>
QuadratureGauss_Line<DT>::QuadratureGauss_Line() {
}

// destructor
template<class DT>
QuadratureGauss_Line<DT>::~QuadratureGauss_Line() {
}

// initialize
template<class DT>
void QuadratureGauss_Line<DT>::init(int order) {
  assert(0 < order <= 4);

  // assign cell type
  this->cell_type_ = CellType::Line;

  // dimension
  this->dim_ = 1;

  // assign order of quadrature points
  this->order_ = order;

  // initialize member variables
  this->coords_.clear();
  this->weights_.clear();
  this->size_ = -1;

  switch (this->order_) {
  case 1:
  {
    set_points_weights_order1();
    break;
  } // order = 1

  case 2:
  {
    set_points_weights_order2();
    break;
  } // order = 2

  case 3:
  {
    set_points_weights_order3();
    break;
  } // order = 3

  case 4:
  {
    set_points_weights_order4();
    break;
  } // order = 4

  default:
  {
    CL_Error("order should be less than 5");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

// order 1
template<class DT>
void QuadratureGauss_Line<DT>::set_points_weights_order1() {
  assert(this->order_ == 1);

  this->size_ = 1;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.5};

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
  }

  // weights
  std::vector<DT> w = {1.0};

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 2
template<class DT>
void QuadratureGauss_Line<DT>::set_points_weights_order2() {
  assert(this->order_ == 2);

  this->size_ = 2;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.211324865405187117745425609748,
                       0.788675134594812882254574390252
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
  }

  // weights
  std::vector<DT> w = {0.5, 0.5};

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 3
template<class DT>
void QuadratureGauss_Line<DT>::set_points_weights_order3() {
  assert(this->order_ == 3);

  this->size_ = 3;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.112701665379258311482073460022,
                       0.5,
                       0.887298334620741688517926539978
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
  }

  // weights
  std::vector<DT> w = {0.277777777777777777777777777779,
                       0.444444444444444444444444444445,
                       0.277777777777777777777777777779
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 4
template<class DT>
void QuadratureGauss_Line<DT>::set_points_weights_order4() {
  assert(this->order_ == 4);

  this->size_ = 4;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.069431844202973712388026755553,
                       0.330009478207571867598667120448,
                       0.669990521792428132401332879552,
                       0.930568155797026287611973244447
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
  }

  // weights
  std::vector<DT> w = {0.173927422568726928686531974611,
                       0.326072577431273071313468025388,
                       0.32607257743127307131346802538,
                       0.173927422568726928686531974611
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// template instantiation
template class QuadratureGauss_Line<double>;
template class QuadratureGauss_Line<float>;

}; // namespace FEM
}; // namespace ChenLiu

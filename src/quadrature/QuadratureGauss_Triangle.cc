// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "quadrature/QuadratureGauss_Triangle.hh"

namespace ChenLiu {
namespace Quadrature {

// constructor
template<class DT>
QuadratureGauss_Triangle<DT>::QuadratureGauss_Triangle() {
}

// destructor
template<class DT>
QuadratureGauss_Triangle<DT>::~QuadratureGauss_Triangle() {
}

// initialize
template<class DT>
void QuadratureGauss_Triangle<DT>::init(int order) {
  assert(0 < order <= 4);

  // assign cell type
  this->cell_type_ = CellType::Triangle;

  // dimension
  this->dim_ = 2;

  // assign order of quadrature points
  this->order_ = order;

  // initialize member variables
  this->coords_.clear();
  this->weights_.clear();
  this->size_ = -1;

  switch (this->order_) {
  case 1:
  {
    set_points_weights_order1();
    break;
  } // order = 1

  case 2:
  {
    set_points_weights_order2();
    break;
  } // order = 2

  case 3:
  {
    set_points_weights_order3();
    break;
  } // order = 3

  case 4:
  {
    set_points_weights_order4();
    break;
  } // order = 4

  default:
  {
    CL_Error("order should be less than 5");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

// order 1
template<class DT>
void QuadratureGauss_Triangle<DT>::set_points_weights_order1() {
  assert(this->order_ == 1);

  this->size_ = 1;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.33333333333333};
  std::vector<DT> y = {0.33333333333333};

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
  }

  // weights
  std::vector<DT> w = {0.5};

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 2
template<class DT>
void QuadratureGauss_Triangle<DT>::set_points_weights_order2() {
  assert(this->order_ == 2);

  this->size_ = 3;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.166666666666666, 0.166666666666666, 0.666666666666666};

  std::vector<DT> y = {0.166666666666666, 0.666666666666666, 0.166666666666666};

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
  }

  // weights
  std::vector<DT> w = {0.166666666666667, 0.166666666666667, 0.166666666666667};

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 3
template<class DT>
void QuadratureGauss_Triangle<DT>::set_points_weights_order3() {
  assert(this->order_ == 3);

  this->size_ = 4;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.333333333333334, 0.2, 0.2, 0.6};

  std::vector<DT> y = {0.333333333333334, 0.2, 0.6, 0.2};

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
  }

  // weights
  std::vector<DT> w = {-0.28125, 0.260416666666667,
                       0.260416666666667, 0.260416666666667
                      };

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// order 4
template<class DT>
void QuadratureGauss_Triangle<DT>::set_points_weights_order4() {
  assert(this->order_ == 4);

  this->size_ = 6;
  this->coords_.resize(this->size_);
  this->weights_.resize(this->size_);

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i].init(this->dim_);
  }

  // coords
  std::vector<DT> x = {0.445948490915965, 0.445948490915965, 0.10810301816807,
                       0.091576213509771, 0.091576213509771, 0.816847572980459
                      };

  std::vector<DT> y = {0.445948490915965, 0.10810301816807,  0.445948490915965,
                       0.091576213509771, 0.816847572980459, 0.091576213509771};

  for (int i = 0; i != this->size_; ++i) {
    this->coords_[i][0] = x[i];
    this->coords_[i][1] = y[i];
  }

  // weights
  std::vector<DT> w = {0.111690794839005, 0.111690794839005, 0.111690794839005,
                       0.054975871827661, 0.054975871827661, 0.054975871827661};

  for (int i = 0; i != this->size_; ++i) {
    this->weights_[i] = w[i];
  }

}

// template instantiation
template class QuadratureGauss_Triangle<double>;
template class QuadratureGauss_Triangle<float>;

}; // namespace FEM
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "quadrature/Quadrature.hh"

namespace ChenLiu {
namespace Quadrature {

// constructor
template<class DT>
Quadrature<DT>::Quadrature() {
  // do nothing
}

// destructor
template<class DT>
Quadrature<DT>::~Quadrature() {
  coords_.clear();
  weights_.clear();
  dim_ = -1;
  size_ = -1;
}

// cell type
template<class DT>
CellType Quadrature<DT>::cell_type() const {
  return cell_type_;
}

// size of quadrature points
template<class DT>
int Quadrature<DT>::size() const {
  assert(size_ > 0);

  return size_;
}

// dimension of quadrature point
template<class DT>
int Quadrature<DT>::dim() const {
  assert(0 < dim_ <= 3);

  return dim_;
}

// return quadrature points
template<class DT>
std::vector<Coordinate> Quadrature<DT>::p() const {
  assert(coords_.size() == weights_.size());
  assert(coords_.size() == size_);

  return coords_;
}

template<class DT>
Coordinate Quadrature<DT>::p(int idx) const {
  assert(coords_.size() == weights_.size());
  assert(coords_.size() == size_);

  return coords_[idx];
}

// return weight of quadrature points
template<class DT>
std::vector<DT> Quadrature<DT>::w() const {
  assert(coords_.size() == weights_.size());
  assert(coords_.size() == size_);

  return weights_;
}

template<class DT>
DT Quadrature<DT>::w(int idx) const {
  assert(coords_.size() == weights_.size());
  assert(coords_.size() == size_);

  return weights_[idx];
}

// template instantiation
template class Quadrature<double>;
template class Quadrature<float>;

}; // namespace FEM
}; // namespace ChenLiu

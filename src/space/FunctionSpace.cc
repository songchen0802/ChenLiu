// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "space/FunctionSpace.hh"

namespace ChenLiu {
namespace Space {

// constructor
template<class DT>
FunctionSpace<DT>::FunctionSpace() {
  mesh_ptr_ = nullptr;
  comm_ = MPI_COMM_WORLD;
  ele_family_ = {Element_Family::CG};
  ele_deg_ = {-1};
  global_dof_offsets_ = {0};
}

template<class DT>
FunctionSpace<DT>::FunctionSpace(const std::shared_ptr<const Mesh::Mesh>
                                 mesh_ptr,
                                 MPI_Comm comm,
                                 std::vector<Element_Family> element_family,
                                 std::vector<int> degree) :
  mesh_ptr_(mesh_ptr), comm_(comm),
  ele_family_(element_family), ele_deg_(degree) {

  assert(element_family.size() == degree.size());

  // resize dof_
  dof_.resize(degree.size());

  for (int i = 0; i != degree.size(); ++i) {
    bool found = false;

    for (int j = 0; j != i; ++j) {
      if ((degree[i] == degree[j]) && (ele_family_[i] == ele_family_[j])) {
        // TODO: thinking of changing to pointer to save memory
        dof_[i] = dof_[j];

        found = true;
        break;
      }
    }

    if (found == true) {
      break;
    }

    // create dofs
    dof_[i].create(mesh_ptr_, comm, ele_family_[i], ele_deg_[i]);
  }

  // compute global dof offests
  global_dof_offsets_.clear();
  global_dof_offsets_.push_back(0);
  for (int i = 0; i != degree.size(); ++i) {
    global_dof_offsets_.push_back(global_dof_offsets_.back() +
                                  dof_[i].total_dof_size());
  }

}

// destructor
template<class DT>
FunctionSpace<DT>::~FunctionSpace() {
  clear();
}

// clear
template<class DT>
void FunctionSpace<DT>::clear() {
  mesh_ptr_ = nullptr;
  ele_family_.clear();
  ele_deg_.clear();
  global_dof_offsets_.clear();
}

// MPI Comm
template<class DT>
MPI_Comm FunctionSpace<DT>::mpi_comm() const {
  return comm_;
}

// mesh pointer
template<class DT>
const std::shared_ptr<const Mesh::Mesh> FunctionSpace<DT>::mesh_ptr() const {
  return mesh_ptr_;
}

// dofs for cells
template<class DT>
std::map<int, Cell_dofs> FunctionSpace<DT>::cell_dofs(int idx) const {
  return dof_[idx].cell_dofs();
}

// dofs for facets
template<class DT>
std::map<int, Facet_dofs> FunctionSpace<DT>::facet_dofs(int idx) const {
  return dof_[idx].facet_dofs();
}


// number of variables
template<class DT>
int FunctionSpace<DT>::n_var() const {
  assert(ele_family_.size() == ele_deg_.size());

  return ele_family_.size();
}

// global dof offsets
template<class DT>
std::vector<int> FunctionSpace<DT>::global_dof_offsets() const {
  return global_dof_offsets_;
}

// instantiation
template class FunctionSpace<double>;
template class FunctionSpace<float>;

}; // namespace Space
}; // namespace ChenLiu

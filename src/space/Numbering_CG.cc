// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "space/Numbering_CG.hh"

namespace ChenLiu {
namespace Space {

// constructor
template<class DT>
Numbering_CG<DT>::Numbering_CG() {
  // do nothing
}

// destructor
template<class DT>
Numbering_CG<DT>::~Numbering_CG() {
  this->clear();
}

// number
template<class DT>
void Numbering_CG<DT>::number(std::map<int, Cell_dofs>& cell_dofs,
                              std::map<int, Facet_dofs>& facet_dofs) {

  // collect coordinates for all possible dofs
  this->collect_dofs_coordinates();

  // initialize dof ID
  this->initial_numbering(cell_dofs, facet_dofs);

  // reduce dof ID that have some coordinates
  this->renumber_common_dofs(cell_dofs, facet_dofs);

  // update local dof IDs regarding to other processors
  this->update_local_dofs_global(cell_dofs, facet_dofs);

  // update ghost dof IDs regarding to other processors
  this->update_ghost_dofs_global(cell_dofs, facet_dofs);

}

// renumber based on common dofs
template<class DT>
void Numbering_CG<DT>::renumber_common_dofs(std::map<int, Cell_dofs>& cell_dofs,
    std::map<int, Facet_dofs>& facet_dofs) {

  // tdim
  const int tdim = this->mesh_ptr_->tdim();

  // mpi rank
  int rank = -1;
  MPI_Comm_rank(this->comm_, &rank);

  //// 1. reduce dof ID to lowerst ID on shared vertices
  std::map<int, int> identification;

  // loop over facets
  for (Mesh::EntityIterator facet_it(this->mesh_ptr_, tdim-1);
       !facet_it.is_end(); ++facet_it) {

    const int loc_idx = facet_it->local_index();
    assert(facet_dofs[loc_idx].dofs.size() == 1 | 2);

    if (facet_dofs[loc_idx].dofs.size() < 2) {
      continue;
    } else {
      assert(facet_dofs[loc_idx].dofs[0].size() ==
             facet_dofs[loc_idx].dofs[1].size());

      for (int i = 0; i != facet_dofs[loc_idx].dofs[0].size(); ++i) {
        const int a = facet_dofs[loc_idx].dofs[0][i];
        const int b = facet_dofs[loc_idx].dofs[1][i];

        // within ghost cells
        if (a == -1 && b == -1) {
          continue;
        }

        if (a == -1 || b == -1) {

          // sub domain ids
          std::vector<int> d_ids;

          // between ghost and local
          for (Mesh::EntityIncidentIterator cell_it(*facet_it, tdim);
               !cell_it.is_end(); ++cell_it) {

            d_ids.push_back(cell_it->sub_domain_id());

          } // for cell_it

          if (std::all_of(d_ids.begin(), d_ids.end(), [rank](int r) {
          return r >= rank;
        })) {
            continue;
          }

        }

        // within lcal cells
        assert(a != b);
        if (a > b) {
          identification[a] = b;
        } else {
          identification[b] = a;
        }

      } // i

    } // else

  } // facet_it

  // reduce to lowerst ID
  for (auto pair : identification) {
    int second = pair.second;
    while(identification.contains(second)) {
      second = identification[second];
    }
    identification[pair.first] = second;
  }

  //// 2. create map (old dof -> new dof) for first reduction

  // collect the maximum dof value from cell_dofs
  std::map<int, int> current_numbering;
  for (auto cell : cell_dofs) {
    for (int val : cell.second.dofs) {
      current_numbering.try_emplace(val, val);
    }
  }

  for (auto pair : identification) {
    current_numbering[pair.first] = pair.second;
  }

  // dof map (old -> new)
  std::map<int, int> dof_reduction;

  //// 3. compute maping
  int curr_dof = 0;
  if (current_numbering.contains(-1)) {
    curr_dof = -1;
  }

  for (auto pair : current_numbering) {

    if (pair.first == pair.second) {
      dof_reduction[pair.second] = curr_dof;
      ++curr_dof;
    } else {

      if (dof_reduction.contains(pair.second)) {
        continue;
      } else {
        dof_reduction[pair.second] = curr_dof;
        ++curr_dof;
      }

    } // else

  } // pair

  //// 4. update dof ID in cell_dofs and facet_dofs

  // cell
  for (auto& cell : cell_dofs) {
    for (int& a : cell.second.dofs) {

      if (identification.contains(a)) {
        a = dof_reduction[identification[a]];
      } else {
        a = dof_reduction[a];
      } // else

    }
  } // for cell

  // facet
  for (auto& facet : facet_dofs) {
    for (auto& a : facet.second.dofs) {
      for (auto& b : a) {

        if (identification.contains(b)) {
          b = dof_reduction[identification[b]];
        } else {
          b = dof_reduction[b];
        } // else

      }

    }
  } // for facet

}

// update dofs on ghost cells
template<class DT>
void Numbering_CG<DT>::update_ghost_dofs_global(
  std::map<int, Cell_dofs>& cell_dofs, std::map<int, Facet_dofs>& facet_dofs) {

  // collect
  this->exchange_cell_dof_info(cell_dofs, facet_dofs);

  // tdim
  const int tdim = this->mesh_ptr_->tdim();

  // mpi rank
  int rank = -1;
  MPI_Comm_rank(this->comm_, &rank);

  // update/correct facet_dofs
  // start with two adjacent cells
  for (Mesh::EntityIterator facet_it(this->mesh_ptr_, tdim-1); !facet_it.is_end();
       ++facet_it) {

    // collect info
    std::vector<std::vector<int>> f_dofs;

    // loop over connected cell(s)
    std::vector<Coordinate> dofs_coords_on_facet =
      this->coords_facets_[facet_it->local_index()];

    // number of adjacent cells
    const int n_adj_cell = facet_dofs[facet_it->local_index()].dofs.size();
    assert(n_adj_cell <= 2);

    //// 1 adjacent cell
    if (n_adj_cell != 2) {
      continue;
    }

    // must contain -1
    bool condition_0 = (std::find(
                          facet_dofs[facet_it->local_index()].dofs[0].begin(),
                          facet_dofs[facet_it->local_index()].dofs[0].end(), -1)
                        == facet_dofs[facet_it->local_index()].dofs[0].end());

    bool condition_1 = (std::find(
                          facet_dofs[facet_it->local_index()].dofs[1].begin(),
                          facet_dofs[facet_it->local_index()].dofs[1].end(), -1)
                        == facet_dofs[facet_it->local_index()].dofs[1].end());

    if (condition_0 && condition_1) {
      continue;
    }

    // cell iterator
    Mesh::EntityIncidentIterator cell_it_first(*facet_it, tdim);
    Mesh::EntityIncidentIterator cell_it_second(*facet_it, tdim);
    ++cell_it_second;

    // initialize size of f_dofs
    f_dofs.resize(2);

    // collect coords two ajdacent cells
    std::vector<Coordinate> dofs_coords_on_cell_first =
      this->coords_cells_[cell_it_first->local_index()];

    std::vector<Coordinate> dofs_coords_on_cell_second =
      this->coords_cells_[cell_it_second->local_index()];

    for (int i = 0; i != dofs_coords_on_facet.size(); ++i) {
      Coordinate coord = dofs_coords_on_facet[i];

      // iterator
      std::vector<Coordinate>::iterator it;

      // first
      it = std::find(dofs_coords_on_cell_first.begin(),
                     dofs_coords_on_cell_first.end(), coord);
      const int pos_first = std::distance(dofs_coords_on_cell_first.begin(), it);
      assert(pos_first >= 0);

      // second
      it = std::find(dofs_coords_on_cell_second.begin(),
                     dofs_coords_on_cell_second.end(), coord);
      const int pos_second = std::distance(dofs_coords_on_cell_second.begin(), it);
      assert(pos_second >= 0);

      // cell dofs
      int c_dof_first = cell_dofs[cell_it_first->local_index()].dofs[pos_first];
      int c_dof_second = cell_dofs[cell_it_second->local_index()].dofs[pos_second];

      c_dof_first = c_dof_first == -1 ? c_dof_second : c_dof_first;
      c_dof_second = c_dof_second == -1 ? c_dof_first : c_dof_second;

      // update into cell_dofs
      cell_dofs[cell_it_first->local_index()].dofs[pos_first] = c_dof_first;
      cell_dofs[cell_it_second->local_index()].dofs[pos_second] = c_dof_second;

      // assign into f_dofs
      f_dofs[0].push_back(c_dof_first);
      f_dofs[1].push_back(c_dof_second);

    }

    facet_dofs[facet_it->local_index()].dofs = f_dofs;

  } // facet_it

  // update/correct facet_dofs
  // after update with two adjacent cells, update one adjacent cell
  for (Mesh::EntityIterator facet_it(this->mesh_ptr_, tdim-1); !facet_it.is_end();
       ++facet_it) {

    // collect info
    std::vector<std::vector<int>> f_dofs;

    // loop over connected cell(s)
    std::vector<Coordinate> dofs_coords_on_facet =
      this->coords_facets_[facet_it->local_index()];

    // number of adjacent cells
    const int n_adj_cell = facet_dofs[facet_it->local_index()].dofs.size();
    assert(n_adj_cell <= 2);

    //// 1 adjacent cell
    if (n_adj_cell != 1) {
      continue;
    }

    // cell iterator
    Mesh::EntityIncidentIterator cell_it(*facet_it, tdim);

    // must contain -1
    if (std::find(facet_dofs[facet_it->local_index()].dofs[0].begin(),
                  facet_dofs[facet_it->local_index()].dofs[0].end(), -1)
        == facet_dofs[facet_it->local_index()].dofs[0].end()) {
      continue;
    }

    // create dof container
    std::vector<int> dofs_on_facet_cell;

    // collect coordinates and dofs of adjacent cell
    std::vector<Coordinate> dofs_coords_on_cell =
      this->coords_cells_[cell_it->local_index()];

    std::vector<int> dofs_on_cell = cell_dofs[cell_it->local_index()].dofs;

    for (Coordinate coord_f : dofs_coords_on_facet) {
      bool found = false;
      for (int i = 0; i != dofs_coords_on_cell.size(); ++i) {

        if (coord_f == dofs_coords_on_cell[i]) {
          dofs_on_facet_cell.push_back(dofs_on_cell[i]);
          found = true;
        } // if

        // stop if dof is already found
        if (found == true) {
          break;
        } // if found

      } // i
    } // coord_f

    f_dofs.push_back(dofs_on_facet_cell);

    // assign dofs
    facet_dofs[facet_it->local_index()].dofs = f_dofs;

  } // facet_it

}

// instantiation
template class Numbering_CG<double>;
template class Numbering_CG<float>;

}; // namespace Space
}; // namepsace ChenLiu



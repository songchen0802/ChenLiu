// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "space/DegreeOfFreedom.hh"

namespace ChenLiu {
namespace Space {

// constractor
template<class DT>
DegreeOfFreedom<DT>::DegreeOfFreedom(): cell_dofs_(), facet_dofs_() {
  mesh_ptr_ = nullptr;

  total_dof_size_ = 0;
}

// copy construcotr
template<class DT>
DegreeOfFreedom<DT>::DegreeOfFreedom(const DegreeOfFreedom& DOF) {
  cell_dofs_ = DOF.cell_dofs_;
  facet_dofs_ = DOF.facet_dofs_;
  mesh_ptr_ = DOF.mesh_ptr_;
  comm_ = DOF.comm_;
}

// destructor
template<class DT>
DegreeOfFreedom<DT>::~DegreeOfFreedom() {
  // do nothing
}

// clear
template<class DT>
void DegreeOfFreedom<DT>::clear() {
  cell_dofs_.clear();
  facet_dofs_.clear();
  total_dof_size_ = 0;
}

// create
template<class DT>
void DegreeOfFreedom<DT>::create(const std::shared_ptr<const Mesh::Mesh>
                                 mesh_ptr, MPI_Comm comm,
                                 Element_Family ele_family, int degree) {

  mesh_ptr_ = mesh_ptr;
  comm_ = comm;

  switch(ele_family) {

  // CG
  case Element_Family::CG: {
    Numbering_CG<DT> numbering_strategy;
    numbering_strategy.init(mesh_ptr_, comm_, degree, FE_Type::Lagrange);

    numbering_strategy.number(cell_dofs_, facet_dofs_);

    compute_total_dof_size(numbering_strategy.dof_size_all());

    break;
  } // CG

  // DG
  case Element_Family::DG: {
    Numbering_DG<DT> numbering_strategy;
    numbering_strategy.init(mesh_ptr_, comm_, degree, FE_Type::Lagrange);

    numbering_strategy.number(cell_dofs_, facet_dofs_);

    compute_total_dof_size(numbering_strategy.dof_size_all());

    break;
  } // DG

  default: {
    CL_Error("Wrong Element Family!");
    exit(EXIT_FAILURE);
  } // default

  } // switch

}

// return total dof size
template<class DT>
int DegreeOfFreedom<DT>::total_dof_size() const {
  assert(total_dof_size_ > 0);

  return total_dof_size_;
}

// compute total dof size based on dof size on each processor
template<class DT>
void DegreeOfFreedom<DT>::compute_total_dof_size(
  std::vector<int> dof_size_per_proc) {
  assert(dof_size_per_proc.size() > 0);

  // re-set to zero
  total_dof_size_ = 0;

  for (int val : dof_size_per_proc) {
    total_dof_size_ += val;
  }
}

// dofs for cells
template<class DT>
std::map<int, Cell_dofs> DegreeOfFreedom<DT>::cell_dofs() const {
  return cell_dofs_;
}

// dofs for facets
template<class DT>
std::map<int, Facet_dofs> DegreeOfFreedom<DT>::facet_dofs() const {
  return facet_dofs_;
}

// instantiation
template class DegreeOfFreedom<double>;
template class DegreeOfFreedom<float>;

}; // namespace Space
}; // namepsace ChenLiu



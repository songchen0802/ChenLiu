// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "space/DofMap.hh"

namespace ChenLiu {
namespace Space {

// constractor
DofMap::DofMap() {
  // do nothing
}

// destructor
DofMap::~DofMap() {
  // do nothing
}

// clear
void DofMap::clear() {
  std::fill(interior_dofs_range_.begin(), interior_dofs_range_.end(), 0);
  ghost_dofs_.clear();
  ghost_owner_pos_.clear();
  ghost_owners_.clear();
  size_global_ = -1;
}

// create
void DofMap::create(int lowest_interior_dof, int highest_interior_dof,
                    std::map<int, std::vector<int>> ghost_map, int size_global) {

  this->clear();

  interior_dofs_range_ = {lowest_interior_dof, highest_interior_dof};

  if (ghost_map.size() > 0) {
    ghost_owner_pos_ = {0};

    for (const auto& pair : ghost_map) {
      // position of ghost owner
      ghost_owner_pos_.push_back(ghost_owner_pos_.back() + pair.second.size());

      // ghost owner, i.e. rank of ghost owner processors
      ghost_owners_.push_back(pair.first);

      // push ghost dof indices into ghost_dofs
      ghost_dofs_.insert(ghost_dofs_.end(), pair.second.begin(), pair.second.end());

    } // for

  } // if

  size_global_ = size_global;
}

// interior size
int DofMap::size_interior() const {
  return (interior_dofs_range_[1] - interior_dofs_range_[0] + 1);
}

// ghost size
int DofMap::size_ghost() const {
  return ghost_dofs_.size();
}

// total size of dofs
int DofMap::size_global() const {
  return size_global_;
}

}; // namespace Space
}; // namepsace ChenLiu



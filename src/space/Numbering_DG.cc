// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "space/Numbering_DG.hh"

namespace ChenLiu {
namespace Space {

// constructor
template<class DT>
Numbering_DG<DT>::Numbering_DG() {
  // do nothing
}

// destructor
template<class DT>
Numbering_DG<DT>::~Numbering_DG() {
  this->clear();
}

// number
template<class DT>
void Numbering_DG<DT>::number(std::map<int, Cell_dofs>& cell_dofs,
                              std::map<int, Facet_dofs>& facet_dofs) {

  // collect coordinates for all possible dofs
  this->collect_dofs_coordinates();

  // initialize dof ID
  this->initial_numbering(cell_dofs, facet_dofs);

  // update local dof IDs regarding to other processors
  this->update_local_dofs_global(cell_dofs, facet_dofs);

  // update ghost dof IDs regarding to other processors
  this->update_ghost_dofs_global(cell_dofs, facet_dofs);
}

// renumber based on common dofs (not needed for DG)
template<class DT>
void Numbering_DG<DT>::renumber_common_dofs(std::map<int, Cell_dofs>& cell_dofs,
    std::map<int, Facet_dofs>& facet_dofs) {

  CL_Error("This function should not be called for DG!");
  exit(EXIT_FAILURE);
}

// update dofs on ghost cells
template<class DT>
void Numbering_DG<DT>::update_ghost_dofs_global(
  std::map<int, Cell_dofs>& cell_dofs, std::map<int, Facet_dofs>& facet_dofs) {

  // collect
  this->exchange_cell_dof_info(cell_dofs, facet_dofs);

  // tdim
  const int tdim = this->mesh_ptr_->tdim();

  // mpi rank
  int rank = -1;
  MPI_Comm_rank(this->comm_, &rank);

  // update/correct facet_dofs
  for (Mesh::EntityIterator facet_it(this->mesh_ptr_, tdim-1); !facet_it.is_end();
       ++facet_it) {

    bool found_facet = false;
    for (std::vector<int> a : facet_dofs[facet_it->local_index()].dofs) {
      for (int b : a) {
        if (b == -1) {
          found_facet = true;
        }
      }
    }
    if (!found_facet) {
      continue;
    }

    // collect info
    std::vector<std::vector<int>> f_dofs;

    // loop over connected cell(s)
    std::vector<int> dofs_on_facet_cell;
    std::vector<Coordinate> dofs_coords_on_facet =
      this->coords_facets_[facet_it->local_index()];

    for (Mesh::EntityIncidentIterator cell_it(*facet_it, tdim);
         !cell_it.is_end(); ++cell_it) {

      dofs_on_facet_cell.clear();

      std::vector<Coordinate> dofs_coords_on_cell =
        this->coords_cells_[cell_it->local_index()];
      std::vector<int> dofs_on_cell = cell_dofs[cell_it->local_index()].dofs;

      for (Coordinate coord_f : dofs_coords_on_facet) {
        bool found = false;
        for (int i = 0; i != dofs_coords_on_cell.size(); ++i) {

          // stop if dof is already found
          if (found == true) {
            break;
          } // if found

          if (coord_f == dofs_coords_on_cell[i]) {
            dofs_on_facet_cell.push_back(dofs_on_cell[i]);
          } // if

        } // i
      } // coord_f

      f_dofs.push_back(dofs_on_facet_cell);

    } // cell_it

    assert(facet_dofs[facet_it->local_index()].dofs.size() == f_dofs.size());
    facet_dofs[facet_it->local_index()].dofs = f_dofs;

  } // facet_it

}

// instantiation
template class Numbering_DG<double>;
template class Numbering_DG<float>;

}; // namespace Space
}; // namepsace ChenLiu



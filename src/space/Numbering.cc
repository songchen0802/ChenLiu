// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "space/Numbering.hh"

namespace ChenLiu {
namespace Space {

// constractor
template<class DT>
Numbering<DT>::Numbering() : coords_cells_(), coords_facets_() {
  // set mesh pointer
  mesh_ptr_ = nullptr;

  ele_deg_ = -1;

  fe_type_ = FE_Type::Lagrange;
}

// destructor
template<class DT>
Numbering<DT>::~Numbering() {
  clear();
}

// clear
template<class DT>
void Numbering<DT>::clear() {
  coords_cells_.clear();
  coords_facets_.clear();

  mesh_ptr_ = nullptr;

  ele_deg_ = -1;

  dof_size_all_.clear();
}

// initialization
template<class DT>
void Numbering<DT>::init(const std::shared_ptr<const Mesh::Mesh> mesh_ptr,
                         MPI_Comm comm,
                         int degree, FE_Type fe_type) {
  assert(degree >= 0);

  mesh_ptr_ = mesh_ptr;
  comm_ = comm;
  ele_deg_ = degree;
  fe_type_ = fe_type;

  int size = -1;
  MPI_Comm_size(comm, &size);
  dof_size_all_.resize(size);
}

// collect coordinates of dofs on cells and facets
template<class DT>
void Numbering<DT>::collect_dofs_coordinates() {
  assert(this->mesh_ptr_ != nullptr);

  // tdim gdim
  const int tdim = this->mesh_ptr_->tdim();
  const int gdim = this->mesh_ptr_->gdim();

  // Element Factory
  FEM::ElementFactory<DT> element_factory;

  // loop over cells
  this->coords_cells_.clear();

  for (Mesh::EntityIterator cell_it(this->mesh_ptr_, tdim);
       !cell_it.is_end(); ++cell_it) {

    std::shared_ptr<FEM::Element<DT>> ele =
                                     element_factory.create(this->fe_type_,
                                         cell_it->cell_type(), this->ele_deg_, gdim);

    // add coordinates of all dofs on a cell
    std::vector<Coordinate> coords_verts = cell_it->vertices_coordinates();
    std::vector<Coordinate> coords_dofs = ele->dof_coordinates_phy(coords_verts);

    this->coords_cells_[cell_it->local_index()] = coords_dofs;

  } // for cell_it

  // loop over facets
  this->coords_facets_.clear();

  for (Mesh::EntityIterator facet_it(this->mesh_ptr_, tdim-1); !facet_it.is_end();
       ++facet_it) {

    Mesh::EntityIncidentIterator cell_it(*facet_it, tdim);
    const int loc_idx_cell = cell_it->local_index();
    const int loc_idx_facet = facet_it->local_index();

    // index of vertices on cell
    std::vector<int> cell_verts_idx = cell_it->vertices_local_index();

    // index of vertices on facet
    std::vector<int> facet_verts_idx = facet_it->vertices_local_index();

    // create an element of cell
    std::shared_ptr<FEM::Element<DT>> ele =
                                     element_factory.create(this->fe_type_,
                                         cell_it->cell_type(), this->ele_deg_, gdim);

    // obtain the local pos of dofs on facet wrt. dofs on cell
    std::vector<int> facet_dofs_on_cell_dofs = ele->dof_indices_on_sub_entity(
          cell_verts_idx, facet_verts_idx);

    // extract coordinates of dofs on facet
    std::vector<Coordinate> coords_dofs;
    for (int i : facet_dofs_on_cell_dofs) {
      coords_dofs.push_back(this->coords_cells_[loc_idx_cell][i]);
    }

    // add coordinates of dofs on facet into coords_facets
    this->coords_facets_[facet_it->local_index()] = coords_dofs;

  } // for facet_it

}

// initial numbering
template<class DT>
void Numbering<DT>::initial_numbering(std::map<int, Cell_dofs>& cell_dofs,
                                      std::map<int, Facet_dofs>& facet_dofs) {

  assert(this->coords_cells_.size() > 0);
  assert(this->coords_facets_.size() > 0);

  // tdim gdim
  const int tdim = this->mesh_ptr_->tdim();
  const int gdim = this->mesh_ptr_->gdim();

  // MPI rank
  int rank = -1;
  MPI_Comm_rank(this->comm_, &rank);

  //////////////////////////////
  // cells
  //////////////////////////////
  cell_dofs.clear();

  int dof_cell_start = 0;
  // loop over all cells
  for (Mesh::EntityIterator cell_it(this->mesh_ptr_, tdim); !cell_it.is_end();
       ++cell_it) {

    // collect info
    Cell_dofs c_dofs;
    std::vector<int> v(this->coords_cells_[cell_it->local_index()].size());

    // check if element is local cell 
    if (rank == cell_it->sub_domain_id()) {
      std::iota(v.begin(), v.end(), dof_cell_start);
      dof_cell_start += this->coords_cells_[cell_it->local_index()].size();
    } else {
      std::fill(v.begin(), v.end(), -1);
    }

    c_dofs.dofs = v;

    // add to cell_dofs
    cell_dofs[cell_it->local_index()] = c_dofs;

  } // for cell_it

  //////////////////////////////
  // facets
  //////////////////////////////
  facet_dofs.clear();

  // loop over all facets
  for (Mesh::EntityIterator facet_it(this->mesh_ptr_, tdim - 1);
       !facet_it.is_end(); ++facet_it) {

    // collect info
    Facet_dofs f_dofs;

    // loop over connected cell(s)
    std::vector<int> dofs_on_facet_cell;
    std::vector<Coordinate> dofs_coords_on_facet =
      this->coords_facets_[facet_it->local_index()];

    for (Mesh::EntityIncidentIterator cell_it(*facet_it, tdim);
         !cell_it.is_end(); ++cell_it) {

      dofs_on_facet_cell.clear();

      f_dofs.cell_local_index.push_back(cell_it->local_index());
      f_dofs.cell_global_index.push_back(cell_it->global_index());
      f_dofs.cell_sub_domain_id.push_back(cell_it->sub_domain_id());

      std::vector<Coordinate> dofs_coords_on_cell =
        this->coords_cells_[cell_it->local_index()];
      std::vector<int> dofs_on_cell = cell_dofs[cell_it->local_index()].dofs;

      for (Coordinate coord_f : dofs_coords_on_facet) {
        bool found = false;
        for (int i = 0; i != dofs_coords_on_cell.size(); ++i) {

          if (coord_f == dofs_coords_on_cell[i]) {
            dofs_on_facet_cell.push_back(dofs_on_cell[i]);
            found = true;
          } // if

          // stop if dof is already found
          if (found == true) {
            break;
          } // if found


        } // i
      } // coord_f

      f_dofs.dofs.push_back(dofs_on_facet_cell);

    } // for cell_it

    // add to facet_dofs
    facet_dofs[facet_it->local_index()] = f_dofs;

  } // for facet_it

}

// update dofs between processors
template<class DT>
void Numbering<DT>::update_local_dofs_global(
  std::map<int, Cell_dofs>& cell_dofs, std::map<int, Facet_dofs>& facet_dofs) {

  assert(this->coords_cells_.size() > 0);
  assert(this->coords_facets_.size() > 0);

  // tdim gdim
  const int tdim = this->mesh_ptr_->tdim();
  const int gdim = this->mesh_ptr_->gdim();

  // MPI rank & size
  int rank = -1;
  MPI_Comm_rank(this->comm_, &rank);

  int size = -1;
  MPI_Comm_size(this->comm_, &size);

  // gather all dof sizes
  int dof_size = -1;
  for (auto cell : cell_dofs) {
    for (int a : cell.second.dofs) {
      dof_size = dof_size < a ? a : dof_size;
    }
  }

  // dof ID stars from 0
  dof_size += 1;

  // MPI Allgather
  MPI_Allgather(&dof_size, 1, MPI_INT, this->dof_size_all_.data(), 1, MPI_INT,
                this->comm_);

  // update dof ID according to offsets defined by size of dofs from other processors
  int offset = 0;
  for (int i = 0; i != rank; ++i) {
    offset += this->dof_size_all_[i];
  }

  // cell_dofs
  for (auto& cell : cell_dofs) {
    for (int& a : cell.second.dofs) {
      if (a != -1) {
        a += offset;
      } // if
    } // a
  } // cell

  // facet_dofs
  for (auto& facet : facet_dofs) {
    for (auto& a : facet.second.dofs) {
      for (int& b : a) {
        if (b != -1) {
          b += offset;
        } // if
      } // b
    } // a
  } // facet

}

// exchange cell dof info
template<class DT>
void Numbering<DT>::exchange_cell_dof_info(std::map<int, Cell_dofs>& cell_dofs,
    std::map<int, Facet_dofs>& facet_dofs) {

  // tdim
  const int tdim = this->mesh_ptr_->tdim();

  // mpi rank
  int rank = -1;
  MPI_Comm_rank(this->comm_, &rank);

  // send
  std::map<int, CellDofInfo> send;
  for (Mesh::EntityIterator cell_it(this->mesh_ptr_, tdim); !cell_it.is_end();
       ++cell_it) {

    if (cell_it->sub_domain_id() != rank) {
      continue;
    }

    for (Mesh::EntityIncidentIterator neighbour_cell_it(*cell_it, tdim);
         !neighbour_cell_it.is_end(); ++neighbour_cell_it) {

      const int sub_d_id = neighbour_cell_it->sub_domain_id();
      if (sub_d_id != rank) {

        if (std::find(send[sub_d_id].global_entity_indices.begin(),
                      send[sub_d_id].global_entity_indices.end(),
                      cell_it->global_index()) != send[sub_d_id].global_entity_indices.end()) {
          continue;
        }

        send[sub_d_id].global_entity_indices.push_back(cell_it->global_index());
        send[sub_d_id].dofs.insert(send[sub_d_id].dofs.end(),
                                   cell_dofs[cell_it->local_index()].dofs.begin(),
                                   cell_dofs[cell_it->local_index()].dofs.end());

        if (send[sub_d_id].dof_offsets.size() == 0) {
          send[sub_d_id].dof_offsets.push_back(0);
        }
        send[sub_d_id].dof_offsets.push_back(send[sub_d_id].dof_offsets.back() +
                                             cell_dofs[cell_it->local_index()].dofs.size());


      } // if sub_d_id

    } // neighbour_cell_it

  } // cell_it

  // point-to-point communication on CellDofInfo

  // send
  std::vector<MPI_Request> send_reqs;

  Communication::CL_MPI_Isend mpi_isend(this->comm_);

  for (auto ele : send) {
    const int send_proc = ele.first;

    std::vector<MPI_Request> tmp_reqs;
    mpi_isend.execute(send[send_proc], send_proc, tmp_reqs);

    send_reqs.insert(send_reqs.end(), tmp_reqs.begin(), tmp_reqs.end());
  }

  // receive
  std::map<int, CellDofInfo> recv_info;
  std::vector<MPI_Request> recv_reqs;

  Communication::CL_MPI_Irecv mpi_irecv(this->comm_);

  for (auto ele : send) {
    const int recv_proc = ele.first;

    std::vector<MPI_Request> tmp_reqs;
    mpi_irecv.execute(recv_info[recv_proc], recv_proc, tmp_reqs);

    recv_reqs.insert(recv_reqs.end(), tmp_reqs.begin(), tmp_reqs.end());
  }

  // mpi wait
  MPI_Waitall(send_reqs.size(), send_reqs.data(), MPI_STATUS_IGNORE);
  MPI_Waitall(recv_reqs.size(), recv_reqs.data(), MPI_STATUS_IGNORE);

  // update/assign info on cell_dofs
  for (Mesh::EntityIterator cell_it(this->mesh_ptr_, tdim); !cell_it.is_end();
       ++cell_it) {

    const int sub_d_id = cell_it->sub_domain_id();
    if (sub_d_id == rank) {
      continue;
    }

    std::vector<int>::iterator it = std::find(
                                      recv_info[sub_d_id].global_entity_indices.begin(),
                                      recv_info[sub_d_id].global_entity_indices.end(),
                                      cell_it->global_index());

    assert(it != recv_info[sub_d_id].global_entity_indices.end());
    const int pos =
      std::distance(recv_info[sub_d_id].global_entity_indices.begin(), it);

    std::vector<int> dofs(recv_info[sub_d_id].dofs.begin() +
                          recv_info[sub_d_id].dof_offsets[pos],
                          recv_info[sub_d_id].dofs.begin() +
                          recv_info[sub_d_id].dof_offsets[pos+1]);
    cell_dofs[cell_it->local_index()].dofs = dofs;

  } // cell_it

}

// total dof size
template<class DT>
std::vector<int> Numbering<DT>::dof_size_all() const {
  assert(this->dof_size_all_.size() > 0);

  return this->dof_size_all_;
}

// instantiation
template class Numbering<double>;
template class Numbering<float>;

}; // namespace Space
}; // namepsace ChenLiu


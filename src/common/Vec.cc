// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "common/PreDef.hh"
#include "common/Vec.hh"

namespace ChenLiu {

template<class DT>
Vec<DT>::~Vec() {
  vec_.clear();
}

template<class DT>
Vec<DT>::Vec(const int N) {
  vec_.resize(N);
}

template<class DT>
typename Vec<DT>::BaseVecType Vec<DT>::vec() const {
  return vec_;
}

template<class DT>
Vec<DT>::Vec(const Vec<DT>& v) {
  vec_.clear();
  vec_ = v.vec();
}

template<class DT>
Vec<DT>::Vec(const std::vector<DT> v) {
  vec_.resize(v.size());

  for (int i = 0; i < v.size(); ++i) {
    vec_(i) = v[i];
  }
}

template<class DT>
Vec<DT>::Vec(const BaseVecType v) {
  vec_.resize(v.size());

  vec_ = v;
}

template<class DT>
int Vec<DT>::size() const {
  return vec_.size();
}

template<class DT>
Vec<DT> &Vec<DT>::operator=(const Vec<DT> v) {
  assert(vec_.size() == v.size());

  vec_ = v.vec();
  return *this;
}

template<class DT>
DT Vec<DT>::operator()(const int i) const {
  return vec_(i);
}

template<class DT>
DT &Vec<DT>::operator()(const int i) {
  return vec_(i);
}

template<class DT>
Vec<DT>& Vec<DT>::operator+=(const Vec<DT> v) {
  assert(vec_.size() == v.size());
  assert(vec_.size() > 0);

  vec_ += v.vec();
  return *this;
}

template<class DT>
Vec<DT>& Vec<DT>::operator-=(const Vec<DT> v) {
  assert(vec_.size() == v.size());
  assert(vec_.size() > 0);

  vec_ -= v.vec();
  return *this;
}

template<class DT>
Vec<DT>& Vec<DT>::operator*=(const DT s) {
  assert(vec_.size() > 0);

  vec_ *= s;
  return *this;
}

template<class DT>
Vec<DT>& Vec<DT>::operator/=(const DT s) {
  assert(vec_.size() > 0);

  vec_ /= s;
  return *this;
}

template<class DT>
bool Vec<DT>::operator==(const Vec<DT> v) {
  assert(vec_.size() == v.size());
  assert(vec_.size() > 0);

  for (std::size_t i = 0; i < vec_.size(); ++i) {
    if (!is_equal(vec_(i), v(i))) {
      return false;
    }
  }
  return true;
}

// explict instantiation

template class Vec<double>;
template class Vec<float>;

} // namespace ChenLiu

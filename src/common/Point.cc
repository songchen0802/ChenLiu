// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "Point.hh"

namespace ChenLiu {

// constructors
template<class DT>
Point<DT>::Point() {
  for (std::size_t i = 0; i < 3; ++i) {
    x_[i] = 0.0;
  }
}

template<class DT>
Point<DT>::Point(const DT x, const DT y, const DT z) {
  x_[0] = x;
  x_[1] = y;
  x_[2] = z;
}

template<class DT>
Point<DT>::Point(const std::array<DT, 3> x) {
  for (std::size_t i = 0; i < 3; ++i) {
    x_[i] = x[i];
  }
}

// return values
template<class DT>
DT Point<DT>::x() const {
  return x_[0];
}

template<class DT>
DT Point<DT>::y() const {
  return x_[1];
}

template<class DT>
DT Point<DT>::z() const {
  return x_[2];
}

//operator []
template<class DT>
DT Point<DT>::operator[](const int& i) const {
  assert(i < 3);
  return x_[i];
}

template<class DT>
DT& Point<DT>::operator[](const int& i) {
  assert(i < 3);
  return x_[i];
}

// constructor
template<class DT>
Point<DT>::Point(const Point<DT>& p) {
  for (std::size_t i = 0; i < 3; ++i) {
    x_[i] = p[i];
  }
}

// operator +
template<class DT>
Point<DT> Point<DT>::operator+(const Point<DT>& p) const {
  Point<DT> q(x_[0] + p[0], x_[1] + p[1], x_[2] + p[2]);
  return q;
}

// operator -
template<class DT>
Point<DT> Point<DT>::operator-(const Point<DT>& p) const {
  Point<DT> q(x_[0] - p[0], x_[1] - p[1], x_[2] - p[2]);
  return q;
}

// operator *
template<class DT>
Point<DT> Point<DT>::operator*(const DT a) const {
  Point<DT> q(x_[0] * a, x_[1] * a, x_[2] * a);
  return q;
}

// operator /
template<class DT>
Point<DT> Point<DT>::operator/(const DT a) const {
  Point<DT> q(x_[0] / a, x_[1] / a, x_[2] / a);
  return q;
}

// operator +=
template<class DT>
const Point<DT>& Point<DT>::operator+=(const Point<DT>& p) {
  for (std::size_t i = 0; i < 3; ++i) {
    x_[i] += p[i];
  }
  return *this;
}

// operator -=
template<class DT>
const Point<DT>& Point<DT>::operator-=(const Point<DT>& p) {
  for (std::size_t i = 0; i < 3; ++i) {
    x_[i] -= p[i];
  }
  return *this;
}

// operator *=
template<class DT>
const Point<DT>& Point<DT>::operator*=(const DT a) {
  for (std::size_t i = 0; i < 3; ++i) {
    x_[i] *= a;
  }
  return *this;
}

// operator /=
template<class DT>
const Point<DT>& Point<DT>::operator/=(const DT a) {
  for (std::size_t i = 0; i < 3; ++i) {
    x_[i] /= a;
  }
  return *this;
}

// operator ==
template<class DT>
bool Point<DT>::operator==(const Point<DT>& p) const {
  for (std::size_t i = 0; i < 3; ++i) {
    if (!is_equal(x_[i], p[i])) {
      return false;
    }
  }
  return true;
}

// operator !=
template<class DT>
bool Point<DT>::operator!=(const Point<DT>& p) const {
  for (std::size_t i = 0; i < 3; ++i) {
    if (!is_equal(x_[i], p[i])) {
      return true;
    }
  }
  return false;
}

template class Point<float>;
template class Point<double>;

} // namespace ChneLiu


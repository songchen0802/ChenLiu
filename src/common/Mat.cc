// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "common/Mat.hh"

namespace ChenLiu {

template<class DT>
Mat<DT>::Mat(const int N, const int M) {
  mat_.resize(N, M);
}

template<class DT>
typename Mat<DT>::BaseMatType Mat<DT>::mat() const {
  return mat_;
}

template<class DT>
Mat<DT>::~Mat() {
  mat_.clear();
}

template<class DT>
int Mat<DT>::r_size() const {
  return mat_.size1();
}

template<class DT>
int Mat<DT>::c_size() const {
  return mat_.size2();
}

template<class DT>
DT Mat<DT>::operator()(const int i, const int j) const {
  return mat_(i, j);
}

template<class DT>
DT &Mat<DT>::operator()(const int i, const int j) {
  return mat_(i, j);
}

template<class DT>
Mat<DT>::Mat(const Mat<DT>& m) {
  assert(m.r_size() > 0 && m.c_size() > 0);

  mat_.resize(m.r_size(), m.c_size());
  mat_ = m.mat();
}

template<class DT>
Mat<DT>& Mat<DT>::operator=(const Mat<DT> m) {
  assert(mat_.size1() == m.r_size() && mat_.size2() == m.c_size());
  assert(mat_.size1() > 0 && mat_.size2() > 0);

  mat_ = m.mat();
  return *this;
}

template<class DT>
Mat<DT>& Mat<DT>::operator+=(const Mat<DT> m) {
  assert(mat_.size1() > 0 && mat_.size2() > 0);

  mat_ += m.mat();
  return *this;
}

template<class DT>
Mat<DT>& Mat<DT>::operator-=(const Mat<DT> m) {
  assert(mat_.size1() > 0 && mat_.size2() > 0);

  mat_ -= m.mat();
  return *this;
}

template<class DT>
Mat<DT>& Mat<DT>::operator*=(const DT s) {
  assert(mat_.size1() > 0 && mat_.size2() > 0);

  mat_ *= s;
  return *this;
}

template<class DT>
Mat<DT>& Mat<DT>::operator/=(const DT s) {
  assert(mat_.size1() > 0 && mat_.size2() > 0);

  mat_ /= s;
  return *this;
}

template<class DT>
bool Mat<DT>::operator==(const Mat<DT> m) {
  assert(mat_.size1() == m.r_size() && mat_.size2() == m.c_size());
  assert(mat_.size1() > 0 && mat_.size2() > 0);

  for (std::size_t i = 0; i < mat_.size1(); ++ i) {
    for (std::size_t j = 0; j < mat_.size2(); ++j) {
      if (!is_equal(mat_(i, j), m(i, j))) {
        return false;
      }
    }
  }

  return true;
}

// explict instantiation

template class Mat<double>;
template class Mat<float>;


} // namepsace ChenLiu

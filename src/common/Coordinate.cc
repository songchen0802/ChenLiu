// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "common/Coordinate.hh"

namespace ChenLiu {

// constructor
Coordinate::Coordinate() : coord_() {
  // do nothing
}

Coordinate::Coordinate(Coord_T x, Coord_T y, Coord_T z) {
  coord_.resize(3);

  coord_[0] = x;
  coord_[1] = y;
  coord_[2] = z;
}

Coordinate::Coordinate(Coord_T x, Coord_T y) {
  coord_.resize(2);

  coord_[0] = x;
  coord_[1] = y;
}

Coordinate::Coordinate(Coord_T x) {
  coord_.resize(1);

  coord_[0] = x;
}

Coordinate::Coordinate(std::vector<Coord_T> coord) {
  assert(coord.size() == 1 || 2 || 3);
  coord_.resize(coord.size());
  for (int d = 0; d != coord.size(); ++d) {
    coord_[d] = coord[d];
  }
}

Coordinate::Coordinate(int gdim) {
  assert(gdim > 0);
  assert(gdim <= 3);

  coord_.resize(gdim);

  for (int i = 0; i != coord_.size(); ++i) {
    coord_[i] = 0.0;
  }
}

// destructor
Coordinate::~Coordinate() {
  // do nothing
}

// initialization
void Coordinate::init(int d) {
  assert(d > 0);
  assert(d <= 3);

  coord_.resize(d);
  for (std::size_t i = 0; i < d; ++i) {
    coord_[i] = 0.0;
  }
}

// x
Coord_T Coordinate::x() const {
  assert(coord_.size() >= 1);

  return coord_[0];
}

// y
Coord_T Coordinate::y() const {
  assert(coord_.size() >= 2);

  return coord_[1];
}

// z
Coord_T Coordinate::z() const {
  assert(coord_.size() >= 3);

  return coord_[2];
}

// operator []
Coord_T Coordinate::operator[](int i) const {
  assert(i < coord_.size());
  return coord_[i];
}


Coord_T& Coordinate::operator[](int i) {
  assert(i < coord_.size());
  return coord_[i];
}

// set_coord
void Coordinate::set_coord(std::vector<Coord_T> coord) {
  assert(coord_.size() == coord.size());
  for (std::size_t i = 0; i < coord.size(); ++i) {
    coord_[i] = coord[i];
  }
}


void Coordinate::set_coord(Coordinate coord) {
  assert(coord_.size() == coord.dim());
  for (std::size_t i = 0; i < coord.dim(); ++i) {
    coord_[i] = coord[i];
  }
}

// get_coord
std::vector<Coord_T> Coordinate::get_coord() const {
  std::vector<Coord_T> coord(coord_.begin(), coord_.end());
  return coord;
}

// move
void Coordinate::move(std::vector<Coord_T> coord) {
  assert(coord_.size() == coord.size());
  for (std::size_t i = 0; i < coord.size(); ++i) {
    coord_[i] += coord[i];
  }
}


void Coordinate::move(Coordinate coord) {
  assert(coord_.size() == coord.dim());
  for (std::size_t i = 0; i < coord.dim(); ++i) {
    coord_[i] += coord[i];
  }
}

// compare operator
bool Coordinate::operator==(Coordinate coord) const {

  const int dim = coord_.size();

  for (std::size_t i = 0; i != dim; ++i) {
    if (std::fabs(coord_[i] - coord.get_coord()[i]) >
        static_cast<Coord_T>(COMP_TOL)) {

      return false;
    }
  }

  return true;
}

// dimension
int Coordinate::dim() const {
  return coord_.size();
}

// print
void Coordinate::print() const {
  for (Coord_T c : coord_) {
    std::cout << c << " ";
  }
  std::cout << std::endl;
}

}; // namespace ChenLiu


// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#include "common/Marker.hh"

namespace ChenLiu {

// constructor
template<class DT>
Marker<DT>::Marker():tdim_(-1), marker_() {
  // do nothing
}

template<class DT>
Marker<DT>::Marker(int tdim):tdim_(tdim), marker_() {
  // do nothing
}

template<class DT>
Marker<DT>::Marker(int tdim, int num_entities):
  tdim_(tdim) {
  assert(0 <= tdim <= 3);

  marker_.resize(num_entities);
}

// desctructor
template<class DT>
Marker<DT>::~Marker() {
  // do nothing
}

// clear
template<class DT>
void Marker<DT>::clear() {
  marker_.clear();
}

// set marker
template<class DT>
void Marker<DT>::set_marker(DT val, int idx_entity) {
  assert(idx_entity >= 0);
  assert(marker_.size() >= idx_entity);

  marker_[idx_entity] = val;
}

// get value
template<class DT>
DT Marker<DT>::get_marker(int idx_entity) const {
  assert(idx_entity >= 0);

  return  marker_.at(idx_entity);
}

// size
template<class DT>
int Marker<DT>::size() const {
  assert(marker_.size() >= 0);

  return marker_.size();
}

// check number of entities
template<class DT>
bool Marker<DT>::check_num_entities(int num_entities) const {

  return (marker_.size() == num_entities);
}

// push back a new marker into marker_
template<class DT>
void Marker<DT>::add_marker(DT val) {
  marker_.push_back(val);
}

template<class DT>
void Marker<DT>::add_marker(DT val, int idx_entity) {
  if (marker_.size() == idx_entity) {
    add_marker(val);
  } else {
    CL_Error("idx_entity is not properly chosen");
    exit(EXIT_FAILURE);
  }
}

template<class DT>
void Marker<DT>::add_marker(std::vector<DT> val) {
  assert(val.size() >= 0);
  marker_.insert(marker_.end(), val.begin(), val.end());
}

template class Marker<int>;
template class Marker<double>;
template class Marker<float>;

}; // namespace ChenLiu

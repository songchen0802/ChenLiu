// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "common/Timer.hh"

namespace ChenLiu {

Timer::Timer() : t_start_(std::clock()), t_end_(std::clock()), task_("") {
}

Timer::Timer(std::string task) : t_start_(std::clock()), t_end_(std::clock()) {
  task_ = task;
}

Timer::~Timer() {
  task_.clear();
}

void Timer::start() {
  t_start_ = std::clock();
}

void Timer::stop() {
  t_end_ = std::clock();
}

void Timer::resume() {
  t_start_ = std::clock();
  t_end_ = std::clock();
}

void Timer::print() {
  std::cout << task_ << " takes: " << (t_end_ - t_start_) << " ms" << std::endl;
}

double Timer::duration() {
  return (t_end_ - t_start_);
}

std::string Timer::task() {
  return task_;
}

} // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "common/MatVecOp.hh"

namespace ChenLiu {

template<class DT>
Vec<DT> operator*(const Vec<DT> v, const DT s) {
  assert(v.size() > 0);

  Vec<DT> v_tmp(v);
  v_tmp *= s;
  return v_tmp;
}

template<class DT>
Vec<DT> operator*(const DT s, const Vec<DT> v) {
  assert(v.size() > 0);

  Vec<DT> v_tmp(v);
  v_tmp *= s;
  return v_tmp;
}

template<class DT>
Vec<DT> operator/(const Vec<DT> v, const DT s) {
  assert(v.size() > 0);

  Vec<DT> v_tmp(v);
  v_tmp /= s;
  return v_tmp;
}

template<class DT>
Vec<DT> operator+(const Vec<DT> v1, const Vec<DT> v2) {
  assert(v1.size() == v2.size());
  assert(v1.size() > 0);

  Vec<DT> v_tmp(v1);
  v_tmp += v2;
  return v_tmp;
}

template<class DT>
Vec<DT> operator-(const Vec<DT> v1, const Vec<DT> v2) {
  assert(v1.size() == v2.size());
  assert(v1.size() > 0);

  Vec<DT> v_tmp(v1);
  v_tmp -= v2;
  return v_tmp;
}

template<class DT>
void VecAxpy(Vec<DT>& y, const DT alpha, const Vec<DT> x) {
  assert(y.size() == x.size());
  assert(y.size() > 0);

  y += alpha * x;
}

template<class DT>
void VecAypx(Vec<DT>& y, const DT beta, const Vec<DT> x) {
  assert(y.size() == x.size());
  assert(y.size() > 0);

  y = x + beta * y;
}

template<class DT>
DT Dot(const Vec<DT> v1, const Vec<DT> v2) {
  assert(v1.size() == v2.size());
  assert(v1.size() > 0);

  return inner_prod(v1.vec(), v2.vec());
}

template<class DT>
Vec<DT> Cross(const Vec<DT> a, const Vec<DT> b) {
  assert(a.size() == b.size());
  assert(a.size() == 3);

  Vec<DT> v(3);
  // a2*b3 - a3*b2
  v(0) = a(1) * b(2) - a(2) * b(1);
  // a3*b1 - a1*b3
  v(1) = a(2) * b(0) - a(0) * b(2);
  // a1*b2 - a2*b1
  v(2) = a(0) * b(1) - a(1) * b(0);

  return v;
}

template<class DT>
Vec<DT> MatVecMult(const Mat<DT> m, const Vec<DT> v) {
  assert(m.r_size() == v.size());

  return Vec<DT>(prod(m.mat(), v.vec()));
}

template<class DT>
DT Norm(const Vec<DT> v) {
  DT sum = 0.0;

  for (int i = 0; i != v.size(); ++i) {
    sum += v(i) * v(i);
  }

  return std::sqrt(sum);
}

template<class DT>
Mat<DT> Inverse(const Mat<DT> m) {
  assert(m.r_size() == m.c_size());
  assert(m.r_size() == 2 || 3 || 4);

  // determinant
  DT det = 0.0;

  // inverse of terminant
  DT inv_det = 0.0;

  // inverse matrix
  Mat<DT> inv_m(m);

  // tolerance
  const DT tol_eps = std::numeric_limits<DT>::epsilon() * 1.0e3;

  switch(m.r_size()) {
  case 2:
  {
    det = + m(0, 0) * m(1, 1) - m(1, 0) * m(0, 1);

    assert(abs(det) > tol_eps);
    inv_det = 1.0 / det;

    inv_m(0, 0) = + m(1, 1) * inv_det;
    inv_m(0, 1) = - m(0, 1) * inv_det;
    inv_m(1, 0) = - m(1, 0) * inv_det;
    inv_m(1, 1) = + m(0, 0) * inv_det;

    return inv_m;
  }

  case 3:
  {
    det = + m(0, 0) * (m(1, 1) * m(2, 2) - m(2, 1) * m(1, 2))
          - m(0, 1) * (m(1, 0) * m(2, 2) - m(1, 2) * m(2, 0))
          + m(0, 2) * (m(1, 0) * m(2, 1) - m(1, 1) * m(2, 0));

    assert(abs(det) > tol_eps);
    inv_det = 1.0 / det;

    inv_m(0, 0) = (m(1, 1) * m(2, 2) - m(2, 1) * m(1, 2)) * inv_det;
    inv_m(0, 1) = (m(0, 2) * m(2, 1) - m(0, 1) * m(2, 2)) * inv_det;
    inv_m(0, 2) = (m(0, 1) * m(1, 2) - m(0, 2) * m(1, 1)) * inv_det;
    inv_m(1, 0) = (m(1, 2) * m(2, 0) - m(1, 0) * m(2, 2)) * inv_det;
    inv_m(1, 1) = (m(0, 0) * m(2, 2) - m(0, 2) * m(2, 0)) * inv_det;
    inv_m(1, 2) = (m(1, 0) * m(0, 2) - m(0, 0) * m(1, 2)) * inv_det;
    inv_m(2, 0) = (m(1, 0) * m(2, 1) - m(2, 0) * m(1, 1)) * inv_det;
    inv_m(2, 1) = (m(2, 0) * m(0, 1) - m(0, 0) * m(2, 1)) * inv_det;
    inv_m(2, 2) = (m(0, 0) * m(1, 1) - m(1, 0) * m(0, 1)) * inv_det;

    return inv_m;
  }

  case 4:
  {
    DT A2323 = m(2, 2) * m(3, 3) - m(2, 3) * m(3, 2);
    DT A1323 = m(2, 1) * m(3, 3) - m(2, 3) * m(3, 1);
    DT A1223 = m(2, 1) * m(3, 2) - m(2, 2) * m(3, 1);
    DT A0323 = m(2, 0) * m(3, 3) - m(2, 3) * m(3, 0);
    DT A0223 = m(2, 0) * m(3, 2) - m(2, 2) * m(3, 0);
    DT A0123 = m(2, 0) * m(3, 1) - m(2, 1) * m(3, 0);
    DT A2313 = m(1, 2) * m(3, 3) - m(1, 3) * m(3, 2);
    DT A1313 = m(1, 1) * m(3, 3) - m(1, 3) * m(3, 1);
    DT A1213 = m(1, 1) * m(3, 2) - m(1, 2) * m(3, 1);
    DT A2312 = m(1, 2) * m(2, 3) - m(1, 3) * m(2, 2);
    DT A1312 = m(1, 1) * m(2, 3) - m(1, 3) * m(2, 1);
    DT A1212 = m(1, 1) * m(2, 2) - m(1, 2) * m(2, 1);
    DT A0313 = m(1, 0) * m(3, 3) - m(1, 3) * m(3, 0);
    DT A0213 = m(1, 0) * m(3, 2) - m(1, 2) * m(3, 0);
    DT A0312 = m(1, 0) * m(2, 3) - m(1, 3) * m(2, 0);
    DT A0212 = m(1, 0) * m(2, 2) - m(1, 2) * m(2, 0);
    DT A0113 = m(1, 0) * m(3, 1) - m(1, 1) * m(3, 0);
    DT A0112 = m(1, 0) * m(2, 1) - m(1, 1) * m(2, 0);

    det = + m(0, 0) * ( m(1, 1) * A2323 - m(1, 2) * A1323 + m(1, 3) * A1223 )
          - m(0, 1) * ( m(1, 0) * A2323 - m(1, 2) * A0323 + m(1, 3) * A0223 )
          + m(0, 2) * ( m(1, 0) * A1323 - m(1, 1) * A0323 + m(1, 3) * A0123 )
          - m(0, 3) * ( m(1, 0) * A1223 - m(1, 1) * A0223 + m(1, 2) * A0123 );

    assert(abs(det) > tol_eps);
    inv_det = 1.0 / det;

    inv_m(0, 0) = + inv_det * (m(1, 1) * A2323 - m(1, 2) * A1323 + m(1, 3) * A1223);
    inv_m(0, 1) = - inv_det * (m(0, 1) * A2323 - m(0, 2) * A1323 + m(0, 3) * A1223);
    inv_m(0, 2) = + inv_det * (m(0, 1) * A2313 - m(0, 2) * A1313 + m(0, 3) * A1213);
    inv_m(0, 3) = - inv_det * (m(0, 1) * A2312 - m(0, 2) * A1312 + m(0, 3) * A1212);
    inv_m(1, 0) = - inv_det * (m(1, 0) * A2323 - m(1, 2) * A0323 + m(1, 3) * A0223);
    inv_m(1, 1) = + inv_det * (m(0, 0) * A2323 - m(0, 2) * A0323 + m(0, 3) * A0223);
    inv_m(1, 2) = - inv_det * (m(0, 0) * A2313 - m(0, 2) * A0313 + m(0, 3) * A0213);
    inv_m(1, 3) = + inv_det * (m(0, 0) * A2312 - m(0, 2) * A0312 + m(0, 3) * A0212);
    inv_m(2, 0) = + inv_det * (m(1, 0) * A1323 - m(1, 1) * A0323 + m(1, 3) * A0123);
    inv_m(2, 1) = - inv_det * (m(0, 0) * A1323 - m(0, 1) * A0323 + m(0, 3) * A0123);
    inv_m(2, 2) = + inv_det * (m(0, 0) * A1313 - m(0, 1) * A0313 + m(0, 3) * A0113);
    inv_m(2, 3) = - inv_det * (m(0, 0) * A1312 - m(0, 1) * A0312 + m(0, 3) * A0112);
    inv_m(3, 0) = - inv_det * (m(1, 0) * A1223 - m(1, 1) * A0223 + m(1, 2) * A0123);
    inv_m(3, 1) = + inv_det * (m(0, 0) * A1223 - m(0, 1) * A0223 + m(0, 2) * A0123);
    inv_m(3, 2) = - inv_det * (m(0, 0) * A1213 - m(0, 1) * A0213 + m(0, 2) * A0113);
    inv_m(3, 3) = + inv_det * (m(0, 0) * A1212 - m(0, 1) * A0212 + m(0, 2) * A0112);

    return inv_m;
  }

  default:
    CL_Error("Incorrect matrix size!");
    exit(EXIT_FAILURE);
  }

}

// explict instantiation

template Vec<double> operator*(const Vec<double>, const double);
template Vec<float> operator*(const Vec<float>, const float);

template Vec<double> operator*(const double, const Vec<double>);
template Vec<float> operator*(const float, const Vec<float>);

template Vec<double> operator/(const Vec<double>, const double);
template Vec<float> operator/(const Vec<float>, const float);

template Vec<double> operator+(const Vec<double>, const Vec<double>);
template Vec<float> operator+(const Vec<float>, const Vec<float>);

template Vec<double> operator-(const Vec<double>, const Vec<double>);
template Vec<float> operator-(const Vec<float>, const Vec<float>);

template void VecAxpy(Vec<double>&, const double, const Vec<double>);
template void VecAxpy(Vec<float>&, const float, const Vec<float>);

template void VecAypx(Vec<double>&, const double, const Vec<double>);
template void VecAypx(Vec<float>&, const float, const Vec<float>);

template double Dot(const Vec<double>, const Vec<double>);
template float Dot(const Vec<float>, const Vec<float>);

template Vec<double> Cross(const Vec<double>, const Vec<double>);
template Vec<float> Cross(const Vec<float>, const Vec<float>);

template Vec<double> MatVecMult(const Mat<double>, const Vec<double>);
template Vec<float> MatVecMult(const Mat<float>, const Vec<float>);

template double Norm(const Vec<double>);
template float Norm(const Vec<float>);

template Mat<double> Inverse(const Mat<double>);
template Mat<float> Inverse(const Mat<float>);
} // namespace ChenLiu


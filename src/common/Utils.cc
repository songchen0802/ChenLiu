// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "common/Utils.hh"

namespace ChenLiu {

bool has_same_elements(std::vector<int> a, std::vector<int> b) {

  if (a.size() != b.size()) {
    return false;
  }

  std::sort(a.begin(), a.end());
  std::sort(b.begin(), b.end());

  return a == b;
}

bool is_all_same(std::vector<int> v) {

  if (v.empty()) {
    return true;
  }

  return std::all_of(v.begin() + 1, v.end(), [&](int x) {
    return x == v[0];
  });
}

// convert from string to CellType
CellType str2cell_type(std::string str) {
  if (str == "point" || str == "Point") {
    return CellType::Point;
  } else if (str == "line" || str == "Line") {
    return CellType::Line;
  } else if (str == "triangle" || str == "Triangle" || str == "tri") {
    return CellType::Triangle;
  } else if (str == "quadrilateral" || str == "Quadrilateral" || str == "quad") {
    return CellType::Quadrilateral;
  } else if (str == "tetrahedron" || str == "Tetrahedron" || str == "tet") {
    return CellType::Tetrahedron;
  } else if (str == "hexahedron" || str == "Hexahedron" || str == "hex") {
    return CellType::Hexahedron;
  } else {
    CL_Error("Wrong name for cell type!");
    exit(EXIT_FAILURE);
  }
}

// convert from CellType to string
std::string cell_type2str(CellType type) {
  switch(type) {
  case CellType::Point:
    return "point";
  case CellType::Line:
    return "line";
  case CellType::Triangle:
    return "triangle";
  case CellType::Quadrilateral:
    return "quadrilateral";
  case CellType::Tetrahedron:
    return "tetrahedron";
  case CellType::Hexahedron:
    return "hexahedron";
  default:
    CL_Error("Wrong CellType!");
    exit(EXIT_FAILURE);
  }
}

// convert from string to FE_Type
FE_Type str2fe_type(std::string str) {
  if (str == "lagrange" || str == "Lagrange") {
    return FE_Type::Lagrange;
  } else {
    CL_Error("Wrong name for finite element type!");
    exit(EXIT_FAILURE);
  }
}

// convert from FE_Type to string
std::string fe_type2str(FE_Type type) {
  switch(type) {
  case FE_Type::Lagrange:
    return "Lagrange";
  default:
    CL_Error("Wrong FE_Type!");
    exit(EXIT_FAILURE);
  }
}


} // namepsace ChenLiu

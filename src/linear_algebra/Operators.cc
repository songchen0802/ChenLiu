// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "linear_algebra/Operators.hh"

namespace ChenLiu {
namespace LA {

namespace OP {

// L2 norm
template<class DT>
DT norm2(const Vector<DT>& x) {
  CL_Error("Not implemented!");
  std::exit(EXIT_FAILURE);
}

// L1 norm
template<class DT>
DT norm1(const Vector<DT>& x) {
  CL_Error("Not implemented!");
  std::exit(EXIT_FAILURE);
}

// dot product
template<class DT>
DT dot(const Vector<DT>& x, const Vector<DT>& y) {
  CL_Error("Not implemented!");
  std::exit(EXIT_FAILURE);
}

// scale
template<class DT>
void scale(Vector<DT>& x, DT s) {
  CL_Error("Not implemented!");
  std::exit(EXIT_FAILURE);
}

// axpy
template<class DT>
void axpy(Vector<DT>& y, DT alpha, const Vector<DT>& x) {
  CL_Error("Not implemented!");
  std::exit(EXIT_FAILURE);
}


// instantiation

// L2 norm
template float norm2(const Vector<float>&);
template double norm2(const Vector<double>&);

// L1 norm
template float norm1(const Vector<float>&);
template double norm1(const Vector<double>&);

// dot
template float dot(const Vector<float>&, const Vector<float>&);
template double dot(const Vector<double>&, const Vector<double>&);

// scale
template void scale(Vector<float>&, float);
template void scale(Vector<double>&, double);

// axpy
template void axpy(Vector<float>&, float, const Vector<float>&);
template void axpy(Vector<double>&, double, const Vector<double>&);

}; // namespace OP

}; // namespace LA
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "linear_algebra/LA_Structure.hh"

namespace ChenLiu {
namespace LA {

// constructor
template<class DT>
LA_Structure<DT>::LA_Structure(const Space::FunctionSpace<DT> space):
  n_var_(space.n_var()), comm_(space.mpi_comm()),
  global_dof_offsets_(space.global_dof_offsets()) {

  // compute dof maps
  compute_dof_maps(space);
}

// destructor
template<class DT>
LA_Structure<DT>::~LA_Structure() {
  clear();
}

// clear
template<class DT>
void LA_Structure<DT>::clear() {
  global_dof_offsets_.clear();
  dof_maps_.clear();
}

// compute map of dof
template<class DT>
void LA_Structure<DT>::compute_dof_maps(const Space::FunctionSpace<DT> space) {

  dof_maps_.resize(n_var_);

  for (int i = 0; i != n_var_; ++i) {
    // create a tmp dof map for computation purpose
    Dof_map d_map;

    // clear dof_map
    d_map.interior_dofs_range = {0, 0};
    d_map.ghost_dofs.clear();

    // local rank
    int rank;
    MPI_Comm_rank(comm_, &rank);

    // collector for interior dofs
    std::unordered_set<int> interior_dofs_collector;

    // get mesh pointer from space
    const std::shared_ptr<const Mesh::Mesh> mesh_ptr = space.mesh_ptr();

    // topological dimention
    const int tdim = mesh_ptr->tdim();

    // cell dofs
    std::map<int, Space::Cell_dofs> cell_dofs = space.cell_dofs(i);

    // loop over cells
    for (Mesh::EntityIterator cell_it(mesh_ptr, tdim); !cell_it.is_end();
         ++cell_it) {

      // check ownership of the cell
      std::vector<int> c_dofs = cell_dofs[cell_it->local_index()].dofs;

      if (rank == cell_it->sub_domain_id()) {
        std::copy(c_dofs.begin(), c_dofs.end(),
                  std::inserter(interior_dofs_collector,
                                interior_dofs_collector.end()));
      } else {
        std::copy(c_dofs.begin(), c_dofs.end(),
                  std::inserter(d_map.ghost_dofs[cell_it->sub_domain_id()],
                                d_map.ghost_dofs[cell_it->sub_domain_id()].end()));
      }

    } // for cell_it

    // facet dofs
    std::map<int, Space::Facet_dofs> facet_dofs = space.facet_dofs(i);

    // loop over facets
    for (Mesh::EntityIterator facet_it(mesh_ptr, tdim-1); !facet_it.is_end();
         ++facet_it) {

      std::vector<std::vector<int>> f_dofs =
                                   facet_dofs[facet_it->local_index()].dofs;

      // if the facet is on boundary, no action is needed
      if ((f_dofs.size() == 1)
          || is_all_same(facet_dofs[facet_it->local_index()].cell_sub_domain_id)) {
        continue;
      }

      // loop over connected cells
      int counter = 0;
      for (Mesh::EntityIncidentIterator cell_it(*facet_it, tdim); !cell_it.is_end();
           ++cell_it) {

        if (cell_it->sub_domain_id() > rank) {
          for (const int& element : f_dofs[counter]) {
            d_map.ghost_dofs[cell_it->sub_domain_id()].erase(element);
          }
        } else if (cell_it->sub_domain_id() < rank) {
          for (const int& element : f_dofs[counter]) {
            interior_dofs_collector.erase(element);
          }
        }

        ++counter;
      } // for cell_it

    } // for facet_it

    // assign to interior_dofs_range by taking min and max of interior_dofs_collector
    const auto [min_e, max_e] = std::ranges::minmax_element(
                                  interior_dofs_collector);
    d_map.interior_dofs_range = {*min_e, *max_e};

    // check the interior dofs are consecutive
    assert(d_map.interior_dofs_range[0] < d_map.interior_dofs_range[1]);
    assert((d_map.interior_dofs_range[1]-d_map.interior_dofs_range[0]+1) ==
           interior_dofs_collector.size());


    // assign the d_map to member variable
    dof_maps_[i] = d_map;

  } // for i

  assert(dof_maps_.size() == n_var_);
}

// global dof offsets
template<class DT>
std::vector<int> LA_Structure<DT>::global_dof_offsets() const {
  return global_dof_offsets_;
}

// global interior IDs
template<class DT>
std::vector<int> LA_Structure<DT>::global_interior_IDs() const {
  assert(n_var_ == dof_maps_.size());

  // total IDs for return
  std::vector<int> total_IDs;

  for (int i = 0; i != n_var_; ++i) {
    // define the size
    const int size = dof_maps_[i].interior_dofs_range[1] -
                     dof_maps_[i].interior_dofs_range[0] + 1;

    // pre-allocate the vector
    std::vector<int> IDs(size);

    // fill the values
    std::iota(IDs.begin(), IDs.end(), dof_maps_[i].interior_dofs_range[0]);

    // add offset
    for (int& id : IDs) {
      id += global_dof_offsets_[i];
    }

    // push IDs into total_IDs
    total_IDs.insert(total_IDs.end(), IDs.begin(), IDs.end());

  } // for i

  return total_IDs;
}

template<class DT>
std::vector<int> LA_Structure<DT>::global_interior_IDs(int idx) const {
  // define the size
  const int size = dof_maps_[idx].interior_dofs_range[1] -
                   dof_maps_[idx].interior_dofs_range[0] + 1;

  // pre-allocate the vector
  std::vector<int> IDs(size);

  // fill the values
  std::iota(IDs.begin(), IDs.end(), dof_maps_[idx].interior_dofs_range[0]);

  for (int& id : IDs) {
    id += global_dof_offsets_[idx];
  }

  return IDs;
}

// global ghost IDs
template<class DT>
std::vector<int> LA_Structure<DT>::global_ghost_IDs() const {
  assert(n_var_ == dof_maps_.size());

  // total IDs for return
  std::vector<int> total_IDs;

  for (int i = 0; i != n_var_; ++i) {
    // create ordered keys
    std::set<int> keys;

    for (const auto& pair : dof_maps_[i].ghost_dofs) {
      keys.insert(pair.first);
    }

    // create container
    std::vector<int> IDs;

    // loop over ghost_dofs_
    for (int key : keys) {
      IDs.insert(IDs.end(),
                 dof_maps_[i].ghost_dofs.at(key).begin(),
                 dof_maps_[i].ghost_dofs.at(key).end());
    }

    // add offset
    for (int& id : IDs) {
      id += global_dof_offsets_[i];
    }

    // push IDs into total_IDs
    total_IDs.insert(total_IDs.end(), IDs.begin(), IDs.end());

  } // for i

  return total_IDs;
}

template<class DT>
std::vector<int> LA_Structure<DT>::global_ghost_IDs(int idx) const {
  // create ordered keys
  std::set<int> keys;

  for (const auto& pair : dof_maps_[idx].ghost_dofs) {
    keys.insert(pair.first);
  }

  // create container
  std::vector<int> IDs;

  // loop over ghost_dofs_
  for (int key : keys) {
    IDs.insert(IDs.end(),
               dof_maps_[idx].ghost_dofs.at(key).begin(),
               dof_maps_[idx].ghost_dofs.at(key).end());
  }

  // add offset
  for (int& id : IDs) {
    id += global_dof_offsets_[idx];
  }

  return IDs;
}

// instantiation
template class LA_Structure<double>;
template class LA_Structure<float>;

}; // namespace LA
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Connectivity.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Connectivity::Connectivity() : d0_(-1), d1_(-1), entity_pos_(1),
  connectivities_() {
  assert(entity_pos_.size() == 1);
  assert(entity_pos_[0] == 0);

  entity_pos_[0] = 0;
  // do nothing
}

Connectivity::Connectivity(int d0, int d1) {
  init(d0, d1);
}

// destructor
Connectivity::~Connectivity() {
  // do nothing
}

// clear data
void Connectivity::clear() {
  connectivities_.clear();
  connectivities_.shrink_to_fit();

  entity_pos_.clear();
  entity_pos_.shrink_to_fit();
}

// initilialize with d0 and d1
void Connectivity::init(int d0, int d1) {
  assert(d0 >= 0);
  assert(d1 >= 0);

  d0_ = d0;
  d1_ = d1;
  entity_pos_.resize(1);
  entity_pos_[0] = 0;

  connectivities_.clear();
  connectivities_.shrink_to_fit();
}

// set d0 and d1
void Connectivity::set_d0_d1(int d0, int d1) {
  assert(d0 >= 0);
  assert(d1 >= 0);
  assert(entity_pos_.size() > 0);
  d0_ = d0;
  d1_ = d1;
}

// return d0
int Connectivity::d0() const {
  return d0_;
}

// return d1
int Connectivity::d1() const {
  return d1_;
}

// add connectivity
// return local index
int Connectivity::add_connectivity(std::vector<int> idx) {
  assert(idx.size() >= 0);
  assert(entity_pos_.size() >= 0);

  connectivities_.insert(connectivities_.end(), idx.begin(), idx.end());
  entity_pos_.push_back(entity_pos_.back() + idx.size());

  return (entity_pos_.size() - 2);
}

// number of connectivities
int Connectivity::num_connectivities(int idx) const {
  assert(idx >= 0);
  assert(connectivities_.size() >= 0);
  assert(entity_pos_.size() > 0);

  return (entity_pos_[idx+1] - entity_pos_[idx]);
}

int Connectivity::num_entities() const {
  assert(connectivities_.size() >= 0);
  assert(entity_pos_.size() > 0);

  return (entity_pos_.size() - 1);
}

// get first by position
//int Connectivity::get_entity_index(const int& pos) const{
//  auto it = connectivities_.begin();
//  std::advance(it, pos);
//
//  return it->first;
//}

// get connectivity by local index
std::vector<int> Connectivity::get_connectivities(int loc_idx) const {
  assert(0 <= loc_idx);
  assert(loc_idx < (entity_pos_.size()-1));
  assert(entity_pos_.size() > 0);
  assert(connectivities_.size() > 0);

  const int pos0 = entity_pos_[loc_idx];
  const int pos1 = entity_pos_[loc_idx+1];

  std::vector<int> c(connectivities_.begin() + pos0,
                     connectivities_.begin() + pos1);

  return c;
}

// check connectivity already exists
bool Connectivity::has_connectivity(std::vector<int> entity_connectivity,
                                    std::string direction) const {
  assert(connectivities_.size() >= 0);
  assert(entity_pos_.size() >= 0);
  assert(entity_connectivity.size() > 0);

  if (direction == "forward") {
    return (find_connectivity_forward(entity_connectivity) >= 0);
  } else if (direction == "backward") {
    return (find_connectivity_backward(entity_connectivity) >= 0);
  } else {
    CL_Error("Wrong direction option!");
    exit(EXIT_FAILURE);
  }

}

// find connectivity index
int Connectivity::find_connectivity(std::vector<int> entity_connectivity,
                                    std::string direction) const {
  assert(connectivities_.size() > 0);
  assert(entity_pos_.size() > 0);
  assert(entity_connectivity.size() > 0);

  if (direction == "forward") {
    return find_connectivity_forward(entity_connectivity);
  } else if (direction == "backward") {
    return find_connectivity_backward(entity_connectivity);
  } else {
    CL_Error("Wrong direction option!");
    exit(EXIT_FAILURE);
  }

}

// find connectivity index already exists in forward direction
int Connectivity::find_connectivity_forward(std::vector<int>
    entity_connectivity) const {

  assert(connectivities_.size() >= 0);
  assert(entity_pos_.size() >= 0);
  assert(entity_connectivity.size() > 0);

  // sort entity_connectivity for comparison purpose
  std::sort(entity_connectivity.begin(), entity_connectivity.end());

  // iterate entity_pos_ in forward direction
  for (std::size_t i = 0; i != (entity_pos_.size() - 1); ++i) {
    std::vector<int> c(connectivities_.begin() + entity_pos_[i],
                       connectivities_.begin() + entity_pos_[i+1]);

    // check size
    if (entity_connectivity.size() != c.size()) {
      continue;
    }

    // sort c for comparison purpose
    std::sort(c.begin(), c.end());

    if (entity_connectivity == c) {
      return i;
    }

  }

  return -1;
}

// find connectivity index already exists in backward direction
int Connectivity::find_connectivity_backward(std::vector<int>
    entity_connectivity) const {
  assert(connectivities_.size() >= 0);
  assert(entity_pos_.size() >= 0);
  assert(entity_connectivity.size() > 0);

  // sort entity_connectivity for comparison purpose
  std::sort(entity_connectivity.begin(), entity_connectivity.end());

  // iterate entity_pos_ in backward direction
  const int N = entity_pos_.size();
  for (std::size_t i = 0; i != N - 1; ++i) {
    std::vector<int> c(connectivities_.begin() + entity_pos_[N - i - 2],
                       connectivities_.begin() + entity_pos_[N - i - 1]);

    // check size
    if (entity_connectivity.size() != c.size()) {
      continue;
    }

    // sort c for comparison purpose
    std::sort(c.begin(), c.end());

    if (entity_connectivity == c) {
      return i;
    }

  } // i

  return -1;
}

}; // namespace Mesh
}; // namespace ChenLiu



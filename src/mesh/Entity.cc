// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#include "mesh/Entity.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Entity::Entity(): mesh_ptr_(nullptr), tdim_(-1), local_idx_(-1) {
  // do nothing
}

Entity::Entity(const std::shared_ptr<const Mesh> mesh_ptr, int tdim,
               int local_idx): mesh_ptr_(mesh_ptr) {
  assert(0 <= tdim <= 3);
  assert(local_idx >= 0);

  tdim_ = tdim;
  local_idx_ = local_idx;
}

Entity::Entity(Mesh& mesh, int tdim,
               int local_idx): mesh_ptr_(std::make_shared<Mesh>(mesh)) {
  assert(0 <= tdim <= 3);
  assert(local_idx >= 0);

  tdim_ = tdim;
  local_idx_ = local_idx;
}

// destructor
Entity::~Entity() {
  // do nothing
}

// topological dimension
int Entity::tdim() const {
  return tdim_;
}

// geometrical dimension
int Entity::gdim() const {
  return mesh_ptr_->gdim();
}

// local index
int Entity::local_index() const {
  assert(tdim_ >= 0);
  assert(local_idx_ >= 0);

  return local_idx_;
}

// global index
int Entity::global_index() const {
  assert(tdim_ >= 0);
  assert(local_idx_ >= 0);

  return mesh_ptr_->topology().get_global_index(tdim_, local_idx_);
}

// mesh
const Mesh& Entity::mesh() const {
  return *mesh_ptr_;
}

// shared pointer mesh
const std::shared_ptr<const Mesh> Entity::mesh_ptr() const {
  return mesh_ptr_;
}

// operators
bool Entity::operator==(const Entity& entity) const {

  return (mesh_ptr_ == entity.mesh_ptr_
          && local_idx_ == entity.local_idx_
          && tdim_ == entity.tdim_
          && global_index() == entity.global_index());
}

bool Entity::operator!=(const Entity& entity) const {
  return !operator==(entity);
}

// cell type
const CellType Entity::cell_type() const {
  // create CellFactory
  CellFactory cell_factory;

  // connectivities to vertices
  std::vector<int> connects = mesh_ptr_->topology().get_connectivities(tdim_, 0,
                              local_idx_);

  std::unique_ptr<Cell> cell = cell_factory.create(tdim_, connects.size());

  return cell->cell_type();
}

// vertices coordinates
const std::vector<Coordinate> Entity::vertices_coordinates() const {
  // connectivities to vertices
  std::vector<int> connects = mesh_ptr_->topology().get_connectivities(tdim_, 0,
                              local_idx_);

  std::vector<Coordinate> coords;

  // loop over vertices
  for (int i : connects) {
    coords.push_back(mesh_ptr_->geometry().coordinate(i));
  }

  return coords;
}

// local index of vertices
const std::vector<int> Entity::vertices_local_index() const {
  const int loc_idx = this->local_index();

  std::vector<int> verts_idx =
    mesh_ptr_->topology().get_connectivities(tdim_, 0, loc_idx);

  return verts_idx;
}

// sub domain id
const int Entity::sub_domain_id() const {
  return mesh_ptr_->sub_domain_id(this->local_idx_);
}

}; // namespace Mesh
}; // namespace ChenLiu


// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Topology.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Topology::Topology() : tdim_(-1), connects_(), idx_l2g_() {
  // do nothing
}

Topology::Topology(int tdim): tdim_(tdim), idx_l2g_() {
  assert(0 < tdim <= 3);

  // initialize connectivities_
  connects_.resize(tdim + 1);
  for (std::size_t i = 0; i != (tdim + 1); ++i) {
    connects_[i].resize(tdim + 1);

    for (std::size_t j = 0; j != (tdim+1); ++j) {
      connects_[i][j].init(i,j);
    }

  }

  // initialize idx_l2g_
  idx_l2g_.resize(tdim + 1);
}

// destructor
Topology::~Topology() {
  clear();
}

// initialization
void Topology::init(int tdim) {
  assert(0 < tdim <= 3);

  // clear
  clear();

  // set tdim
  tdim_ = tdim;

  // initialize connects_
  connects_.resize(tdim + 1);
  for (std::size_t i = 0; i != (tdim + 1); ++i) {
    connects_[i].resize(tdim + 1);
  }

  // initialize idx_l2g_
  idx_l2g_.resize(tdim + 1);
}

// clear
void Topology::clear() {

  // clear all connectivities
  connects_.clear();
  connects_.shrink_to_fit();

  // clear all idx_l2g
  idx_l2g_.clear();
  idx_l2g_.shrink_to_fit();

}

// number of entities
int Topology::num_entities(int tdim) const {
  assert(0 <= tdim);
  assert(tdim <= tdim_);

  return connects_[tdim][0].num_entities();
}

// tdim
int Topology::tdim() const {
  assert(0 <= tdim_ <= 3);

  return tdim_;
}

// add connectivity
int Topology::add_connectivity(int d0, const int d1, std::vector<int> idx,
                               bool check_rpl) {
  assert(0 <= d0 <= tdim_ );
  assert(0 <= d1 <= tdim_);
  assert(idx.size() >= 0);

  if (check_rpl) {

    if (!connects_[d0][d1].has_connectivity(idx)) {
      return connects_[d0][d1].add_connectivity(idx);
    } else {
      return connects_[d0][d1].find_connectivity(idx);
    }

  } else {
    return connects_[d0][d1].add_connectivity(idx);
  }

}

// get global index
void Topology::set_global_index(int tdim, int loc_idx, int glo_idx) {
  assert(0 <= tdim <= tdim_);
  assert(loc_idx >= 0);
  assert(glo_idx >= 0);
  assert(loc_idx < idx_l2g_[tdim].size());

  idx_l2g_[tdim][loc_idx] = glo_idx;
}

// add global index
int Topology::add_global_index(int tdim, int glo_idx) {
  assert(0 <= tdim <= tdim_);
  assert(glo_idx >= -1);

  idx_l2g_[tdim].push_back(glo_idx);

  return (idx_l2g_[tdim].size() - 1);
}

// find entity
int Topology::find_entity_local(int tdim, std::vector<int> loc_idx,
                                std::string direction) const {
  assert(0 <= tdim <= tdim_);
  assert(loc_idx.size() > 0);

  if (direction == "forward") {
    return connects_[tdim][0].find_connectivity_forward(loc_idx);
  } else if (direction == "backward") {
    return connects_[tdim][0].find_connectivity_backward(loc_idx);
  } else {
    CL_Error("Wrong search direction");
    exit(EXIT_FAILURE);
  }

}

// get global index
int Topology::get_global_index(int tdim, int loc_idx) const {
  assert(0 <= tdim <= tdim_);
  assert(loc_idx >= 0);
  assert(loc_idx < idx_l2g_[tdim].size());

  return  idx_l2g_[tdim][loc_idx];
}

// get global indices
std::vector<int> Topology::get_global_indices(int tdim) const {
  assert(0 <= tdim <= tdim_);
  assert(idx_l2g_[tdim].size() > 0) ;

  return idx_l2g_[tdim];
}

// initialize idx_l2g
void Topology::init_idx_l2g(int tdim, int n) {
  assert(0 <= tdim <= tdim_);
  assert(0 < n);

  idx_l2g_[tdim].clear();
  idx_l2g_[tdim].resize(n);
  std::iota(std::begin(idx_l2g_[tdim]), std::end(idx_l2g_[tdim]), 0);
}

// initialize idx_l2g with value
void Topology::init_idx_l2g(int tdim, int n, int val) {
  assert(0 <= tdim <= tdim_);
  assert(0 < n);

  idx_l2g_[tdim].clear();
  idx_l2g_[tdim].resize(n, val);
}

// get connectivity by local index
std::vector<int> Topology::get_connectivities(int d0, int d1,
    int loc_idx) const {
  assert(0 <= d0 <= tdim_);
  assert(0 <= d1 <= tdim_);
  assert(loc_idx >= 0);

  return connects_[d0][d1].get_connectivities(loc_idx);
}

// clear connectivities d0 -> d1
void Topology::clear_connectivities(int d0, int d1) {
  connects_[d0][d1].init(d0, d1);
}

// compute connecitivies d -> 0
// assume connectivity tdim_  -> 0 is available
void Topology::compute_connectivities_d2zero(int d, bool check_rpl) {
  assert(0 <= d < tdim_);
  assert(connects_[tdim_][0].num_entities() > 0);

  // check the number of entities, if more than 0,
  // clear the connecitivity, and re-initialize
  //if (connects_[0][d].num_entities() > 0) {
  //  connects_[d][0].clear();
  //  connects_[d][0].init(d,0);
  //}

  // create cell factory
  CellFactory cell_factory;

  // get number of entities on topological dimension tdim_
  const int num_e = connects_[tdim_][0].num_entities();

  for (std::size_t n_e = 0; n_e != num_e; ++n_e) {
    // number of vertices of this entity
    const int n_verts = connects_[tdim_][0].num_connectivities(n_e);

    // get connectivities of this entity
    const std::vector<int> connects_e = connects_[tdim_][0].get_connectivities(n_e);

    // create a cell
    std::unique_ptr<Cell> cell(cell_factory.create(tdim_, n_verts));

    // get numeration for sub entities with topological dimension d
    std::vector<std::vector<int>> sub_entities_num =
                                 cell->sub_entities_numeration(d);

    for (std::size_t i = 0; i != sub_entities_num.size(); ++i) {
      // get numeration for sub entity tdim = d
      std::vector<int> entity_connectivity(sub_entities_num[i].size());

      for (std::size_t j = 0; j != sub_entities_num[i].size(); ++j) {
        entity_connectivity[j] = connects_e[sub_entities_num[i][j]];
      }

      const int id = add_connectivity(d, 0, entity_connectivity, check_rpl);
      assert(id >= 0);

      //// add found sub entity into connectivity
      //if (!connects_[d][0].has_connectivity(entity_connectivity, "backward")) {
      //  const int id = connects_[d][0].add_connectivity(entity_connectivity);
      //  assert(id >= 0);
      //}

    } // i

  } // n_e

}

// compute connectivity d0 -> d1 and d0 > d1
// assume d0 -> 0 is already available
void Topology::compute_connectivities_h2l(int d0, int d1, bool check_rpl) {
  assert(0 <= d1);
  assert(d1 < d0);
  assert(connects_[d0][0].num_entities() > 0);
  assert(connects_[d1][0].num_entities() > 0);

  // create cell factory
  CellFactory cell_factory;

  // get number of entities for topological dimension d0
  const int n_entities = connects_[d0][0].num_entities();

  for (std::size_t n_e = 0; n_e != n_entities; ++n_e) {
    // number of vertices for entity
    const int n_verts = connects_[d0][0].num_connectivities(n_e);

    // index of vertices for entity
    const std::vector<int> verts_idx = connects_[d0][0].get_connectivities(n_e);

    // create a cell
    std::unique_ptr<Cell> cell(cell_factory.create(d0, n_verts));

    // get numeration for sub entities with topological dimension d1
    std::vector<std::vector<int>> sub_entities_numeration =
                                 cell->sub_entities_numeration(d1);

    // index for d1 entities within d0 entity
    std::vector<int> d1_idx(sub_entities_numeration.size(), -1);

    for (std::size_t i = 0; i != sub_entities_numeration.size(); ++i) {

      std::vector<int> entity_connectivity(sub_entities_numeration[i].size());
      for (std::size_t j = 0; j != sub_entities_numeration[i].size(); ++j) {
        entity_connectivity[j] = verts_idx[sub_entities_numeration[i][j]];
      }

      const int idx = find_entity_local(d1, entity_connectivity);

      if (idx >= 0) {
        d1_idx[i] = idx;
      } else {
        CL_Error("Sub entity does not exist!");
        exit(EXIT_FAILURE);
      }
    } // i

    // add connectivity into connects_
    add_connectivity(d0, d1, d1_idx, check_rpl);

  } // n_e

}

// compute connectivity d0 -> d1 and d0 < d1
void Topology::compute_connectivities_l2h(int d0, int d1, bool check_rpl) {
  assert(d0 >= 0);
  assert(d1 > d0);
  assert(d1 <= tdim_);
  assert(connects_[d1][d0].num_entities() > 0);

  // clear connectivities d0 -> d1
  clear_connectivities(d0, d1);

  std::unordered_map<int, std::set<int>> connects_map;

  const int n_e = connects_[d1][d0].num_entities();
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> d0_entities = connects_[d1][d0].get_connectivities(i);

    for (std::size_t j = 0; j != d0_entities.size(); ++j) {
      connects_map[d0_entities[j]].insert(i);
    }
  } // n_e

  // assign information from connects_map to connects_[d0][d1]
  for (std::size_t i = 0; i != connects_map.size(); ++i) {
    std::set<int> set_idx = connects_map[i];

    assert(set_idx.size() > 0);

    // convert set to vector
    std::vector<int> idx(set_idx.begin(), set_idx.end());

    // add idx into connects_
    add_connectivity(d0, d1, idx, check_rpl);
  }

}

// compute connectivity d0 -> d1 and d0 = d1
void Topology::compute_connectivities_d2d(int d, bool check_rpl) {
  assert(d >= 0);
  assert(d <= 3);
  assert(connects_[tdim_][0].num_entities() > 0);

  if (d < tdim_) {
    assert(connects_[tdim_][d].num_entities() > 0);
  }

  if (d > 0) {
    assert(connects_[d][0].num_entities() > 0);
  }

  // clear connectivities
  clear_connectivities(d, d);

  std::unordered_map<int, std::set<int>> connects_map;

  // number of entities on tdim = dd
  const int dd = d > 0 ? d : tdim_;
  const int n_e = connects_[dd][0].num_entities();

  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> zero_entities = connects_[dd][0].get_connectivities(i);

    for (std::size_t j = 0; j != zero_entities.size(); ++j) {
      if (d > 0) {
        connects_map[zero_entities[j]].insert(i);
      } else {
        for (std::size_t k = 0; k != zero_entities.size(); ++k) {
          connects_map[zero_entities[j]].insert(zero_entities[k]);
        }
      }

    } // j

  } // n_e

  // number of entities on tdim = d
  const int ddd = d < tdim_ ? tdim_ : 0;
  const int n_e_d = connects_[d][ddd].num_entities();

  for (std::size_t i = 0; i != n_e_d; ++i) {
    std::vector<int> d_entities = d > 0 ? connects_[d][0].get_connectivities(i)
                                  : std::vector<int>(1, i);

    std::set<int> idx;
    for (std::size_t j = 0; j != d_entities.size(); ++j) {
      // get set<int> for vertex in entity
      std::set<int> set_idx = connects_map[d_entities[j]];

      assert(set_idx.size() > 0);

      idx.merge(set_idx);
    }

    // remove the own entity index
    idx.extract(i);

    std::vector<int> v_idx(idx.begin(), idx.end());

    // add idx into connects_
    add_connectivity(d, d, v_idx, check_rpl);

  }// n_e_d

}

}; // namespace Mesh
}; // namespace ChenLiu

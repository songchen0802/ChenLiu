// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Mesh.hh"
#include "mesh/Iterators.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Mesh::Mesh() {
  // do nothing
}

// destructor
Mesh::~Mesh() {
  // do nothing
}

// clear
void Mesh::clear() {
  geometry_.clear();
  topology_.clear();

  material_id_.clear();
  sub_domain_id_.clear();
}

// tdim
int Mesh::tdim() const {
  assert(topology_.tdim() > 0);
  return topology_.tdim();
}

// gdim
int Mesh::gdim() const {
  assert(geometry_.gdim() > 0);
  return geometry_.gdim();
}

// Topology
const Topology& Mesh::topology() const {
  return topology_;
}

// Geometry
const Geometry& Mesh::geometry() const {
  return geometry_;
}

// number of entities
int Mesh::num_entities(int tdim) const {
  return topology_.num_entities(tdim);
}

// material id
int Mesh::material_id(int tdim, int idx) const {
  return material_id_.at(tdim).get_marker(idx);
}

// sub domain id
int Mesh::sub_domain_id(int idx) const {
  return sub_domain_id_.get_marker(idx);
}

}; // namespace Mesh
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Graph.hh"

namespace ChenLiu {

namespace Mesh {

// constructor
Graph::Graph(const std::shared_ptr<const Mesh> mesh_ptr) {
  create(mesh_ptr);
}

// destructor
Graph::~Graph() {
  // use member function clear()
  clear();
}

// create
void Graph::create(const std::shared_ptr<const Mesh> mesh_ptr) {

  // number of nodes
  int tdim = mesh_ptr->topology().tdim();
  int num_nodes = mesh_ptr->topology().num_entities(tdim);


  // fill node_offsets_ and ajd_nodes_
  node_offsets_.clear();
  node_offsets_.push_back(0); // first value is 0
  adj_nodes_.clear();

  for (int i = 0; i != num_nodes; ++i) {
    std::vector<int> idx_nodes = mesh_ptr->topology().
                                 get_connectivities(tdim, tdim, i);
    assert(idx_nodes.size() > 0);

    const int curr_num = node_offsets_.back();
    node_offsets_.push_back(curr_num + idx_nodes.size());

    for (int idx :idx_nodes) {
      adj_nodes_.push_back(idx);
    }

  }

  num_node_wgts_ = 1;
  node_wgts_.clear();
  edge_wgts_.clear();
}

// clear
void Graph::clear() {
  node_offsets_.clear();
  node_offsets_.shrink_to_fit();

  adj_nodes_.clear();
  adj_nodes_.shrink_to_fit();

  node_wgts_.clear();
  node_wgts_.shrink_to_fit();

  edge_wgts_.clear();
  edge_wgts_.shrink_to_fit();
}

// add vertex weights
void Graph::add_node_weights(std::vector<int> node_wgts) {
  CL_Error("Not yet implemented!");
  exit(EXIT_FAILURE);
}

// add edge weights
void Graph::add_edge_weights(std::vector<int> edge_wgts) {
  CL_Error("Not yet implemented!");
  exit(EXIT_FAILURE);
}

// return node offsets
std::vector<int> Graph::node_offsets() const {
  return node_offsets_;
}

// return adjacent nodes
std::vector<int> Graph::adjacent_nodes() const {
  return adj_nodes_;
}

// return vertex weights
std::vector<int> Graph::node_weights() const {
  return node_wgts_;
}

// return edge weights
std::vector<int> Graph::edge_weights() const {
  return edge_wgts_;
}


// return number of vertex weights
int Graph::num_node_weights() const {
  return num_node_wgts_;
}

}; // namespace Mesh

}; // namespace ChenLiu

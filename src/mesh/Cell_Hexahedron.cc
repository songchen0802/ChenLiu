// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Cell_Hexahedron.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Cell_Hexahedron::Cell_Hexahedron() : Cell(CellType::Hexahedron) {
  // do nothing
}

// topological dimension
int Cell_Hexahedron::tdim() const {
  return 3;
}

// number of vertices
int Cell_Hexahedron::num_vertices() const {
  return 8;
}

// number of entities for each topological dimension
int Cell_Hexahedron::num_entities(int tdim) const {
  assert(0 <= tdim <= 3);

  switch(tdim) {
  case 0:
    return 8;
  case 1:
    return 12;
  case 2:
    return 6;
  case 3:
    return 1;
  default:
    CL_Error("Wrong topological dimension!");
    exit(EXIT_FAILURE);
  }

}

// check cell geometry
bool Cell_Hexahedron::check_geometry(std::vector<Coordinate> coords,
                                    int gdim) const {
  assert(gdim == 3);

  // check number of vertices
  if (coords.size() != 8) {
    return false;
  }

  Vec<Coord_T> v1(3), v2(3), v3(3), v4(3);
  for (std::size_t i = 0; i < 3; ++i) {
    v1(i) = coords[1][i] - coords[0][i];
    v2(i) = coords[2][i] - coords[0][i];
    v3(i) = coords[3][i] - coords[0][i];
    v4(i) = coords[4][i] - coords[0][i];
  }

  // TODO: think whether if check here is enough
  const Coord_T orientation1 = Dot(Cross(v1, v2), Cross(v2, v3));
  const Coord_T orientation2 = Dot(Cross(v1, v2), v4);

  return ((orientation1 > 0.0) && (orientation2 > 0.0));
}

// numeration for sub entities
std::vector<std::vector<int>> Cell_Hexahedron::sub_entities_numeration(
int tdim) const {
  assert(0 <= tdim <= 2);

  int n_e = -1;
  std::vector<std::vector<int>> loc_indices;

  switch(tdim) {
  case 0:
    n_e = 8;
    loc_indices.resize(n_e);
    // point 0
    loc_indices[0] = {0};
    // point 1
    loc_indices[1] = {1};
    // point 2
    loc_indices[2] = {2};
    // point 3
    loc_indices[3] = {3};
    // point 4
    loc_indices[4] = {4};
    // point 5
    loc_indices[5] = {5};
    // point 6
    loc_indices[6] = {6};
    // point 7
    loc_indices[7] = {7};

    return loc_indices;
    break;

  case 1:
    n_e = 12;
    loc_indices.resize(n_e);
    // line 0
    loc_indices[0] = {0, 1};
    // line 1
    loc_indices[1] = {1, 2};
    // line 2
    loc_indices[2] = {2, 3};
    // line 3
    loc_indices[3] = {3, 0};
    // line 4
    loc_indices[4] = {4, 5};
    // line 5
    loc_indices[5] = {5, 6};
    // line 6
    loc_indices[6] = {6, 7};
    // line 7
    loc_indices[7] = {7, 4};
    // line 8
    loc_indices[8] = {0, 4};
    // line 9
    loc_indices[9] = {1, 5};
    // line 10
    loc_indices[10] = {2, 6};
    // line 11
    loc_indices[11] = {3, 7};

    return loc_indices;
    break;

  case 2:
    n_e = 6;
    loc_indices.resize(n_e);
    // quadrilateral 0
    loc_indices[0] = {0, 1, 2, 3};
    // quadrilateral 1
    loc_indices[1] = {4, 5, 6, 7};
    // quadrilateral 2
    loc_indices[2] = {0, 1, 5, 4};
    // quadrilateral 3
    loc_indices[3] = {1, 2, 6, 5};
    // quadrilateral 4
    loc_indices[4] = {2, 3, 7, 6};
    // quadrilateral 5
    loc_indices[5] = {0, 4, 7, 3};

    return loc_indices;
    break;

  default:
    CL_Error("Wrong topological dimension of sub entity");
    exit(EXIT_FAILURE);
  }

}

}; // namespace Mesh
}; // namespace ChenLiut

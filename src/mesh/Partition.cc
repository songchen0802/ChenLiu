// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Partition.hh"
#include "mesh/Editor.hh"

namespace ChenLiu {

namespace Mesh {

// partition mesh
std::shared_ptr<Mesh> Partition::partition_mesh(
  const std::shared_ptr<const Mesh> mesh_ptr, const MPI_Comm& comm,
  const int master_rank, std::string partitioner) {

  assert(partitioner == "METIS");
  assert(comm != MPI_COMM_NULL);

  // number of partitions
  int n_parts;
  MPI_Comm_size(comm, &n_parts);
  if (n_parts == 1) {
    return std::make_shared<Mesh>(*mesh_ptr);
  }

  if (partitioner == "METIS") {
    return Metis_partitioner(mesh_ptr, comm, master_rank);
  } else {
    CL_Error("Not yet implemented!");
    exit(EXIT_FAILURE);
  }

}

// add ghost layer
std::shared_ptr<Mesh> Partition::add_ghost_layer(
  const std::shared_ptr<const Mesh> mesh_ptr, const MPI_Comm& comm) {
  assert(comm != MPI_COMM_NULL);

  // create new shared_ptr pointing a copy of the content of mesh_ptr
  std::shared_ptr mesh_ptr_new = std::make_shared<Mesh>(*mesh_ptr);

  // rank
  int rank;
  MPI_Comm_rank(comm, &rank);

  // number of processors
  int n_parts;
  MPI_Comm_size(comm, &n_parts);

  // topological dimension of mesh
  const int tdim = mesh_ptr->tdim();

  // geometrical dimension of mesh
  const int gdim = mesh_ptr->gdim();

  // topological dimension of facet
  const int tdim_facet = tdim - 1;

  //// 1. Collect vertices on boundaries
  // vertices on local boundaries
  std::map<int, Coordinate> boundary_verts = collect_boundary_vertices(mesh_ptr);

  //// 2. Obtain all boundary vertices of each processor
  // create VertexInfo
  VertexInfo send_verts;

  // assign VetexInfo to send
  send_verts.gdim = gdim;
  send_verts.n_verts = boundary_verts.size();

  assert(send_verts.coords.size() == 0);
  send_verts.coords.clear();
  for (const auto& ele : boundary_verts) {
    Coordinate coord = mesh_ptr->geometry().coordinate(ele.first);

    std::vector<Coord_T> coord_vec = coord.get_coord();
    send_verts.coords.insert(send_verts.coords.end(), coord_vec.begin(),
                             coord_vec.end());
  }

  // receive vector for VertexInfo
  std::vector<VertexInfo> recv_verts(n_parts);

  // all gather VertexInfo cross sub-domains
  Communication::CL_MPI_Allgather mpi_allgather(comm);
  mpi_allgather.execute(send_verts, recv_verts);
  mpi_allgather.clear();

  //// 3. Identify common vertices
  // shared_vertices: key: sub domains. value: vertices index
  std::map<int, std::set<int>> shared_vertices = identify_common_vertices(
                              boundary_verts, recv_verts, gdim, rank);

  //// 4. Create cells and material ids to be sent
  // collect cells to send
  std::map<int, std::set<int>> send_cell_idx;
  for (const auto ele : shared_vertices) {

    for (const int i : ele.second) {
      std::vector<int> idx_cells = mesh_ptr->topology().
                                   get_connectivities(0, tdim, i);
      // insert index of cells, which are connected to boundary vertices
      std::copy(idx_cells.begin(), idx_cells.end(),
                std::inserter(send_cell_idx[ele.first], send_cell_idx[ele.first].end()));

    } // i
  } // ele

  // container for cell info and material_ids
  std::map<int, EntityInfo> send_cell_info;
  std::map<int, std::map<int, MaterialIdInfo>> send_material_ids;
  std::map<int, std::map<int, GlobalIndexInfo>> send_global_indices;

  for (const auto& ele : send_cell_idx) {
    const int sub_d = ele.first;

    // makers
    const int num_cells = mesh_ptr->topology().num_entities(tdim);
    const int marker_val = 1;
    std::vector<int> marker(num_cells, 0);
    for (const int& id : ele.second) {
      marker[id] = marker_val;
    }

    // cell info and vertex index permutation
    std::map<int, int> v_idx_permut_send = collect_cell_info(mesh_ptr,
                                           send_cell_info[sub_d], marker, marker_val);

    // collect material id and global indices
    for (int d = 0; d != tdim; ++d) {
      // material id
      collect_cell_material_ids(mesh_ptr, send_material_ids[d][sub_d], d,
                                marker, marker_val, v_idx_permut_send);

      // global index
      collect_cell_global_indices(mesh_ptr, send_global_indices[d][sub_d], d,
                                  marker, marker_val, v_idx_permut_send);

    }

  }

  //// 5. point-to-point communication on cells

  // send
  std::vector<MPI_Request> send_cell_reqs;

  Communication::CL_MPI_Isend mpi_isend(comm);
  for (const auto& ele : send_cell_info) {
    const int send_proc = ele.first;

    std::vector<MPI_Request> tmp_reqs;
    mpi_isend.execute(send_cell_info[send_proc], send_proc, tmp_reqs);

    send_cell_reqs.insert(send_cell_reqs.end(), tmp_reqs.begin(), tmp_reqs.end());
  }

  // receive
  std::map<int, EntityInfo> recv_cell_info;
  std::vector<MPI_Request> recv_cell_reqs;

  Communication::CL_MPI_Irecv mpi_irecv(comm);
  for (const auto& ele : send_cell_info) {
    const int recv_proc = ele.first;

    std::vector<MPI_Request> tmp_reqs;
    mpi_irecv.execute(recv_cell_info[recv_proc], recv_proc, tmp_reqs);

    recv_cell_reqs.insert(recv_cell_reqs.end(), tmp_reqs.begin(), tmp_reqs.end());
  }

  // mpi wait
  MPI_Waitall(send_cell_reqs.size(), send_cell_reqs.data(), MPI_STATUSES_IGNORE);
  MPI_Waitall(recv_cell_reqs.size(), recv_cell_reqs.data(), MPI_STATUSES_IGNORE);

  //// 6. add ghost cells into interior mesh
  std::map<int, std::map<int, int>> v_idx_permut_recv;
  Editor mesh_editor;
  for (const auto& ele : recv_cell_info) {
    const int sub_d = ele.first;
    v_idx_permut_recv[sub_d] = mesh_editor.add_cells(mesh_ptr_new,
                               ele.second, boundary_verts,
                               sub_d, true);
  }

  //// 7. point-to-point communication on material ids
  std::map<int, std::map<int, MaterialIdInfo>> recv_material_ids;

  for (int d = 0; d != tdim; ++d) {
    // send
    std::vector<MPI_Request> send_mat_id_reqs;

    for (const auto& ele : send_material_ids[d]) {
      const int send_proc = ele.first;

      std::vector<MPI_Request> tmp_reqs;
      mpi_isend.execute(send_material_ids[d][send_proc], send_proc, tmp_reqs);

      send_mat_id_reqs.insert(send_mat_id_reqs.end(), tmp_reqs.begin(),
                              tmp_reqs.end());
    }

    // receive
    std::vector<MPI_Request> recv_mat_id_reqs;

    for (const auto& ele : send_material_ids[d]) {
      const int recv_proc = ele.first;

      std::vector<MPI_Request> tmp_reqs;
      mpi_irecv.execute(recv_material_ids[d][recv_proc], recv_proc, tmp_reqs);

      recv_mat_id_reqs.insert(recv_mat_id_reqs.end(), tmp_reqs.begin(),
                              tmp_reqs.end());
    }

    // mpi wait
    MPI_Waitall(recv_mat_id_reqs.size(), recv_mat_id_reqs.data(),
                MPI_STATUSES_IGNORE);
    MPI_Waitall(send_mat_id_reqs.size(), send_mat_id_reqs.data(),
                MPI_STATUSES_IGNORE);
  }

  //// 8. add material ids on sub entities

  // loop over tdim
  for (int d = 0; d != tdim; ++d) {
    // loop over sub domain
    for (auto& ele : recv_material_ids[d]) {
      const int sub_d = ele.first;

      if (ele.second.n_entities == 0) {
        continue;
      } else {
        // permut vertex index
        for (int& idx : ele.second.connections) {
          idx = v_idx_permut_recv[sub_d][idx];
        } // idx

        // assign material number
        mesh_editor.add_material_ids(mesh_ptr_new, recv_material_ids[d][sub_d]);
      }

    } // ele

  } // d

  //// 9. point-to-point communication on global indices
  std::map<int, std::map<int, GlobalIndexInfo>> recv_global_indices;

  for (int d = 0; d != tdim; ++d) {
    // send
    std::vector<MPI_Request> send_glo_idx_reqs;

    for (const auto& ele : send_global_indices[d]) {
      const int send_proc = ele.first;

      std::vector<MPI_Request> tmp_reqs;
      mpi_isend.execute(send_global_indices[d][send_proc], send_proc, tmp_reqs);

      send_glo_idx_reqs.insert(send_glo_idx_reqs.end(), tmp_reqs.begin(),
                               tmp_reqs.end());
    }

    // receive
    std::vector<MPI_Request> recv_glo_idx_reqs;

    for (const auto& ele : send_global_indices[d]) {
      const int recv_proc = ele.first;

      std::vector<MPI_Request> tmp_reqs;
      mpi_irecv.execute(recv_global_indices[d][recv_proc], recv_proc, tmp_reqs);

      recv_glo_idx_reqs.insert(recv_glo_idx_reqs.end(), tmp_reqs.begin(),
                               tmp_reqs.end());
    }

    // mpi wait
    MPI_Waitall(recv_glo_idx_reqs.size(), recv_glo_idx_reqs.data(),
                MPI_STATUSES_IGNORE);
    MPI_Waitall(send_glo_idx_reqs.size(), send_glo_idx_reqs.data(),
                MPI_STATUSES_IGNORE);
  }

  // clear mpi isend and irecv
  mpi_isend.clear();
  mpi_irecv.clear();

  //// 10. add global indices on sub entities

  // loop over tdim
  for (int d = 0; d != tdim; ++d) {
    // loop over sub domain
    for (auto& ele : recv_global_indices[d]) {
      const int sub_d = ele.first;

      if (ele.second.n_entities == 0) {
        continue;
      } else {
        // permut vertex index
        for (int& idx : ele.second.connections) {
          idx = v_idx_permut_recv[sub_d][idx];
        } // idx

        // assign material number
        mesh_editor.add_global_indices(mesh_ptr_new, recv_global_indices[d][sub_d]);
      }

    } // ele

  } // d

  return mesh_ptr_new;
}

std::shared_ptr<Mesh> Partition::Metis_partitioner(
  const std::shared_ptr<const Mesh> mesh_ptr, const MPI_Comm &comm,
  const int master_rank) {

  assert(comm != MPI_COMM_NULL);

  // MPI rank
  int rank;
  MPI_Comm_rank(comm, &rank);

  // partition marker
  std::vector<int> partition_marker;

  if (rank == master_rank) {

    // create Graph from mesh_ptr
    Graph graph(mesh_ptr);

    // Metis options
    int options[METIS_NOPTIONS];
    METIS_SetDefaultOptions(options);
    options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_VOL;
    options[METIS_OPTION_NUMBERING] = 0;
    options[METIS_OPTION_MINCONN] = 1;
    options[METIS_OPTION_CONTIG] = 1;

    // number of vertices
    int nvtxs = graph.node_offsets().size() - 1;

    // number of balancing constraints
    int ncon = graph.num_node_weights();

    // size of the vertices for computing the total communication volume
    int* vsize = nullptr;

    // target partitione weights
    float* tpwgts = nullptr;

    // tolerance for load imbalance
    float* ubvec = nullptr;

    // edge cut
    int edge_cut;

    // number of partitions
    int n_parts;
    MPI_Comm_size(comm, &n_parts);

    // resize partition marker
    partition_marker.resize(nvtxs);

    METIS_PartGraphKway(&nvtxs, &ncon, graph.node_offsets().data(),
                        graph.adjacent_nodes().data(),
                        graph.node_weights().data(), vsize,
                        graph.edge_weights().data(), &n_parts,
                        tpwgts, ubvec, options, &edge_cut, partition_marker.data());
  }

  return create_distributed_mesh(mesh_ptr, comm, master_rank, partition_marker);
}

std::shared_ptr<Mesh> Partition::create_distributed_mesh(
  const std::shared_ptr<const Mesh> master_mesh_ptr, const MPI_Comm &comm,
  const int master_rank, std::vector<int> partition_marker) {

  // MPI rank
  int rank;
  MPI_Comm_rank(comm, &rank);

  // MPI size
  int n_parts;
  MPI_Comm_size(comm, &n_parts);

  // topological dimension
  const int tdim = master_mesh_ptr->tdim();

  // collect entity information on cell level
  std::vector<EntityInfo> cell_info(n_parts);

  // collect entity global indices and  material ids
  std::map<int, std::vector<MaterialIdInfo>> send_material_ids;
  std::map<int, std::vector<GlobalIndexInfo>> send_global_indices;
  for (int d = 0; d != tdim; ++d) {
    send_material_ids[d] = std::vector<MaterialIdInfo>(n_parts);
    send_global_indices[d] = std::vector<GlobalIndexInfo>(n_parts);
  }

  if (rank == master_rank) {
    for (int sub_d = 0; sub_d != n_parts; ++sub_d) {
      // get cell info for each sub domain and vertex permutation
      std::map<int, int> v_idx_permut = collect_cell_info(master_mesh_ptr,
                                        cell_info[sub_d], partition_marker, sub_d);

      for (int d = 0; d != tdim; ++d) {
        // material ids
        collect_cell_material_ids(master_mesh_ptr,
                                  send_material_ids[d][sub_d], d,
                                  partition_marker, sub_d, v_idx_permut);

        // global indices
        collect_cell_global_indices(master_mesh_ptr,
                                    send_global_indices[d][sub_d], d,
                                    partition_marker, sub_d, v_idx_permut);
      } // d

    } // sub_d

  } // rank == master_rank

  // send EntityInfo to processors
  EntityInfo recv_cell_info;
  Communication::CL_MPI_Scatterv mpi_scatter(master_rank, comm);
  mpi_scatter.execute(cell_info, recv_cell_info);

  // send MaterialIdInfo to processors
  std::map<int, MaterialIdInfo> recv_material_ids;
  for (int d = 0; d != tdim; ++d) {
    mpi_scatter.execute(send_material_ids[d], recv_material_ids[d]);
  }

  // send GlobalIndexInfo to processors
  std::map<int, GlobalIndexInfo> recv_global_indices;
  for (int d = 0; d != tdim; ++d) {
    mpi_scatter.execute(send_global_indices[d], recv_global_indices[d]);
  }

  mpi_scatter.clear();

  // create new mesh_ptr from EntityInfo and MaterialIdInfo
  Editor mesh_editor;
  std::shared_ptr<Mesh> distributed_mesh_ptr = std::make_shared<Mesh>();
  mesh_editor.build_mesh(distributed_mesh_ptr, recv_cell_info, true);

  // add material ids
  for (int d = 0; d != tdim; ++d) {
    mesh_editor.add_material_ids(distributed_mesh_ptr, recv_material_ids[d]);
  }

  // add global indices
  for (int d = 0; d != tdim; ++d) {
    mesh_editor.add_global_indices(distributed_mesh_ptr, recv_global_indices[d]);
  }

  // add sub domain id
  mesh_editor.add_sub_domain_ids(distributed_mesh_ptr, rank);

  return distributed_mesh_ptr;
}

// collect cell information for one distributed domain with partition marker
std::map<int, int> Partition::collect_cell_info(
  const std::shared_ptr<const Mesh> mesh_ptr,
  EntityInfo& cell_info, std::vector<int> partition_marker,
  const int marker_val) {

  // topological dimension
  int tdim = mesh_ptr->tdim();

  // geometrical dimension
  int gdim = mesh_ptr->gdim();

  // initialize EntityInfo for desired sub domain
  cell_info.tdim = tdim;
  cell_info.gdim = gdim;

  cell_info.n_verts = 0;
  cell_info.coords.clear();

  cell_info.n_entities = 0;
  cell_info.connections.clear();
  cell_info.entity_offsets.clear();
  cell_info.entity_offsets.push_back(0);
  cell_info.material_ids.clear();
  cell_info.global_indices.clear();

  // number of cells
  const int n_cells = mesh_ptr->topology().num_entities(tdim);

  // vertices index permutation
  std::map<int, int> v_idx_permut;

  for (std::size_t i = 0; i != n_cells; ++i) {
    if (partition_marker[i] != marker_val) {
      continue;
    }

    // compute number of entities in sub domain
    ++cell_info.n_entities;

    // vertices index associated to cell
    std::vector<int> idx_verts = mesh_ptr->topology().
                                 get_connectivities(tdim, 0, i);

    // cell offsets
    const int last = cell_info.entity_offsets.back();
    cell_info.entity_offsets.push_back(last + idx_verts.size());

    // material id
    cell_info.material_ids.push_back(mesh_ptr->material_id(tdim, i));

    // global index
    cell_info.global_indices.push_back(mesh_ptr->topology().get_global_index(tdim,
                                       i));

    // connections
    const std::vector<int>::iterator c_end = cell_info.connections.end();

    cell_info.connections.insert(c_end, idx_verts.begin(), idx_verts.end());

    for (const int& idx_1 : idx_verts) {
      if (v_idx_permut.count(idx_1) == 0) {
        // compute permutation
        // idx_1: orignal index for vertex
        // idx_2: permutated index for vertex
        int idx_2 = cell_info.n_verts;
        v_idx_permut[idx_1] = idx_2;
        ++cell_info.n_verts;

        // insert coordinate vector into coords
        std::vector<Coord_T> co = mesh_ptr->geometry().coordinate(idx_1).get_coord();
        std::vector<Coord_T>::iterator coord_end = cell_info.coords.end();
        cell_info.coords.insert(coord_end, co.begin(), co.end());
      }
    }
  }

  // modify vertex index to new local index
  const int size = cell_info.connections.size();

  for (std::size_t i = 0; i != size; ++i) {
    const int idx_1 = cell_info.connections[i];
    cell_info.connections[i] = v_idx_permut[idx_1];
  }

  return v_idx_permut;

}

// collect global index on cells wrt. partition_marker and marker_val
void Partition::collect_cell_global_indices(
  const std::shared_ptr<const Mesh> mesh_ptr, GlobalIndexInfo& global_idx_info,
  const int e_tdim, std::vector<int> partition_marker, const int marker_val,
  const std::map<int, int> v_idx_permut) {

  // topological dimension
  const int tdim = mesh_ptr->tdim();
  assert(e_tdim < tdim);

  // geometrical dimension
  const int gdim = mesh_ptr->gdim();

  // initialize GlobalIndexInfo
  global_idx_info.tdim = e_tdim;
  global_idx_info.gdim = gdim;

  global_idx_info.n_entities = 0;
  global_idx_info.connections.clear();
  global_idx_info.entity_offsets.clear();
  global_idx_info.entity_offsets.push_back(0);
  global_idx_info.global_indices.clear();

  // entity manager for storing enetity index (local)
  std::set<int> entity_manager;

  // number of entities
  const int n_cells = mesh_ptr->topology().num_entities(tdim);

  for (std::size_t i = 0; i != n_cells; ++i) {
    if (partition_marker[i] != marker_val) {
      continue;
    }

    // get indices of entities within cell
    std::vector<int> idx_entities = mesh_ptr->topology().
                                    get_connectivities(tdim, e_tdim, i);

    for (const int& idx : idx_entities) {

      const int size_o = entity_manager.size();
      entity_manager.insert(idx);

      // check if entity is in entity_manager
      if (entity_manager.size() > size_o) {
        assert(entity_manager.size() - size_o == 1);

        // compute number of entities
        ++global_idx_info.n_entities;

        // compute offsets
        std::vector<int> idx_verts;
        if (e_tdim == 0) {
          idx_verts = {idx};
        } else {
          idx_verts = mesh_ptr->topology().get_connectivities(e_tdim, 0, idx);
        }

        const int last = global_idx_info.entity_offsets.back();
        global_idx_info.entity_offsets.push_back(last + idx_verts.size());

        // compute connections
        // NOTE: the index of vertices is still on global level
        const std::vector<int>::iterator e_end = global_idx_info.connections.end();

        global_idx_info.connections.insert(e_end, idx_verts.begin(),idx_verts.end());

        // compute global index
        const int g_idx = mesh_ptr->topology().get_global_index(e_tdim, idx);
        global_idx_info.global_indices.push_back(g_idx);

      } // if

    } // idx

  } // i

  // map vertex index to local
  const size_t size = global_idx_info.connections.size();

  for (std::size_t i = 0; i != size; ++i) {
    // idx_1: global index
    // idx_2: local (mapped) index
    int idx_1 = global_idx_info.connections[i];
    int idx_2 = v_idx_permut.at(idx_1);

    // set to mapped index
    global_idx_info.connections[i] = idx_2;

  } // i

}

// collect material ids wrt. partition_marker and marker_val
void Partition::collect_cell_material_ids(
  const std::shared_ptr<const Mesh> mesh_ptr, MaterialIdInfo& material_id_info,
  const int e_tdim, std::vector<int> partition_marker, const int marker_val,
  const std::map<int, int> v_idx_permut) {

  // topological dimension
  const int tdim = mesh_ptr->tdim();
  assert(e_tdim < tdim);

  // geometrical dimension
  const int gdim = mesh_ptr->gdim();

  // initialize MaterialIdInfo
  material_id_info.tdim = e_tdim;
  material_id_info.gdim = gdim;

  material_id_info.n_entities = 0;
  material_id_info.connections.clear();
  material_id_info.entity_offsets.clear();
  material_id_info.entity_offsets.push_back(0);
  material_id_info.material_ids.clear();

  std::set<int> entity_manager;

  // number of entities
  const int n_cells = mesh_ptr->topology().num_entities(tdim);

  for (std::size_t i = 0; i != n_cells; ++i) {
    if (partition_marker[i] != marker_val) {
      continue;
    }

    // get indices of entities within cell
    std::vector<int> idx_entities = mesh_ptr->topology().
                                    get_connectivities(tdim, e_tdim, i);

    for (const int& idx : idx_entities) {
      // check material id not be -1
      const int mat_id = mesh_ptr->material_id(e_tdim, idx);
      if (mat_id == -1) {
        continue;
      }

      const int size_o = entity_manager.size();
      entity_manager.insert(idx);

      // check if entity is in entity_manager
      if (entity_manager.size() > size_o) {
        assert(entity_manager.size() - size_o == 1);

        // compute number of entities
        ++material_id_info.n_entities;

        // compute offsets
        std::vector<int> idx_verts;
        if (e_tdim == 0) {
          idx_verts = {idx};
        } else {
          idx_verts = mesh_ptr->topology().get_connectivities(e_tdim, 0, idx);
        }

        const int last = material_id_info.entity_offsets.back();
        material_id_info.entity_offsets.push_back(last + idx_verts.size());

        // compute connections
        // NOTE: the index of vertices is still on global level
        const std::vector<int>::iterator e_end =
          material_id_info.connections.end();

        material_id_info.connections.insert(e_end, idx_verts.begin(),idx_verts.end());

        // compute material ids
        //const int mat_id = master_mesh_ptr->material_id(e_tdim, idx);
        material_id_info.material_ids.push_back(mat_id);
      } // if

    } // idx

  } // i

  // map vertex index to local
  const size_t size = material_id_info.connections.size();

  for (std::size_t i = 0; i != size; ++i) {
    // idx_1: global index
    // idx_2: local (mapped) index
    int idx_1 = material_id_info.connections[i];
    int idx_2 = v_idx_permut.at(idx_1);

    // set to mapped index
    material_id_info.connections[i] = idx_2;

  } // i

}

// collect index of boundary vertices
std::map<int, Coordinate> Partition::collect_boundary_vertices(
  const std::shared_ptr<const Mesh> mesh_ptr) {

  // topological dimension
  const int tdim = mesh_ptr->tdim();

  // topological dimension of facet
  const int tdim_facet = tdim - 1;

  // number of facets
  const int n_facets = mesh_ptr->topology().num_entities(tdim_facet);

  // collect vertices on boundary elements
  std::set<int> idx_verts = {};

  for (std::size_t i = 0; i != n_facets; ++i) {
    // cells connected to a facet
    std::vector<int> connects_cells = mesh_ptr->topology().
                                      get_connectivities(tdim_facet, tdim, i);
    assert(connects_cells.size() == 1 || 2);
    // check if facet is on boundary
    if (connects_cells.size() == 1) {
      std::vector<int> connects_verts = mesh_ptr->topology().
                                        get_connectivities(tdim_facet, 0, i);
      // add index of vertices into idx_verts
      for (const int& ele : connects_verts) {
        idx_verts.insert(ele);
      }
    } // check
  } // i

  assert(idx_verts.size() > 0);

  // vertices on local boundaries
  std::map<int, Coordinate> boundary_verts;

  for (const int& i : idx_verts) {
    Coordinate coord = mesh_ptr->geometry().coordinate(i);
    boundary_verts[i] = coord;
  }
  assert(boundary_verts.size() > 0);

  return boundary_verts;
}

// identify common vertices
// key: sub domain. value: vertex index
std::map<int, std::set<int>> Partition::identify_common_vertices(
                            std::map<int, Coordinate> boundary_verts,
std::vector<VertexInfo> recv_verts, int gdim, int my_rank) {

  assert(recv_verts.size() > 0);
  const int n_parts = recv_verts.size();

  // shared_vertices: key: sub domains. elements: vertices index
  std::map<int, std::set<int>> shared_vertices;
  assert(shared_vertices.size() == 0);

  for (int sub_d = 0; sub_d != n_parts; ++sub_d) {
    // ignore itself
    if (sub_d == my_rank) {
      continue;
    }

    // check vertices in recv_verts if they match in local_coords
    for (int i = 0; i != recv_verts[sub_d].n_verts; ++i) {
      // create a Coordinate from received data, then use it to compare
      std::vector<Coord_T> tmp_v(recv_verts[sub_d].coords.begin() + i * gdim,
                                 recv_verts[sub_d].coords.begin() + (i+1) * gdim);
      Coordinate coord(tmp_v);
      // loop over boundary_verts
      for (const auto ele : boundary_verts) {
        if (coord == ele.second) {
          shared_vertices[sub_d].insert(ele.first);
          break;
        }
      }
    } // i
  } // sub_d

  assert(shared_vertices.size() > 0);
  return shared_vertices;
}


}; // namespace Mesh

}; // namespace ChenLiu

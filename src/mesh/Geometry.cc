// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#include "mesh/Geometry.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Geometry::Geometry() : coordinates_(), gdim_(-1) {
  // do nothing
}

Geometry::Geometry(int d) : gdim_(d) {
  assert(0 < d <= 3);
  assert(coordinates_.size() == 0);
  // do nothing
}

Geometry::Geometry(int n, int d) : gdim_(d) {
  assert(n > 0);
  assert(0 < d <= 3);

  coordinates_.resize(n);
}

// deconstructor
Geometry::~Geometry() {
  // do nothing
}

// clear
void Geometry::clear() {
  coordinates_.clear();
  coordinates_.shrink_to_fit();

  gdim_ = -1;
}

// init
void Geometry::init(int n, int d) {
  assert(0 < d <= 3);
  assert(n > 0);

  gdim_ = d;
  coordinates_.resize(n);
}

// set size
void Geometry::set_size(int n) {
  assert(n > 0);

  coordinates_.resize(n);
}

// set geoemtrical dimension
void Geometry::set_gdim(int d) {
  assert(0 < d <= 3);

  gdim_ = d;
}

// get number of vertices
int Geometry::num_vertices() const {
  assert(0 < gdim_ <= 3);

  return coordinates_.size();
}

// set vertex
int Geometry::add_vertex(Coordinate vertex) {
  assert(0 < gdim_ <= 3);
  assert(vertex.dim() == gdim_);

  coordinates_.push_back(vertex);
  return (coordinates_.size() - 1);
}

// set vertex
void Geometry::set_vertex(int idx, Coordinate vertex) {
  assert(0 <= idx < coordinates_.size());
  assert(0 < gdim_ <= 3);
  assert(vertex.dim() == gdim_);

  coordinates_[idx] = vertex;
}


// geometrical dimension
int Geometry::gdim() const {
  assert(coordinates_.size() > 0);
  assert(0 < gdim_ <= 3);

  return gdim_;
}

// coordinate
Coordinate Geometry::coordinate(int idx) const {
  assert(0 <= idx < coordinates_.size());

  return coordinates_[idx];
}

}; // namespace Mesh
}; // namespace ChenLiu

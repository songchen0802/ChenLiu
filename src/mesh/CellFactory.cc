// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/CellFactory.hh"

#include "mesh/Cell_Point.hh"
#include "mesh/Cell_Line.hh"
#include "mesh/Cell_Triangle.hh"
#include "mesh/Cell_Quadrilateral.hh"
#include "mesh/Cell_Tetrahedron.hh"
#include "mesh/Cell_Hexahedron.hh"

namespace ChenLiu {
namespace Mesh {

// create

std::unique_ptr<Cell> CellFactory::create(CellType type) {

  switch(type) {
  case CellType::Point:
    return std::unique_ptr<Cell>(new Cell_Point());
  case CellType::Line:
    return std::unique_ptr<Cell>(new Cell_Line());
  case CellType::Triangle:
    return std::unique_ptr<Cell>(new Cell_Triangle());
  case CellType::Quadrilateral:
    return std::unique_ptr<Cell>(new Cell_Quadrilateral());
  case CellType::Tetrahedron:
    return std::unique_ptr<Cell>(new Cell_Tetrahedron());
  case CellType::Hexahedron:
    return std::unique_ptr<Cell>(new Cell_Hexahedron());
  default:
    CL_Error("Cell doesn't exist!");
    exit(EXIT_FAILURE);
  }
}

std::unique_ptr<Cell> CellFactory::create(std::string type) {
  return create(str2cell_type(type));
}

std::unique_ptr<Cell> CellFactory::create(int tdim, int num_verts) {
  assert(tdim >= 0);
  assert(tdim <= 3);
  assert(num_verts > 0);

  return create(type(tdim, num_verts));
}

// return CellType based on topological dimension and number of vertices
CellType CellFactory::type(int tdim, int num_verts) {
  assert(tdim >= 0);
  assert(num_verts >= 0);

  switch(tdim) {
  // Point
  case 0:
    assert(num_verts == 1);
    return CellType::Point;

  // Line
  case 1:
    assert(num_verts == 2);
    return CellType::Line;

  case 2:
    switch(num_verts) {
    // Triangle
    case 3:
      return CellType::Triangle;
    // Quadrilateral
    case 4:
      return CellType::Quadrilateral;
    default:
      CL_Error("Wrong topological dimension or number of vertices!");
      std::exit(EXIT_FAILURE);
    }

  case 3:
    switch(num_verts) {
    // Tetrahedron
    case 4:
      return CellType::Tetrahedron;
    // Hexahedron
    case 8:
      return CellType::Hexahedron;
    default:
      CL_Error("Wrong topological dimension or number of vertices!");
      std::exit(EXIT_FAILURE);
    }

  default:
    CL_Error("Wrong topological dimension or number of vertices!");
    std::exit(EXIT_FAILURE);
  }
}

}; // namespace Mesh
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Editor.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Editor::Editor() {
  // do nothing
}

// destructor
Editor::~Editor() {
  // do nothing
}

// read mesh
void Editor::read_mesh(std::filesystem::path filename,
                       std::shared_ptr<Mesh> mesh_ptr, int tdim,
                       int gdim, bool check_geo) {

  if (filename.extension() == ".inp") {
    // read .inp file
    reader_UCD(filename, mesh_ptr, tdim, gdim, check_geo);
  } else {
    // Exit program and throw error message
    CL_Error("Mesh file format is not correct!");
    std::exit(EXIT_FAILURE);
  }

}

void Editor::read_mesh(std::filesystem::path filename,
                       std::shared_ptr<Mesh> mesh_ptr, int tdim,
                       int gdim, MPI_Comm comm, bool check_geo) {

  assert(tdim > 0);

  // initialize topology_ in mesh
  mesh_ptr->topology_.init(tdim);

  int rank;
  MPI_Comm_rank(comm, &rank);

  if (rank == 0) {
    read_mesh(filename, mesh_ptr, tdim, gdim, check_geo);
  }

}

// UCD reader
void Editor::reader_UCD(std::filesystem::path filename,
                        std::shared_ptr<Mesh> mesh_ptr,
                        int tdim, int gdim, bool check_geo) {
  assert(0 < tdim <= 3);

  // open file
  std::ifstream file(filename.string().c_str());

  if (!file) {
    CL_Error("Can not open the file:" << filename.string().c_str());
    exit(EXIT_FAILURE);
  }

  // create str for reading each line
  std::string str_line;

  //// 1. number of vertices and entities ////

  // get number of coordinates and cells in file
  std::getline(file, str_line);

  std::vector<std::string> vals;
  split_string_space(str_line, vals);

  const int n_verts = std::stoi(vals[0]);
  const int n_cells = std::stoi(vals[1]);

  assert(n_verts > 0);
  assert(n_cells > 0);

  //// 2. vertices ////

  // set geometric dimension.
  mesh_ptr->geometry_.init(n_verts, gdim);

  // create coordinate
  // Note: data type of coord is pre-defined with Coord_T, extract values in double (stod),
  // implicit conversion will be done after
  Coordinate coord(gdim);

  // get all coordinates in .inp file
  for (std::size_t i = 0; i != n_verts; ++i) {
    // read one line
    std::getline(file, str_line);

    // split string
    split_string_space(str_line, vals);

    // get index
    const int idx_vert = std::stoi(vals[0]);
    assert(idx_vert == i);

    for (std::size_t n = 0; n != gdim; ++n) {
      coord[n] = std::stod(vals[n+1]);
    }

    // set vertex
    mesh_ptr->geometry_.set_vertex(idx_vert, coord);
  }

  //// 3. cell entities ////

  // get all entities in .inp file
  // be aware: .inp file connains all entities in topological dimention tdim,
  // and possible some entities with lower topological dimention tdim.

  // create CellFactory
  CellFactory cell_factory;

  // TODO: think about pre-locate some memory capacity
  // in order to avoid memory relocation
  for (std::size_t i_cell = 0; i_cell != n_cells; ++i_cell) {
    // read one line
    std::getline(file, str_line);

    // split string
    split_string_space(str_line, vals);

    // get material id
    const int id_material = std::stoi(vals[1]);

    // get entity type
    const std::string type = vals[2];

    // create a cell from CellFactory
    std::unique_ptr<Cell> cell = cell_factory.create(type);

    // check dimension
    assert((vals.size()-3) == cell->num_vertices());

    std::vector<int> idx_verts(cell->num_vertices());
    for (std::size_t n = 0; n != cell->num_vertices(); ++n) {
      idx_verts[n] = stoi(vals[3+n]);
    }

    if (check_geo) {
      std::vector<Coordinate> coords(cell->num_vertices());
      for (std::size_t n = 0; n != cell->num_vertices(); ++n) {
        coords[n].init(gdim);
        coords[n] = mesh_ptr->geometry_.coordinate(idx_verts[n]);
      }

      if (!cell->check_geometry(coords, gdim)) {
        CL_Error("Cell geometry is no correct!");
        exit(EXIT_FAILURE);
      }
    }

    const int cell_tdim = cell->tdim();
    const int idx_entity = add_entity(cell_tdim, idx_verts, mesh_ptr);

    // set material id
    mesh_ptr->material_id_[cell_tdim].set_marker(id_material, idx_entity);

    // set sub domain id to default -1
    if (cell_tdim == tdim) {
      mesh_ptr->sub_domain_id_.add_marker(-1, idx_entity);
    }
  }

  assert(mesh_ptr->material_id_[tdim].size() ==
         mesh_ptr->topology_.num_entities(tdim));

  assert(mesh_ptr->sub_domain_id_.size() ==
         mesh_ptr->topology_.num_entities(tdim));

  //// 4. set material ids for all topological dimensions ////
  for (std::size_t d = 0; d < tdim; ++d) {
    mesh_ptr->topology_.compute_connectivities_d2zero(d);
    const int n_total = mesh_ptr->topology_.num_entities(d);
    const int n = n_total - mesh_ptr->material_id_[d].size();

    std::vector<int> val(n, -1);
    mesh_ptr->material_id_[d].add_marker(val);

    assert(mesh_ptr->material_id_[d].size() ==
           mesh_ptr->topology_.num_entities(d));

  }

  //// 5. compute connectivities from low to high topological dimension ////
  // compute the rest of connectivities d0 -> d1, where d0 > d1
  for (std::size_t d0 = tdim; d0 >= 2; --d0) {
    for (std::size_t d1 = d0 - 1; d1 >= 1; --d1) {
      mesh_ptr->topology_.compute_connectivities_h2l(d0, d1);
    }
  }

  for (std::size_t d0 = 0; d0 != tdim; ++d0) {
    for (std::size_t d1 = d0 + 1; d1 <= tdim; ++d1) {
      mesh_ptr->topology_.compute_connectivities_l2h(d0, d1);
    }
  }

  for (std::size_t d = 0; d <= tdim; ++d) {
    mesh_ptr->topology_.compute_connectivities_d2d(d);
  }

  // 6. initialize idx_l2g_ in Topology //
  for (std::size_t d = 0; d <= tdim; ++d) {
    const int n_e = mesh_ptr->topology_.num_entities(d);

    mesh_ptr->topology_.init_idx_l2g(d, n_e);
  }

}

// add entity
int Editor::add_entity(int tdim, std::vector<int> idx_verts,
                       std::shared_ptr<Mesh> mesh_ptr) {
  assert(tdim >= 0);
  assert(tdim <= mesh_ptr->topology_.tdim());
  assert(idx_verts.size() > 0);

  // create entity index in connectivity
  // entity index is set based on the size of connectivity wrt. its
  // connectivity, i.e. tdim --> 0
  const int idx = mesh_ptr->topology_.num_entities(tdim);

  // add entity into connectivity table
  const int id = mesh_ptr->topology_.add_connectivity(tdim, 0, idx_verts);

  // set default material id
  mesh_ptr->material_id_[tdim].add_marker(-1, idx);

  return idx;
}

// build mesh fron EntityInfo
void Editor::build_mesh(std::shared_ptr<Mesh> mesh_ptr,
                        EntityInfo entity_info, bool check_geo) {
  assert(mesh_ptr != nullptr);

  //// 1. prepare some variables
  const int tdim = entity_info.tdim;
  const int gdim = entity_info.gdim;
  const int n_verts = entity_info.n_verts;
  const int n_cells = entity_info.n_entities;

  //// 2. vertices

  // initialize geometry
  mesh_ptr->geometry_.init(n_verts, gdim);

  // initilize topology
  mesh_ptr->topology_.init(tdim);

  // create coordinate
  Coordinate coord(gdim);

  for (std::size_t idx_vert = 0; idx_vert != n_verts; ++idx_vert) {
    for (std::size_t d = 0; d != gdim; ++d) {
      coord[d] = entity_info.coords[idx_vert * gdim + d];
    }

    // set vertex into geomtery_
    mesh_ptr->geometry_.set_vertex(idx_vert, coord);
  }

  //// 3. cell entities

  // CellFactory
  CellFactory cell_factory;

  for (std::size_t i_cell = 0; i_cell != n_cells; ++i_cell) {

    // number of vertices in cell
    const int n_verts_cell = entity_info.entity_offsets[i_cell+1] -
                             entity_info.entity_offsets[i_cell];

    // create a cell from CellFactory
    std::unique_ptr<Cell> cell(cell_factory.create(tdim, n_verts_cell));

    // index for vertices in cell
    std::vector<int> idx_verts(n_verts_cell);

    // pos in coords
    const int pos = entity_info.entity_offsets[i_cell];

    for (std::size_t n = 0; n != n_verts_cell; ++n) {
      idx_verts[n] = entity_info.connections[pos + n];
    }

    if (check_geo) {
      std::vector<Coordinate> coords(n_verts_cell);
      for (std::size_t n = 0; n != n_verts_cell; ++n) {
        coords[n].init(gdim);
        coords[n] = mesh_ptr->geometry_.coordinate(idx_verts[n]);
      }

      if (!cell->check_geometry(coords, gdim)) {
        CL_Error("Cell geometry is no correct!");
        exit(EXIT_FAILURE);
      }
    }

    const int idx_entity = add_entity(tdim, idx_verts, mesh_ptr);

    // HERE
    // set global index
    const int global_index = entity_info.global_indices[i_cell];
    mesh_ptr->topology_.add_global_index(tdim, global_index);
    // HERE

    // set material id
    const int id_material = entity_info.material_ids[i_cell];
    mesh_ptr->material_id_[tdim].set_marker(id_material, idx_entity);

    // set sub domain id to default -1
    mesh_ptr->sub_domain_id_.add_marker(-1, idx_entity);
  }

  assert(mesh_ptr->material_id_[tdim].size() ==
         mesh_ptr->topology_.num_entities(tdim));

  assert(mesh_ptr->sub_domain_id_.size() ==
         mesh_ptr->topology_.num_entities(tdim));

  //// 4. set material ids for all topological dimensions
  for (std::size_t d = 0; d < tdim; ++d) {
    // initialize connectivities for 0 < d < tdim
    mesh_ptr->topology_.compute_connectivities_d2zero(d);

    const int n_total = mesh_ptr->topology_.num_entities(d);
    const int n = n_total - mesh_ptr->material_id_[d].size();

    std::vector<int> val(n, -1);
    mesh_ptr->material_id_[d].add_marker(val);

    assert(mesh_ptr->material_id_[d].size() ==
           mesh_ptr->topology_.num_entities(d));

  }

  //// 5. compute connectivities from low to high topological dimension
  for (std::size_t d0 = tdim; d0 >= 2; --d0) {
    for (std::size_t d1 = d0 - 1; d1 >= 1; --d1) {
      mesh_ptr->topology_.compute_connectivities_h2l(d0, d1);
    }
  }

  for (std::size_t d0 = 0; d0 != tdim; ++d0) {
    for (std::size_t d1 = d0 + 1; d1 <= tdim; ++d1) {
      mesh_ptr->topology_.compute_connectivities_l2h(d0, d1);
    }
  }

  for (std::size_t d = 0; d <= tdim; ++d) {
    mesh_ptr->topology_.compute_connectivities_d2d(d);
  }

  //// 6. initialize idx_l2g_ in Topology (with value -1)
  assert(mesh_ptr->topology_.num_entities(tdim) ==
         mesh_ptr->topology_.idx_l2g_[tdim].size());
  for (std::size_t d = 0; d < tdim; ++d) {
    const int n_e = mesh_ptr->topology_.num_entities(d);
    mesh_ptr->topology_.init_idx_l2g(d, n_e, -1);
  }

}

// add cells on current mesh
std::map<int, int> Editor::add_cells(std::shared_ptr<Mesh> mesh_ptr,
                                     EntityInfo entity_info,
                                     std::map<int, Coordinate> boundary_verts,
                                     int sub_domain, bool check_geo) {

  assert(mesh_ptr != nullptr);
  assert(boundary_verts.size() > 0);
  assert(sub_domain >= 0);

  //// 1. prepare some info
  const int tdim = entity_info.tdim;
  const int gdim = entity_info.gdim;
  const int n_verts = entity_info.n_verts;
  const int n_cells = entity_info.n_entities;

  assert(mesh_ptr->tdim() == tdim);
  assert(mesh_ptr->gdim() == gdim);

  //// 2. vertices

  //create coordinate
  Coordinate coord(gdim);

  // permutation map
  // first: index in entity_info. second: index in mesh_ptr
  std::map<int,int> v_idx_permut;

  for (std::size_t i = 0; i != n_verts; ++i) {
    // assign a coord
    for (std::size_t d = 0; d != gdim; ++d) {
      coord[d] = entity_info.coords[i * gdim + d];
    }

    // check if coord is in boundary_verts
    for (const auto& ele : boundary_verts) {
      const int idx = ele.first;
      if (coord == ele.second) {
        v_idx_permut[i] = idx;
        break;
      } // if
    } // ele

    if (v_idx_permut.count(i) == 0) {
      const int idx_new = mesh_ptr->geometry_.add_vertex(coord);
      v_idx_permut[i] = idx_new;
    }

  } // i

  //// 3. permut vertex index in connections
  for (int& idx : entity_info.connections) {
    idx = v_idx_permut[idx];
  }

  //// 4. cell entities, material_id, sub_domain_id

  // CellFactory
  CellFactory cell_factory;

  for (std::size_t i_cell = 0; i_cell != n_cells; ++i_cell) {

    // number of vertices in cell
    const int n_verts_cell = entity_info.entity_offsets[i_cell + 1] -
                             entity_info.entity_offsets[i_cell];

    // create a cell from CellFactory
    std::unique_ptr<Cell> cell(cell_factory.create(tdim, n_verts_cell));

    // index for vertices in cell
    std::vector<int> idx_verts(n_verts_cell);

    // pos in coords
    const int pos = entity_info.entity_offsets[i_cell];

    for (std::size_t n = 0; n != n_verts_cell; ++n) {
      idx_verts[n] = entity_info.connections[pos + n];
    }

    if (check_geo) {
      std::vector<Coordinate> coords(n_verts_cell);
      for (std::size_t n = 0; n != n_verts_cell; ++n) {
        coords[n].init(gdim);
        coords[n] = mesh_ptr->geometry_.coordinate(idx_verts[n]);
      }
      if (!cell->check_geometry(coords, gdim)) {
        CL_Error("Cell geometry is no correct!");
        exit(EXIT_FAILURE);
      }
    }

    // add global index
    const int glo_idx = entity_info.global_indices[i_cell];
    mesh_ptr->topology_.add_global_index(tdim, glo_idx);

    const int idx_entity = add_entity(tdim, idx_verts, mesh_ptr);

    // set material id
    const int id_material = entity_info.material_ids[i_cell];
    mesh_ptr->material_id_[tdim].set_marker(id_material, idx_entity);

    // set sub domain id to sub_domain
    mesh_ptr->sub_domain_id_.add_marker(sub_domain, idx_entity);
  }

  assert(mesh_ptr->material_id_[tdim].size() ==
         mesh_ptr->topology_.num_entities(tdim));
  assert(mesh_ptr->sub_domain_id_.size() ==
         mesh_ptr->topology_.num_entities(tdim));

  //// 5. update connectivities for topological dimension 0 < d < tdim
  //// and their material_ids
  for (std::size_t d = 0; d < tdim; ++d) {
    // update connectivities
    mesh_ptr->topology_.compute_connectivities_d2zero(d, true);

    const int n_total = mesh_ptr->topology_.num_entities(d);
    const int n = n_total - mesh_ptr->material_id_[d].size();

    // set default value to -1
    std::vector<int> val(n, -1);
    mesh_ptr->material_id_[d].add_marker(val);

    assert(mesh_ptr->material_id_[d].size() ==
           mesh_ptr->topology_.num_entities(d));
  }

  //// 6. compute connectivities from low to high topological dimension
  for (std::size_t d0 = tdim; d0 >= 2; --d0) {
    for (std::size_t d1 = d0 - 1; d1 >= 1; --d1) {
      mesh_ptr->topology_.compute_connectivities_h2l(d0, d1, true);
    }
  }

  for (std::size_t d0 = 0; d0 != tdim; ++d0) {
    for (std::size_t d1 = d0 + 1; d1 <= tdim; ++d1) {
      mesh_ptr->topology_.compute_connectivities_l2h(d0, d1, false);
    }
  }

  for (std::size_t d = 0; d <= tdim; ++d) {
    mesh_ptr->topology_.compute_connectivities_d2d(d, false);
  }

  //// 7. initialize idx_l2g_ in Topology
  for (std::size_t d = 0; d <= tdim; ++d) {
    const int n_e = mesh_ptr->topology_.num_entities(d);
    //mesh_ptr->topology_.init_idx_l2g(d, n_e);
    const int n_e_current = mesh_ptr->topology_.idx_l2g_[d].size();
    assert(n_e >= n_e_current);
    for (int i = n_e_current; i != n_e; ++i) {
      mesh_ptr->topology_.add_global_index(d, -1);
    }
  }

  return v_idx_permut;
}

// add material ids from MaterialIdInfo
void Editor::add_material_ids(std::shared_ptr<Mesh> mesh_ptr,
                              MaterialIdInfo material_id_info) {
  assert(mesh_ptr != nullptr);
  assert(material_id_info.material_ids.size() == material_id_info.n_entities);
  assert((material_id_info.material_ids.size()+1) ==
         material_id_info.entity_offsets.size());

  // topological dimension
  const int tdim = material_id_info.tdim;

  // number of entities
  const int n_entities = material_id_info.n_entities;

  // create container for searching
  std::map<int, std::vector<int>> entity_manager;
  for (std::size_t i = 0; i != n_entities; ++i) {
    assert(entity_manager[i].size() == 0);

    const int start = material_id_info.entity_offsets[i];
    const int end = material_id_info.entity_offsets[i + 1];
    entity_manager[i].resize(end - start);

    for (int pos = start; pos != end; ++pos) {

      // index of vertex
      const int idx_vert = material_id_info.connections[pos];

      // TODO: think use copy for better performance
      // assign index
      entity_manager[i][pos - start] = idx_vert;
    }
    assert(entity_manager[i].size() > 0);

    // sort the connections, easier for comparison later
    std::sort(entity_manager[i].begin(), entity_manager[i].end());
  }

  // identify entities in mesh_ptr
  for (int i = 0; i != mesh_ptr->num_entities(tdim); ++i) {

    // connectitions in mesh_ptr
    // std::vector<int> connects = mesh_ptr->topology().get_connectivities(tdim, 0, i);
    std::vector<int> connects;
    if (tdim == 0) {
      connects = {i};
    } else {
      connects = mesh_ptr->topology().get_connectivities(tdim, 0, i);
      std::sort(connects.begin(), connects.end());
    }

    for (int j = 0; j != n_entities; ++j) {
      if (connects == entity_manager[j]) {
        // assign material number
        const int mat_id = material_id_info.material_ids[j];
        mesh_ptr->material_id_[tdim].set_marker(mat_id, i);

        // remove element from entity_manager
        entity_manager.erase(j);
        break;
      }

      // entity_manager is empty, for loop can stop
      if (entity_manager.size() == 0) {
        break;
      }

    } // j
  } // i

}

// add global indices from GlobalIndexInfo
void Editor::add_global_indices(std::shared_ptr<Mesh> mesh_ptr,
                                GlobalIndexInfo global_idx_info) {
  assert(mesh_ptr != nullptr);
  assert(global_idx_info.global_indices.size() == global_idx_info.n_entities);
  assert((global_idx_info.global_indices.size()+1) ==
         global_idx_info.entity_offsets.size());

  // topological dimension
  const int tdim = global_idx_info.tdim;

  // number of entities
  const int n_entities = global_idx_info.n_entities;

  // create container for searching
  std::map<int, std::vector<int>> entity_manager;
  for (std::size_t i = 0; i != n_entities; ++i) {
    assert(entity_manager[i].size() == 0);

    const int start = global_idx_info.entity_offsets[i];
    const int end = global_idx_info.entity_offsets[i + 1];
    entity_manager[i].resize(end - start);

    for (int pos = start; pos != end; ++pos) {

      // index of vertex
      const int idx_vert = global_idx_info.connections[pos];

      // TODO: think use copy for better performance
      // assign index
      entity_manager[i][pos - start] = idx_vert;
    }
    assert(entity_manager[i].size() > 0);

    // sort the connections, easier for comparison later
    std::sort(entity_manager[i].begin(), entity_manager[i].end());
  }

  // identify entities in mesh_ptr
  for (int i = 0; i != mesh_ptr->num_entities(tdim); ++i) {

    // connectitions in mesh_ptr
    // std::vector<int> connects = mesh_ptr->topology().get_connectivities(tdim, 0, i);
    std::vector<int> connects;
    if (tdim == 0) {
      connects = {i};
    } else {
      connects = mesh_ptr->topology().get_connectivities(tdim, 0, i);
      std::sort(connects.begin(), connects.end());
    }

    for (int j = 0; j != n_entities; ++j) {
      if (connects == entity_manager[j]) {
        // assign material number
        const int glo_idx = global_idx_info.global_indices[j];
        mesh_ptr->topology_.set_global_index(tdim, i, glo_idx);

        // remove element from entity_manager
        entity_manager.erase(j);
        break;
      }

      // entity_manager is empty, for loop can stop
      if (entity_manager.size() == 0) {
        break;
      }

    } // j
  } // i

}

// add sub domain id
void Editor::add_sub_domain_ids(std::shared_ptr<Mesh> mesh_ptr, int id) {
  assert(mesh_ptr != nullptr);
  assert(mesh_ptr->sub_domain_id_.size() > 0);

  const int tdim = mesh_ptr->tdim();
  assert(mesh_ptr->sub_domain_id_.size() == mesh_ptr->num_entities(tdim));

  for (std::size_t i = 0; i != mesh_ptr->num_entities(tdim); ++i) {
    mesh_ptr->sub_domain_id_.set_marker(id, i);
  }

}

// split string by space
void Editor::split_string_space(std::string str,
                                std::vector<std::string>& results) {
  assert(!str.empty());

  // clean string vector
  results.clear();

  std::istringstream iss(str);
  for (std::string s; iss >> s; ) {
    results.push_back(s);
  }

}

}; // namespace Mesh
}; // namespace ChenLiu


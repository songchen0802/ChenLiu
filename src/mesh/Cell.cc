// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Cell.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Cell::Cell(CellType cell_type) : cell_type_(cell_type) {
  // do nothing
}

// destructor
Cell::~Cell() {
  // do nothing
}

// return cell type
CellType Cell::cell_type() const {
  return cell_type_;
}

}; // namespace Mesh
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Cell_Tetrahedron.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Cell_Tetrahedron::Cell_Tetrahedron() : Cell(CellType::Tetrahedron) {
  // do nothing
}

// topological dimension
int Cell_Tetrahedron::tdim() const {
  return 3;
}

// number of vertices
int Cell_Tetrahedron::num_vertices() const {
  return 4;
}

// number of entities for each topological dimension
int Cell_Tetrahedron::num_entities(int tdim) const {
  assert(0 <= tdim <= 3);

  switch(tdim) {
  case 0:
    return 4;
  case 1:
    return 6;
  case 2:
    return 4;
  case 3:
    return 1;
  default:
    CL_Error("Wrong topological dimension!");
    exit(EXIT_FAILURE);
  }

}

// check cell geometry
bool Cell_Tetrahedron::check_geometry(std::vector<Coordinate> coords,
                                     int gdim) const {

  assert(gdim == 3);

  // check number of vertices
  if (coords.size() != 4) {
    return false;
  }

  Vec<Coord_T> v1(3), v2(3), v3(3);
  for (std::size_t i = 0; i < 3; ++i) {
    v1(i) = coords[1][i] - coords[0][i];
    v2(i) = coords[2][i] - coords[0][i];
    v3(i) = coords[3][i] - coords[0][i];
  }

  const Coord_T orientation = Dot(v3, Cross(v1, v2));

  return (orientation > 0.0);
}

// numeration for sub entities
std::vector<std::vector<int>> Cell_Tetrahedron::sub_entities_numeration(
int tdim) const {
  assert(0 <= tdim < 3);

  int n_e = -1;
  std::vector<std::vector<int>> loc_indices;

  switch(tdim) {
  case 0:
    n_e = 3;
    loc_indices.resize(n_e);
    // point 0
    loc_indices[0] = {0};
    // point 1
    loc_indices[1] = {1};
    // point 2
    loc_indices[2] = {2};

    return loc_indices;
    break;

  case 1:
    n_e = 6;
    loc_indices.resize(n_e);
    // line 0
    loc_indices[0] = {0, 1};
    // line 1
    loc_indices[1] = {1, 2};
    // line 2
    loc_indices[2] = {2, 0};
    // line 3
    loc_indices[3] = {0, 3};
    // line 4
    loc_indices[4] = {1, 3};
    // line 5
    loc_indices[5] = {2, 3};

    return loc_indices;
    break;

  case 2:
    n_e = 4;
    loc_indices.resize(n_e);
    // triangle 0
    loc_indices[0] = {0, 1, 2};
    // triangle 1
    loc_indices[1] = {0, 1, 3};
    // triangle 2
    loc_indices[2] = {1, 2, 3};
    // triangle 3
    loc_indices[3] = {0, 3, 2};

    return loc_indices;
    break;

  default:
    CL_Error("Wrong topological dimension of sub entity");
    exit(EXIT_FAILURE);
  }

}


}; // namespace Mesh
}; // namespace ChenLiut

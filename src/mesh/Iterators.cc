// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Iterators.hh"

namespace ChenLiu {

namespace Mesh {

// EntityIterator

// constructor
EntityIterator::EntityIterator(): entity_(), pos_(-1), pos_end_(-1) {
  // do nothing
}

EntityIterator::EntityIterator(const std::shared_ptr<const Mesh> mesh_ptr, int tdim,
                               std::string opt):
  entity_(mesh_ptr, tdim, 0), pos_(0), pos_end_(-1) {

  // pos_ is already initialized to 0
  pos_end_ = mesh_ptr->num_entities(tdim);

  if (opt == "interior") {
    //TODO: implement ghost offset in Topology
    std::cout << "Not yet implemented!" << std::endl;
  } else if (opt == "ghost") {
    //TODO: same as above
    // pos_ = ghost_offset from Topology
    std::cout << "Not yet implemented!" << std::endl;
  } else if (opt != "full") {
    CL_Error("option should only be full, interior or ghost!");
  }

}

// copy constructor
EntityIterator::EntityIterator(const EntityIterator& it): entity_(it.entity_),
  pos_(it.pos_), pos_end_(it.pos_end_) {
  // do nothing
}

// destructor
EntityIterator::~EntityIterator() {
  // do nothing
}

// operators
EntityIterator& EntityIterator::operator++() {
  assert(pos_ >= 0);
  assert(pos_ <= pos_end_);

  ++pos_;
  update_local_idx();
  return *this;
}

EntityIterator& EntityIterator::operator--() {
  assert(pos_ > 0);
  assert(pos_ <= pos_end_);

  --pos_;
  update_local_idx();
  return *this;
}

Entity* EntityIterator::operator->() {
  // use operator *()
  return &operator*();
}

Entity& EntityIterator::operator*() {
  update_local_idx();
  return entity_;
}

bool EntityIterator::operator==(const EntityIterator& it) const {
  return ((entity_ == it.entity_)
          && (pos_ == it.pos_));
}

bool EntityIterator::operator!=(const EntityIterator& it) const {
  // use operator==
  return !operator==(it);
}

// update local index in Entity
void EntityIterator::update_local_idx() {
  entity_.local_idx_ = pos_;
}

// check end of iterator
bool EntityIterator::is_end() const {
  return (pos_ == pos_end_);
}

// EntityIncidentIterator

// constructor
EntityIncidentIterator::EntityIncidentIterator(): incident_entity_(), pos_(-1),
  pos_end_(-1) {
  // do nothing
}

EntityIncidentIterator::EntityIncidentIterator(const Entity& entity,int tdim):
  incident_entity_(entity.mesh_ptr(), tdim, 0), pos_(0), pos_end_(-1),
  incident_idx_() {

  // get entity incidents from topology
  incident_idx_ = entity.mesh_ptr()->topology().get_connectivities(
                    entity.tdim(), tdim, entity.local_index());

  // compute number of incidents
  pos_end_ = incident_idx_.size();
  assert(pos_end_ > 0);

  // modify local index in entity
  incident_entity_.local_idx_ = incident_idx_[0];
}

// copy constructor
EntityIncidentIterator::EntityIncidentIterator(const EntityIncidentIterator& it)
  : incident_entity_(it.incident_entity_), pos_(it.pos_), pos_end_(it.pos_end_),
    incident_idx_(it.incident_idx_) {
  // do nothing
}

// destructor
EntityIncidentIterator::~EntityIncidentIterator() {
  // do nothing
}

// operators
EntityIncidentIterator& EntityIncidentIterator::operator++() {
  assert(pos_ >= 0);
  assert(pos_ <= pos_end_);

  ++pos_;
  update_local_idx();
  return *this;
}

EntityIncidentIterator& EntityIncidentIterator::operator--() {
  assert(pos_ > 0);
  assert(pos_ <= pos_end_);

  --pos_;
  update_local_idx();
  return *this;
}

Entity* EntityIncidentIterator::operator->() {
  // use operator *()
  return &operator*();
}

Entity& EntityIncidentIterator::operator*() {
  update_local_idx();
  return incident_entity_;
}

bool EntityIncidentIterator::operator==(const EntityIncidentIterator& it)
const {

  return ((incident_entity_ == it.incident_entity_)
          && (pos_ == it.pos_)
          && (pos_end_ == it.pos_end_)
          && (incident_idx_ == it.incident_idx_));
}

bool EntityIncidentIterator::operator!=(const EntityIncidentIterator& it)
const {
  // use operator==
  return !operator==(it);
}

// update local index in Entity
void EntityIncidentIterator::update_local_idx() {
  if (pos_ == pos_end_) {
    incident_entity_.local_idx_ = -1;
  } else {

    incident_entity_.local_idx_ = incident_idx_[pos_];
  }
}

// check end of iterator
bool EntityIncidentIterator::is_end() const {
  return (pos_ == pos_end_);
}

}; // namespace Mesh

}; // namespace ChenLiu

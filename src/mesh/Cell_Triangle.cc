// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Cell_Triangle.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Cell_Triangle::Cell_Triangle() : Cell(CellType::Triangle) {
  // do nothing
}

// topological dimension
int Cell_Triangle::tdim() const {
  return 2;
}

// number of vertices
int Cell_Triangle::num_vertices() const {
  return 3;
}

// number of entities for each topoplogical dimension
int Cell_Triangle::num_entities(int tdim) const {
  assert(0 <= tdim <= 2);

  switch(tdim) {
  case 0:
    return 3;
  case 1:
    return 3;
  case 2:
    return 1;
  default:
    CL_Error("Wrong topological dimension!");
    exit(EXIT_FAILURE);
  }

}

// check cell geometry
bool Cell_Triangle::check_geometry(std::vector<Coordinate> coords,
                                  int gdim) const {
  assert(2 <= gdim <= 3);

  // check number of vertices
  if (coords.size() != 3) {
    return false;
  }

  if (gdim == 3) {
    for (std::size_t i = 0; i < coords.size(); ++i) {
      const bool is_zero = is_equal<Coord_T>(coords[i][3], 0.0);
      if (!is_zero) {
        CL_Error("For gdim = 3, coordinates on z-axis should be zero!");
        exit(EXIT_FAILURE);
      }
    }
  }

  // compute vec1 and vec2
  Vec<Coord_T> v1(3), v2(3);
  for (int i = 0; i < 2; ++i) {
    v1(i) = coords[1][i] - coords[0][i];
    v2(i) = coords[2][i] - coords[0][i];
  }
  v1(2) = 0.0;
  v2(2) = 0.0;

  // compute orientation on z-axis
  const Coord_T orientation = Cross(v1, v2)(2);

  return (orientation > 0.0);
}

// numeration for sub entities
std::vector<std::vector<int>> Cell_Triangle::sub_entities_numeration(
int tdim) const {
  assert(0 <= tdim <= 1);

  int n_e = -1;
  std::vector<std::vector<int>> loc_indices;

  switch(tdim) {
  case 0:
    n_e = 3;
    loc_indices.resize(n_e);
    // point 0
    loc_indices[0] = {0};
    // point 1
    loc_indices[1] = {1};
    // point 2
    loc_indices[2] = {2};

    return loc_indices;
    break;

  case 1:
    n_e = 3;
    loc_indices.resize(n_e);
    // line 0
    loc_indices[0] = {0, 1};
    // line 1
    loc_indices[1] = {1, 2};
    // line 2
    loc_indices[2] = {2, 0};

    return loc_indices;
    break;

  default:
    CL_Error("Wrong topological dimension of sub entity");
    exit(EXIT_FAILURE);
  }

}

}; // namespace Mesh
}; // namespace ChenLiut

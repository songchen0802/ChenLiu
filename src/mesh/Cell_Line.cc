// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Cell_Line.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Cell_Line::Cell_Line() : Cell(CellType::Line) {
  // do nothing
}

// topological dimension
int Cell_Line::tdim() const {
  return 1;
}

// number of vertices
int Cell_Line::num_vertices() const {
  return 2;
}

// number of entities for each topological dimension
int Cell_Line::num_entities(int tdim) const {
  assert(0 <= tdim <= 1);

  switch(tdim) {
  case 0:
    return 2;
  case 1:
    return 1;
  default:
    CL_Error("Wrong topological dimension!");
    exit(EXIT_FAILURE);
  }

}

// check cell geometry
bool Cell_Line::check_geometry(std::vector<Coordinate> coords, int gdim) const {
  assert(1 < gdim <= 3);

  // check number of vertices
  if (coords.size() != 2) {
    return false;
  }

  return true;
}

// numeration for sub entities
std::vector<std::vector<int>> Cell_Line::sub_entities_numeration(
int tdim) const {
  assert(tdim == 0);

  // number of sub entities
  const int n_e = 2;

  std::vector<std::vector<int>> loc_indices(n_e);
  // point 0
  loc_indices[0] = {0};
  // point 1
  loc_indices[1] = {1};

  return loc_indices;
}

}; // namespace Mesh
}; // namespace ChenLiut

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "mesh/Cell_Point.hh"

namespace ChenLiu {
namespace Mesh {

// constructor
Cell_Point::Cell_Point() : Cell(CellType::Point) {
  // do nothing
}

// topological dimension
int Cell_Point::tdim() const {
  return 0;
}

// number of vertices
int Cell_Point::num_vertices() const {
  return 1;
}

// number of entities for each topological dimention
int Cell_Point::num_entities(int tdim) const {
  assert(tdim == 0);

  switch(tdim) {
  case 0:
    return 1;
  default:
    CL_Error("Wrong topological dimention!");
    exit(EXIT_FAILURE);
  }

}

// check cell geometry
bool Cell_Point::check_geometry(std::vector<Coordinate> coords, int gdim) const {
  assert(0 < gdim <= 3);

  // check number of vertices
  if (coords.size() != 1) {
    return false;
  }

  return true;
}

// local indices of sub entity
std::vector<std::vector<int>> Cell_Point::sub_entities_numeration(
int tdim) const {
  CL_Error("No sub entities fo Point");
  exit(EXIT_FAILURE);
}

}; // namespace Mesh
}; // namespace ChenLiut

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "polynomials/Polynomial.hh"

namespace ChenLiu {
namespace POLY {

// constructor
template<class DT>
Polynomial<DT>::Polynomial() {
  // do nothing
}

// destructor
template<class DT>
Polynomial<DT>::~Polynomial() {
  clear();
}

// clear
template<class DT>
void Polynomial<DT>::clear() {
  poly_coeffs_.clear();
  poly_coeffs_.shrink_to_fit();
}

// template instantiation
template class Polynomial<double>;
template class Polynomial<float>;

}; // namespace Poly
}; // namespace ChenLiu

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "polynomials/PolyCoeff.hh"

namespace ChenLiu {
namespace POLY {

// constructor
template<class DT>
PolyCoeff<DT>::PolyCoeff(): deg_(-1) {
  // do nothing
}

template<class DT>
PolyCoeff<DT>::PolyCoeff(int deg) {
  assert(deg > 0);
  init(deg);
}

// destructor
template<class DT>
PolyCoeff<DT>::~PolyCoeff() {
  poly_.clear();
  poly_deriv1_.clear();
  poly_deriv2_.clear();
}

// init
template<class DT>
void PolyCoeff<DT>::init(int deg) {
  deg_ = deg;

  poly_.resize(deg + 1);
  poly_deriv1_.resize(deg + 1);
  poly_deriv2_.resize(deg + 1);

  for (int i = 0; i != (deg + 1); ++i) {
    poly_[i].resize(deg + 1);
    poly_deriv1_[i].resize(deg + 1);
    poly_deriv2_[i].resize(deg + 1);
  }
}

// degree
template<class DT>
int PolyCoeff<DT>::degree() const {
  return deg_;
}

// clear
template<class DT>
void PolyCoeff<DT>::clear() {
  poly_.clear();
  poly_deriv1_.clear();
  poly_deriv2_.clear();
}

//
template<class DT>
std::vector<DT> PolyCoeff<DT>::poly(int i) const {
  assert(poly_.size() == (deg_ + 1));
  assert(poly_[i].size() == (deg_ + 1));

  return poly_[i];
}

template<class DT>
std::vector<DT> PolyCoeff<DT>::poly_deriv1(int i) const {
  assert(poly_deriv1_.size() == (deg_ + 1));
  assert(poly_deriv1_[i].size() == (deg_ + 1));

  return poly_deriv1_[i];
}

template<class DT>
std::vector<DT> PolyCoeff<DT>::poly_deriv2(int i) const {
  assert(poly_deriv2_.size() == (deg_ + 1));
  assert(poly_deriv2_[i].size() == (deg_ + 1));

  return poly_deriv2_[i];
}

// set
template<class DT>
void PolyCoeff<DT>::set_poly(std::vector<std::vector<DT>> val) {
  assert(poly_.size() == val.size());

  // copy
  for (int i = 0; i != (deg_ + 1); ++i) {
    assert(poly_[i].size() == val[i].size());
    std::copy(val[i].begin(), val[i].end(), poly_[i].begin());
  }

}

template<class DT>
void PolyCoeff<DT>::set_poly_deriv1(std::vector<std::vector<DT>> val) {
  assert(poly_deriv1_.size() == val.size());

  // copy
  for (int i = 0; i != (deg_ + 1); ++i) {
    assert(poly_deriv1_[i].size() == val[i].size());
    std::copy(val[i].begin(), val[i].end(), poly_deriv1_[i].begin());
  }

}

template<class DT>
void PolyCoeff<DT>::set_poly_deriv2(std::vector<std::vector<DT>> val) {
  assert(poly_deriv2_.size() == val.size());

  // copy
  for (int i = 0; i != (deg_ + 1); ++i) {
    assert(poly_deriv2_[i].size() == val[i].size());
    std::copy(val[i].begin(), val[i].end(), poly_deriv2_[i].begin());
  }

}

// template instantiation
template class PolyCoeff<double>;
template class PolyCoeff<float>;

}; // namespace Poly
}; // namespace ChenLiu

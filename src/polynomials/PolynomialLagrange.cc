// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "polynomials/PolynomialLagrange.hh"

namespace ChenLiu {
namespace POLY {

// constructor
template<class DT>
PolynomialLagrange<DT>::PolynomialLagrange() {
  init_coefficients();
}

// destructor
template<class DT>
PolynomialLagrange<DT>::~PolynomialLagrange() {
  for (int i = 0; i != this->poly_coeffs_.size(); ++i) {
    this->poly_coeffs_[i].clear();
  }
}

// initialize coefficients
template<class DT>
void PolynomialLagrange<DT>::init_coefficients() {
  // initialize size of poly_coeffs_
  this->poly_coeffs_.resize(max_deg_);

  // set coefficients from 1 to max_deg_
  set_coeff_deg_1();
  set_coeff_deg_2();
  set_coeff_deg_3();
  set_coeff_deg_4();
}

// poly
template<class DT>
DT PolynomialLagrange<DT>::poly(int deg, int i, DT x) const {
  assert(0 < deg <= max_deg_);

  return evaluate(deg, this->poly_coeffs_[deg - 1].poly(i), x);
}

// poly_deriv1
template<class DT>
DT PolynomialLagrange<DT>::poly_deriv1(int deg, int i, DT x) const {
  assert(0 < deg <= max_deg_);

  return evaluate(deg, this->poly_coeffs_[deg - 1].poly_deriv1(i), x);
}

// poly_deriv2
template<class DT>
DT PolynomialLagrange<DT>::poly_deriv2(int deg, int i, DT x) const {
  assert(0 < deg <= max_deg_);

  return evaluate(deg, this->poly_coeffs_[deg - 1].poly_deriv2(i), x);
}

// evaluate
template<class DT>
DT PolynomialLagrange<DT>::evaluate(int deg, std::vector<DT> coeffs,
                                    DT x) const {
  assert(coeffs.size() == (deg + 1));
  assert(0 < deg <= max_deg_);

  DT val = 0.0;
  for (int i = 0; i != (deg + 1); ++i) {
    val += coeffs[i] * std::pow(x, i);
  }

  return val;
}

// set coefficient for degree 1
template<class DT>
void PolynomialLagrange<DT>::set_coeff_deg_1() {
  // polynomial degree
  const int deg = 1;

  // initialize
  this->poly_coeffs_[deg - 1].init(deg);

  // set values
  std::vector<std::vector<DT>> val;

  // poly
  val = {
    // 0
    { 1.000000000000000000000000000000e+00,
      -1.000000000000000000000000000000e+00
    },
    // 1
    { 0.000000000000000000000000000000e+00,
      1.000000000000000000000000000000e+00
    }
  };

  this->poly_coeffs_[deg - 1].set_poly(val);

  // poly_deriv1
  val = {
    // 0
    { -1.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e+00
    },
    // 1
    { 1.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e+00
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv1(val);

  // poly_deriv2
  val = {
    // 0
    { 0.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e+00
    },
    // 1
    { 0.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e+00
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv2(val);
}

// set coefficient for degree 2
template<class DT>
void PolynomialLagrange<DT>::set_coeff_deg_2() {
  // polynomial degree
  const int deg = 2;

  // initialize
  this->poly_coeffs_[deg - 1].init(deg);

  // set values
  std::vector<std::vector<DT>> val;

  // poly
  val = {
    // 0
    { 1.000000000000000000000000000000e+00,
      -3.000000000000000000000000000000e+00,
      2.000000000000000000000000000000e+00
    },
    // 1
    { 0.000000000000000000000000000000e-01,
      4.000000000000000000000000000000e+00,
      -4.000000000000000000000000000000e+00
    },
    //2
    { 0.000000000000000000000000000000e-01,
      -1.000000000000000000000000000000e+00,
      2.000000000000000000000000000000e+00
    }
  };

  this->poly_coeffs_[deg - 1].set_poly(val);

  // poly_deriv1
  val = {
    // 0
    { -3.000000000000000000000000000000e+00,
      4.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e-01
    },
    // 1
    { 4.000000000000000000000000000000e+00,
      -8.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e-01
    },
    // 2
    { -1.000000000000000000000000000000e+00,
      4.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e-01
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv1(val);

  // poly_deriv2
  val = {
    // 0
    { 4.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 1
    { -8.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 2
    { 4.000000000000000000000000000000e+00,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv2(val);
}

// set coefficient for degree 3
template<class DT>
void PolynomialLagrange<DT>::set_coeff_deg_3() {
  // polynomial degree
  const int deg = 3;

  // initialize
  this->poly_coeffs_[deg - 1].init(deg);

  // set values
  std::vector<std::vector<DT>> val;

  // poly
  val = {
    // 0
    { 1.000000000000000000000000000000e+00,
      -5.500000000000000000000000000000e+00,
      9.000000000000000000000000000000e+00,
      -4.500000000000000000000000000000e+00
    },
    // 1
    { 0.000000000000000000000000000000e-01,
      9.000000000000000000000000000000e+00,
      -2.250000000000000000000000000000e+01,
      1.350000000000000000000000000000e+01
    },
    // 2
    { 0.000000000000000000000000000000e-01,
      -4.500000000000000000000000000000e+00,
      1.800000000000000000000000000000e+01,
      -1.350000000000000000000000000000e+01
    },
    // 3
    { 0.000000000000000000000000000000e-01,
      1.000000000000000000000000000000e+00,
      -4.500000000000000000000000000000e+00,
      4.500000000000000000000000000000e+00
    }
  };

  this->poly_coeffs_[deg - 1].set_poly(val);

  // poly_deriv1
  val = {
    // 0
    { -5.500000000000000000000000000000e+00,
      1.800000000000000000000000000000e+01,
      -1.350000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01
    },
    // 1
    { 9.000000000000000000000000000000e+00,
      -4.500000000000000000000000000000e+01,
      4.050000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01
    },
    // 2
    { -4.500000000000000000000000000000e+00,
      3.600000000000000000000000000000e+01,
      -4.050000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01
    },
    // 3
    { 1.000000000000000000000000000000e+00,
      -9.000000000000000000000000000000e+00,
      1.350000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv1(val);

  // poly_deriv2
  val = {
    // 0
    { 1.800000000000000000000000000000e+01,
      -2.700000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 1
    { -4.500000000000000000000000000000e+01,
      8.100000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 2
    { 3.600000000000000000000000000000e+01,
      -8.100000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 3
    { -9.000000000000000000000000000000e+00,
      2.700000000000000000000000000000e+01,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv2(val);
}

// set coefficient for degree 4
template<class DT>
void PolynomialLagrange<DT>::set_coeff_deg_4() {
  // polynomial degree
  const int deg = 4;

  // initialize
  this->poly_coeffs_[deg - 1].init(deg);

  // set values
  std::vector<std::vector<DT>> val;

  // poly
  val = {
    // 0
    { 1.000000000000000000000000000000e+00,
      -8.333333333333333333333333333330e+00,
      2.333333333333333333333333333330e+01,
      -2.666666666666666666666666666670e+01,
      1.066666666666666666666666666670e+01
    },
    // 1
    { 0.000000000000000000000000000000e-01,
      1.600000000000000000000000000000e+01,
      -6.933333333333333333333333333330e+01,
      9.600000000000000000000000000000e+01,
      -4.266666666666666666666666666670e+01
    },
    // 2
    { 0.000000000000000000000000000000e-01,
      -1.200000000000000000000000000000e+01,
      7.600000000000000000000000000000e+01,
      -1.280000000000000000000000000000e+02,
      6.400000000000000000000000000000e+01
    },
    // 3
    { 0.000000000000000000000000000000e-01,
      5.333333333333333333333333333330e+00,
      -3.733333333333333333333333333330e+01,
      7.466666666666666666666666666670e+01,
      -4.266666666666666666666666666670e+01
    },
    // 4
    { 0.000000000000000000000000000000e-01,
      -1.000000000000000000000000000000e+00,
      7.333333333333333333333333333330e+00,
      -1.600000000000000000000000000000e+01,
      1.066666666666666666666666666670e+01
    }
  };

  this->poly_coeffs_[deg - 1].set_poly(val);

  // poly_deriv1
  val = {
    // 0
    { -8.333333333333333333333333333330e+00,
      4.666666666666666666666666666670e+01,
      -8.000000000000000000000000000000e+01,
      4.266666666666666666666666666670e+01,
      0.000000000000000000000000000000e-01
    },
    // 1
    { 1.600000000000000000000000000000e+01,
      -1.386666666666666666666666666670e+02,
      2.880000000000000000000000000000e+02,
      -1.706666666666666666666666666670e+02,
      0.000000000000000000000000000000e-01
    },
    // 2
    { -1.200000000000000000000000000000e+01,
      1.520000000000000000000000000000e+02,
      -3.840000000000000000000000000000e+02,
      2.560000000000000000000000000000e+02,
      0.000000000000000000000000000000e-01
    },
    // 3
    { 5.333333333333333333333333333330e+00,
      -7.466666666666666666666666666670e+01,
      2.240000000000000000000000000000e+02,
      -1.706666666666666666666666666670e+02,
      0.000000000000000000000000000000e-01
    },
    // 4
    { -1.000000000000000000000000000000e+00,
      1.466666666666666666666666666670e+01,
      -4.800000000000000000000000000000e+01,
      4.266666666666666666666666666670e+01,
      0.000000000000000000000000000000e-01
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv1(val);

  // poly_deriv2
  val = {
    // 0
    { 4.666666666666666666666666666670e+01,
      -1.600000000000000000000000000000e+02,
      1.280000000000000000000000000000e+02,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 1
    { -1.386666666666666666666666666670e+02,
      5.760000000000000000000000000000e+02,
      -5.120000000000000000000000000000e+02,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 2
    { 1.520000000000000000000000000000e+02,
      -7.680000000000000000000000000000e+02,
      7.680000000000000000000000000000e+02,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 3
    { -7.466666666666666666666666666670e+01,
      4.480000000000000000000000000000e+02,
      -5.120000000000000000000000000000e+02,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    },
    // 4
    { 1.466666666666666666666666666670e+01,
      -9.600000000000000000000000000000e+01,
      1.280000000000000000000000000000e+02,
      0.000000000000000000000000000000e-01,
      0.000000000000000000000000000000e-01
    }
  };

  this->poly_coeffs_[deg - 1].set_poly_deriv2(val);
}

// template instantiation
template class PolynomialLagrange<double>;
template class PolynomialLagrange<float>;

}; // namespace Poly
}; // namespace ChenLiu

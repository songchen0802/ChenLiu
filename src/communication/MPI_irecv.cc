// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "communication/MPI_irecv.hh"

namespace ChenLiu {

namespace Communication {

// constructor
CL_MPI_Irecv::CL_MPI_Irecv(const MPI_Comm& comm) : CL_MPI(comm) {
}

// destructor
CL_MPI_Irecv::~CL_MPI_Irecv() {
  clear();
}

// execute
void CL_MPI_Irecv::execute(Mesh::EntityInfo& recv, const int origin,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);

  // initilize mpi status
  const int n_reqs = 5;
  reqs.resize(n_reqs);

  //// 1. receive necessary sizes in order to initialize variables
  const int n_size = recv.buffer_size;
  std::vector<int> recv_sizes(n_size);

  // blocking receive, as following receives need the size
  MPI_Status stat;
  MPI_Recv(recv_sizes.data(), n_size, MPI_INT,
           origin, E_SIZES_TAG, comm_, &stat);

  for (const auto ele : recv_sizes) {
    assert(ele >= 0);
  }

  //// initialize sizes
  recv.tdim = recv_sizes[0];
  recv.gdim = recv_sizes[1];
  recv.n_verts = recv_sizes[2];
  recv.n_entities = recv_sizes[3];
  const int n_connects = recv_sizes[4];
  recv.coords.resize(recv.n_verts * recv.gdim);
  recv.connections.resize(n_connects);
  recv.entity_offsets.resize(recv.n_entities + 1);
  recv.material_ids.resize(recv.n_entities);
  recv.global_indices.resize(recv.n_entities);

  //// 2. coords
  MPI_Irecv(recv.coords.data(), recv.n_verts * recv.gdim,
            MPI_DOUBLE, origin, E_COORDS_TAG, comm_, &reqs[0]);

  //// 3. connections
  MPI_Irecv(recv.connections.data(), recv.connections.size(),
            MPI_INT, origin, E_CONNECTIONS_TAG, comm_, &reqs[1]);

  //// 4. entity_offsets
  MPI_Irecv(recv.entity_offsets.data(), recv.n_entities + 1,
            MPI_INT, origin, E_ENTITY_OFFSETS_TAG, comm_, &reqs[2]);

  //// 5. material_ids
  MPI_Irecv(recv.material_ids.data(), recv.n_entities,
            MPI_INT, origin, E_MATERIAL_IDS_TAG, comm_, &reqs[3]);

  //// 6. global_indices
  MPI_Irecv(recv.global_indices.data(), recv.n_entities,
            MPI_INT, origin, E_GLOBAL_INDICES_TAG, comm_, &reqs[4]);
}

// exectue
void CL_MPI_Irecv::execute(Mesh::MaterialIdInfo& recv, const int origin,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);

  // initilize mpi status
  const int n_reqs = 3;
  reqs.resize(n_reqs);

  //// 1. receive necessary sizes in order to initialize variables
  const int n_size = recv.buffer_size;
  std::vector<int> recv_sizes(n_size);

  // blocking receive, as following receives need the size
  MPI_Status stat;
  MPI_Recv(recv_sizes.data(), n_size, MPI_INT,
           origin, M_SIZES_TAG, comm_, &stat);

  for (const auto ele : recv_sizes) {
    assert(ele >= 0);
  }

  //// initialize sizes
  recv.tdim = recv_sizes[0];
  recv.gdim = recv_sizes[1];
  recv.n_entities = recv_sizes[2];
  const int n_connects = recv_sizes[3];
  recv.connections.resize(n_connects);
  recv.entity_offsets.resize(recv.n_entities + 1);
  recv.material_ids.resize(recv.n_entities);

  //// 2. connections
  MPI_Irecv(recv.connections.data(), recv.connections.size(),
            MPI_INT, origin, M_CONNECTIONS_TAG, comm_, &reqs[0]);

  //// 3. entity_offsets
  MPI_Irecv(recv.entity_offsets.data(), recv.n_entities + 1,
            MPI_INT, origin, M_ENTITY_OFFSETS_TAG, comm_, &reqs[1]);

  //// 4. material_ids
  MPI_Irecv(recv.material_ids.data(), recv.n_entities,
            MPI_INT, origin, M_MATERIAL_IDS_TAG, comm_, &reqs[2]);
}

// exectue
void CL_MPI_Irecv::execute(Mesh::GlobalIndexInfo& recv, const int origin,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);

  // initilize mpi status
  const int n_reqs = 3;
  reqs.resize(n_reqs);

  //// 1. receive necessary sizes in order to initialize variables
  const int n_size = recv.buffer_size;
  std::vector<int> recv_sizes(n_size);

  // blocking receive, as following receives need the size
  MPI_Status stat;
  MPI_Recv(recv_sizes.data(), n_size, MPI_INT,
           origin, G_SIZES_TAG, comm_, &stat);

  for (const auto ele : recv_sizes) {
    assert(ele >= 0);
  }

  //// initialize sizes
  recv.tdim = recv_sizes[0];
  recv.gdim = recv_sizes[1];
  recv.n_entities = recv_sizes[2];
  const int n_connects = recv_sizes[3];
  recv.connections.resize(n_connects);
  recv.entity_offsets.resize(recv.n_entities + 1);
  recv.global_indices.resize(recv.n_entities);

  //// 2. connections
  MPI_Irecv(recv.connections.data(), recv.connections.size(),
            MPI_INT, origin, G_CONNECTIONS_TAG, comm_, &reqs[0]);

  //// 3. entity_offsets
  MPI_Irecv(recv.entity_offsets.data(), recv.n_entities + 1,
            MPI_INT, origin, G_ENTITY_OFFSETS_TAG, comm_, &reqs[1]);

  //// 4. global_indices
  MPI_Irecv(recv.global_indices.data(), recv.n_entities,
            MPI_INT, origin, G_GLOBAL_INDICES_TAG, comm_, &reqs[2]);
}

// exectue
void CL_MPI_Irecv::execute(Space::CellDofInfo& recv, const int origin,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);

  // initilize mpi status
  const int n_reqs = 3;
  reqs.resize(n_reqs);

  //// 1. receive necessary sizes in order to initialize variables
  const int n_size = recv.buffer_size;
  std::vector<int> recv_sizes(n_size);

  // blocking receive, as following receives need the size
  MPI_Status stat;
  MPI_Recv(recv_sizes.data(), n_size, MPI_INT,
           origin, D_SIZES_TAG, comm_, &stat);

  for (const auto ele : recv_sizes) {
    assert(ele >= 0);
  }

  //// initialize sizes
  recv.global_entity_indices.resize(recv_sizes[0]);
  recv.dofs.resize(recv_sizes[1]);
  recv.dof_offsets.resize(recv_sizes[0]+1);


  //// 2. global entity indices
  MPI_Irecv(recv.global_entity_indices.data(), recv.global_entity_indices.size(),
            MPI_INT, origin, D_GLOBAL_ENTITY_INDICES_TAG, comm_, &reqs[0]);

  //// 3. dofs
  MPI_Irecv(recv.dofs.data(), recv.dofs.size(),
            MPI_INT, origin, D_DOFS_TAG, comm_, &reqs[1]);

  //// 4. dof_offsets
  MPI_Irecv(recv.dof_offsets.data(), recv.dof_offsets.size(),
            MPI_INT, origin, D_DOF_OFFSETS_TAG, comm_, &reqs[2]);
}


}; // namespace Communication

}; // namespace ChenLiu


// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "communication/MPI_isend.hh"

namespace ChenLiu {

namespace Communication {

// constructor
CL_MPI_Isend::CL_MPI_Isend(const MPI_Comm& comm) : CL_MPI(comm) {
}

// destructor
CL_MPI_Isend::~CL_MPI_Isend() {
  clear();
}

// execute
void CL_MPI_Isend::execute(const Mesh::EntityInfo& send, const int dest,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);
  assert(send.tdim >= 0);
  assert(send.gdim > 0);
  assert(send.n_verts >= 0);
  assert(send.connections.size() >= 0);
  assert(send.n_entities >= 0);
  assert(send.entity_offsets.size() >= 0);
  assert(send.material_ids.size() >= 0);
  assert(send.global_indices.size() >= 0);

  // initilize mpi requests
  const int n_reqs = 6;
  reqs.resize(n_reqs);

  //// 1. send necessary sizes
  const int n_size = send.buffer_size;
  std::vector<int> send_sizes = collect_entity_info_sizes(send);
  assert(send_sizes.size() == n_size);

  MPI_Isend(send_sizes.data(), n_size, MPI_INT,
            dest, E_SIZES_TAG, comm_, &reqs[0]);

  //// 2. coords
  MPI_Isend(send.coords.data(), send.n_verts * send.gdim,
            MPI_DOUBLE, dest, E_COORDS_TAG, comm_, &reqs[1]);

  //// 3. connections
  MPI_Isend(send.connections.data(), send.connections.size(),
            MPI_INT, dest, E_CONNECTIONS_TAG, comm_, &reqs[2]);

  //// 4. entity_offsets
  MPI_Isend(send.entity_offsets.data(), send.n_entities + 1,
            MPI_INT, dest, E_ENTITY_OFFSETS_TAG, comm_, &reqs[3]);

  //// 5. material_ids
  MPI_Isend(send.material_ids.data(), send.n_entities,
            MPI_INT, dest, E_MATERIAL_IDS_TAG, comm_, &reqs[4]);

  //// 6. global_indices
  MPI_Isend(send.global_indices.data(), send.n_entities,
            MPI_INT, dest, E_GLOBAL_INDICES_TAG, comm_, &reqs[5]);
}

// execute
void CL_MPI_Isend::execute(const Mesh::MaterialIdInfo& send, const int dest,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);
  assert(send.tdim >= 0);
  assert(send.gdim > 0);
  assert(send.connections.size() >= 0);
  assert(send.n_entities >= 0);
  assert(send.entity_offsets.size() >= 0);
  assert(send.material_ids.size() >= 0);

  // initilize mpi requests
  const int n_reqs = 4;
  reqs.resize(n_reqs);

  //// 1. send necessary sizes
  const int n_size = send.buffer_size;
  std::vector<int> send_sizes = collect_material_id_info_sizes(send);
  assert(send_sizes.size() == n_size);

  MPI_Isend(send_sizes.data(), n_size,
            MPI_INT, dest, M_SIZES_TAG, comm_, &reqs[0]);

  //// 2. connections
  MPI_Isend(send.connections.data(), send.connections.size(),
            MPI_INT, dest, M_CONNECTIONS_TAG, comm_, &reqs[1]);

  //// 3. entity_offsets
  MPI_Isend(send.entity_offsets.data(), send.n_entities + 1,
            MPI_INT, dest, M_ENTITY_OFFSETS_TAG, comm_, &reqs[2]);

  //// 4. material_ids
  MPI_Isend(send.material_ids.data(), send.n_entities,
            MPI_INT, dest, M_MATERIAL_IDS_TAG, comm_, &reqs[3]);
}

// execute
void CL_MPI_Isend::execute(const Mesh::GlobalIndexInfo& send, const int dest,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);
  assert(send.tdim >= 0);
  assert(send.gdim > 0);
  assert(send.connections.size() >= 0);
  assert(send.n_entities >= 0);
  assert(send.entity_offsets.size() >= 0);
  assert(send.global_indices.size() >= 0);

  // initilize mpi requests
  const int n_reqs = 4;
  reqs.resize(n_reqs);

  //// 1. send necessary sizes
  const int n_size = send.buffer_size;
  std::vector<int> send_sizes = collect_global_index_info_sizes(send);
  assert(send_sizes.size() == n_size);

  MPI_Isend(send_sizes.data(), n_size,
            MPI_INT, dest, G_SIZES_TAG, comm_, &reqs[0]);

  //// 2. connections
  MPI_Isend(send.connections.data(), send.connections.size(),
            MPI_INT, dest, G_CONNECTIONS_TAG, comm_, &reqs[1]);

  //// 3. entity_offsets
  MPI_Isend(send.entity_offsets.data(), send.n_entities + 1,
            MPI_INT, dest, G_ENTITY_OFFSETS_TAG, comm_, &reqs[2]);

  //// 4. global_indices
  MPI_Isend(send.global_indices.data(), send.n_entities,
            MPI_INT, dest, G_GLOBAL_INDICES_TAG, comm_, &reqs[3]);
}

// execute
void CL_MPI_Isend::execute(const Space::CellDofInfo& send, const int dest,
                           std::vector<MPI_Request>& reqs) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);
  assert(send.global_entity_indices.size() >= 0);
  assert(send.dofs.size() >= 0);
  assert(send.dof_offsets.size() > send.global_entity_indices.size());


  // initilize mpi requests
  const int n_reqs = 4;
  reqs.resize(n_reqs);

  //// 1. send necessary sizes
  const int n_size = send.buffer_size;
  std::vector<int> send_sizes = collect_cell_dof_info_sizes(send);
  assert(send_sizes.size() == n_size);

  MPI_Isend(send_sizes.data(), n_size,
            MPI_INT, dest, D_SIZES_TAG, comm_, &reqs[0]);

  //// 2. global entity indices
  MPI_Isend(send.global_entity_indices.data(), send.global_entity_indices.size(),
            MPI_INT, dest, D_GLOBAL_ENTITY_INDICES_TAG, comm_, &reqs[1]);

  //// 3. dofs
  MPI_Isend(send.dofs.data(), send.dofs.size(),
            MPI_INT, dest, D_DOFS_TAG, comm_, &reqs[2]);

  //// 4. dof_offsets
  MPI_Isend(send.dof_offsets.data(), send.dof_offsets.size(),
            MPI_INT, dest, D_DOF_OFFSETS_TAG, comm_, &reqs[3]);
}


}; // namespace Communication

}; // namespace ChenLiu


// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "communication/MPI_allgather.hh"

namespace ChenLiu {

namespace Communication {

// constructor
CL_MPI_Allgather::CL_MPI_Allgather(const MPI_Comm& comm) : CL_MPI(comm) {
  // do nothing
}

// destructor
CL_MPI_Allgather::~CL_MPI_Allgather() {
  clear();
}

// execute
void CL_MPI_Allgather::execute(const Mesh::VertexInfo& send,
                               std::vector<Mesh::VertexInfo>& recv) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);

  // number of processors
  int n_procs;
  MPI_Comm_size(comm_, &n_procs);

  // 1. geometrical dimension
  // set gdim directly from send
  const int gdim = send.gdim;
  for (Mesh::VertexInfo& ele : recv) {
    ele.gdim = gdim;
  }

  // 2. number of vertices
  std::vector<int> recv_n_verts(n_procs);
  MPI_Allgather(&send.n_verts, 1, MPI_INT,
                recv_n_verts.data(), 1, MPI_INT, comm_);

  for (int i = 0; i != n_procs; ++i) {
    recv[i].n_verts = recv_n_verts[i];
  }

  // 3. coords
  std::vector<Coord_T> recv_coords;
  std::vector<int> recv_coords_displs(1, 0);
  std::vector<int> recv_counts;
  assert(recv_counts.size() == 0);

  for (const Mesh::VertexInfo& ele : recv) {
    recv_coords_displs.push_back(recv_coords_displs.back() + ele.n_verts * gdim);
    recv_counts.push_back(ele.n_verts * gdim);
  }
  // remove last element in displs
  recv_coords_displs.pop_back();

  // resize receive vector
  const int counts = std::accumulate(recv_counts.begin(), recv_counts.end(), 0);
  recv_coords.resize(counts);

  assert(send.n_verts * gdim == send.coords.size());
  // allgatherv
  MPI_Allgatherv(send.coords.data(), send.coords.size(), MPI_DOUBLE,
                 recv_coords.data(), recv_counts.data(), recv_coords_displs.data(),
                 MPI_DOUBLE, comm_);;


  // assign receive info to recv
  for (int i = 0; i != recv.size(); ++i) {
    assert(recv[i].coords.size() == 0);

    recv[i].coords.insert(recv[i].coords.end(),
                          recv_coords.begin() + recv_coords_displs[i],
                          recv_coords.begin() + recv_coords_displs[i] + recv_counts[i]);
  }

}

}; // namespace Communication

}; // namespace ChenLiu


// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "communication/MPI_scatterv.hh"

namespace ChenLiu {

namespace Communication {

// constructor
CL_MPI_Scatterv::CL_MPI_Scatterv(int root, const MPI_Comm& comm):
  CL_MPI(root, comm) {
}

// destructor
CL_MPI_Scatterv::~CL_MPI_Scatterv() {
  clear();
}

// execute
void CL_MPI_Scatterv::execute(const std::vector<Mesh::EntityInfo>& send,
                              Mesh::EntityInfo& recv) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);
  assert(recv.connections.size() == 0);
  assert(recv.entity_offsets.size() == 0);
  assert(recv.coords.size() == 0);
  assert(recv.material_ids.size() == 0);
  assert(recv.global_indices.size() == 0);

  // number of processors
  int n_procs;
  MPI_Comm_size(comm_, &n_procs);

  assert(send.size() == n_procs);

  // collect sizes to communicate
  const int n_size = recv.buffer_size;
  std::vector<std::vector<int>> send_sizes(n_procs, std::vector<int>(n_size));
  for (int i = 0; i != n_procs; ++i) {
    send_sizes[i] = collect_entity_info_sizes(send[i]);
  }

  std::vector<int> recv_size;
  scatterv(send_sizes, recv_size, n_size, MPI_INT, root_, comm_);

  //// 1. topological dimension
  recv.tdim = recv_size[0];

  //// 2. geometrical dimension
  recv.gdim = recv_size[1];

  //// 3. number of vertices
  recv.n_verts = recv_size[2];

  //// 4. coords
  std::vector<std::vector<Coord_T>> send_coords(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_coords[i].size() == 0);
    send_coords[i].insert(send_coords[i].end(), send[i].coords.begin(),
                          send[i].coords.end());
  }

  scatterv(send_coords, recv.coords, recv.n_verts * recv.gdim,
           MPI_DOUBLE, root_, comm_);

  //// 5. connections
  std::vector<std::vector<int>> send_connects(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_connects[i].size() == 0);
    send_connects[i].insert(send_connects[i].end(),
                            send[i].connections.begin(), send[i].connections.end());
  }

  const int n_connects = recv_size[4];
  scatterv(send_connects, recv.connections, n_connects, MPI_INT, root_, comm_);

  //// 6. n_entities
  recv.n_entities = recv_size[3];

  //// 7. entity_offsets
  std::vector<std::vector<int>> send_entity_offsets(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_entity_offsets[i].size() == 0);
    send_entity_offsets[i].insert(send_entity_offsets[i].end(),
                                  send[i].entity_offsets.begin(),
                                  send[i].entity_offsets.end());
  }

  scatterv(send_entity_offsets, recv.entity_offsets, recv.n_entities + 1,
           MPI_INT, root_, comm_);

  //// 8. material_ids
  std::vector<std::vector<int>> send_material_ids(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_material_ids[i].size() == 0);
    send_material_ids[i].insert(send_material_ids[i].end(),
                                send[i].material_ids.begin(),
                                send[i].material_ids.end());
  }

  scatterv(send_material_ids, recv.material_ids, recv.n_entities,
           MPI_INT, root_, comm_);

  //// 9. global_indices
  std::vector<std::vector<int>> send_global_indices(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_global_indices[i].size() == 0);
    send_global_indices[i].insert(send_global_indices[i].end(),
                                  send[i].global_indices.begin(),
                                  send[i].global_indices.end());
  }

  scatterv(send_global_indices, recv.global_indices, recv.n_entities,
           MPI_INT, root_, comm_);

}

// execute
void CL_MPI_Scatterv::execute(const std::vector<Mesh::MaterialIdInfo>& send,
                              Mesh::MaterialIdInfo& recv) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);
  assert(recv.connections.size() == 0);
  assert(recv.entity_offsets.size() == 0);
  assert(recv.material_ids.size() == 0);

  // number of processors
  int n_procs;
  MPI_Comm_size(comm_, &n_procs);

  // collect sizes to communicate
  const int n_size = recv.buffer_size;
  std::vector<std::vector<int>> send_sizes(n_procs, std::vector<int>(n_size));
  for (int i = 0; i != n_procs; ++i) {
    send_sizes[i][0] = send[i].tdim;
    send_sizes[i][1] = send[i].gdim;
    send_sizes[i][2] = send[i].n_entities;
    send_sizes[i][3] = send[i].connections.size();
  }

  std::vector<int> recv_size;
  scatterv(send_sizes, recv_size, n_size, MPI_INT, root_, comm_);

  //// 1. topological dimension
  recv.tdim = recv_size[0];
  //bcast_tdim(send, recv, root_, comm_);

  //// 2. geometrical dimension
  recv.gdim = recv_size[1];
  //bcast_gdim(send, recv, root_, comm_);

  //// 3. number of entities
  recv.n_entities = recv_size[2];
  //scatter_n_entities(send, recv, root_, comm_);

  //// 4. connections
  std::vector<std::vector<int>> send_connects(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_connects[i].size() == 0);
    send_connects[i].insert(send_connects[i].end(),
                            send[i].connections.begin(), send[i].connections.end());
  }

  const int n_connects = recv_size[3];
  scatterv(send_connects, recv.connections, n_connects, MPI_INT, root_, comm_);

  //scatterv_connections(send, recv, root_, comm_);

  //// 5. entity_offsets
  //scatterv_entity_offsets(send, recv, root_, comm_);
  std::vector<std::vector<int>> send_entity_offsets(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_entity_offsets[i].size() == 0);
    send_entity_offsets[i].insert(send_entity_offsets[i].end(),
                                  send[i].entity_offsets.begin(),
                                  send[i].entity_offsets.end());
  }

  scatterv(send_entity_offsets, recv.entity_offsets, recv.n_entities + 1,
           MPI_INT, root_, comm_);

  //// 6. material_ids
  std::vector<std::vector<int>> send_material_ids(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_material_ids[i].size() == 0);
    send_material_ids[i].insert(send_material_ids[i].end(),
                                send[i].material_ids.begin(),
                                send[i].material_ids.end());
  }

  scatterv(send_material_ids, recv.material_ids, recv.n_entities,
           MPI_INT, root_, comm_);
  //scatterv_material_ids(send, recv, root_, comm_);

}

// execute
void CL_MPI_Scatterv::execute(const std::vector<Mesh::GlobalIndexInfo>& send,
                              Mesh::GlobalIndexInfo& recv) {
  assert(comm_ != MPI_COMM_NULL);
  assert(rank_ >= 0);
  assert(root_ >= 0);
  assert(recv.connections.size() == 0);
  assert(recv.entity_offsets.size() == 0);
  assert(recv.global_indices.size() == 0);

  // number of processors
  int n_procs;
  MPI_Comm_size(comm_, &n_procs);

  // collect sizes to communicate
  const int n_size = recv.buffer_size;
  std::vector<std::vector<int>> send_sizes(n_procs, std::vector<int>(n_size));
  for (int i = 0; i != n_procs; ++i) {
    send_sizes[i][0] = send[i].tdim;
    send_sizes[i][1] = send[i].gdim;
    send_sizes[i][2] = send[i].n_entities;
    send_sizes[i][3] = send[i].connections.size();
  }

  std::vector<int> recv_size;
  scatterv(send_sizes, recv_size, n_size, MPI_INT, root_, comm_);

  //// 1. topological dimension
  recv.tdim = recv_size[0];
  //bcast_tdim(send, recv, root_, comm_);

  //// 2. geometrical dimension
  recv.gdim = recv_size[1];
  //bcast_gdim(send, recv, root_, comm_);

  //// 3. number of entities
  recv.n_entities = recv_size[2];
  //scatter_n_entities(send, recv, root_, comm_);

  //// 4. connections
  std::vector<std::vector<int>> send_connects(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_connects[i].size() == 0);
    send_connects[i].insert(send_connects[i].end(),
                            send[i].connections.begin(), send[i].connections.end());
  }

  const int n_connects = recv_size[3];
  scatterv(send_connects, recv.connections, n_connects, MPI_INT, root_, comm_);

  //scatterv_connections(send, recv, root_, comm_);

  //// 5. entity_offsets
  //scatterv_entity_offsets(send, recv, root_, comm_);
  std::vector<std::vector<int>> send_entity_offsets(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_entity_offsets[i].size() == 0);
    send_entity_offsets[i].insert(send_entity_offsets[i].end(),
                                  send[i].entity_offsets.begin(),
                                  send[i].entity_offsets.end());
  }

  scatterv(send_entity_offsets, recv.entity_offsets, recv.n_entities + 1,
           MPI_INT, root_, comm_);

  //// 6. material_ids
  std::vector<std::vector<int>> send_global_indices(n_procs);
  for (int i = 0; i != n_procs; ++i) {
    assert(send_global_indices[i].size() == 0);
    send_global_indices[i].insert(send_global_indices[i].end(),
                                  send[i].global_indices.begin(),
                                  send[i].global_indices.end());
  }

  scatterv(send_global_indices, recv.global_indices, recv.n_entities,
           MPI_INT, root_, comm_);
  //scatterv_material_ids(send, recv, root_, comm_);

}

// scatterv
template<class DT>
void CL_MPI_Scatterv::scatterv(const std::vector<std::vector<DT>>& send,
                               std::vector<DT>& recv,
                               int recv_size, MPI_Datatype mpi_dtype,
                               int root, const MPI_Comm& comm) const {

  // resize recv
  recv.resize(recv_size);

  // prepare data to send
  std::vector<DT> send_data;
  std::vector<int> send_data_counts;
  std::vector<int> send_data_displs(1, 0);

  assert(send_data.size() == 0);
  assert(send_data_counts.size() == 0);

  if (rank_ == root) {
    for (const std::vector<DT>& ele : send) {
      send_data.insert(send_data.end(), ele.begin(), ele.end());

      send_data_counts.push_back(ele.size());

      send_data_displs.push_back(send_data_displs.back() + ele.size());
    }
    // remove last element in displs
    send_data_displs.pop_back();
  }

  MPI_Scatterv(send_data.data(), send_data_counts.data(),
               send_data_displs.data(),
               mpi_dtype, recv.data(),
               recv.size(), mpi_dtype, root, comm);

}

// Instantiation
template void CL_MPI_Scatterv::scatterv(const std::vector<std::vector<int>>&,
                                        std::vector<int>&, int, MPI_Datatype,
                                        int, const MPI_Comm&) const;
template void CL_MPI_Scatterv::scatterv(const std::vector<std::vector<double>>&,
                                        std::vector<double>&, int, MPI_Datatype,
                                        int, const MPI_Comm&) const;

}; // namespace Communication

}; // namespace ChenLiu


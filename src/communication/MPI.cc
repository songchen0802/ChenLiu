// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#include "communication/MPI.hh"

namespace ChenLiu {

namespace Communication {

// constructor
CL_MPI::CL_MPI(int root, const MPI_Comm& comm):
  root_(root), comm_(comm) {
  assert(comm != MPI_COMM_NULL);

  MPI_Comm_rank(comm_, &rank_);
}

CL_MPI::CL_MPI(const MPI_Comm& comm): comm_(comm) {
  assert(comm != MPI_COMM_NULL);
  root_ = 0;
  MPI_Comm_rank(comm_, &rank_);
}

// destructor
CL_MPI::~CL_MPI() {
  clear();
}

// clear
void CL_MPI::clear() {
  comm_ = MPI_COMM_NULL;
  rank_ = -1;
  root_ = -1;
}

// collect necessary sizes in EntityInfo
std::vector<int> CL_MPI::collect_entity_info_sizes(const Mesh::EntityInfo
    entity_info) {

  std::vector<int> sizes(entity_info.buffer_size);

  sizes[0] = entity_info.tdim;
  sizes[1] = entity_info.gdim;
  sizes[2] = entity_info.n_verts;
  sizes[3] = entity_info.n_entities;
  sizes[4] = entity_info.connections.size();

  return sizes;
}

// collect necessary sizes in MaterialIdInfo
std::vector<int> CL_MPI::collect_material_id_info_sizes(
  const Mesh::MaterialIdInfo material_id_info) {

  std::vector<int> sizes(material_id_info.buffer_size);

  sizes[0] = material_id_info.tdim;
  sizes[1] = material_id_info.gdim;
  sizes[2] = material_id_info.n_entities;
  sizes[3] = material_id_info.connections.size();

  return sizes;
}

// collect necessary sizes in GlobalIndexInfo
std::vector<int> CL_MPI::collect_global_index_info_sizes(
  const Mesh::GlobalIndexInfo global_index_info) {

  std::vector<int> sizes(global_index_info.buffer_size);

  sizes[0] = global_index_info.tdim;
  sizes[1] = global_index_info.gdim;
  sizes[2] = global_index_info.n_entities;
  sizes[3] = global_index_info.connections.size();

  return sizes;
}

// collect necessary sizes in CellDofInfo
std::vector<int> CL_MPI::collect_cell_dof_info_sizes(
  const Space::CellDofInfo cell_dof_info) {

  std::vector<int> sizes(cell_dof_info.buffer_size);

  sizes[0] = cell_dof_info.global_entity_indices.size();
  sizes[1] = cell_dof_info.dofs.size();

  return sizes;
}

}; // namespace Communication

}; // namespace ChenLiu


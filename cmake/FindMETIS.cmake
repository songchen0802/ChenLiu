# ChenLiu Project
# Copyright (C) 2020--2024 Chen Song 宋宸
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

include(FindPackageHandleStandardArgs)

find_path(METIS_DIR include/metis.h HINTS ENV METIS_DIR DOC "METIS Install Directory")

if(EXISTS ${METIS_DIR}/include/metis.h)
  set(METIS_FOUND TRUE)
  find_path(METIS_INCLUDE_DIR NAMES metis.h 
                              HINTS ${METIS_DIR} 
                              PATH_SUFFIXES include/ 
                              REQUIRED NO_DEFAULT_PATH)
  find_library(METIS_LIBRARY NAMES metis 
                             HINTS ${METIS_DIR} 
	                  		     PATH_SUFFIXES build/Linux-x86_64/libmetis/ OR lib/x86_64-linux-gnu/ OR lib/
                             REQUIRED NO_DEFAULT_PATH)
else(EXISTS ${METIS_DIR}/include/metis.h)
  set(METIS_FOUND FALSE)
endif(EXISTS ${METIS_DIR}/include/metis.h)

find_package_handle_standard_args(METIS DEFAULT_MSG METIS_INCLUDE_DIR METIS_LIBRARY)

mark_as_advanced(CLEAR METIS_INCLUDE_DIR METIS_LIBRARY METIS_DIR)

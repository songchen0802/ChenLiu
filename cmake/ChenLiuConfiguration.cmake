# ChenLiu Project
# Copyright (C) 2020--2024 Chen Song 宋宸
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

include(CMakePrintHelpers)

# mpi
find_package(MPI REQUIRED)

# Boost library
set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
set(BOOST_ALL_DYN_LINK ON) 

option(Boost_NO_BOOST_CMAKE OFF)
find_package(Boost 1.80.0 REQUIRED)

# METIS 
set(WITH_METIS OFF)
option(WANT_METIS "Compile Mesh module with METIS partitioner.")
if(WANT_METIS)
    find_package(METIS REQUIRED)
    if(METIS_FOUND)
        set(WITH_METIS TRUE)
        add_library(metis SHARED IMPORTED)
        set_target_properties(metis PROPERTIES IMPORTED_LOCATION ${METIS_LIBRARY})
        set(METIS_VERSION "5")
    endif(METIS_FOUND)
endif(WANT_METIS)

# PETSC
set(WITH_PETSC OFF)
option(WANT_PETSC "Compile linear algebra and linear solver module with PETSC library.")
if (WANT_PETSC)
  find_package(PETSC REQUIRED)
  if (PETSC_FOUND)
    set(WITH_PETSC TRUE)
    add_library(petsc SHARED IMPORTED)
    set_target_properties(petsc PROPERTIES IMPORTED_LOCATION ${PETSC_LIBRARY})
  endif (PETSC_FOUND)
endif (WANT_PETSC)


# ChenLiu Project
# Copyright (C) 2020--2024 Chen Song 宋宸
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

include(FindPackageHandleStandardArgs)

find_file(PETSC_CONFIG include/petsc/petscconf.h OR lib/petsc/include/petscconf.h HINTS ENV PETSC_CONFIG DOC "PETSC configure file")

file(READ ${PETSC_CONFIG} PETSC_CONF_CONTENT)

# Parse PETSC_DIR
string(REGEX MATCH "#define PETSC_DIR \"([^\"]+)" PETSC_DIR_MATCH "${PETSC_CONF_CONTENT}")
if(PETSC_DIR_MATCH)
    set(PETSC_DIR ${CMAKE_MATCH_1})
else()
    message(FATAL_ERROR "PETSC_DIR not found in petscconf.h")
endif()

string(REGEX MATCH "#define PETSC_ARCH \"([^\"]*)" PETSC_ARCH_MATCH "${PETSC_CONF_CONTENT}")
if(PETSC_ARCH_MATCH)
    set(PETSC_ARCH ${CMAKE_MATCH_1})
    if(NOT PETSC_ARCH)
        message("PETSC_ARCH is empty")
    else()
        message("PETSC_ARCH found: ${PETSC_ARCH}")
    endif()
else()
    message(FATAL_ERROR "PETSC_ARCH not found in petscconf.h")
endif()

if(EXISTS ${PETSC_DIR}/include/petsc.h)
  set(PETSC_FOUND TRUE)
  find_path(PETSC_INCLUDE_DIR NAMES petsc.h
                              HINTS ${PETSC_DIR}
                              PATH_SUFFIXES include/
                              REQUIRED NO_DEFAULT_PATH)
  find_library(PETSC_LIBRARY NAMES petsc OR petsc_real OR petsc_complex
                             HINTS ${PETSC_DIR}/${PETSC_ARCH}
                             PATH_SUFFIXES lib/
                             REQUIRED NO_DEFAULT_PATH)
else(EXISTS ${PETSC_DIR}/include/petsc.h)
  set(PETSC_FOUND FALSE)
endif(EXISTS ${PETSC_DIR}/include/petsc.h)

find_package_handle_standard_args(PETSC DEFAULT_MSG PETSC_INCLUDE_DIR PETSC_LIBRARY)

mark_as_advanced(CLEAR PETSC_CONFIG PETSC_CONF_CONTENT PETSC_DIR PETSC_ARCH PETSC_DIR_MATCH PETSC_ARCH_MATCH PETSC_INCLUDE_DIR PETSC_LIBRARY)

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef QUADRATURE_QUADRATURE_HH
#define QUADRATURE_QUADRATURE_HH

#include <cstddef>
#include <numeric>

#include "common/PreDef.hh"
#include "common/Coordinate.hh"

namespace ChenLiu {
namespace Quadrature {

template<class DT>
class Quadrature {

public:
  // constructor
  Quadrature();

  // destructor
  ~Quadrature();

  // initialize
  virtual void init(int order) = 0;

  // cell type
  CellType cell_type() const;

  // size of quadrature points
  int size() const;

  // dimension of quadrature point
  int dim() const;

  // quadrature point order
  int order() const;

  // quadrature points
  std::vector<Coordinate> p() const;
  Coordinate p(int idx) const;

  // weights
  std::vector<DT> w() const;
  DT w(int idx) const;

protected:

  // quadrature points
  std::vector<Coordinate> coords_;

  // weights
  std::vector<DT> weights_;

  // size of quadrature points
  int size_;

  // dimension of quadrature point
  int dim_;

  // order
  int order_;

  // cell type
  CellType cell_type_;

}; // Quadrature

}; // namespace Quadrature
}; // namepsace ChenLiu


#endif // QUADRATURE_QUADRATURE_HH

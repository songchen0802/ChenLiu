// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef QUADRATURE_QUADRATUREFACTORY_HH
#define QUADRATURE_QUADRATUREFACTORY_HH

#include <cstddef>
#include <numeric>
#include <memory>

#include "Quadrature.hh"
#include "QuadratureGauss.hh"
#include "QuadratureGauss_Line.hh"
#include "QuadratureGauss_Triangle.hh"
#include "QuadratureGauss_Quadrilateral.hh"
#include "QuadratureGauss_Tetrahedron.hh"
#include "QuadratureGauss_Hexahedron.hh"

namespace ChenLiu {
namespace Quadrature {

template<class DT>
class QuadratureFactory {

public:
  // constructor
  std::unique_ptr<Quadrature<DT>> create(QP_Type qp_type, CellType cell_type, int order);

  // element type
  QP_Type qp_type() const;

  // cell type
  CellType cell_type() const;

  // order
  int order() const;

private:
  // quadrature point type
  QP_Type qp_type_;

  // cell type
  CellType cell_type_;

  // order
  int order_;

  // Lagrange element
  std::unique_ptr<Quadrature<DT>> Gauss_quadrature(CellType cell_type, int order) const;

}; // QuadratureFactory

}; // namespace Quadrature
}; // namepsace ChenLiu


#endif // QUADRATURE_QUADRATUREFACTORY_HH

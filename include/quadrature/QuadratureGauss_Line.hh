// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef QUADRATURE_QUADRATUREGAUSS_LINE_HH
#define QUADRATURE_QUADRATUREGAUSS_LINE_HH

#include <cstddef>
#include <numeric>

#include "QuadratureGauss.hh"

namespace ChenLiu {
namespace Quadrature {

template<class DT>
class QuadratureGauss_Line : public QuadratureGauss<DT> {

public:
  // constructor
  QuadratureGauss_Line();

  // destructor
  ~QuadratureGauss_Line();

  // initialize
  void init(int order) override;

private:
  void set_points_weights_order1();
  void set_points_weights_order2();
  void set_points_weights_order3();
  void set_points_weights_order4();

}; // Quadrature

}; // namespace Quadrature
}; // namepsace ChenLiu


#endif // QUADRATURE_QUADRATUREGAUSS_LINE_HH

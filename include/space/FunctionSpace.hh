// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef SPACE_FUNCTIONSPACE_HH
#define SPACE_FUNCTIONSPACE_HH

#include "mesh/Mesh.hh"
#include "DegreeOfFreedom.hh"

namespace ChenLiu {
namespace Space {

template<class DT>
class FunctionSpace {
public:
  // constructor
  FunctionSpace();
  FunctionSpace(const std::shared_ptr<const Mesh::Mesh> mesh_ptr, MPI_Comm comm,
                std::vector<Element_Family> element_family, std::vector<int> degree);

  // destructor
  ~FunctionSpace();

  // clear
  void clear();

  // number of varaibles
  int n_var() const;

  // MPI Coom
  MPI_Comm mpi_comm() const;

  // mesh pointer
  const std::shared_ptr<const Mesh::Mesh> mesh_ptr() const;

  // dofs for cells
  std::map<int, Cell_dofs> cell_dofs(int idx) const;

  // dofs for facets
  std::map<int, Facet_dofs> facet_dofs(int idx) const;

  // global dof offsets
  std::vector<int> global_dof_offsets() const;

private:
  // variables //

  // mesh pointer
  std::shared_ptr<const Mesh::Mesh> mesh_ptr_;

  // MPI Comm
  MPI_Comm comm_;

  // Element Family
  std::vector<Element_Family> ele_family_;

  // element degree
  std::vector<int> ele_deg_;

  // degree of freedom
  std::vector<DegreeOfFreedom<DT>> dof_;

  // global dof offsets
  std::vector<int> global_dof_offsets_;

}; // FunctionSpace

}; // namespace Space
}; // namespace ChenLiu

#endif // SPACE_FUNCTIONSPACE_HH

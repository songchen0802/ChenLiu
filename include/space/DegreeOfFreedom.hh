// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef SPACE_DEGREEOFFREEDOM_HH
#define SPACE_DEGREEOFFREEDOM_HH

#include <cstddef>
#include <numeric>
#include <map>
#include <unordered_set>
#include <ranges>

#include "common/PreDef.hh"
#include "common/Coordinate.hh"

#include "Dof_Data_Structure.hh"
#include "Numbering.hh"
#include "Numbering_CG.hh"
#include "Numbering_DG.hh"

namespace ChenLiu {
namespace Space {

template<class DT>
class DegreeOfFreedom {

public:
  // constructor
  DegreeOfFreedom();

  // copy constructor
  DegreeOfFreedom(const DegreeOfFreedom& DOF);

  // destructor
  ~DegreeOfFreedom();

  // clear
  void clear();

  // create
  void create(const std::shared_ptr<const Mesh::Mesh> mesh_ptr, MPI_Comm comm,
              Element_Family ele_family, int degree);

  // total dof size
  int total_dof_size() const;

  // dofs for cells
  std::map<int, Cell_dofs> cell_dofs() const;

  // dodfs for facets
  std::map<int, Facet_dofs> facet_dofs() const;

protected:
  //// functions ////

  // compute total dof size
  void compute_total_dof_size(std::vector<int> dof_size_per_proc);

  //// varialbes ////

  // degree of freedom for cells
  std::map<int, Cell_dofs> cell_dofs_;

  // degree of freedom for facets
  std::map<int, Facet_dofs> facet_dofs_;

  // pointer to Mesh
  std::shared_ptr<const Mesh::Mesh> mesh_ptr_;

  // MPI Comm
  MPI_Comm comm_;

  // offset for global dofs
  int total_dof_size_;

}; // DegreeOfFreedom

}; // namespace Space
}; // namepsace ChenLiu

#endif // SPACE_DEGREEOFFREEDOM_HH

// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef DOF_COMMUNICATION_HH
#define DOF_COMMUNICATION_HH

#include <vector>
#include <cassert>
#include <set>

#include "common/PreDef.hh"

namespace ChenLiu {

namespace Space {

struct CellDofInfo {
  std::vector<int> global_entity_indices;
  std::vector<int> dofs;
  std::vector<int> dof_offsets;

  const int buffer_size = 2;
};

}; // namespace Space

}; // namespace ChenLiu

#endif // DOF_COMMUNICATION_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef SPACE_DOFMAP_HH
#define SPACE_DOFMAP_HH

#include <cstddef>
#include <map>
#include <array>
#include <vector>


namespace ChenLiu {
namespace Space {

class DofMap {

public:
  // constructor
  DofMap();

  // destructor
  ~DofMap();

  // clear
  void clear();

  // create
  void create(int lowest_interior_dof, int highest_interior_dof,
              std::map<int, std::vector<int>> ghost_map, int size_global);

  // interior size
  int size_interior() const;

  // ghost size
  int size_ghost() const;

  // total size of dofs
  int size_global() const;

protected:
  // interior dof index range
  std::array<int, 2> interior_dofs_range_;

  // ghost dof indices
  std::vector<int> ghost_dofs_;

  // ghost dof owner positions in ghost_dofs
  std::vector<int> ghost_owner_pos_;

  // ghost owners 
  std::vector<int> ghost_owners_;

  // global size
  int size_global_;

}; // DofMap

}; // namespace Space
}; // namepsace ChenLiu

#endif // SPACE_DOFMAP_HH

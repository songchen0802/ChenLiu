// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef SPACE_NUMBERING_DG_HH
#define SPACE_NUMBERING_DG_HH

#include "Numbering.hh"

namespace ChenLiu {
namespace Space {

template<class DT>
class Numbering_DG : public Numbering<DT> {

public:
  // constructor
  Numbering_DG();

  // destructor
  ~Numbering_DG();

  // number function
  void number(std::map<int, Cell_dofs>& cell_dofs,
              std::map<int, Facet_dofs>& facet_dofs);

  // renumber based on common dofs (not needed for DG)
  void renumber_common_dofs(std::map<int, Cell_dofs>& cell_dofs,
                            std::map<int, Facet_dofs>& facet_dofs);

  // update dofs on ghost cells
  void update_ghost_dofs_global(std::map<int, Cell_dofs>& cell_dofs,
                                std::map<int, Facet_dofs>& facet_dofs);


}; // Numbering_DG

}; // namespace Space
}; // namepsace ChenLiu


#endif // SPACE_NUMBERING_DG_HH

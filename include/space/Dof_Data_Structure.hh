// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef DOF_DATA_STRUCTURE_HH
#define DOF_DATA_STRUCTURE_HH

#include <vector>
#include <cassert>
#include <set>

#include "common/PreDef.hh"

namespace ChenLiu {

namespace Space {

// data structure for cell dofs
struct Cell_dofs {
  std::vector<int> dofs;
};

// data structure for facet dofs
struct Facet_dofs {
  std::vector<std::vector<int>> dofs;

  std::vector<int> cell_local_index;
  std::vector<int> cell_global_index;

  std::vector<int> cell_sub_domain_id;
};

}; // namespace Space

}; // namespace ChenLiu

#endif // DOF_DATA_STRUCTURE_HH

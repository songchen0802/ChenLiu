// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef SPACE_NUMBERING_HH
#define SPACE_NUMBERING_HH

#include <cstddef>
#include <numeric>
#include <memory>

#include "common/Utils.hh"
#include "common/Coordinate.hh"
#include "mesh/Iterators.hh"
#include "fem/ElementFactory.hh"
#include "fem/Element.hh"
#include "communication/MPI_isend.hh"
#include "communication/MPI_irecv.hh"

#include "Dof_Data_Structure.hh"
#include "Dof_Communication.hh"

namespace ChenLiu {
namespace Space {

template<class DT>
class Numbering {

public:
  // constructor
  Numbering();

  // destructor
  ~Numbering();

  // clear
  void clear();

  // initialization
  void init(const std::shared_ptr<const Mesh::Mesh> mesh_ptr, MPI_Comm comm,
            int degree, FE_Type fe_type);

  // collect coordinates of dofs on cells and facets
  void collect_dofs_coordinates();

  // number function
  virtual void number(std::map<int, Cell_dofs>& cell_dofs,
                      std::map<int, Facet_dofs>& facet_dofs) = 0;

  // initial numbering
  void initial_numbering(std::map<int, Cell_dofs>& cell_dofs,
                         std::map<int, Facet_dofs>& facet_dofs);

  // renumber based on common dofs
  virtual void renumber_common_dofs(std::map<int, Cell_dofs>& cell_dofs,
                                    std::map<int, Facet_dofs>& facet_dofs) = 0;

  // update dofs between processors
  void update_local_dofs_global(std::map<int, Cell_dofs>& cell_dofs,
                                std::map<int, Facet_dofs>& facet_dofs);

  // update dofs on ghost cells
  virtual void update_ghost_dofs_global(std::map<int, Cell_dofs>& cell_dofs,
                                        std::map<int, Facet_dofs>& facet_dofs) = 0;

  // total dof size
  std::vector<int> dof_size_all() const;

protected:
  //// variables ////

  // coordinates of dofs on cells
  std::map<int, std::vector<Coordinate>> coords_cells_;

  // coordinates of dofs on facet
  std::map<int, std::vector<Coordinate>> coords_facets_;

  // element degree
  int ele_deg_;

  // pointer to Mesh
  std::shared_ptr<const Mesh::Mesh> mesh_ptr_;

  // MPI Comm
  MPI_Comm comm_;

  // finite element type
  FE_Type fe_type_;

  // dof size on each processor
  std::vector<int> dof_size_all_;

  //// functions ////

  // exchange cell dof info
  void exchange_cell_dof_info(std::map<int, Cell_dofs>& cell_dofs,
                              std::map<int, Facet_dofs>& facet_dofs);

}; // Numbering

}; // namespace Space
}; // namepsace ChenLiu


#endif // SPACE_NUMBERING_HH

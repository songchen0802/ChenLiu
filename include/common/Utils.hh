// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef UTIL_HH
#define UTIL_HH

#include <vector>
#include <map>
#include <cassert>
#include <cstddef>
#include <algorithm>

#include "PreDef.hh"

namespace ChenLiu {

bool has_same_elements(std::vector<int>a, std::vector<int> b);

bool is_all_same(std::vector<int> v);

// convert from string to CellType
CellType str2cell_type(std::string str);

// convert from CellType to string
std::string cell_type2str(CellType type);

// convert from string to FE_Type
FE_Type str2fe_type(std::string str);

// convert from FE_Type to string
std::string fe_type2str(FE_Type type);

// check is equal
template<class DT>
bool is_equal(const DT v0, const DT v1, double tol = COMP_TOL) {

  if (std::fabs(v0 - v1) > static_cast<DT>(COMP_TOL)) {
    return false;
  } else {
    return true;
  }

}

template<class DT>
bool is_equal(const std::vector<DT> v0, const std::vector<DT> v1,
              double tol = COMP_TOL) {
  assert(v0.size() == v1.size());

  for (int i = 0; i != v0.size(); ++i) {
    if (std::fabs(v0[i] - v1[i]) > static_cast<DT>(COMP_TOL)) {
      return false;
    }

  }

  return true;
}

} // namespace ChenLiu

#endif // UTIL_HH

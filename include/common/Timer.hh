// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#ifndef TIMER_HH
#define TIMER_HH

#include <iostream>
#include <assert.h>
#include <string>
#include <ctime>

namespace ChenLiu {

class Timer {
public:

  Timer();

  Timer(std::string stack);

  ~Timer();

  void start();
  void stop();

  void resume();

  double duration();

  void print();

  std::string task();

private:

  std::clock_t t_start_, t_end_;

  std::string task_;

};

}

#endif

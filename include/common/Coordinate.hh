// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef COORDINATE_H
#define COORDINATE_H

#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
#include <cassert>
#include <math.h>
#include <mpi.h>
#include <array>

#include "PreDef.hh"

namespace ChenLiu {

class Coordinate {
public:

  // constructor
  Coordinate();
  Coordinate(Coord_T x, Coord_T y, Coord_T z);
  Coordinate(Coord_T x, Coord_T y);
  Coordinate(Coord_T x);
  Coordinate(std::vector<Coord_T> coord);
  Coordinate(int gdim);

  // destructor
  ~Coordinate();

  // initialize
  void init(int d);

  // x, y, z
  Coord_T x() const;
  Coord_T y() const;
  Coord_T z() const;

  // operator []
  Coord_T operator[](int i) const;
  Coord_T& operator[](int i);

  // set
  void set_coord(std::vector<Coord_T> coord);
  void set_coord(Coordinate coord);

  // get
  std::vector<Coord_T> get_coord() const;

  // move
  void move(std::vector<Coord_T> coord);
  void move(Coordinate coord);

  // compare operator
  bool operator==(Coordinate coord) const;

  // dimension
  int dim() const;

  // print
  void print() const;

private:
  std::vector<Coord_T> coord_;

}; // Coordinate

// overload operator <<
inline std::ostream &operator<<(std::ostream &os, const Coordinate& coord) {
  for (int i = 0; i != coord.dim(); ++i) {
    os << coord[i] << " ";
  }
  return os;
}

}; // namespace ChenLiu

#endif // COORDINATE_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef PREDEFINITION_HH
#define PREDEFINITION_HH

#include <cmath>
#include <ostream>
#include <iostream>

namespace ChenLiu {

// predefine data type for coordinate in mesh in order to avoid too many
// template classes
typedef double Coord_T;

// tolerance
const double COMP_TOL = 1.0e-10;

// cell type
enum class CellType : int {Point, Line, Triangle, Quadrilateral,
                           Tetrahedron, Hexahedron};

// element type
enum class FE_Type : int {Lagrange};

// quadrature point type
enum class QP_Type: int {Gauss};

// Element family
enum class Element_Family: int {CG, DG};
}


#define CL_Error(msg) \
{ \
  std::cerr << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl; \
}
#endif


//#ifndef CL_Error
//#define CL_Error(Message) my_error(__FILE__, __LINE__, Message)
//
//// error message
//void my_error(char* file, int line, char* msg) {
//  std::cerr << file << "(" << line << "): " << msg << std::endl;
//}
//
//#endif

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef VEC_HH
#define VEC_HH

#include <iostream>
#include <vector>
#include <cassert>
#include <cstddef>

#include <boost/numeric/ublas/vector.hpp>

#include "Utils.hh"

namespace ChenLiu {

template<class DT> 
class Vec {
public:

  typedef boost::numeric::ublas::vector<DT> BaseVecType;

  ~Vec();

  Vec(const int N);
  Vec(const Vec<DT>& v);
  Vec(const std::vector<DT> v);
  Vec(const BaseVecType v);

  void clear();

  BaseVecType vec() const;

  int size() const;

  Vec<DT> &operator=(const Vec<DT> v);
  DT operator()(const int i) const;
  DT& operator()(const int i);
  Vec<DT>& operator+=(const Vec<DT> v);
  Vec<DT>& operator-=(const Vec<DT> v);
  Vec<DT>& operator*=(const DT s);
  Vec<DT>& operator/=(const DT s);
  bool operator==(const Vec<DT> v);

private:
  BaseVecType vec_;

}; // Vec

} // namespace ChenLiu

#endif // VECTOR_BASE_HH

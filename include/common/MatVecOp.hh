// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#ifndef MATVECTOP_HH
#define MATVECTOP_HH

#include "Vec.hh"
#include "Mat.hh"

namespace ChenLiu {

template<class DT>
Vec<DT> operator*(const Vec<DT> v, const DT s);

template<class DT>
Vec<DT> operator*(const DT s, const Vec<DT> v);

template<class DT>
Vec<DT> operator/(const Vec<DT> v, const DT s);

template<class DT>
Vec<DT> operator+(const Vec<DT> v1, const Vec<DT> v2);

template<class DT>
Vec<DT> operator-(const Vec<DT> v1, const Vec<DT> v2);

template<class DT>
void VecAxpy(Vec<DT>& y, const DT alpha, const Vec<DT> x);

template<class DT>
void VecAypx(Vec<DT>& y, const DT beta, const Vec<DT> x);

template<class DT>
DT Dot(const Vec<DT> v1, const Vec<DT> v2);

template<class DT>
Vec<DT> Cross(const Vec<DT> a, const Vec<DT> b);

template<class DT>
Vec<DT> MatVecMult(const Mat<DT> m, const Vec<DT> v);

template<class DT>
DT Norm(const Vec<DT> v);

template<class DT>
Mat<DT> Inverse(const Mat<DT> m);

} // namespace ChenLiu

//#include "MatVecOp.cc"

#endif // MATVECTOP_HH


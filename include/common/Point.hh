// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#ifndef POINT_HH
#define POINT_HH

#include <cassert>
#include <array>
#include <vector>
#include <math.h>

#include "Utils.hh"

namespace ChenLiu {

template<class DT> 
class Point {

public:
  // constructors
  Point();
  Point(const DT x, const DT y, const DT z);
  Point(const Point<DT>& p);
  Point(const std::array<DT, 3> x);
  
  // destructor
  ~Point();

  // return values
  DT x() const;
  DT y() const;
  DT z() const;

  // operator []
  DT operator[](const int& i) const;
  DT& operator[](const int& i);

  // operator +
  Point<DT> operator+(const Point<DT>& p) const;

  // operator -
  Point<DT> operator-(const Point<DT>& p) const;

  // operator *
  Point<DT> operator*(const DT a) const;

  // operator /
  Point<DT> operator/(const DT a) const;

  // operator +=
  const Point<DT>& operator+=(const Point<DT>& p);

  // operator -=
  const Point<DT>& operator-=(const Point<DT>& p);

  // operator *=
  const Point<DT>& operator*=(const DT a);

  // operator /=
  const Point<DT>& operator/=(const DT a);

  // operator ==
  bool operator==(const Point<DT>& p) const;

  // operator !=
  bool operator!=(const Point<DT>& p) const;

private:

  std::array<DT, 3> x_;

};

}

#endif // POINT_HH

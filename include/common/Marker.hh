// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef MARKER_HH
#define MARKER_HH

#include <vector>
#include <cstddef>
#include <unordered_map>
#include <cassert>
#include <iostream>

#include "PreDef.hh"

namespace ChenLiu {

template<class DT>
class Marker {

public:

  // constructor
  Marker();
  Marker(int tdim);
  Marker(int tdim, int num_entities);

  // desctructor
  ~Marker();

  // clear
  void clear();

  // set marker
  // idx_entity: entity index
  // val: value to assign
  void set_marker(DT val, int idx_entity);

  // get value
  DT get_marker(int idx_entity) const;

  // size
  int size() const;

  // check number of entities
  bool check_num_entities(int num_entities) const;

  // push back a new marker into marker_
  void add_marker(DT val);
  void add_marker(DT val, const int idx);
  void add_marker(std::vector<DT> val);

private:
  // marker
  std::vector<DT> marker_;

  // topological dimension
  int tdim_;

}; // MeshMarker

}; // namespace ChenLiu

#endif // MARKER_HH

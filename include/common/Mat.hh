// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MAT_HH
#define MAT_HH

#include <iostream>
#include <vector>
#include <cassert>
#include <cstddef>

#include <boost/numeric/ublas/matrix.hpp>

#include "Utils.hh"

namespace ChenLiu {

template<class DT>
class Mat {
public:

  typedef boost::numeric::ublas::matrix<DT> BaseMatType;

  ~Mat();

  Mat(const int N, const int M);
  Mat(const Mat<DT>& m);

  void clear();
  int r_size() const;
  int c_size() const;

  BaseMatType mat() const;

  DT operator()(const int i, const int j) const;
  DT& operator()(const int i, const int j);
  Mat<DT>& operator=(const Mat<DT> m);
  Mat<DT>& operator+=(const Mat<DT> m);
  Mat<DT>& operator-=(const Mat<DT> m);
  Mat<DT>& operator*=(const DT s);
  Mat<DT>& operator/=(const DT s);
  bool operator==(const Mat<DT> m);

private:
  //std::vector<std::vector<DT>> mat_;
  BaseMatType mat_;

}; // Mat

} // namespace ChenLiu

#endif // MAT_HH

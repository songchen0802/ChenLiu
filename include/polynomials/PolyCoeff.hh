// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef POLY_POLYCOEFF_HH
#define POLY_POLYCOEFF_HH

#include <vector>
#include <cstddef>
#include <cassert>
#include <iostream>

namespace ChenLiu {
namespace POLY {

template<class DT>
class PolyCoeff {

public:
  // constructor
  PolyCoeff();
  PolyCoeff(int deg);

  // destructor
  ~PolyCoeff();

  // initialize
  void init(int deg);

  // degree
  int degree() const;

  // clear
  void clear();

  //
  std::vector<DT> poly(int i) const;
  std::vector<DT> poly_deriv1(int i) const;
  std::vector<DT> poly_deriv2(int i) const;

  // set
  void set_poly(std::vector<std::vector<DT>> val);
  void set_poly_deriv1(std::vector<std::vector<DT>> val);
  void set_poly_deriv2(std::vector<std::vector<DT>> val);

protected:
  int deg_;

  std::vector< std::vector<DT> > poly_;
  std::vector< std::vector<DT> > poly_deriv1_;
  std::vector< std::vector<DT> > poly_deriv2_;

}; // PolyCoeff

}; // namespace POLY
}; // namepsace ChenLiu


#endif // POLY_POLYCOEFF_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef POLY_POLYNOMIALLAGRANGE_HH
#define POLY_POLYNOMIALLAGRANGE_HH

#include "Polynomial.hh"
#include "PolyCoeff.hh"

namespace ChenLiu {
namespace POLY {

template<class DT>
class PolynomialLagrange : public Polynomial<DT> {

public:
  // constructor
  PolynomialLagrange();

  // destructor
  ~PolynomialLagrange();

  DT poly(int deg, int i, DT x) const override;
  DT poly_deriv1(int deg, int i, DT x) const override;
  DT poly_deriv2(int deg, int i, DT x) const override;

private:
  //// variables ////

  // maximum degree
  const int max_deg_ = 4;

  // initialize coefficients
  void init_coefficients();

  // evaluate point x
  DT evaluate(int deg, std::vector<DT> coeffs, DT x) const;

  //// functions ////

  // coefficients
  void set_coeff_deg_1();
  void set_coeff_deg_2();
  void set_coeff_deg_3();
  void set_coeff_deg_4();

}; // PolynomialLagrange

}; // namespace POLY
}; // namepsace ChenLiu


#endif // POLY_POLYNOMIALLAGRANGE_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef POLY_POLYNOMIAL_HH
#define POLY_POLYNOMIAL_HH

#include <vector>
#include <cstddef>
#include <cmath>

#include "PolyCoeff.hh"

namespace ChenLiu {
namespace POLY {

template<class DT>
class Polynomial {

public:
  // constructor
  Polynomial();

  // destructor
  virtual ~Polynomial() = 0;

  // clear
  void clear();

  virtual DT poly(int deg, int i, DT x) const = 0;
  virtual DT poly_deriv1(int deg, int i, DT x) const = 0;
  virtual DT poly_deriv2(int deg, int i, DT x) const = 0;

protected:
  // coefficient of polynomials
  std::vector< PolyCoeff<DT> > poly_coeffs_;

}; // Polynomial

}; // namespace POLY
}; // namepsace ChenLiu


#endif // POLY_POLYNOMIAL_HH

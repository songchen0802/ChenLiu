// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_COMMUNICATION_HH
#define MESH_COMMUNICATION_HH

#include <vector>
#include <cassert>
#include <set>

#include "common/PreDef.hh"

namespace ChenLiu {

namespace Mesh {

struct EntityInfo {
  int tdim;
  int gdim;

  int n_verts;
  std::vector<Coord_T> coords;

  std::vector<int> connections;

  int n_entities;
  std::vector<int> entity_offsets;
  std::vector<int> material_ids;
  std::vector<int> global_indices;

  const int buffer_size = 5;
};

struct MaterialIdInfo {
  int tdim;
  int gdim;

  std::vector<int> connections;

  int n_entities;
  std::vector<int> entity_offsets;
  std::vector<int> material_ids;

  const int buffer_size = 4;
};

struct GlobalIndexInfo {
  int tdim;
  int gdim;

  std::vector<int> connections;

  int n_entities;
  std::vector<int> entity_offsets;
  std::vector<int> global_indices;

  const int buffer_size = 4;
};

struct VertexInfo {
  int gdim;

  int n_verts;
  std::vector<Coord_T> coords;
};

struct SharedVertex {
  std::map<int, std::set<int>> table;
};

}; // namespace Mesh

}; // namespace ChenLiu

#endif // MESH_COMMUNICATION_HH

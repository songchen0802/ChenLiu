// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_CELL_HH
#define MESH_CELL_HH

#include <iostream>
#include <cassert>
#include <cstdlib>

#include "common/Coordinate.hh"
#include "common/Vec.hh"
#include "common/MatVecOp.hh"

namespace ChenLiu {
namespace Mesh {

class Cell {
public:
  // constructor
  Cell(CellType cell_type);

  // destructor
  virtual ~Cell();

  // topological dimension
  virtual int tdim() const = 0;

  // number of vertices
  virtual int num_vertices() const = 0;

  // return cell type
  CellType cell_type() const;

  // number of entities for each topological dimension
  virtual int num_entities(int tdim) const = 0;

  // check cell geometry
  virtual bool check_geometry(std::vector<Coordinate> coords, int gdim) const = 0;

  // numeration for sub entities
  virtual std::vector<std::vector<int>> sub_entities_numeration(
                                       int tdim) const = 0;

protected:
  // cell type
  CellType cell_type_;

}; // Cell


}; // namepspace Mesh
}; // namespace ChenLiu

#endif //MESH_CELL_HH


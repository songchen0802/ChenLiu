// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_EDITOR_HH
#define MESH_EDITOR_HH

#include <filesystem>
#include <fstream>
#include <numeric>

#include "common/Coordinate.hh"
#include "Mesh.hh"
#include "Mesh_Communication.hh"

namespace ChenLiu {
namespace Mesh {

class Editor {

public:

  // constructor
  Editor();

  // destructor
  ~Editor();

  // read mesh
  static void read_mesh(std::filesystem::path filename,
                        std::shared_ptr<Mesh> mesh_ptr,
                        int tdim, int gdim, bool check_geo = true);
  static void read_mesh(std::filesystem::path filename,
                        std::shared_ptr<Mesh> mesh_ptr,
                        int tdim, int gdim, MPI_Comm comm, bool check_geo = true);

  // add entity
  // input:
  // 1) tdim: topological dimension
  // 2) idx_verts: vertices index of this entity
  // output:
  // 1) entity index
  static int add_entity(int tdim, std::vector<int> idx_verts,
                        std::shared_ptr<Mesh> mesh_ptr);

  // build mesh from EntityInfo
  static void build_mesh(std::shared_ptr<Mesh> mesh_ptr, EntityInfo entity_info,
                         bool check_geo = true);

  // add cells on current mesh
  static std::map<int, int> add_cells(std::shared_ptr<Mesh> mesh_ptr,
                                      EntityInfo entity_info,
                                      std::map<int, Coordinate> boundary_verts,
                                      int sud_domain,
                                      bool check_geo = true);

  // add material id from MaterialIdInfo for mesh_ptr
  static void add_material_ids(std::shared_ptr<Mesh> mesh_ptr,
                               MaterialIdInfo material_id_info);

  // add global indices from GlobalIndexInfo for mesh_ptr
  static void add_global_indices(std::shared_ptr<Mesh> mesh_ptr,
                                 GlobalIndexInfo global_idx_info);

  // add sub domain id
  static void add_sub_domain_ids(std::shared_ptr<Mesh> mesh_ptr, int id);

  // refine
  static std::shared_ptr<Mesh> refine(const std::shared_ptr<const Mesh> mesh_ptr);

private:
  // split string by space
  static void split_string_space(std::string str,
                                 std::vector<std::string>& results);

  // inp reader
  static void reader_UCD(std::filesystem::path filename,
                         std::shared_ptr<Mesh> mesh_ptr,
                         int tdim, int gdim, bool check_geo);

  // compute connectivities d -> 0
  static void compute_connectivities_d2zero(int d);

}; // Editor

}; // namespace Mesh
}; // namespace ChenLiut

#endif // MESH_EDITOR_HH

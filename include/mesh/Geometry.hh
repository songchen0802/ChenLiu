// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef MESH_GEOMETRY_HH
#define MESH_GEOMETRY_HH

#include <cstddef>
#include <map>
#include <unordered_map>

#include "common/Coordinate.hh"

namespace ChenLiu {
namespace Mesh {

class Geometry {

public:
  // constructor
  Geometry();
  Geometry(int d);
  Geometry(int n, int d);

  // destructor
  ~Geometry();

  // get number of vertices
  int num_vertices() const;

  // geometrical dimension
  int gdim() const;

  // coordinates
  Coordinate coordinate(int idx) const;

private:
  // friend class
  friend class Editor;
  friend class Mesh;

  //// variables ////

  // geometrical dimension
  int gdim_;

  // vertices
  std::vector<Coordinate> coordinates_;

  //// functions ////

  // clear
  void clear();

  // init
  void init(int n, int d);

  // set geometrical dimension
  void set_gdim(int d);

  // set size
  void set_size(int n);

  // add vertex
  int add_vertex(Coordinate vertex);

  // set vertex
  void set_vertex(int idx, Coordinate vertex);
};

}; // namespace Mesh
}; // namespace ChenLiu

#endif // MESH_GEOMETRY_HH

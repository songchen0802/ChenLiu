// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_MESH_HH
#define MESH_MESH_HH

#include <cstdlib>
#include <iostream>
#include <mpi.h>
#include <algorithm>

#include "Geometry.hh"
#include "Topology.hh"
#include "common/Marker.hh"

namespace ChenLiu {

namespace Mesh {

class EntityIterator;

class Mesh {

public:
  // constructor
  Mesh();

  // destructor
  ~Mesh();

  // clear
  void clear();

  // tdim
  int tdim() const;

  // gdim
  int gdim() const;

  // Topology
  const Topology& topology() const;

  // Geometry
  const Geometry& geometry() const;

  // number of entities
  int num_entities(int tdim) const;

  // material id
  int material_id(int tdim, int idx) const;

  // sub domain id
  int sub_domain_id(int idx) const;

private:

  friend class Editor;

  // mesh topology
  Topology topology_;

  // mesh geometry
  Geometry geometry_;

  // material id
  std::map<int, Marker<int> > material_id_;

  // sub domain
  Marker<int> sub_domain_id_;

}; // Mesh

}; // namespace Mesh

}; // namespace ChenLiu

#endif // MESH_MESH_HH

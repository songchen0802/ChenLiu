// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_PARTITION_HH
#define MESH_PARTITION_HH

#include "Graph.hh"
#include "Mesh_Communication.hh"
#include "Iterators.hh"
#include "communication/MPI_scatterv.hh"
#include "communication/MPI_allgather.hh"
#include "communication/MPI_isend.hh"
#include "communication/MPI_irecv.hh"

extern "C" {
#include <metis.h>
}

namespace ChenLiu {
namespace Mesh {

class Partition {

public:
  // partition mesh (mesh_ptr) into disjoint parts
  static std::shared_ptr<Mesh> partition_mesh(
    const std::shared_ptr<const Mesh> mesh_ptr, const MPI_Comm &comm,
    const int master_rank, std::string partitioner="METIS");

  // add ghost layer
  static std::shared_ptr<Mesh> add_ghost_layer(
    const std::shared_ptr<const Mesh> mesh_ptr, const MPI_Comm& comm);

private:
  // partition function based Metis
  static std::shared_ptr<Mesh> Metis_partitioner(
    const std::shared_ptr<const Mesh> mesh_ptr, const MPI_Comm &comm,
    const int master_rank);

  // create distributed mesh based on graph partition results
  // graph partition results are taken cells as nodes to build the graph
  static std::shared_ptr<Mesh> create_distributed_mesh(
    const std::shared_ptr<const Mesh> master_mesh_ptr, const MPI_Comm &comm,
    const int master_rank, std::vector<int> partition_marker);

  // collect cell information for all cells wrt. partition_marker and marker_val
  static std::map<int, int> collect_cell_info(const std::shared_ptr<const Mesh>
      mesh_ptr, EntityInfo& cell_info, std::vector<int> partition_marker,
      const int marker_val);

  // collect global idx information wrt. partition_marker and marker_val
  // on cels
  static void collect_cell_global_indices(
    const std::shared_ptr<const Mesh> mesh_ptr,
    GlobalIndexInfo& global_idx_info,
    const int e_tdim,
    std::vector<int> partition_marker,
    const int marker_val,
    const std::map<int, int> v_idx_permut);

  // collect material id information wrt. partition_marker and marker_val
  // only entities with material id != -1
  static void collect_cell_material_ids(
    const std::shared_ptr<const Mesh>
    mesh_ptr,
    MaterialIdInfo& material_id_info,
    const int e_tdim,
    std::vector<int> partition_marker,
    const int marker_val,
    const std::map<int, int> v_idx_permut);

  // collect index of boundary vertices
  // return: a map. key: vertex index, value: coordinate
  static std::map<int, Coordinate> collect_boundary_vertices(
    const std::shared_ptr<const Mesh> mesh_ptr);

  // identify common vertices
  // key: sub domain. value: vertex index
  static std::map<int, std::set<int>> identify_common_vertices(
                                     std::map<int, Coordinate> boundary_verts,
                                     std::vector<VertexInfo> recv_verts,
                                     int gdim, int my_rank);

}; // Partition

}; // namespace Mesh
}; // namespace ChenLiu

#endif // MESH_PARTITION_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef MESH_ITERATORS_HH
#define MESH_ITERATORS_HH

#include "Mesh.hh"
#include "Entity.hh"

namespace ChenLiu {

namespace Mesh {

class Mesh;

//
// Iterator for Entities and Incidents connected to Entity
// The implementation does not follow stricted format of custorm iterator, 
// to do so, by means of add iterator into Mesh, it involves forward 
// declartions with Mesh and Entity, here it tries to avoid these. It brings
// also one additional simplification, the end of iterator can be simplied
// by using the function is_end()
//

// EntityIterator
class EntityIterator {
public:
  // constructor
  EntityIterator();
  EntityIterator(const std::shared_ptr<const Mesh> mesh_ptr, int tdim, std::string opt="full");
  // copy constructor
  EntityIterator(const EntityIterator& it);

  // destructor
  ~EntityIterator();

  // operators
  EntityIterator& operator++();
  EntityIterator& operator--();
  Entity* operator->();
  Entity& operator*();
  bool operator==(const EntityIterator& it) const;
  bool operator!=(const EntityIterator& it) const;

  // check end of iterator
  bool is_end() const;

private:
  // update local index in Entity
  void update_local_idx();

  // Mesh Entity
  Entity entity_;

  // current position
  int pos_;

  // end position
  int pos_end_;

}; // EntityIterator

// EntityIncidentIterator
class EntityIncidentIterator {
public:
  // constructor
  EntityIncidentIterator();
  EntityIncidentIterator(const Entity& entity, int tdim);
  // copy constructor
  EntityIncidentIterator(const EntityIncidentIterator& it);

  // destructor
  ~EntityIncidentIterator();

  // operators
  EntityIncidentIterator& operator++();
  EntityIncidentIterator& operator--();
  Entity* operator->();
  Entity& operator*();
  bool operator==(const EntityIncidentIterator& it) const;
  bool operator!=(const EntityIncidentIterator& it) const;

  // check end of iterator
  bool is_end() const;

private:
  //// variables ////

  // Mesh Entity
  Entity incident_entity_;

  // current position
  int pos_;

  // number of incidents
  int pos_end_;

  // incident entities indices
  std::vector<int> incident_idx_;

  //// functions ////

  // update local index in Entity
  void update_local_idx();

}; // EntityIncidentIterator

}; // namespace Mesh

}; // namespace ChenLiu

#endif // MESH_ITERATORS_HH

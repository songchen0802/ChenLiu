// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_GRAPH_HH
#define MESH_GRAPH_HH

#include "Mesh.hh"

namespace ChenLiu {
namespace Mesh {

class Graph {

public:
  // constructor
  Graph(const std::shared_ptr<const Mesh> mesh_ptr);

  // destructor
  ~Graph();

  // create
  void create(const std::shared_ptr<const Mesh> mesh_ptr);

  // clear
  void clear();

  // add vertex weights
  void add_node_weights(std::vector<int> node_wgts);

  // add edge weights
  void add_edge_weights(std::vector<int> edge_wgts);

  // return node offsets
  std::vector<int> node_offsets() const;

  // return adjacent nodes 
  std::vector<int> adjacent_nodes() const;

  // return vertex weights
  std::vector<int> node_weights() const;

  // return edge weights
  std::vector<int> edge_weights() const;

  // return number of vertex weights
  int num_node_weights() const;

private:
  std::vector<int> node_offsets_;
  std::vector<int> adj_nodes_;
  std::vector<int> node_wgts_;
  std::vector<int> edge_wgts_;
  int num_node_wgts_;

}; // Graph

}; // namespace Mesh
}; // namespace ChenLiu

#endif // MESH_GRAPH_HH

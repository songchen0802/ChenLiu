// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_TOPOLOGY_HH
#define MESH_TOPOLOGY_HH

#include <vector>
#include <cassert>
#include <list>
#include <set>
#include <numeric>

#include "Connectivity.hh"
#include "CellFactory.hh"

namespace ChenLiu {
namespace Mesh {

class Topology {

public:
  // constructor
  Topology();
  Topology(int tdim);

  // destructor
  ~Topology();

  // initialization
  void init(int tdim);

  // clear
  void clear();

  // number of entities
  int num_entities(int tdim) const;

  // tdim
  int tdim() const;

  // connectivity by local index
  std::vector<int> get_connectivities(int d0, int d1, int loc_idx) const;

  // get global inddex
  int get_global_index(int tdim, int loc_idx) const;

  // get global indices
  std::vector<int> get_global_indices(int tdim) const;

  // find entity. if found, return index, otherwise, return -1
  int find_entity_local(int tdim, std::vector<int> loc_idx,
                        std::string direction = "forward") const;

private:

  // friend class
  friend class Editor;
  friend class Mesh;

  //// variables ////

  // topological dimension
  int tdim_;

  // connectivities
  std::vector<std::vector<Connectivity>> connects_;

  // local to global index
  std::vector< std::vector<int> > idx_l2g_;

  // flag for check whether the full connectivies is computed
  bool full_connects_;

  //// functions ////

  // add connectivity d0 --> d1
  // return local index
  int add_connectivity(int d0, int d1, std::vector<int> idx,
                       bool check_rpl = true);

  // set global index to local index
  void set_global_index(int tdim, int loc_idx, int glo_indx);

  // add global index to local index
  int add_global_index(int tdim, int glo_index);

  // initialize idx_l2g
  void init_idx_l2g(int tdim, int n);

  // initialize idx_l2g with value
  void init_idx_l2g(int tdim, int n, int val);

  // clear connectivities d0 -> d1
  void clear_connectivities(int d0, int d1);

  // compute connectivities d -> 0
  void compute_connectivities_d2zero(int d, bool check_rpl = true);

  // compute connectivities d0 -> d1, d0 > d1
  // from high to low
  void compute_connectivities_h2l(int d0, int d1, bool check_rpl = false);

  // compute connectivities d0 -> d1, d0 < d1
  // from low to high
  void compute_connectivities_l2h(int d0, int d1, bool check_rpl = false);

  // compute connectivities d0 -> d1, d0 = d1
  void compute_connectivities_d2d(int d, bool check_rpl = false);

}; // Topology

}; // namespace Mesh
}; // namespace ChenLiu

#endif // MESH_TOPOLOGY_HH

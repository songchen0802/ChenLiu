// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef MESH_CONNECTIVITY_HH
#define MESH_CONNECTIVITY_HH

#include <iostream>
#include <vector>
#include <cstddef>
#include <cassert>
#include <unordered_map>
#include <algorithm>
#include <iterator>

#include "common/PreDef.hh"

namespace ChenLiu {
namespace Mesh {

class Connectivity {

  // connectivities represent the relation between one entity to others
  // w.r.t. topological dimensions d0 and d1.

public:
  // constructor
  Connectivity();
  Connectivity(int d0, int d1);

  // destructor
  ~Connectivity();

  // clear
  void clear();

  // initialize with d0 and d1
  void init(int d0, int d1);

  // set d0 and d1
  void set_d0_d1(int d0, int d1);

  // add connectivity
  // return local index
  int add_connectivity(std::vector<int> idx);

  // total number of connectivities
  int num_connectivities(int idx) const;
  int num_entities() const;

  // return d0
  int d0() const;

  // return d1
  int d1() const;

  // get first by position
  //int get_entity_indices(const int& pos) const;

  // get connectivity by local index
  std::vector<int> get_connectivities(int loc_idx) const;

  // check the connectivity already exists
  bool has_connectivity(std::vector<int> entity_connectivity,
                        std::string direction = "forward") const;

  // check connectivity already
  int find_connectivity(std::vector<int> entity_connectivity,
                        std::string direction = "forward") const;

  // check connectivity already exists in forward direction
  int find_connectivity_forward(std::vector<int> entity_connectivity) const;

  // check connectivity already exists in forward direction
  int find_connectivity_backward(std::vector<int> entity_connectivity) const;

private:
  // topological dimension, connectivity d0 -> d1
  int d0_, d1_;

  // connectivities of all entities
  std::vector<int> connectivities_;

  // entity positions
  std::vector<int> entity_pos_;

}; // Connectivity

}; // namespace Mesh
}; // namespace ChenLiu

#endif // MESH_CONNECTIVITY_HH


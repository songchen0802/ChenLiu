// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_CELL_POINT_HH
#define MESH_CELL_POINT_HH

#include "Cell.hh"

namespace ChenLiu {
namespace Mesh {

class Cell_Point : public Cell {
public:
  // constructor
  Cell_Point();

  // topological dimension
  int tdim() const override;

  // number of vertices
  int num_vertices() const override;

  // number of entities for each topological dimension
  int num_entities(int tdim) const override;

  // check cell geometry
  bool check_geometry(std::vector<Coordinate> coords, int gdim) const override;

  // numeration for sub entities
  std::vector<std::vector<int>> sub_entities_numeration(int tdim) const override;

}; // Cell_Point

}; // namespace Mesh
}; // namespace ChenLiu

#endif // MESH_CELL_POINT_HH

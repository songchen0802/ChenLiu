// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MESH_CELLFACTORY_HH
#define MESH_CELLFACTORY_HH

#include <memory>

#include "common/Utils.hh"
#include "Cell.hh"

namespace ChenLiu {
namespace Mesh {

class CellFactory {

public:
  std::unique_ptr<Cell> create(CellType type);
  std::unique_ptr<Cell> create(std::string type);
  std::unique_ptr<Cell> create(int tdim, int num_verts);

  // return CellType based on topological dimension and number of vertices
  CellType type(int tdim, int num_verts);

}; // CellFactory

}; // namespace Mesh
}; // namespace ChenLiu

#endif // MESH_CELLFACTORY_HH

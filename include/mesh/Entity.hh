// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef MESH_ENTITY_HH
#define MESH_ENTITY_HH

#include <cstddef>

#include "Mesh.hh"

namespace ChenLiu {
namespace Mesh {

class Entity {

public:
  // constructor
  Entity();
  Entity(const std::shared_ptr<const Mesh> mesh_ptr, int tdim, int local_idx);
  Entity(Mesh& mesh, int tdim, int local_idx);

  // destructor
  ~Entity();

  // topolgical dimension
  int tdim() const;

  // geometrical dimension
  int gdim() const;

  // local index
  int local_index() const;

  // global index
  int global_index() const;

  // mesh
  const Mesh& mesh() const;

  // shared pointer mesh
  const std::shared_ptr<const Mesh> mesh_ptr() const;

  // operators
  bool operator==(const Entity& entity) const;
  bool operator!=(const Entity& entity) const;

  // cell type
  const CellType cell_type() const;

  // vertices coordinates
  const std::vector<Coordinate> vertices_coordinates() const;

  // local index of vertices
  const std::vector<int> vertices_local_index() const;

  // sub domain id
  const int sub_domain_id() const;

protected:
  // friend class
  friend class EntityIterator;
  friend class EntityIncidentIterator;

  // topological dimension
  int tdim_;

  // local index within its topological dimension
  int local_idx_;

  // const pointer of Mesh
  const std::shared_ptr<const Mesh> mesh_ptr_;

}; // Entity

}; // namespace Mesh
}; // namepsace ChenLiu


#endif // MESH_ENTITY_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef LA_LA_STRUCTURE_HH
#define LA_LA_STRUCTURE_HH

#include "space/Dof_Data_Structure.hh"
#include "space/FunctionSpace.hh"

namespace ChenLiu {
namespace LA {

struct Dof_map {
  // interior dof range
  std::array<int, 2> interior_dofs_range;

  // ghost dof
  std::map<int, std::unordered_set<int>> ghost_dofs;
};

template<class DT>
class LA_Structure {
public:
  // constructor
  LA_Structure(const Space::FunctionSpace<DT> space);

  // destructor
  ~LA_Structure();

  // clear
  void clear();

  // global dof offsets
  std::vector<int> global_dof_offsets() const;

  // global interior IDs
  std::vector<int> global_interior_IDs() const;
  std::vector<int> global_interior_IDs(int idx) const;

  // global ghost IDs
  std::vector<int> global_ghost_IDs() const;
  std::vector<int> global_ghost_IDs(int idx) const;

private:
  /// functions ///
  // compute dof map 
  void compute_dof_maps(const Space::FunctionSpace<DT> space);

  /// variables ///
  // number of variables
  int n_var_;

  // MPI Comm
  MPI_Comm comm_;

  // global dof offsets
  std::vector<int> global_dof_offsets_;

  // dof_maps
  std::vector<Dof_map> dof_maps_;

}; // LA_Structure

}; // namespace LA
}; // namespace ChenLiu

#endif // LA_LA_STRUCTURE_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef LA_VECTOR_HH
#define LA_VECTOR_HH

#include <cstddef>
#include <numeric>
#include <memory>

#include "common/Utils.hh"

namespace ChenLiu {
namespace LA {

template<class DT>
class Vector {

public:
  // constructor
  Vector();

  // destructor
  virtual ~Vector();

  // clear
  virtual void clear();

  // local size of vector
  virtual int size() const = 0;

  // global size of vector
  virtual int size_global() const = 0;

  // local size of interior
  virtual int size_local_interior() const = 0;

  // local size of ghost
  virtual int size_local_ghost() const = 0;

  // set to zeros
  virtual void zeros() = 0;

  // set interior to a value
  virtual void set_interior(DT val) = 0;

  // set value to a given index
  virtual void set(int idx, DT val) = 0;

  // set values to given indices
  virtual void set(std::vector<int> indices, std::vector<DT> vals) = 0;

  // get values of interior
  virtual std::vector<DT> get_interior() const = 0;

  // get value for a given index
  virtual DT get(int index) const = 0;

  // get values for given indices
  virtual std::vector<DT> get(std::vector<int> indices) const = 0;

  // add value to all elements
  virtual void add(DT val) = 0;

  // add value to interior
  virtual void add_interior(DT val) = 0;

  // add value to a given index
  virtual void add(int idx, DT val) = 0;

  // add values to given indices
  virtual void add(std::vector<int> indices, std::vector<DT> vals) = 0;

}; // Vector

}; // namespace LA
}; // namepsace ChenLiu


#endif // LA_VECTOR_HH

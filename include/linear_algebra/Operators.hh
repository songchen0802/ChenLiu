// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef LA_OPERATORS_HH
#define LA_OPERATORS_HH

#include "Vector.hh"

namespace ChenLiu {
namespace LA {

namespace OP {

// L2 norm
template<class DT>
DT norm2(const Vector<DT>& x);

// L1 norm
template<class DT>
DT norm1(const Vector<DT>& x);

// max norm
template<class DT>
DT norm_max(const Vector<DT>& x);

// dot product
template<class DT>
DT dot(const Vector<DT>& x, const Vector<DT>& y);

// scale
template<class DT>
void scale(Vector<DT>& x, DT s);

// axpy
template<class DT>
void axpy(Vector<DT>& y, DT alpha, const Vector<DT>& x);

}; // namespace OP

}; // namespace LA
}; // namespace ChenLiu

#endif // LA_OPERATORS_HH

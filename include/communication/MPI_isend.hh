// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef COMMUNICATION_MPI_ISEND_HH
#define COMMUNICATION_MPI_ISEND_HH

#include <mpi.h>
#include <vector>
#include <cassert>

#include "MPI.hh"

namespace ChenLiu {

namespace Communication {

class CL_MPI_Isend : public CL_MPI {
public:
  // constructor
  CL_MPI_Isend(const MPI_Comm& comm);

  // deconstructor
  ~CL_MPI_Isend();

  // execute
  void execute(const Mesh::EntityInfo& send, const int dest,
               std::vector<MPI_Request>& reqs);

  void execute(const Mesh::MaterialIdInfo& send, const int dest,
               std::vector<MPI_Request>& reqs);

  void execute(const Mesh::GlobalIndexInfo& send, const int dest,
               std::vector<MPI_Request>& reqs);

  void execute(const Space::CellDofInfo& send, const int dest,
               std::vector<MPI_Request>& reqs);
};

}; // namespace Communication

}; // namespace ChenLiu

#endif // COMMUNICATION_MPI_ISEND_HH

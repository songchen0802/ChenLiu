// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef COMMUNICATION_MPI_HH
#define COMMUNICATION_MPI_HH

#include <mpi.h>
#include <vector>
#include <cassert>
#include <numeric>
#include <typeinfo>

#include "../common/PreDef.hh"
#include "../mesh/Mesh_Communication.hh"
#include "../space/Dof_Communication.hh"

namespace ChenLiu {

namespace Communication {

class CL_MPI {
public:
  // constructor
  CL_MPI(int root, const MPI_Comm& comm);
  CL_MPI(const MPI_Comm& comm);

  // deconstructor
  ~CL_MPI();

  // clear
  void clear();

protected:
  //// variables ////

  int rank_;
  int root_;
  MPI_Comm comm_;

  // tags

  // entity
  const int E_SIZES_TAG = 100;
  const int E_COORDS_TAG = 101;
  const int E_CONNECTIONS_TAG = 102;
  const int E_ENTITY_OFFSETS_TAG = 103;
  const int E_MATERIAL_IDS_TAG = 104;
  const int E_GLOBAL_INDICES_TAG = 105;

  // material id
  const int M_SIZES_TAG = 200;
  const int M_CONNECTIONS_TAG = 201;
  const int M_ENTITY_OFFSETS_TAG = 202;
  const int M_MATERIAL_IDS_TAG = 203;

  // global index
  const int G_SIZES_TAG = 300;
  const int G_CONNECTIONS_TAG = 301;
  const int G_ENTITY_OFFSETS_TAG = 302;
  const int G_GLOBAL_INDICES_TAG = 303;

  // dof
  const int D_SIZES_TAG = 400;
  const int D_GLOBAL_ENTITY_INDICES_TAG = 401;
  const int D_DOFS_TAG = 402;
  const int D_DOF_OFFSETS_TAG = 403;
  
  //// functions ////

  // collect necessary sizes in EntityInfo
  std::vector<int> collect_entity_info_sizes(const Mesh::EntityInfo entity_info);
  std::vector<int> collect_material_id_info_sizes(const Mesh::MaterialIdInfo
      material_id_info);
  std::vector<int> collect_global_index_info_sizes(const Mesh::GlobalIndexInfo
      global_index_info);
  std::vector<int> collect_cell_dof_info_sizes(const Space::CellDofInfo cell_dof_info);

};

}; // namespace Communication

}; // namespace ChenLiu

#endif // COMMUNICATION_MPI_HH

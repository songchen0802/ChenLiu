// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef FEM_ELEMENTLAGRANGE_LINE_HH
#define FEM_ELEMENTLAGRANGE_LINE_HH

#include "ElementLagrange.hh"
#include "../ReferenceCell_Line.hh"

namespace ChenLiu {
namespace FEM {

template<class DT>
class ElementLagrange_Line : public ElementLagrange<DT> {

public:
  // constructor
  ElementLagrange_Line();

  // destructor
  ~ElementLagrange_Line();

  // initilize
  void init(int ele_deg, int gdim = 1) override;

  // clear
  void clear();

  // number of dofs on cell
  int num_dofs_on_cell() const override;

  // evaluate reference basis function
  void eval_ref_basis(const Coordinate coord_ref,
                      std::vector<DT>& val) const override;

  // evaluate derivative reference basis function
  void eval_ref_basis_deriv1(const Coordinate coord_ref,
                             int d, std::vector<DT>& val) const override;

  void eval_ref_basis_deriv2(const Coordinate coord_ref,
                             int d1, int d2, std::vector<DT>& val) const override;

  // provide indices of dofs (local) on sub-entity
  std::vector<int> dof_indices_on_sub_entity(std::vector<int> entity_vert_idx,
      std::vector<int> sub_entity_vert_idx) const override;

private:
  // index ordering from plane (x=i)
  int i2idx(int i) const;

  // function for initialize coordinates of dofs on reference cell
  void init_ref_dof_coords() override;

}; // ElementLagrange

}; // namespace FEM
}; // namepsace ChenLiu


#endif // FEM_ELEMENTLAGRANGE_LINE_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef FEM_REFERENCECELL_HH
#define FEM_REFERENCECELL_HH

#include <cstddef>

#include "common/PreDef.hh"
#include "common/Coordinate.hh"
#include "common/Mat.hh"
#include "common/Mat.hh"
#include "common/MatVecOp.hh"

namespace ChenLiu {
namespace FEM {

template<class DT>
class ReferenceCell {

public:
  // constructor
  ReferenceCell();

  // destructor
  ~ReferenceCell();

  // clear
  void clear();

  // cell type
  CellType cell_type() const;

  // topological dimension
  int tdim() const;

  // map a point on reference cell to physical cell
  virtual DT map2phy(const int d, const Coordinate coord_ref,
                     const std::vector<Coordinate> cell_verts) const = 0;

  virtual DT map2phy_deriv1(const int d, const int d1,
                            const Coordinate coord_ref,
                            const std::vector<Coordinate> cell_verts) const = 0;


  virtual DT map2phy_deriv2(const int d, const int d1, const int d2,
                            const Coordinate coord_ref,
                            const std::vector<Coordinate> cell_verts) const = 0;

  // map a point on physical cell to reference cell
  virtual void map2ref(const Coordinate coord_phy,
                       const std::vector<Coordinate> cell_verts,
                       Coordinate& coord) const = 0;

protected:
  //// variables ////
  // topological dimension
  int tdim_;

  // cell type
  CellType cell_type_;

  // coordinates of dofs on reference cell
  std::vector<Coordinate> ref_vert_coords_;

  //// functions ////

  // function for initialize coordinates of vertices of referece cell
  virtual void init_ref_vert_coords() = 0;

  // check the coordinate is on reference cell
  virtual bool is_on_reference_cell(const Coordinate coord_ref) const = 0;

}; // ReferenceCell

}; // namespace FEM
}; // namepsace ChenLiu


#endif // FEM_REFERENCECELL_HH

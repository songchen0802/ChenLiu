// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef FEM_ELEMENT_HH
#define FEM_ELEMENT_HH

#include <cstddef>
#include <numeric>

#include "common/PreDef.hh"
#include "common/Coordinate.hh"
#include "common/Utils.hh"

#include "ReferenceCell.hh"

namespace ChenLiu {
namespace FEM {

template<class DT>
class Element {

public:
  // constructor
  Element();

  // destructor
  ~Element();

  // clear
  void clear();

  // initilize
  virtual void init(int ele_deg, int gdim) = 0;

  // element degree
  int ele_degree() const;

  // finite element type
  FE_Type fe_type() const;

  // geometrical dimension
  int gdim() const;

  // topological dimension
  int tdim() const;

  // number of dofs on cell
  virtual int num_dofs_on_cell() const = 0;

  // coordinates of dofs on reference cell
  std::vector<Coordinate> dof_coordinates_ref();

  // coordinates of dofs on physical cell
  std::vector<Coordinate> dof_coordinates_phy(const std::vector<Coordinate>
      cell_verts);

  // evaluate reference basis function
  virtual void eval_ref_basis(const Coordinate coord_ref,
                              std::vector<DT>& val) const = 0;

  DT eval_ref_basis_sum(const Coordinate coord_ref) const;

  // evaluate derivative reference basis function
  virtual void eval_ref_basis_deriv1(const Coordinate coord_ref,
                                     int d, std::vector<DT>& val) const = 0;

  virtual void eval_ref_basis_deriv2(const Coordinate coord_ref,
                                     int d1, int d2, std::vector<DT>& val) const = 0;

  DT eval_ref_basis_deriv1_sum(const Coordinate coord_ref, int d) const;

  DT eval_ref_basis_deriv2_sum(const Coordinate coord_ref, int d1, int d2) const;

  // provide indices of dofs (local) on sub-entity
  virtual std::vector<int> dof_indices_on_sub_entity(
    std::vector<int> entity_vert_idx,
    std::vector<int> sub_entity_vert_idx) const = 0;

protected:
  //// varialbes ////

  // geometrical dimension
  int gdim_;

  // element degree
  int ele_deg_;

  // finite element type
  FE_Type fe_type_;

  // coordinates of dofs on reference cell
  std::vector<Coordinate> ref_dof_coords_;

  // ReferenceCell
  std::unique_ptr<ReferenceCell<DT>> reference_cell_;

  //// functions ////

  // function for initialize coordinates of dofs on reference cell
  virtual void init_ref_dof_coords() = 0;

  // identify indices of dofs on a sub entity
  std::vector<int> identify_dof_idx_on_sub_entity(
    std::vector<int> entity_vert_idx,
    std::vector<int> sub_entity_vert_idx,
    std::vector<std::vector<int>> entity_idx,
    std::vector<std::vector<int>> dof_idx) const;

}; // Element

}; // namespace FEM
}; // namepsace ChenLiu


#endif // FEM_ELEMENT_HH

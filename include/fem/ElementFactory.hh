// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.


#ifndef FEM_ELEMENTFACTORY_HH
#define FEM_ELEMENTFACTORY_HH

#include <cstddef>
#include <numeric>
#include <memory>

#include "common/Utils.hh"
#include "common/Coordinate.hh"
#include "Element.hh"

namespace ChenLiu {
namespace FEM {

template<class DT>
class ElementFactory {

public:
  std::shared_ptr<Element<DT>> create(FE_Type fe_type, CellType cell_type);
  std::shared_ptr<Element<DT>> create(FE_Type fe_type, CellType cell_type, int ele_deg, int gdim);

  std::shared_ptr<Element<DT>> create(std::string fe_type, std::string cell_type);
  std::shared_ptr<Element<DT>> create(std::string fe_type, std::string cell_type, int ele_deg, int gdim);

}; // ElementFactory

}; // namespace FEM
}; // namepsace ChenLiu


#endif // FEM_ELEMENTFACTORY_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#ifndef FEM_REFERENCECELL_HEXAHEDRON_HH
#define FEM_REFERENCECELL_HEXAHEDRON_HH

#include <cstddef>

#include "ReferenceCell.hh"

namespace ChenLiu {
namespace FEM {

template<class DT>
class ReferenceCell_Hexahedron : public ReferenceCell<DT> {

public:
  // constructor
  ReferenceCell_Hexahedron();

  // destructor
  ~ReferenceCell_Hexahedron();

  // map a point on reference cell to physical cell
  DT map2phy(const int d, const Coordinate coord_ref,
             const std::vector<Coordinate> cell_verts) const override;

  DT map2phy_deriv1(const int d, const int d1,
                    const Coordinate coord_ref,
                    const std::vector<Coordinate> cell_verts) const override;

  DT map2phy_deriv2(const int d, const int d1, const int d2,
                    const Coordinate coord_ref,
                    const std::vector<Coordinate> cell_verts) const override;

  // map a point on physical cell to reference cell
  void map2ref(const Coordinate coord_phy,
               const std::vector<Coordinate> cell_verts,
               Coordinate& coord) const override;

protected:
  // function for initialize coordinates of vertices on referece cell
  void init_ref_vert_coords() override;

  // check the coordinate is on reference cell
  bool is_on_reference_cell(const Coordinate coord_ref) const override;

  //// analytical mapping
  //bool map_to_ref_analytic(const Coordinate coord_phy,
  //                         const std::vector<Coordinate> cell_verts,
  //                         Coordinate& coord) const;

  // mapping by Newton method
  bool map2ref_newton(const Coordinate coord_phy,
                      const std::vector<Coordinate> cell_verts,
                      Coordinate& coord) const;

}; // ReferenceCell_Hexahedron

}; // namespace FEM
}; // namepsace ChenLiu


#endif // FEM_REFERENCECELL_HEXAHEDRON_HH

// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test7

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include "common/PreDef.hh"
#include "common/Coordinate.hh"
#include "mesh/CellFactory.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test7){
  // create CellFactory
  Mesh::CellFactory cell_factory;

  // Point
  std::unique_ptr<Mesh::Cell> point = cell_factory.create(0, 1);
  BOOST_CHECK(point->cell_type() == CellType::Point);
  BOOST_CHECK(point->tdim() == 0);
  BOOST_CHECK(point->num_vertices() == 1);

  point = cell_factory.create("point");
  BOOST_CHECK(point->cell_type() == CellType::Point);

  // Line
  std::unique_ptr<Mesh::Cell> line = cell_factory.create(1, 2);
  BOOST_CHECK(line->cell_type() == CellType::Line);
  BOOST_CHECK(line->tdim() == 1);
  BOOST_CHECK(line->num_vertices() == 2);
  
  line = cell_factory.create("line");
  BOOST_CHECK(line->cell_type() == CellType::Line);

  // Triangle
  std::unique_ptr<Mesh::Cell> triangle = cell_factory.create(2, 3);
  BOOST_CHECK(triangle->cell_type() == CellType::Triangle);
  BOOST_CHECK(triangle->tdim() == 2);
  BOOST_CHECK(triangle->num_vertices() == 3);

  triangle = cell_factory.create("triangle");
  BOOST_CHECK(triangle->cell_type() == CellType::Triangle);

  Coordinate vert0(0.0, 0.0);
  Coordinate vert1(1.0, 0.0);
  Coordinate vert2(1.0, 1.0);

  std::vector<Coordinate> v = {vert0, vert1, vert2};
  BOOST_CHECK(triangle->check_geometry(v, 2) == true);

  std::vector<Coordinate> v1 = {vert1, vert0, vert2};
  BOOST_CHECK(triangle->check_geometry(v1, 2) == false);

  // Quadrilateral
  std::unique_ptr<Mesh::Cell> quadrilateral = cell_factory.create(2, 4);
  BOOST_CHECK(quadrilateral->cell_type() == CellType::Quadrilateral);
  BOOST_CHECK(quadrilateral->tdim() == 2);
  BOOST_CHECK(quadrilateral->num_vertices() == 4);

  quadrilateral = cell_factory.create("quadrilateral");
  BOOST_CHECK(quadrilateral->cell_type() == CellType::Quadrilateral);

  Coordinate vert3(0.0, 1.0);
  std::vector<Coordinate> v2 = {vert0, vert1, vert2, vert3};
  BOOST_CHECK(quadrilateral->check_geometry(v2, 2) == true);

  std::vector<Coordinate> v3 = {vert0, vert1, vert3, vert2};
  BOOST_CHECK(quadrilateral->check_geometry(v3, 2) == false);

  // Tetrahedron
  std::unique_ptr<Mesh::Cell> tetrahedron = cell_factory.create(3, 4);
  BOOST_CHECK(tetrahedron->cell_type() == CellType::Tetrahedron);
  BOOST_CHECK(tetrahedron->tdim() == 3);
  BOOST_CHECK(tetrahedron->num_vertices() == 4);

  tetrahedron = cell_factory.create("tetrahedron");
  BOOST_CHECK(tetrahedron->cell_type() == CellType::Tetrahedron);

  Coordinate vert4(0.0, 0.0, 0.0);
  Coordinate vert5(1.0, 0.0, 0.0);
  Coordinate vert6(1.0, 1.0, 0.0);
  Coordinate vert7(0.0, 0.0, 1.0);

  std::vector<Coordinate> v4 = {vert4, vert5, vert6, vert7};
  BOOST_CHECK(tetrahedron->check_geometry(v4, 3) == true);

  std::vector<Coordinate> v5 = {vert4, vert6, vert5, vert7};
  BOOST_CHECK(tetrahedron->check_geometry(v5, 3) == false);

  // Hexahedron
  std::unique_ptr<Mesh::Cell> hexahedron = cell_factory.create(3, 8);
  BOOST_CHECK(hexahedron->cell_type() == CellType::Hexahedron);
  BOOST_CHECK(hexahedron->tdim() == 3);
  BOOST_CHECK(hexahedron->num_vertices() == 8);

  hexahedron = cell_factory.create("hexahedron");
  BOOST_CHECK(hexahedron->cell_type() == CellType::Hexahedron);

  Coordinate vert8(0.0, 1.0, 0.0);
  Coordinate vert9(1.0, 0.0, 1.0);
  Coordinate vert10(1.0, 1.0, 1.0);
  Coordinate vert11(0.0, 1.0, 1.0);

  std::vector<Coordinate> v6 = {vert4, vert5, vert6, vert8, vert7, vert9, vert10, vert11};
  BOOST_CHECK(hexahedron->check_geometry(v6, 3) == true);

  std::vector<Coordinate> v7 = {vert4, vert5, vert8, vert6, vert7, vert9, vert10, vert11};
  BOOST_CHECK(hexahedron->check_geometry(v7, 3) == false);
}


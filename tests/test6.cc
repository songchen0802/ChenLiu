// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test6

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>
#include <boost/mpi/communicator.hpp>

#include <math.h>
#include <filesystem>

#include "mesh/Editor.hh"


using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test6) {

  MPI_Init(NULL, NULL);

  MPI_Comm comm = MPI_COMM_WORLD;

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  std::filesystem::path mesh_file = "./data/unit_square.inp";

  mesh_editor.read_mesh(mesh_file, mesh_ptr, 2, 2, comm, true);

  MPI_Finalize();

}


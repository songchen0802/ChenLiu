// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test16

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "polynomials/PolynomialLagrange.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test16) {

  MPI_Init(NULL, NULL);

  POLY::PolynomialLagrange<double> poly;

  BOOST_REQUIRE_EQUAL(poly.poly(1, 0, 0.0), 1.0);
  BOOST_REQUIRE_EQUAL(poly.poly(1, 0, 1.0), 0.0);
  BOOST_REQUIRE_EQUAL(poly.poly(1, 1, 1.0), 1.0);
  BOOST_REQUIRE_EQUAL(poly.poly(1, 1, 0.0), 0.0);
  
  BOOST_REQUIRE_EQUAL(poly.poly(1, 0, 0.5), 0.5);
  BOOST_REQUIRE_EQUAL(poly.poly(1, 1, 0.5), 0.5);

  BOOST_REQUIRE_EQUAL(poly.poly_deriv1(1, 0, 0.0), -1.0);
  BOOST_REQUIRE_EQUAL(poly.poly_deriv1(1, 0, 0.5), -1.0);
  BOOST_REQUIRE_EQUAL(poly.poly_deriv1(1, 1, 0.0), 1.0);
  BOOST_REQUIRE_EQUAL(poly.poly_deriv1(1, 1, 0.5), 1.0);

  BOOST_REQUIRE_EQUAL(poly.poly_deriv2(1, 0, 0.0), 0.0);
  BOOST_REQUIRE_EQUAL(poly.poly_deriv2(1, 0, 1.0), 0.0);
  BOOST_REQUIRE_EQUAL(poly.poly_deriv2(1, 1, 0.0), 0.0);
  BOOST_REQUIRE_EQUAL(poly.poly_deriv2(1, 1, 1.0), 0.0);

  MPI_Finalize();

}


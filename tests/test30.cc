// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test30

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "common/Utils.hh"
#include "fem/Element.hh"
#include "fem/ElementFactory.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test30, *boost::unit_test::tolerance(1.0e-10)) {

  MPI_Init(NULL, NULL);

  // element factory
  FEM::ElementFactory<double> ele_factory;

  // cell vertices
  std::vector<Coordinate> cell_verts;

  // create pointer to Element
  std::shared_ptr<FEM::Element<double>> ele;

  // dof coords on phyiscal cell
  std::vector<Coordinate> coords_phy; 

  // Line
  ele = ele_factory.create(FE_Type::Lagrange, CellType::Line);

  std::vector<std::vector<double>> coords = {
    {0.0, 0.0}, {1.5, 0.0}
  };

  cell_verts.clear();
  for (int i = 0; i != coords.size(); ++i) {
    Coordinate coord(coords[i]);
    cell_verts.push_back(coord);
  }

  // degree 1
  ele->init(1, 2);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 1.5);

  // degree 2
  ele->init(2,2);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 1.5);

  // Triangle
  ele = ele_factory.create(FE_Type::Lagrange, CellType::Triangle);

  coords.clear();
  coords = {
    {0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {0.0, 1.5, 0.0}
  };

  cell_verts.clear();
  for (int i = 0; i != coords.size(); ++i) {
    Coordinate coord(coords[i]);
    cell_verts.push_back(coord);
  }

  // degree 1
  ele->init(1,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 1.5);

  // degree 2
  ele->init(2,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[4][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[4][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[5][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[5][1], 1.5);

  // Quadrilateral
  ele = ele_factory.create(FE_Type::Lagrange, CellType::Quadrilateral);

  coords.clear();
  coords = {
    {0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {1.5, 1.5, 0.0}, {0.0, 1.5, 0.0}
  };

  cell_verts.clear();
  for (int i = 0; i != coords.size(); ++i) {
    Coordinate coord(coords[i]);
    cell_verts.push_back(coord);
  }

  // degree 1
  ele->init(1,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[3][0], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[3][1], 1.5);

  // degree 2
  ele->init(2,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[4][0], 0.625);
  BOOST_REQUIRE_EQUAL(coords_phy[4][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[5][0], 1.25);
  BOOST_REQUIRE_EQUAL(coords_phy[5][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[6][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[6][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[7][0], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[7][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[8][0], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[8][1], 1.5);

  // Tetrahedron
  ele = ele_factory.create(FE_Type::Lagrange, CellType::Tetrahedron);

  coords.clear();
  coords = {
    {0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {0.0, 1.5, 0.0}, {0.0, 0.0, 2.0}
  };

  cell_verts.clear();
  for (int i = 0; i != coords.size(); ++i) {
    Coordinate coord(coords[i]);
    cell_verts.push_back(coord);
  }

  // degree 1
  ele->init(1,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[2][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[3][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][2], 2.0);

  // degree 2
  ele->init(2,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[3][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[3][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[4][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[4][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[4][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[5][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[5][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[5][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[6][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[6][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[6][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[7][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[7][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[7][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[8][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[8][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[8][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[9][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[9][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[9][2], 2.0);

  // Hexahedron
  ele = ele_factory.create(FE_Type::Lagrange, CellType::Hexahedron);

  coords.clear();
  coords = {
    {0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {1.5, 1.5, 0.0}, {0.0, 1.5, 0.0},
    {0.0, 0.0, 2.0}, {1.0, 0.0, 2.0}, {1.5, 1.5, 2.0}, {0.0, 1.5, 2.0}
  };

  cell_verts.clear();
  for (int i = 0; i != coords.size(); ++i) {
    Coordinate coord(coords[i]);
    cell_verts.push_back(coord);
  }

  // degree 1
  ele->init(1,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[2][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[3][0], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[3][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[3][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[4][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[4][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[4][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[5][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[5][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[5][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[6][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[6][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[6][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[7][0], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[7][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[7][2], 2.0);

  // degree 2
  ele->init(2,3);

  coords_phy.clear();
  coords_phy = ele->dof_coordinates_phy(cell_verts);

  BOOST_REQUIRE_EQUAL(coords_phy[0][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[0][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[1][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[1][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[1][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[2][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[2][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[3][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[3][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[3][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[4][0], 0.625);
  BOOST_REQUIRE_EQUAL(coords_phy[4][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[4][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[5][0], 1.25);
  BOOST_REQUIRE_EQUAL(coords_phy[5][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[5][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[6][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[6][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[6][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[7][0], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[7][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[7][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[8][0], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[8][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[8][2], 0.0);

  BOOST_REQUIRE_EQUAL(coords_phy[9][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[9][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[9][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[10][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[10][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[10][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[11][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[11][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[11][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[12][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[12][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[12][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[13][0], 0.625);
  BOOST_REQUIRE_EQUAL(coords_phy[13][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[13][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[14][0], 1.25);
  BOOST_REQUIRE_EQUAL(coords_phy[14][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[14][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[15][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[15][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[15][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[16][0], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[16][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[16][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[17][0], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[17][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[17][2], 1.0);

  BOOST_REQUIRE_EQUAL(coords_phy[18][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[18][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[18][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[19][0], 0.5);
  BOOST_REQUIRE_EQUAL(coords_phy[19][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[19][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[20][0], 1.0);
  BOOST_REQUIRE_EQUAL(coords_phy[20][1], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[20][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[21][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[21][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[21][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[22][0], 0.625);
  BOOST_REQUIRE_EQUAL(coords_phy[22][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[22][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[23][0], 1.25);
  BOOST_REQUIRE_EQUAL(coords_phy[23][1], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[23][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[24][0], 0.0);
  BOOST_REQUIRE_EQUAL(coords_phy[24][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[24][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[25][0], 0.75);
  BOOST_REQUIRE_EQUAL(coords_phy[25][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[25][2], 2.0);

  BOOST_REQUIRE_EQUAL(coords_phy[26][0], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[26][1], 1.5);
  BOOST_REQUIRE_EQUAL(coords_phy[26][2], 2.0);

  MPI_Finalize();

}


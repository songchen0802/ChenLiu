// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test12

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>

#include "mesh/Editor.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test12) {

  MPI_Init(NULL, NULL);

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  std::filesystem::path mesh_file = "./data/2hex.inp";

  mesh_editor.read_mesh(mesh_file, mesh_ptr, 3, 3, MPI_COMM_WORLD, true);

  Coordinate coord0(0.0, 0.0, 0.0);
  Coordinate coord1(0.5, 0.0, 0.0);
  Coordinate coord2(1.0, 0.0, 0.0);
  Coordinate coord3(1.0, 1.0, 0.0);
  Coordinate coord4(0.5, 1.0, 0.0);
  Coordinate coord5(0.0, 1.0, 0.0);
  Coordinate coord6(0.0, 0.0, 1.0);
  Coordinate coord7(0.5, 0.0, 1.0);
  Coordinate coord8(1.0, 0.0, 1.0);
  Coordinate coord9(1.0, 1.0, 1.0);
  Coordinate coord10(0.5, 1.0, 1.0);
  Coordinate coord11(0.0, 1.0, 1.0);

  std::vector<Coordinate> coords = {coord0, coord1, coord2, coord3,
                                    coord4, coord5, coord6, coord7,
                                    coord8, coord9, coord10, coord11
                                   };

  for (std::size_t i = 0; i != 12; ++i) {
    for (std::size_t d = 0; d != 3; ++d) {
      BOOST_REQUIRE_EQUAL(mesh_ptr->geometry().coordinate(i)[d], coords[i][d]);
    }
  }

  // d0 = 3, d1 = 0
  int tdim_0 = 3;
  int tdim_1 = 0;

  std::vector<std::vector<int>> connects_3d = {{0, 1, 4, 5, 6, 7, 10, 11},
    {1, 2, 3, 4, 7, 8, 9, 10}
  };

  int n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3d[i][j]);
    }
  }

  // d0 = 2, d1 = 0
  tdim_0 = 2;
  tdim_1 = 0;

  std::vector<std::vector<int>> connects_2d = {{0, 1, 4, 5}, {6, 7, 10, 11},
    {0, 1, 7, 6}, {1, 4, 10, 7}, {4, 5, 11, 10}, {0, 6, 11, 5}, {1, 2, 3, 4},
    {7, 8, 9, 10}, {1, 2, 8, 7}, {2, 3, 9, 8}, {3, 4, 10, 9}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2d[i][j]);
    }
  }

  // d0 = 1, d0 = 0
  tdim_0 = 1;
  tdim_1 = 0;

  std::vector<std::vector<int>> connects_1d = {{0, 1}, {1, 4}, {4, 5}, {5, 0},
    {6, 7}, {7, 10}, {10, 11}, {11, 6}, {0, 6}, {1, 7}, {4, 10}, {5, 11}, {1, 2},
    {2, 3}, {3, 4}, {7, 8}, {8, 9}, {9, 10}, {2, 8}, {3, 9}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1d[i][j]);
    }
  }

  // d0 = 2, d1 = 3
  tdim_0 = 2;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_2to3 = {{0}, {0}, {0}, {0, 1}, {0}, {0},
    {1}, {1}, {1}, {1}, {1}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2to3[i][j]);
    }
  }

  // d0 = 3, d1 = 2
  tdim_0 = 3;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_3to2 = {{0, 1, 2, 3, 4, 5},
    {6, 7, 8, 9, 10, 3}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3to2[i][j]);
    }
  }

  // d0 = 3, d1 = 1
  tdim_0 = 3;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_3to1 = {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
    {12, 13, 14, 1, 15, 16, 17, 5, 9, 18, 19, 10}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3to1[i][j]);
    }
  }

  // d0 = 1, d1 = 3
  tdim_0 = 1;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_1to3 = {{0}, {0, 1}, {0}, {0}, {0},
    {0, 1}, {0}, {0}, {0}, {0, 1}, {0, 1}, {0}, {1}, {1}, {1}, {1}, {1},
    {1}, {1}, {1}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1to3[i][j]);
    }
  }

  // d0 = 2, d1 = 1
  tdim_0 = 2;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_2to1 = {{0, 1, 2, 3}, {4, 5, 6, 7}, {0, 9, 4, 8},
    {1, 10, 5, 9}, {2, 11, 6, 10}, {8, 7, 11, 3}, {12, 13, 14, 1}, {15, 16, 17, 5},
    {12, 18, 15, 9}, {13, 19, 16, 18}, {14, 10, 17, 19}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2to1[i][j]);
    }
  }

  // d0 = 1, d1 = 2
  tdim_0 = 1;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_1to2 = {{0, 2}, {0, 3, 6}, {0, 4},
    {0, 5}, {1, 2}, {1, 3, 7}, {1, 4}, {1, 5}, {2, 5}, {2, 3, 8}, {3, 4, 10},
    {4, 5}, {6, 8}, {6, 9}, {6, 10}, {7, 8}, {7, 9}, {7, 10}, {8, 9}, {9, 10}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1to2[i][j]);
    }
  }

  // d0 = 0, d1 = 3
  tdim_0 = 0;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_0to3 = {{0}, {0, 1}, {1}, {1}, {0, 1}, {0},
    {0}, {0, 1}, {1}, {1}, {0, 1}, {0}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to3[i][j]);
    }
  }

  // d0 = 0, d1 = 2
  tdim_0 = 0;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_0to2 = {{0, 2, 5}, {0, 2, 3, 6, 8},
    {6, 8, 9}, {6, 9, 10}, {0, 3, 4, 6, 10}, {0, 4, 5}, {1, 2, 5}, {1, 2, 3, 7, 8},
    {7, 8, 9}, {7, 9, 10}, {1, 3, 4, 7, 10}, {1, 4, 5}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to2[i][j]);
    }
  }

  // d0 = 0, d1 = 1
  tdim_0 = 0;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_0to1 = {
    {0, 3, 8}, {0, 1, 9, 12}, {12, 13, 18},
    {13, 14, 19}, {1, 2, 10, 14}, {2, 3, 11}, {4, 7, 8},
    {4, 5, 9, 15}, {15, 16, 18}, {16, 17, 19},
    {5, 6, 10, 17}, {6, 7, 11}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to1[i][j]);
    }
  }

  // d0 = 0, d1 = 0
  tdim_0 = 0;
  tdim_1 = 0;

  std::vector<std::vector<int>> connects_0to0 = {
    {1, 4, 5, 6, 7, 10, 11}, {0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
    {1, 3, 4, 7, 8, 9, 10}, {1, 2, 4, 7, 8, 9, 10},
    {0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11},
    {0, 1, 4, 6, 7, 10, 11}, {0, 1, 4, 5, 7, 10, 11},
    {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11}, {1, 2, 3, 4, 7, 9, 10},
    {1, 2, 3, 4, 7, 8, 10}, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
    {0, 1, 4, 5, 6, 7, 10}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to0[i][j]);
    }
  }

  // d0 = 1, d1 = 1
  tdim_0 = 1;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_1to1 = {
    {1, 3, 8, 9, 12}, {0, 2, 9, 10, 12, 14},
    {1, 3, 10, 11, 14}, {0, 2, 8, 11},
    {5, 7, 8, 9, 15}, {4, 6, 9, 10, 15, 17}, {5, 7, 10, 11, 17},
    {4, 6, 8, 11}, {0, 3, 4, 7}, {0, 1, 4, 5, 12, 15},
    {1, 2, 5, 6, 14, 17}, {2, 3, 6, 7}, {0, 1, 9, 13, 18},
    {12, 14, 18, 19}, {1, 2, 10, 13, 19}, {4, 5, 9, 16, 18},
    {15, 17, 18, 19}, {5, 6, 10, 16, 19}, {12, 13, 15, 16},
    {13, 14, 16, 17}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1to1[i][j]);
    }
  }

  // d0 = 2, d1 = 2
  tdim_0 = 2;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_2to2 = {
    {2, 3, 4, 5, 6, 8, 10 }, {2, 3, 4, 5, 7, 8, 10},
    {0, 1, 3, 5, 6, 7, 8}, {0, 1, 2, 4, 6, 7, 8, 10},
    {0, 1, 3, 5, 6, 7, 10}, {0, 1, 2, 4},
    {0, 2, 3, 4, 8, 9, 10}, {1, 2, 3, 4, 8, 9, 10},
    {0, 1, 2, 3, 6, 7, 9}, {6, 7, 8, 10}, {0, 1, 3, 4, 6, 7, 9}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2to2[i][j]);
    }
  }

  // d0 = 3, d1 = 3
  tdim_0 = 3;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_3to3 = {{1}, {0}};

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3to3[i][j]);
    }
  }

  MPI_Finalize();

}


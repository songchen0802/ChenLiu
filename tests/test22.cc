// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test22

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "quadrature/QuadratureFactory.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test22, *boost::unit_test::tolerance(1.0e-10)) {

  MPI_Init(NULL, NULL);

  Quadrature::QuadratureFactory<double> qp_factory;

  // Line
  std::shared_ptr<Quadrature::Quadrature<double>> qp =
    qp_factory.create(QP_Type::Gauss, CellType::Line, 2);

  BOOST_REQUIRE_EQUAL(qp->dim(), 1);
  BOOST_REQUIRE_EQUAL(qp->size(), 2);
  
  BOOST_TEST(qp->p(0)[0] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(1)[0] == 0.788675134594812882254574390252);

  BOOST_TEST(qp->w(0) == 0.5);
  BOOST_TEST(qp->w(1) == 0.5);

  // Triangle
  qp = qp_factory.create(QP_Type::Gauss, CellType::Triangle, 2);

  BOOST_REQUIRE_EQUAL(qp->dim(), 2);
  BOOST_REQUIRE_EQUAL(qp->size(), 3);

  BOOST_TEST(qp->p(0)[0] == 0.166666666666666);
  BOOST_TEST(qp->p(0)[1] == 0.166666666666666);

  BOOST_TEST(qp->p(1)[0] == 0.166666666666666);
  BOOST_TEST(qp->p(1)[1] == 0.666666666666666);

  BOOST_TEST(qp->p(2)[0] == 0.666666666666666);
  BOOST_TEST(qp->p(2)[1] == 0.166666666666666);

  BOOST_TEST(qp->w(0) == 0.166666666666667);
  BOOST_TEST(qp->w(1) == 0.166666666666667);
  BOOST_TEST(qp->w(2) == 0.166666666666667);

  // Quadrilateral
  qp = qp_factory.create(QP_Type::Gauss, CellType::Quadrilateral, 2);

  BOOST_REQUIRE_EQUAL(qp->dim(), 2);
  BOOST_REQUIRE_EQUAL(qp->size(), 4);

  BOOST_TEST(qp->p(0)[0] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(0)[1] == 0.211324865405187117745425609748);

  BOOST_TEST(qp->p(1)[0] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(1)[1] == 0.211324865405187117745425609748);

  BOOST_TEST(qp->p(2)[0] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(2)[1] == 0.788675134594812882254574390252);

  BOOST_TEST(qp->p(3)[0] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(3)[1] == 0.788675134594812882254574390252);

  BOOST_TEST(qp->w(0) == 0.25);
  BOOST_TEST(qp->w(1) == 0.25);
  BOOST_TEST(qp->w(2) == 0.25);
  BOOST_TEST(qp->w(3) == 0.25);

  // Tetrahedron
  qp = qp_factory.create(QP_Type::Gauss, CellType::Tetrahedron, 2);

  BOOST_REQUIRE_EQUAL(qp->dim(), 3);
  BOOST_REQUIRE_EQUAL(qp->size(), 4);

  BOOST_TEST(qp->p(0)[0] == 0.13819660112501);
  BOOST_TEST(qp->p(0)[1] == 0.13819660112501);
  BOOST_TEST(qp->p(0)[2] == 0.13819660112501);

  BOOST_TEST(qp->p(1)[0] == 0.585410196624968);
  BOOST_TEST(qp->p(1)[1] == 0.138196601125010);
  BOOST_TEST(qp->p(1)[2] == 0.138196601125010);

  BOOST_TEST(qp->p(2)[0] == 0.13819660112501);
  BOOST_TEST(qp->p(2)[1] == 0.585410196624968);
  BOOST_TEST(qp->p(2)[2] == 0.13819660112501);

  BOOST_TEST(qp->p(3)[0] == 0.138196601125010);
  BOOST_TEST(qp->p(3)[1] == 0.138196601125010);
  BOOST_TEST(qp->p(3)[2] == 0.585410196624968);

  BOOST_TEST(qp->w(0) == 0.0416666666666666);
  BOOST_TEST(qp->w(1) == 0.0416666666666666);
  BOOST_TEST(qp->w(2) == 0.0416666666666666);
  BOOST_TEST(qp->w(3) == 0.0416666666666666);

  // Hexahedron
  qp = qp_factory.create(QP_Type::Gauss, CellType::Hexahedron, 2);

  BOOST_REQUIRE_EQUAL(qp->dim(), 3);
  BOOST_REQUIRE_EQUAL(qp->size(), 8);

  BOOST_TEST(qp->p(0)[0] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(0)[1] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(0)[2] == 0.211324865405187117745425609748);

  BOOST_TEST(qp->p(1)[0] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(1)[1] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(1)[2] == 0.211324865405187117745425609748);

  BOOST_TEST(qp->p(2)[0] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(2)[1] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(2)[2] == 0.211324865405187117745425609748);

  BOOST_TEST(qp->p(3)[0] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(3)[1] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(3)[2] == 0.211324865405187117745425609748);

  BOOST_TEST(qp->p(4)[0] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(4)[1] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(4)[2] == 0.788675134594812882254574390252);

  BOOST_TEST(qp->p(5)[0] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(5)[1] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(5)[2] == 0.788675134594812882254574390252);

  BOOST_TEST(qp->p(6)[0] == 0.211324865405187117745425609748);
  BOOST_TEST(qp->p(6)[1] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(6)[2] == 0.788675134594812882254574390252);

  BOOST_TEST(qp->p(7)[0] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(7)[1] == 0.788675134594812882254574390252);
  BOOST_TEST(qp->p(7)[2] == 0.788675134594812882254574390252);

  BOOST_TEST(qp->w(0) == 0.124999999999999999999999999999);
  BOOST_TEST(qp->w(1) == 0.124999999999999999999999999999);
  BOOST_TEST(qp->w(2) == 0.124999999999999999999999999999);
  BOOST_TEST(qp->w(3) == 0.124999999999999999999999999999);
  BOOST_TEST(qp->w(4) == 0.124999999999999999999999999999);
  BOOST_TEST(qp->w(5) == 0.124999999999999999999999999999);
  BOOST_TEST(qp->w(6) == 0.124999999999999999999999999999);
  BOOST_TEST(qp->w(7) == 0.124999999999999999999999999999);

  MPI_Finalize();

}


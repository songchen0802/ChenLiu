// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test20

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "common/Utils.hh"
#include "fem/ElementFactory.hh"
#include "fem/lagrange/ElementLagrange_Hexahedron.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test20, *boost::unit_test::tolerance(1.0e-10)) {

  MPI_Init(NULL, NULL);

  FEM::ElementFactory<double> element_factory;

  std::shared_ptr<FEM::Element<double>> ele =
                                       element_factory.create(FE_Type::Lagrange, CellType::Hexahedron);


  std::vector<double> val;
  Coordinate coord(3);

  double sum;

  // order = 1
  ele->init(1, 3);

  BOOST_REQUIRE_EQUAL(ele->num_dofs_on_cell(), 8);

  coord[0] = 0.0;
  coord[1] = 0.0;
  coord[2] = 0.0;
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val.size(), 8);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 1.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 1.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], -1.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], -1.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], -1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], -1.0);
  BOOST_REQUIRE_EQUAL(val[5], 1.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], -1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], -1.0);
  BOOST_REQUIRE_EQUAL(val[5], 1.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], -1.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 1.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], -1.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 1.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 1);
  BOOST_TEST(sum == 0.0);

  /////////////////////////////////////////////////////////

  coord[0] = 1.0;
  coord[1] = 1.0;
  coord[2] = 0.0;
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], -1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 1.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], -1.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], -1.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 1.0);
  BOOST_REQUIRE_EQUAL(val[3], -1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], -1.0);
  BOOST_REQUIRE_EQUAL(val[7], 1.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 1.0);
  BOOST_REQUIRE_EQUAL(val[3], -1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], -1.0);
  BOOST_REQUIRE_EQUAL(val[7], 1.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], -1.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 1.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -1.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], -1.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 1.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 1);
  BOOST_TEST(sum == 0.0);

  /////////////////////////////////////////////////////////

  coord[0] = 0.25;
  coord[1] = 0.0;
  coord[2] = 0.25;
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.5625);
  BOOST_REQUIRE_EQUAL(val[1], 0.1875);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.1875);
  BOOST_REQUIRE_EQUAL(val[5], 0.0625);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  // cell transform
  std::vector<Coordinate> cell_verts(8);
  cell_verts[0].init(3);
  cell_verts[1].init(3);
  cell_verts[2].init(3);
  cell_verts[3].init(3);
  cell_verts[4].init(3);
  cell_verts[5].init(3);
  cell_verts[6].init(3);
  cell_verts[7].init(3);
  cell_verts[0][0] = 0.0;
  cell_verts[0][1] = 0.0;
  cell_verts[0][2] = 0.0;
  cell_verts[1][0] = 2.0;
  cell_verts[1][1] = 0.0;
  cell_verts[1][2] = 0.0;
  cell_verts[2][0] = 2.0;
  cell_verts[2][1] = 2.0;
  cell_verts[2][2] = 0.0;
  cell_verts[3][0] = 0.0;
  cell_verts[3][1] = 2.0;
  cell_verts[3][2] = 0.0;
  cell_verts[4][0] = 0.0;
  cell_verts[4][1] = 0.0;
  cell_verts[4][2] = 2.0;
  cell_verts[5][0] = 2.0;
  cell_verts[5][1] = 0.0;
  cell_verts[5][2] = 2.0;
  cell_verts[6][0] = 2.0;
  cell_verts[6][1] = 2.0;
  cell_verts[6][2] = 2.0;
  cell_verts[7][0] = 0.0;
  cell_verts[7][1] = 2.0;
  cell_verts[7][2] = 2.0;

//  Coordinate coord_phy(3);
//  coord_phy[0] = 1.0;
//  coord_phy[1] = 1.0;
//  coord_phy[2] = 1.0;
//  ele->map2ref(coord_phy, cell_verts, coord);
//  BOOST_REQUIRE_EQUAL(coord[0], 0.5);
//  BOOST_REQUIRE_EQUAL(coord[1], 0.5);
//  BOOST_REQUIRE_EQUAL(coord[2], 0.5);
//
//  Coordinate coord_ref(3);
//  coord_ref[0] = 1.0;
//  coord_ref[1] = 0.25;
//  coord_ref[2] = 0.25;
//  BOOST_REQUIRE_EQUAL(ele->map2phy(0, coord_ref, cell_verts), 2.0);
//  BOOST_REQUIRE_EQUAL(ele->map2phy(1, coord_ref, cell_verts), 0.5);
//  BOOST_REQUIRE_EQUAL(ele->map2phy(2, coord_ref, cell_verts), 0.5);

  // order = 2
  ele->init(2, 3);

  BOOST_REQUIRE_EQUAL(ele->num_dofs_on_cell(), 27);

  coord[0] = 0.0;
  coord[1] = 0.0;
  coord[2] = 0.0;
  val.clear();
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val.size(), 27);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], 0.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 0.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], -3.0);
  BOOST_REQUIRE_EQUAL(val[1], 4.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], 0.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 0.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], -3.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 4.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], -1.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], 0.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 0.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], -3.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], 4.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], -1.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  std::cout << std::endl;
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -8.0);
  BOOST_REQUIRE_EQUAL(val[2], 4.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], 0.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 0.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -8.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 4.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], 0.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 0.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], -8.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 4.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 9.0);
  BOOST_REQUIRE_EQUAL(val[1], -12.0);
  BOOST_REQUIRE_EQUAL(val[2], 3.0);
  BOOST_REQUIRE_EQUAL(val[3], -12.0);
  BOOST_REQUIRE_EQUAL(val[4], 16.0);
  BOOST_REQUIRE_EQUAL(val[5], -4.0);
  BOOST_REQUIRE_EQUAL(val[6], 3.0);
  BOOST_REQUIRE_EQUAL(val[7], -4.0);
  BOOST_REQUIRE_EQUAL(val[8], 1.0);
  BOOST_REQUIRE_EQUAL(val[9], 0.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 0.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 9.0);
  BOOST_REQUIRE_EQUAL(val[1], -12.0);
  BOOST_REQUIRE_EQUAL(val[2], 3.0);
  BOOST_REQUIRE_EQUAL(val[3], -12.0);
  BOOST_REQUIRE_EQUAL(val[4], 16.0);
  BOOST_REQUIRE_EQUAL(val[5], -4.0);
  BOOST_REQUIRE_EQUAL(val[6], 3.0);
  BOOST_REQUIRE_EQUAL(val[7], -4.0);
  BOOST_REQUIRE_EQUAL(val[8], 1.0);
  BOOST_REQUIRE_EQUAL(val[9], 0.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 0.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 9.0);
  BOOST_REQUIRE_EQUAL(val[1], -12.0);
  BOOST_REQUIRE_EQUAL(val[2], 3.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], -12.0);
  BOOST_REQUIRE_EQUAL(val[10], 16.0);
  BOOST_REQUIRE_EQUAL(val[11], -4.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 3.0);
  BOOST_REQUIRE_EQUAL(val[19], -4.0);
  BOOST_REQUIRE_EQUAL(val[20], 1.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 9.0);
  BOOST_REQUIRE_EQUAL(val[1], -12.0);
  BOOST_REQUIRE_EQUAL(val[2], 3.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 0.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], -12.0);
  BOOST_REQUIRE_EQUAL(val[10], 16.0);
  BOOST_REQUIRE_EQUAL(val[11], -4.0);
  BOOST_REQUIRE_EQUAL(val[12], 0.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], 0.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 3.0);
  BOOST_REQUIRE_EQUAL(val[19], -4.0);
  BOOST_REQUIRE_EQUAL(val[20], 1.0);
  BOOST_REQUIRE_EQUAL(val[21], 0.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 0.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 2, val);
  BOOST_REQUIRE_EQUAL(val[0], 9.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -12.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 3.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], -12.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 16.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], -4.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 3.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], -4.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 1.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 9.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -12.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);
  BOOST_REQUIRE_EQUAL(val[6], 3.0);
  BOOST_REQUIRE_EQUAL(val[7], 0.0);
  BOOST_REQUIRE_EQUAL(val[8], 0.0);
  BOOST_REQUIRE_EQUAL(val[9], -12.0);
  BOOST_REQUIRE_EQUAL(val[10], 0.0);
  BOOST_REQUIRE_EQUAL(val[11], 0.0);
  BOOST_REQUIRE_EQUAL(val[12], 16.0);
  BOOST_REQUIRE_EQUAL(val[13], 0.0);
  BOOST_REQUIRE_EQUAL(val[14], 0.0);
  BOOST_REQUIRE_EQUAL(val[15], -4.0);
  BOOST_REQUIRE_EQUAL(val[16], 0.0);
  BOOST_REQUIRE_EQUAL(val[17], 0.0);
  BOOST_REQUIRE_EQUAL(val[18], 3.0);
  BOOST_REQUIRE_EQUAL(val[19], 0.0);
  BOOST_REQUIRE_EQUAL(val[20], 0.0);
  BOOST_REQUIRE_EQUAL(val[21], -4.0);
  BOOST_REQUIRE_EQUAL(val[22], 0.0);
  BOOST_REQUIRE_EQUAL(val[23], 0.0);
  BOOST_REQUIRE_EQUAL(val[24], 1.0);
  BOOST_REQUIRE_EQUAL(val[25], 0.0);
  BOOST_REQUIRE_EQUAL(val[26], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 1);
  BOOST_TEST(sum == 0.0);

  coord[0] = 0.25;
  coord[1] = 0.5;
  coord[2] = 0.75;
  val.clear();
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val.size(), 27);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == -0.046875);
  BOOST_TEST(val[4] == -0.09375);
  BOOST_TEST(val[5] == 0.015625);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.0);
  BOOST_TEST(val[10] == 0.0);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == 0.28125);
  BOOST_TEST(val[13] == 0.5625);
  BOOST_TEST(val[14] == -0.09375);
  BOOST_TEST(val[15] == 0.0);
  BOOST_TEST(val[16] == 0.0);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.0);
  BOOST_TEST(val[19] == 0.0);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == 0.140625);
  BOOST_TEST(val[22] == 0.28125);
  BOOST_TEST(val[23] == -0.046875);
  BOOST_TEST(val[24] == 0.0);
  BOOST_TEST(val[25] == 0.0);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.25);
  BOOST_TEST(val[4] == -0.25);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.0);
  BOOST_TEST(val[10] == 0.0);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == -1.5);
  BOOST_TEST(val[13] == 1.5);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == 0.0);
  BOOST_TEST(val[16] == 0.0);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.0);
  BOOST_TEST(val[19] == 0.0);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == -0.75);
  BOOST_TEST(val[22] == 0.75);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == 0.0);
  BOOST_TEST(val[25] == 0.0);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_TEST(val[0] == 0.046875);
  BOOST_TEST(val[1] == 0.09375);
  BOOST_TEST(val[2] == -0.015625);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == -0.046875);
  BOOST_TEST(val[7] == -0.09375);
  BOOST_TEST(val[8] == 0.015625);
  BOOST_TEST(val[9] == -0.28125);
  BOOST_TEST(val[10] == -0.5625);
  BOOST_TEST(val[11] == 0.09375);
  BOOST_TEST(val[12] == 0.0);
  BOOST_TEST(val[13] == 0.0);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == 0.28125);
  BOOST_TEST(val[16] == 0.5625);
  BOOST_TEST(val[17] == -0.09375);
  BOOST_TEST(val[18] == -0.140625);
  BOOST_TEST(val[19] == -0.28125);
  BOOST_TEST(val[20] == 0.046875);
  BOOST_TEST(val[21] == 0.0);
  BOOST_TEST(val[22] == 0.0);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == 0.140625);
  BOOST_TEST(val[25] == 0.28125);
  BOOST_TEST(val[26] == -0.046875);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 2, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.);
  BOOST_TEST(val[10] == 0.);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == -0.75);
  BOOST_TEST(val[13] == -1.5);
  BOOST_TEST(val[14] == 0.25);
  BOOST_TEST(val[15] == 0.0);
  BOOST_TEST(val[16] == 0.0);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.0);
  BOOST_TEST(val[19] == 0.);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == 0.75);
  BOOST_TEST(val[22] == 1.5);
  BOOST_TEST(val[23] == -0.25);
  BOOST_TEST(val[24] == 0.0);
  BOOST_TEST(val[25] == 0.0);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == -0.5);
  BOOST_TEST(val[4] == 1.0);
  BOOST_TEST(val[5] == -0.5);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.0);
  BOOST_TEST(val[10] == 0.0);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == 3.0);
  BOOST_TEST(val[13] == -6.0);
  BOOST_TEST(val[14] == 3.0);
  BOOST_TEST(val[15] == 0.0);
  BOOST_TEST(val[16] == 0.0);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.0);
  BOOST_TEST(val[19] == 0.0);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == 1.5);
  BOOST_TEST(val[22] == -3.0);
  BOOST_TEST(val[23] == 1.5);
  BOOST_TEST(val[24] == 0.0);
  BOOST_TEST(val[25] == 0.0);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_TEST(val[0] == -0.1875);
  BOOST_TEST(val[1] == -0.375);
  BOOST_TEST(val[2] == 0.0625);
  BOOST_TEST(val[3] == 0.375);
  BOOST_TEST(val[4] == 0.75);
  BOOST_TEST(val[5] == -0.125);
  BOOST_TEST(val[6] == -0.1875);
  BOOST_TEST(val[7] == -0.375);
  BOOST_TEST(val[8] == 0.0625);
  BOOST_TEST(val[9] == 1.125);
  BOOST_TEST(val[10] == 2.25);
  BOOST_TEST(val[11] == -0.375);
  BOOST_TEST(val[12] == -2.25);
  BOOST_TEST(val[13] == -4.5);
  BOOST_TEST(val[14] == 0.75);
  BOOST_TEST(val[15] == 1.125);
  BOOST_TEST(val[16] == 2.25);
  BOOST_TEST(val[17] == -0.375);
  BOOST_TEST(val[18] == 0.5625);
  BOOST_TEST(val[19] == 1.125);
  BOOST_TEST(val[20] == -0.1875);
  BOOST_TEST(val[21] == -1.125);
  BOOST_TEST(val[22] == -2.25);
  BOOST_TEST(val[23] == 0.375);
  BOOST_TEST(val[24] == 0.5625);
  BOOST_TEST(val[25] == 1.125);
  BOOST_TEST(val[26] == -0.1875);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 2, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 1.5);
  BOOST_TEST(val[4] == 3.0);
  BOOST_TEST(val[5] == -0.5);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.0);
  BOOST_TEST(val[10] == 0.0);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == -3.0);
  BOOST_TEST(val[13] == -6.0);
  BOOST_TEST(val[14] == 1.0);
  BOOST_TEST(val[15] == 0.0);
  BOOST_TEST(val[16] == 0.0);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.0);
  BOOST_TEST(val[19] == 0.0);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == 1.5);
  BOOST_TEST(val[22] == 3.0);
  BOOST_TEST(val[23] == -0.5);
  BOOST_TEST(val[24] == 0.0);
  BOOST_TEST(val[25] == 0.0);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_TEST(val[0] == -0.25);
  BOOST_TEST(val[1] == 0.25);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.25);
  BOOST_TEST(val[7] == -0.25);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 1.5);
  BOOST_TEST(val[10] == -1.5);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == 0.0);
  BOOST_TEST(val[13] == 0.0);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == -1.5);
  BOOST_TEST(val[16] == 1.5);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.75);
  BOOST_TEST(val[19] == -0.75);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == 0.0);
  BOOST_TEST(val[22] == 0.0);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == -0.75);
  BOOST_TEST(val[25] == 0.75);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_TEST(val[0] == -0.25);
  BOOST_TEST(val[1] == 0.25);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.25);
  BOOST_TEST(val[7] == -0.25);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 1.5);
  BOOST_TEST(val[10] == -1.5);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == 0.0);
  BOOST_TEST(val[13] == 0.0);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == -1.5);
  BOOST_TEST(val[16] == 1.5);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.75);
  BOOST_TEST(val[19] == -0.75);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == 0.0);
  BOOST_TEST(val[22] == 0.0);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == -0.75);
  BOOST_TEST(val[25] == 0.75);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 2, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.0);
  BOOST_TEST(val[10] == 0.0);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == 4.0);
  BOOST_TEST(val[13] == -4.0);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == 0.0);
  BOOST_TEST(val[16] == 0.0);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.0);
  BOOST_TEST(val[19] == 0.0);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == -4.0);
  BOOST_TEST(val[22] == 4.0);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == 0.0);
  BOOST_TEST(val[25] == 0.0);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 0, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.0);
  BOOST_TEST(val[10] == 0.0);
  BOOST_TEST(val[11] == 0.0);
  BOOST_TEST(val[12] == 4.0);
  BOOST_TEST(val[13] == -4.0);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == 0.0);
  BOOST_TEST(val[16] == 0.0);
  BOOST_TEST(val[17] == 0.0);
  BOOST_TEST(val[18] == 0.0);
  BOOST_TEST(val[19] == 0.0);
  BOOST_TEST(val[20] == 0.0);
  BOOST_TEST(val[21] == -4.0);
  BOOST_TEST(val[22] == 4.0);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == 0.0);
  BOOST_TEST(val[25] == 0.0);
  BOOST_TEST(val[26] == 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 2, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.75);
  BOOST_TEST(val[10] == 1.5);
  BOOST_TEST(val[11] == -0.25);
  BOOST_TEST(val[12] == 0.0);
  BOOST_TEST(val[13] == 0.0);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == -0.75);
  BOOST_TEST(val[16] == -1.5);
  BOOST_TEST(val[17] == 0.25);
  BOOST_TEST(val[18] == -0.75);
  BOOST_TEST(val[19] == -1.5);
  BOOST_TEST(val[20] == 0.25);
  BOOST_TEST(val[21] == 0.0);
  BOOST_TEST(val[22] == 0.0);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == 0.75);
  BOOST_TEST(val[25] == 1.5);
  BOOST_TEST(val[26] == -0.25);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 2);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 2, 1, val);
  BOOST_TEST(val[0] == 0.0);
  BOOST_TEST(val[1] == 0.0);
  BOOST_TEST(val[2] == 0.0);
  BOOST_TEST(val[3] == 0.0);
  BOOST_TEST(val[4] == 0.0);
  BOOST_TEST(val[5] == 0.0);
  BOOST_TEST(val[6] == 0.0);
  BOOST_TEST(val[7] == 0.0);
  BOOST_TEST(val[8] == 0.0);
  BOOST_TEST(val[9] == 0.75);
  BOOST_TEST(val[10] == 1.5);
  BOOST_TEST(val[11] == -0.25);
  BOOST_TEST(val[12] == 0.0);
  BOOST_TEST(val[13] == 0.0);
  BOOST_TEST(val[14] == 0.0);
  BOOST_TEST(val[15] == -0.75);
  BOOST_TEST(val[16] == -1.5);
  BOOST_TEST(val[17] == 0.25);
  BOOST_TEST(val[18] == -0.75);
  BOOST_TEST(val[19] == -1.5);
  BOOST_TEST(val[20] == 0.25);
  BOOST_TEST(val[21] == 0.0);
  BOOST_TEST(val[22] == 0.0);
  BOOST_TEST(val[23] == 0.0);
  BOOST_TEST(val[24] == 0.75);
  BOOST_TEST(val[25] == 1.5);
  BOOST_TEST(val[26] == -0.25);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 2, 1);
  BOOST_TEST(sum == 0.0);

  MPI_Finalize();

}


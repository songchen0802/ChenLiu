// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test8

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>

#include "mesh/Editor.hh"


using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test8) {

  // create a connectivity
  Mesh::Connectivity conn;

  // check initialization
  BOOST_REQUIRE_EQUAL(conn.d0(), -1);
  BOOST_REQUIRE_EQUAL(conn.d1(), -1);
  BOOST_REQUIRE_EQUAL(conn.num_entities(), 0);

  // basic settings
  conn.set_d0_d1(2,0);
  BOOST_REQUIRE_EQUAL(conn.d0(), 2);
  BOOST_REQUIRE_EQUAL(conn.d1(), 0);

  // add first connectivity
  std::vector<int> idx = {1, 2, 3};
  int id = conn.add_connectivity(idx);
  BOOST_REQUIRE_EQUAL(id, 0);
  BOOST_REQUIRE_EQUAL(conn.num_connectivities(id), 3);

  for (std::size_t i = 0; i != idx.size(); ++i) {
    BOOST_REQUIRE_EQUAL(conn.get_connectivities(id)[i], idx[i]);
  }

  // add second connectivity
  idx.clear();
  idx = {3, 2, 5, 6};
  id = conn.add_connectivity(idx);
  BOOST_REQUIRE_EQUAL(id, 1);
  BOOST_REQUIRE_EQUAL(conn.num_connectivities(id), 4);

  for (std::size_t i = 0; i != idx.size(); ++i) {
    BOOST_REQUIRE_EQUAL(conn.get_connectivities(id)[i], idx[i]);
  }

  // check existence with two search directions
  bool is_found = conn.has_connectivity(idx, "forward");
  BOOST_REQUIRE_EQUAL(is_found, true);

  is_found = conn.has_connectivity(idx, "backward");
  BOOST_REQUIRE_EQUAL(is_found, true);

  // create a connectivity not in data
  idx.clear();
  idx = {4, 3, 6, 7};

  is_found = conn.has_connectivity(idx, "forward");
  BOOST_REQUIRE_EQUAL(is_found, false);

}


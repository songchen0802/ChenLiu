// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test11

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>

#include "mesh/Editor.hh"


using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test11) {

  MPI_Init(NULL, NULL);

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  std::filesystem::path mesh_file = "./data/2tet.inp";

  mesh_editor.read_mesh(mesh_file, mesh_ptr, 3, 3, MPI_COMM_WORLD, true);

  Coordinate coord0(0.0, 0.0, 0.0);
  Coordinate coord1(1.0, 0.0, 0.0);
  Coordinate coord2(1.0, 1.0, 0.0);
  Coordinate coord3(0.0, 1.0, 0.0);
  Coordinate coord4(0.0, 1.0, 1.0);

  std::vector<Coordinate> coords = {coord0, coord1, coord2, coord3,
                                    coord4
                                   };

  for (std::size_t i = 0; i != 5; ++i) {
    for (std::size_t d = 0; d != 3; ++d) {
      BOOST_REQUIRE_EQUAL(mesh_ptr->geometry().coordinate(i)[d], coords[i][d]);
    }
  }


  // d0 = 3, d1 = 0
  int tdim_0 = 3;
  int tdim_1 = 0;

  std::vector<std::vector<int>> connects_3d = {{0, 1, 3, 4}, {1, 2, 3, 4}};

  int n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3d[i][j]);
    }
  }

  // d0 = 2, d1 = 0
  tdim_0 = 2;
  tdim_1 = 0;

  std::vector<std::vector<int>> connects_2d = {
    {0, 1, 3}, {0, 1, 4}, {1, 3, 4},
    {0, 4, 3}, {1, 2, 3}, {1, 2, 4}, {2, 3, 4}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2d[i][j]);
    }
  }

  // d0 = 1, d0 = 0
  tdim_0 = 1;
  tdim_1 = 0;

  std::vector<std::vector<int>> connects_1d = {
    {0, 1}, {1, 3}, {3, 0}, {0, 4},
    {1, 4}, {3, 4}, {1, 2}, {2, 3}, {2, 4}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1d[i][j]);
    }
  }


  // d0 = 2, d1 = 3
  tdim_0 = 2;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_2to3 = {
    {0}, {0}, {0, 1}, {0}, {1}, {1}, {1}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2to3[i][j]);
    }
  }

  // d0 = 3, d1 = 2
  tdim_0 = 3;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_3to2 = {
    {0, 1, 2, 3}, {4, 5, 6, 2}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3to2[i][j]);
    }
  }

  // d0 = 3, d1 = 1
  tdim_0 = 3;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_3to1 = {
    {0, 1, 2, 3, 4, 5},
    {6, 7, 1, 4, 8, 5}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3to1[i][j]);
    }
  }

  // d0 = 1, d1 = 3
  tdim_0 = 1;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_1to3 = {
    {0}, {0, 1}, {0}, {0},
    {0, 1}, {0, 1}, {1}, {1}, {1}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1to3[i][j]);
    }
  }

  // d0 = 2, d1 = 1
  tdim_0 = 2;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_2to1 = {
    {0, 1, 2}, {0, 4, 3}, {1, 5, 4},
    {3, 5, 2}, {6, 7, 1}, {6, 8, 4}, {7, 5, 8}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2to1[i][j]);
    }
  }

  // d0 = 1, d1 = 2
  tdim_0 = 1;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_1to2 = {
    {0, 1}, {0, 2, 4}, {0, 3},
    {1, 3}, {1, 2, 5}, {2, 3, 6},
    {4, 5}, {4, 6}, {5, 6}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1to2[i][j]);
    }
  }

  // d0 = 0, d1 = 3
  tdim_0 = 0;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_0to3 = {{0}, {0, 1}, {1}, {0, 1}, {0, 1}};

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to3[i][j]);
    }
  }

  // d0 = 0, d1 = 2
  tdim_0 = 0;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_0to2 = {
    {0, 1, 3}, {0, 1, 2, 4, 5},
    {4, 5, 6}, {0, 2, 3, 4, 6}, {1, 2, 3, 5, 6}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to2[i][j]);
    }
  }

  // d0 = 0, d1 = 1
  tdim_0 = 0;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_0to1 = {
    {0, 2, 3}, {0, 1, 4, 6},
    {6, 7, 8}, {1, 2, 5, 7}, {3, 4, 5, 8}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to1[i][j]);
    }
  }

  // d0 = 0, d1 = 0
  tdim_0 = 0;
  tdim_1 = 0;

  std::vector<std::vector<int>> connects_0to0 = {
    {1, 3, 4}, {0, 2, 3, 4},
    {1, 3, 4}, {0, 1, 2, 4}, {0, 1, 2, 3}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_0to0[i][j]);
    }
  }

  // d0 = 1, d1 = 1
  tdim_0 = 1;
  tdim_1 = 1;

  std::vector<std::vector<int>> connects_1to1 = {
    {1, 2, 3, 4, 6}, {0, 2, 4, 5, 6, 7},
    {0, 1, 3, 5, 7}, {0, 2, 4, 5, 8},
    {0, 1, 3, 5, 6, 8}, {1, 2, 3, 4, 7, 8}, {0, 1, 4, 7, 8},
    {1, 2, 5, 6, 8}, {3, 4, 5, 6, 7}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_1to1[i][j]);
    }
  }

  // d0 = 2, d1 = 2
  tdim_0 = 2;
  tdim_1 = 2;

  std::vector<std::vector<int>> connects_2to2 = {
    {1, 2, 3, 4, 5, 6}, {0, 2, 3, 4, 5, 6},
    {0, 1, 3, 4, 5, 6}, {0, 1, 2, 4, 5, 6}, {0, 1, 2, 3, 5, 6},
    {0, 1, 2, 3, 4, 6}, {0, 1, 2, 3, 4, 5}
  };

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_2to2[i][j]);
    }
  }


  // d0 = 3, d1 = 3
  tdim_0 = 3;
  tdim_1 = 3;

  std::vector<std::vector<int>> connects_3to3 = {{1}, {0}};

  n_e = mesh_ptr->topology().num_entities(tdim_0);
  for (std::size_t i = 0; i != n_e; ++i) {
    std::vector<int> c = mesh_ptr->topology().get_connectivities(tdim_0, tdim_1, i);

    for (std::size_t j = 0; j != c.size(); ++j) {
      BOOST_REQUIRE_EQUAL(c[j], connects_3to3[i][j]);
    }
  }

  MPI_Finalize();

}


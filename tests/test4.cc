// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test4

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include "common/Coordinate.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test4) {

  Coordinate vert1;
  vert1.init(3);

  for (int i = 0; i < 3; ++i) {
    BOOST_REQUIRE_EQUAL(vert1[i], 0.0);
  }

  Coordinate vert2(1.0, 2.0, 3.0);
  for(int i = 0; i < 3; ++i) {
    BOOST_REQUIRE_EQUAL(vert2.get_coord()[i], 1.0+i);
  }

  std::vector<double> vec(3, 1.0);
  vert1.move(vec);
  for (int i = 0; i < 3; ++i) {
    BOOST_REQUIRE_EQUAL(vert1[i], 1.0);
  }

}


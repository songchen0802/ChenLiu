// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test3

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include "common/Vec.hh"
#include "common/Mat.hh"
#include "common/MatVecOp.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test3) {

  const int c = 3;
  std::vector<double> v_tmp(c, 1.0);

  // create vector
  ChenLiu::Vec<double> v1(v_tmp);
  ChenLiu::Vec<double> v2(c);

  v2 = v1;
  v2 *= 2.0;

  ChenLiu::VecAxpy(v2, 2.0, v1);
  for (int i = 0; i < v2.size(); ++i) {
    BOOST_REQUIRE_EQUAL(v2(i), 4.0);
  }

  ChenLiu::VecAypx(v2, 2.0, v1);
  for (int i = 0; i < v2.size(); ++i) {
    BOOST_REQUIRE_EQUAL(v2(i), 9.0);
  }

  double val = ChenLiu::Dot(v1, v2);
  BOOST_REQUIRE_EQUAL(val, 27.0);

  v2 /= 3;
  ChenLiu::Vec<double> v3(v2.size());
  v3 =  v2 * 2.0;
  for (int i = 0; i < v3.size(); ++i) {
    BOOST_REQUIRE_EQUAL(v3(i), 6.0);
  }

  v3 = v1 + v2;
  for (int i = 0; i < v3.size(); ++i) {
    BOOST_REQUIRE_EQUAL(v3(i), 4.0);
  }

  v3 = v1 - v2;
  for (int i = 0; i < v3.size(); ++i) {
    BOOST_REQUIRE_EQUAL(v3(i), -2.0);
  }

  v3 = v2 / 3.0;
  for (int i = 0; i < v3.size(); ++i) {
    BOOST_REQUIRE_EQUAL(v3(i), 1.0);
  }

  ChenLiu::Mat<double> A(3,3);
  A(0,0) = 4.0;
  A(0,1) = 2.0;
  A(0,2) = -3.0;
  A(1,0) = 1.0;
  A(1,1) = -1.0;
  A(1,2) = 2.0;
  A(2,0) = 5.0;
  A(2,1) = 3.0;
  A(2,2) = 0.0;

  ChenLiu::Mat<double> inv_A = ChenLiu::Inverse(A);

  ChenLiu::Mat<double> inv_A_check(3,3);
  inv_A_check(0,0) = 3.0/14;
  inv_A_check(0,1) = 9.0/28;
  inv_A_check(0,2) = -1.0/28;
  inv_A_check(1,0) = -5.0/14;
  inv_A_check(1,1) = -15.0/28;
  inv_A_check(1,2) = 11.0/28;
  inv_A_check(2,0) = -2.0/7;
  inv_A_check(2,1) = 1.0/14;
  inv_A_check(2,2) = 3.0/14;

  for (int i = 0; i != 3; ++i) {
    for (int j = 0; j !=3; ++j) {
      const double scale = 1e10;
      BOOST_REQUIRE_EQUAL(round(inv_A(i,j)*scale)/scale,
                          round(inv_A_check(i,j)*scale)/scale);
    }
  }

}


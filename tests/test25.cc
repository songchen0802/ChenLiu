// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test25

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "common/Utils.hh"
#include "fem/lagrange/ElementLagrange_Triangle.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test25, *boost::unit_test::tolerance(1.0e-10)) {

  FEM::ElementLagrange_Triangle<double> element;
  std::vector<int> verts = {3, 5, 2};
  std::vector<int> sub_verts;
  std::vector<int> ref_dofs, dofs;

  // point

  // ele_deg = 1
  element.init(1, 1);

  // #1
  sub_verts = {3};
  ref_dofs = {0};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {5};
  ref_dofs = {1};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {2};
  ref_dofs = {2};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // ele_deg = 2
  element.init(2, 2);

  // #1
  sub_verts = {3};
  ref_dofs = {0};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {5};
  ref_dofs = {2};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {2};
  ref_dofs = {5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // Line

  // ele_deg = 1
  element.init(1, 1);

  // #1
  sub_verts = {3, 5};
  ref_dofs = {0, 1};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {2, 5};
  ref_dofs = {2, 1};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {2, 3};
  ref_dofs = {2, 0};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // ele_deg = 2
  element.init(2, 2);

  // #1
  sub_verts = {3, 5};
  ref_dofs = {0, 1, 2};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {2, 5};
  ref_dofs = {2, 4, 5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {2, 3};
  ref_dofs = {0, 3, 5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

}


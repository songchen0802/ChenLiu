// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test2

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include "common/Mat.hh"

BOOST_AUTO_TEST_CASE(test2){

  const int r = 2;
  const int c = 3;

  // create matrix
  ChenLiu::Mat<double> m1(r,c);
  for (int i = 0; i < r; ++i) {
      for (int j = 0; j < c; ++j) {
          m1(i, j) = i;
      }
  }
  BOOST_REQUIRE_EQUAL(m1(1, 2), 1.0);
  
  ChenLiu::Mat<double> m2(r,c);
  m2 = m1;
  for (int i = 0; i < r; ++i) {
      for (int j = 0; j < c; ++j) {
          BOOST_REQUIRE_EQUAL(m1(i, j), m2(i, j));
      }
  }
 
  // multiply
  m2 *= 2.0;
  BOOST_REQUIRE_EQUAL(m2(1, 2), 2.0);

  // divide
  m2 /= 2.0;
  BOOST_REQUIRE_EQUAL(m2(1, 2), 1.0);

  // addition
  m2 += m1;
  BOOST_REQUIRE_EQUAL(m2(1, 2), 2.0);

  // subtraction
  m2 -= m1;
  BOOST_REQUIRE_EQUAL(m2(1, 2), 1.0);

}


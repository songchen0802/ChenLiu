// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test15

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>

#include "mesh/Editor.hh"
#include "communication/MPI_scatterv.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test15) {

  MPI_Init(NULL, NULL);

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  const int tdim = 2;
  const int gdim = 2;

  // entity information
  Mesh::EntityInfo entity_info;

  entity_info.tdim = tdim;
  entity_info.gdim = gdim;

  entity_info.n_verts = 9;
  entity_info.coords.resize(entity_info.n_verts * entity_info.gdim);
  std::vector<double> coords = {0.0, 0.0,
                                0.5, 0.0,
                                1.0, 0.0,
                                1.0, 0.5,
                                0.5, 0.5,
                                0.0, 0.5,
                                0.0, 1.0,
                                0.5, 1.0,
                                1.0, 1.0
                               };
  for (int i = 0; i != entity_info.coords.size(); ++i) {
    entity_info.coords[i] = coords[i];
  }

  entity_info.connections = {0, 1, 4, 5,
                             1, 2, 3, 4,
                             4, 3, 8, 7,
                             5, 4, 7, 6
                            };

  entity_info.n_entities = 4;

  entity_info.entity_offsets = {0, 4, 8, 12, 16};

  entity_info.material_ids = {10, 12, 14, 16};

  entity_info.global_indices = {0, 1, 2, 3};


  // build from EntityInfo
  mesh_editor.build_mesh(mesh_ptr, entity_info, true);

  // checks
  BOOST_REQUIRE_EQUAL(mesh_ptr->tdim(), tdim);
  BOOST_REQUIRE_EQUAL(mesh_ptr->gdim(), gdim);

  //int c = 0;
  for (int n_cell = 0, c = 0; n_cell != entity_info.n_entities; ++n_cell) {
    Coordinate co(gdim);
    for (int d = 0; d != gdim; ++d) {
      BOOST_REQUIRE_EQUAL(coords[c], mesh_ptr->geometry().coordinate(n_cell)[d]);
      ++c;
    }
  }

  for (int n_cell = 0, c = 0; n_cell != entity_info.n_entities; ++n_cell) {
    std::vector<int> connects = mesh_ptr->topology().get_connectivities(tdim, 0,
                                n_cell);
    for (int el : connects) {
      BOOST_REQUIRE_EQUAL(entity_info.connections[c], el);
      ++c;
    }

  }

  // check material ids
  Mesh::MaterialIdInfo material_id_info;

  // assign info
  material_id_info.tdim = tdim - 1;
  material_id_info.gdim = gdim;
  material_id_info.n_entities = 4;
  material_id_info.connections = {0, 1,
                                  1, 2,
                                  5, 4,
                                  4, 3
                                 };
  material_id_info.entity_offsets = {0, 2, 4, 6, 8};
  material_id_info.material_ids = {20, 20, 30, 30};

  // add material ids into mesh_ptr by mesh_editor
  mesh_editor.add_material_ids(mesh_ptr, material_id_info);

  std::vector<int> mat_ids = {20, -1, 30, -1, 20, -1, 30, -1, -1, -1, -1, -1};
  for (int i = 0; i != mesh_ptr->num_entities(tdim - 1); ++i) {
    BOOST_REQUIRE_EQUAL(mesh_ptr->material_id(tdim-1, i), mat_ids[i]);
  }

  // check global indices
  Mesh::GlobalIndexInfo global_index_info;

  // assign info
  global_index_info.tdim = tdim - 1;
  global_index_info.gdim = gdim;
  global_index_info.n_entities = 4;
  global_index_info.connections = {0, 1,
                                   1, 2,
                                   5, 4,
                                   4, 3
                                  };
  global_index_info.entity_offsets = {0, 2, 4, 6, 8};
  global_index_info.global_indices = {20, 20, 30, 30};

  // add global indices into mesh_ptr by mesh_editor
  mesh_editor.add_global_indices(mesh_ptr, global_index_info);

  std::vector<int> glo_idx = {20, -1, 30, -1, 20, -1, 30, -1, -1, -1, -1, -1};
  for (int i = 0; i != mesh_ptr->num_entities(tdim - 1); ++i) {
    BOOST_REQUIRE_EQUAL(mesh_ptr->topology().get_global_index(tdim-1, i),
                        glo_idx[i]);
  }


  MPI_Finalize();

}


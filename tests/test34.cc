// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test34

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <mpi.h>

#include "common/PreDef.hh"

#include <petsc.h>

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test34, *boost::unit_test::tolerance(1.0e-10)) {

  MPI_Init(NULL, NULL);

  MPI_Comm comm = MPI_COMM_WORLD;
  int master_rank = 0;
  int rank;
  MPI_Comm_rank(comm, &rank);

  PetscInitialize(0, NULL, NULL, NULL);

  PetscInt n = 5;
  PetscScalar one = 1.0, two = 2.0;
  Vec x, y;

  VecCreate(PETSC_COMM_SELF, &x);
  VecSetSizes(x, n, n);
  VecSetType(x, "mpi");
  VecSetType(x, "seq");
  VecDuplicate(x, &y);
  VecSetType(x, "mpi");

  for (int i = 0; i != n; ++i) {
    double val;
    VecGetValues(x, 1, &i, &val);
    BOOST_REQUIRE_EQUAL(val, 0.0);
  }

  VecSet(x, one);
  for (int i = 0; i != n; ++i) {
    double val;
    VecGetValues(x, 1, &i, &val);
    BOOST_REQUIRE_EQUAL(val, 1.0);
  }

  VecSet(y, two);
  for (int i = 0; i != n; ++i) {
    double val;
    VecGetValues(y, 1, &i, &val);
    BOOST_REQUIRE_EQUAL(val, 2.0);
  }

  VecDestroy(&x);
  VecDestroy(&y);

  PetscFinalize();

  MPI_Finalize();

}


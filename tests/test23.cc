// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test23

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "common/Utils.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test23, *boost::unit_test::tolerance(1.0e-10)) {

  std::vector<int> a = {1,2,3,4};
  std::vector<int> b = {2,4,1,3};

  BOOST_REQUIRE_EQUAL(has_same_elements(a,b), true);

  b = {3,4,5,6};
  BOOST_REQUIRE_EQUAL(has_same_elements(a,b), false);

}


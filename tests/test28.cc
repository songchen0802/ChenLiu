// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test28

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "common/Utils.hh"
#include "fem/lagrange/ElementLagrange_Hexahedron.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test28, *boost::unit_test::tolerance(1.0e-10)) {

  FEM::ElementLagrange_Hexahedron<double> element;
  std::vector<int> verts = {10, 20, 30, 40, 50, 60, 70, 80};
  std::vector<int> sub_verts;
  std::vector<int> ref_dofs, dofs;

  // point

  // ele_deg = 1
  element.init(1, 3);

  // #1
  sub_verts = {10};
  ref_dofs = {0};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {20};
  ref_dofs = {1};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {30};
  ref_dofs = {3};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #4
  sub_verts = {40};
  ref_dofs = {2};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #5
  sub_verts = {50};
  ref_dofs = {4};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #6
  sub_verts = {60};
  ref_dofs = {5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #7
  sub_verts = {70};
  ref_dofs = {7};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #8
  sub_verts = {80};
  ref_dofs = {6};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // ele_deg = 2
  element.init(2, 3);

  // #1
  sub_verts = {10};
  ref_dofs = {0};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {20};
  ref_dofs = {2};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {30};
  ref_dofs = {8};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #4
  sub_verts = {40};
  ref_dofs = {6};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #5
  sub_verts = {50};
  ref_dofs = {18};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #6
  sub_verts = {60};
  ref_dofs = {20};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #7
  sub_verts = {70};
  ref_dofs = {26};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #8
  sub_verts = {80};
  ref_dofs = {24};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // Line

  // ele_deg = 1
  element.init(1, 3);

  // #1
  sub_verts = {10, 20};
  ref_dofs = {0, 1};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {20, 30};
  ref_dofs = {1, 3};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {30, 40};
  ref_dofs = {2, 3};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #4
  sub_verts = {10, 40};
  ref_dofs = {0, 2};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #5
  sub_verts = {50, 60};
  ref_dofs = {4, 5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #6
  sub_verts = {60, 70};
  ref_dofs = {5, 7};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #7
  sub_verts = {70, 80};
  ref_dofs = {6, 7};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #8
  sub_verts = {50, 80};
  ref_dofs = {4, 6};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #9
  sub_verts = {10, 50};
  ref_dofs = {0, 4};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #10
  sub_verts = {20, 60};
  ref_dofs = {1, 5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #11
  sub_verts = {30, 70};
  ref_dofs = {3, 7};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #12
  sub_verts = {40, 80};
  ref_dofs = {2, 6};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // ele_deg = 2
  element.init(2, 3);

  // #1
  sub_verts = {10, 20};
  ref_dofs = {0, 1, 2};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {20, 30};
  ref_dofs = {2, 5, 8};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {30, 40};
  ref_dofs = {6, 7, 8};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #4
  sub_verts = {10, 40};
  ref_dofs = {0, 3, 6};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #5
  sub_verts = {50, 60};
  ref_dofs = {18, 19, 20};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #6
  sub_verts = {60, 70};
  ref_dofs = {20, 23, 26};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #7
  sub_verts = {70, 80};
  ref_dofs = {24, 25, 26};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #8
  sub_verts = {50, 80};
  ref_dofs = {18, 21, 24};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #9
  sub_verts = {10, 50};
  ref_dofs = {0, 9, 18};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #10
  sub_verts = {20, 60};
  ref_dofs = {2, 11, 20};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #11
  sub_verts = {30, 70};
  ref_dofs = {8, 17, 26};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #12
  sub_verts = {40, 80};
  ref_dofs = {6, 15, 24};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // Quadrilateral

  // ele_deg = 1
  element.init(1, 3);

  // #1
  sub_verts = {10, 20, 30, 40};
  ref_dofs = {0, 1, 2, 3};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {50, 60, 70, 80};
  ref_dofs = {4, 5, 6, 7};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {10, 20, 60, 50};
  ref_dofs = {0, 1, 4, 5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #4
  sub_verts = {20, 30, 70, 60};
  ref_dofs = {1, 3, 7, 5};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #5
  sub_verts = {30, 40, 80, 70};
  ref_dofs = {2, 3, 6, 7};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #6
  sub_verts = {40, 10, 50, 80};
  ref_dofs = {2, 0, 4, 6};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // ele_deg = 2
  element.init(2, 3);

  // #1
  sub_verts = {10, 20, 30, 40};
  ref_dofs = {0, 1, 2, 3, 4, 5, 6, 7, 8};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #2
  sub_verts = {50, 60, 70, 80};
  ref_dofs = {18, 19, 20, 21, 22, 23, 24, 25, 26};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #3
  sub_verts = {10, 20, 60, 50};
  ref_dofs = {0, 1, 2, 9, 10, 11, 18, 19, 20};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #4
  sub_verts = {20, 30, 70, 60};
  ref_dofs = {2, 5, 8, 11, 14, 17, 20, 23, 26};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #5
  sub_verts = {30, 40, 80, 70};
  ref_dofs = {6, 7, 8, 15, 16, 17, 24, 25, 26};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

  // #6
  sub_verts = {40, 10, 50, 80};
  ref_dofs = {0, 3, 6, 9, 12, 15, 18, 21, 24};
  dofs = element.dof_indices_on_sub_entity(verts, sub_verts);
  BOOST_REQUIRE_EQUAL(has_same_elements(ref_dofs, dofs), true);

}


// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test18

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "fem/ElementFactory.hh"
#include "fem/lagrange/ElementLagrange_Triangle.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test18, *boost::unit_test::tolerance(1.0e-10)) {

  MPI_Init(NULL, NULL);

  FEM::ElementFactory<double> element_factory;

  std::shared_ptr<FEM::Element<double>> ele =
                                       element_factory.create(FE_Type::Lagrange, CellType::Triangle);


  std::vector<double> val;
  Coordinate coord(2);

  double sum;

  // order = 1
  ele->init(1, 1);

  BOOST_REQUIRE_EQUAL(ele->num_dofs_on_cell(), 3);

  coord[0] = 0.0;
  coord[1] = 0.0;
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val.size(), 3);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 1.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  coord[0] = 1.0;
  coord[1] = 0.0;
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 1.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  coord[0] = 0.25;
  coord[1] = 0.25;
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.5);
  BOOST_REQUIRE_EQUAL(val[1], 0.25);
  BOOST_REQUIRE_EQUAL(val[2], 0.25);

  sum = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 1.0);

  sum = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);

  sum = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

//  // cell transform
//  std::vector<Coordinate> cell_verts(3);
//  cell_verts[0].init(2);
//  cell_verts[1].init(2);
//  cell_verts[2].init(2);
//  cell_verts[0][0] = 0.0;
//  cell_verts[0][1] = 0.0;
//  cell_verts[1][0] = 2.0;
//  cell_verts[1][1] = 0.0;
//  cell_verts[2][0] = 0.0;
//  cell_verts[2][1] = 2.0;
//
//  Coordinate coord_phy(2);
//  coord_phy[0] = 1.0;
//  coord_phy[1] = 0.0;
//  ele->map2ref(coord_phy, cell_verts, coord);
//  BOOST_REQUIRE_EQUAL(coord[0], 0.5);
//  BOOST_REQUIRE_EQUAL(coord[1], 0.0);
//
//  Coordinate coord_ref(2);
//  coord_ref[0] = 0.25;
//  coord_ref[1] = 0.25;
//  BOOST_REQUIRE_EQUAL(ele->map2phy(0, coord_ref, cell_verts), 0.5);
//  BOOST_REQUIRE_EQUAL(ele->map2phy(1, coord_ref, cell_verts), 0.5);

  // order = 2
  ele->init(2, 2);

  BOOST_REQUIRE_EQUAL(ele->num_dofs_on_cell(), 6);

  coord[0] = 0.0;
  coord[1] = 0.0;
  val.clear();
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val.size(), 6);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], -3.0);
  BOOST_REQUIRE_EQUAL(val[1], 4.0);
  BOOST_REQUIRE_EQUAL(val[2], -1.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv1(coord, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], -3.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], 2.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], -1.0);

  sum  = ele->eval_ref_basis_deriv1_sum(coord, 1);
  BOOST_TEST(sum == -2.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -8.0);
  BOOST_REQUIRE_EQUAL(val[2], 4.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -8.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 4.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -4.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -4.0);
  BOOST_REQUIRE_EQUAL(val[4], 4.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -4.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -4.0);
  BOOST_REQUIRE_EQUAL(val[4], 4.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  coord[0] = 1.0;
  coord[1] = 0.0;
  val.clear();
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val.size(), 6);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 1.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 1.0);
  BOOST_REQUIRE_EQUAL(val[1], -4.0);
  BOOST_REQUIRE_EQUAL(val[2], 3.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -8.0);
  BOOST_REQUIRE_EQUAL(val[2], 4.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -8.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 4.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -4.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -4.0);
  BOOST_REQUIRE_EQUAL(val[4], 4.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -4.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -4.0);
  BOOST_REQUIRE_EQUAL(val[4], 4.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  coord[0] = 0.25;
  coord[1] = 0.25;
  val.clear();
  ele->eval_ref_basis(coord, val);
  BOOST_REQUIRE_EQUAL(val.size(), 6);
  BOOST_REQUIRE_EQUAL(val[0], 0.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.5);
  BOOST_REQUIRE_EQUAL(val[2], -0.125);
  BOOST_REQUIRE_EQUAL(val[3], 0.5);
  BOOST_REQUIRE_EQUAL(val[4], 0.25);
  BOOST_REQUIRE_EQUAL(val[5], -0.125);

  sum  = ele->eval_ref_basis_sum(coord);
  BOOST_TEST(sum == 1.0);

  ele->eval_ref_basis_deriv1(coord, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], -1.0);
  BOOST_REQUIRE_EQUAL(val[1], 1.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -1.0);
  BOOST_REQUIRE_EQUAL(val[4], 1.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv1_sum(coord, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -8.0);
  BOOST_REQUIRE_EQUAL(val[2], 4.0);
  BOOST_REQUIRE_EQUAL(val[3], 0.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 0, 0);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], 0.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -8.0);
  BOOST_REQUIRE_EQUAL(val[4], 0.0);
  BOOST_REQUIRE_EQUAL(val[5], 4.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 1, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 0, 1, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -4.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -4.0);
  BOOST_REQUIRE_EQUAL(val[4], 4.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 0, 1);
  BOOST_TEST(sum == 0.0);

  ele->eval_ref_basis_deriv2(coord, 1, 0, val);
  BOOST_REQUIRE_EQUAL(val[0], 4.0);
  BOOST_REQUIRE_EQUAL(val[1], -4.0);
  BOOST_REQUIRE_EQUAL(val[2], 0.0);
  BOOST_REQUIRE_EQUAL(val[3], -4.0);
  BOOST_REQUIRE_EQUAL(val[4], 4.0);
  BOOST_REQUIRE_EQUAL(val[5], 0.0);

  sum  = ele->eval_ref_basis_deriv2_sum(coord, 1, 0);
  BOOST_TEST(sum == 0.0);

  MPI_Finalize();

}


// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test32

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>
#include <mpi.h>

#include "mesh/Editor.hh"
#include "mesh/Partition.hh"
#include "space/FunctionSpace.hh"
#include "linear_algebra/LA_Structure.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test32, *boost::unit_test::tolerance(1.0e-10)) {

  MPI_Init(NULL, NULL);

  MPI_Comm comm = MPI_COMM_WORLD;
  int master_rank = 0;
  int rank;
  MPI_Comm_rank(comm, &rank);

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  std::filesystem::path mesh_file = "./data/unit_16squares.inp";

  mesh_editor.read_mesh(mesh_file, mesh_ptr, 2, 2, MPI_COMM_WORLD, true);

  Mesh::Partition partitioner = Mesh::Partition();
  std::shared_ptr mesh_ptr_no_ghost = partitioner.partition_mesh(mesh_ptr, comm,
                                      master_rank);

  std::shared_ptr mesh_ptr_d = partitioner.add_ghost_layer(mesh_ptr_no_ghost,
                               comm);

  std::vector<Element_Family> ele_family = {Element_Family::CG, Element_Family::CG};
  std::vector<int> degree = {1, 1};

  Space::FunctionSpace<double> V(mesh_ptr_d, comm, ele_family, degree);

  LA::LA_Structure<double> LA_struct(V);


  std::vector<int> g_dof_offsets = LA_struct.global_dof_offsets();
  BOOST_REQUIRE_EQUAL(g_dof_offsets[0], 0);
  BOOST_REQUIRE_EQUAL(g_dof_offsets[1], 25);
  BOOST_REQUIRE_EQUAL(g_dof_offsets[2], 50);

  std::vector<int> interior_IDs;
  for (int i = 0; i != 5*(-rank+3); ++i) {
    interior_IDs.push_back(i + 15*rank + g_dof_offsets[0]);
  }
  for (int i = 0; i != 5*(-rank+3); ++i) {
    interior_IDs.push_back(i + 15*rank + g_dof_offsets[1]);
  }

  std::vector<int> ghost_IDs;
  if (rank == 0) {
    ghost_IDs = {23, 22, 19, 18, 16, 48, 47, 44, 43, 41};
  } else if (rank == 1) {
    ghost_IDs = {12, 10, 9, 6, 8, 3, 2, 14, 1, 0, 37, 35, 34, 31, 33, 28, 27, 39, 26, 25};

  }

  BOOST_REQUIRE_EQUAL(has_same_elements(LA_struct.global_interior_IDs(),
                                        interior_IDs),
                      true);

  BOOST_REQUIRE_EQUAL(has_same_elements(LA_struct.global_ghost_IDs(), ghost_IDs),
                      true);



  MPI_Finalize();

}


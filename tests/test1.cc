// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


#define BOOST_TEST_MODULE Test1

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include "common/Vec.hh"

BOOST_AUTO_TEST_CASE(test1){

  const int c = 3;
  // create vector
  ChenLiu::Vec<double> v1(c);
  BOOST_REQUIRE_EQUAL(v1.size(), c);

  // vector assign
  for (int i = 0; i < c; ++i) {
      v1(i) = 2.0;
      BOOST_REQUIRE_EQUAL(v1(i), 2.0);
  }

  // multiply 
  std::vector<double> v_tmp(c, 1.0);
  ChenLiu::Vec<double> v2(v_tmp);

  v2 *= 3.0;
  for (int i = 0; i < v2.size(); ++i) {
      BOOST_REQUIRE_EQUAL(v2(i), 3.0);
  }

  // divide
  v2 /= 3.0;
  for (int i = 0; i < v2.size(); ++i) {
      BOOST_REQUIRE_EQUAL(v2(i), 1.0);
  }

  // addition
  v2 += v1;
  for (int i = 0; i < c; ++i) {
      BOOST_REQUIRE_EQUAL(v2(i), 3.0);
  }

  // subtraction
  v2 -= v1;
  for (int i = 0; i < v2.size(); ++i) {
      BOOST_REQUIRE_EQUAL(v2(i), 1.0);
  }

}


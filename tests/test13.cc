// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test13

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>

#include "mesh/Editor.hh"
#include "mesh/Iterators.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test13) {

  MPI_Init(NULL, NULL);

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  std::filesystem::path mesh_file = "./data/unit_square.inp";

  mesh_editor.read_mesh(mesh_file, mesh_ptr, 2, 2, MPI_COMM_WORLD, true);

  int i = 0;
  int ii = 0;
  int iii = 0;
  for (Mesh::EntityIterator cell_it(mesh_ptr, 2);
       !cell_it.is_end(); ++cell_it) {

    BOOST_REQUIRE_EQUAL(cell_it->local_index(), i);
    BOOST_CHECK(cell_it->cell_type() == CellType::Quadrilateral);
    ++i;

    for (Mesh::EntityIncidentIterator facet_it(*cell_it, 1);
         !facet_it.is_end(); ++facet_it) {

      BOOST_REQUIRE_EQUAL(facet_it->local_index(), ii);
      BOOST_CHECK(facet_it->cell_type() == CellType::Line);
      ++ii;

      std::vector<int> v_idx = {0, 1, 1, 2, 2, 3, 3, 0};
      for (Mesh::EntityIncidentIterator vertex_it(*facet_it, 0);
           !vertex_it.is_end(); ++vertex_it) {

        BOOST_REQUIRE_EQUAL(vertex_it->local_index(), v_idx[iii]);
        ++iii;
      }

    }
  }

  MPI_Finalize();

}


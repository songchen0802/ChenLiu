// ChenLiu Project
// Copyright (C) 2020--2024 Chen Song 宋宸
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define BOOST_TEST_MODULE Test14

#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include <math.h>
#include <filesystem>

#include "mesh/Editor.hh"
#include "mesh/Graph.hh"

using namespace ChenLiu;

BOOST_AUTO_TEST_CASE(test14) {

  MPI_Init(NULL, NULL);

  std::shared_ptr<Mesh::Mesh> mesh_ptr = std::make_shared<Mesh::Mesh>();

  Mesh::Editor mesh_editor;

  std::filesystem::path mesh_file = "./data/unit_4squares.inp";

  mesh_editor.read_mesh(mesh_file, mesh_ptr, 2, 2, MPI_COMM_WORLD, true);

  Mesh::Graph graph(mesh_ptr);

  std::vector<int> v_wgts = graph.node_weights();
  BOOST_REQUIRE_EQUAL(v_wgts.size(), 0);

  std::vector<int> e_wgts = graph.edge_weights();
  BOOST_REQUIRE_EQUAL(e_wgts.size(), 0);

  int tdim = mesh_ptr->topology().tdim();
  std::vector<int> node_offsets = graph.node_offsets();
  BOOST_REQUIRE_EQUAL(mesh_ptr->num_entities(tdim), node_offsets.size()-1);

  std::vector<int> offsets = {0,3,6,9,12};
  for (int i = 0; i != node_offsets.size(); ++i) {
    BOOST_REQUIRE_EQUAL(node_offsets[i], offsets[i]);
  }

  std::vector<int> adj_nodes = graph.adjacent_nodes();
  std::vector<std::vector<int>> adj(node_offsets.size()-1);
  adj[0] = {1,2,3};
  adj[1] = {0,2,3};
  adj[2] = {0,1,3};
  adj[3] = {0,1,2};;

  for (int i = 0; i != node_offsets.size()-1; ++i) {
    for (int j = node_offsets[i], c = 0; j != node_offsets[i+1]; ++j, ++c) {
      BOOST_REQUIRE_EQUAL(adj_nodes[j], adj[i][c]);
    }
  }

  MPI_Finalize();

}

